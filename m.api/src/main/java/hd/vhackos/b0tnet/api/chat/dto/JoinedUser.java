package hd.vhackos.b0tnet.api.chat.dto;

/**
 * Informace o připojeném uživateli do chatu.
 */
public final class JoinedUser extends AbstractMessage {

    private String channel;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
