package hd.vhackos.b0tnet.api.brandonos;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.tool.SocketReceiver;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.shared.utils.HashUtils;
import hd.vhackos.b0tnet.shared.utils.SentryGuard;

import java.io.*;
import java.net.ConnectException;
import java.text.MessageFormat;

/**
 * Přijímá zprávy z Brandon OS (třeba PvP nebo deface).
 */
@Inject
public final class BrandonOsReceiver extends SocketReceiver<IBrandonOsClient, IBrandonOsListener> {

    // informace o Brandon OS serveru
    private static final String SERVER_IP = "51.255.93.109";
    private static final int SERVER_PORT = 35401;

    /**
     * Vytvoří novou instanci.
     */
    public BrandonOsReceiver(IB0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Zahájí naslouchání Brandon OS.
     */
    public void listen() {
        listen("Connecting to Brandon OS...", "vHack Brandon OS Client", new BrandonOsClient());
    }

    private void fireReadRaw(String line) {
        for (IBrandonOsListener listener : listeners) {
            listener.readRaw(line);
        }
    }

    /**
     * Implementace Brandon OS klienta.
     */
    private final class BrandonOsClient extends BasicClient implements IBrandonOsClient {

        @Override
        public void run() {
            final var userName = b0tnet.getConnectionData().getUserName();
            final var uid = b0tnet.getConnectionData().getUid();
            final var accessToken = b0tnet.getConnectionData().getAccessToken();

            try {
                getLog().info("Connecting to a Brandon OS server with user: {}", userName);
                socket = createConnection(b0tnet.getConfig().getBrandonOsProxyData(), SERVER_IP, SERVER_PORT);
                getLog().info("Connection to the Brandon OS server was successful");

                getLog().debug("Opening streams for IO operations");
                reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

                receiver = new Receiver();
                receiver.start();
                trySleep();

                // připraví připojovací string v base64
                var connData = MessageFormat.format(":LOGIN::{0}::{1}", uid, accessToken).getBytes();
                var connString = HashUtils.toBase64(connData, 0, connData.length, HashUtils.WEBSAFE_ALPHABET, false);

                send(writer, connString);
                trySleep();

                getLog().debug("Brandon OS client has been started");
                receiver.join();
                getLog().info("Stopping Brandon OS client");

            } catch (ConnectException e) {
                getLog().error("Unable to join Brandon OS. Please try again later or change the proxy server");

            } catch (IOException e) {
                getLog().error("There was an error connecting to the Brandon OS server", e);
                SentryGuard.log(e);

            } catch (InterruptedException e) {
                getLog().info("Brandon OS client will be stopped");
            } finally {
                if (socket != null) {
                    closeClientThread();
                }
            }
        }

        @Override
        public void sendQuit() {
            var commandData = ":QUIT".getBytes();
            var command = HashUtils.toBase64(commandData, 0, commandData.length, HashUtils.WEBSAFE_ALPHABET, false);

            try {
                send(writer, command);
            } catch (IOException e) {
                getLog().error("There was an error sending the quit message", e);
            }
        }

        /**
         * Přijímá zprávy a případně odesílá odpovědi.
         */
        private final class Receiver extends Thread {

            private Receiver() {
                setDaemon(true);
                setPriority(Thread.MIN_PRIORITY);
                setName("vHack Brandon OS Receiver");
            }

            @Override
            public void run() {
                getLog().debug("Starting listening...");
                String line;

                try {
                    while (true) {
                        line = reader.readLine();

                        if (line != null) {
                            getLog().trace("Incoming message: {}", line);
                            fireReadRaw(line);
                        }
                        if (!isReady() && line.startsWith("Welcome")) {
                            setReady(true);

                            // server nás odpojil
                        } else if (line == null) {
                            break;

                        } else if (isReady()) {
                            if (line.contains("--BYE")) {
                                break;
                            }
                        }
                    }
                } catch (IOException e) {
                    if (e.getMessage() == null || !e.getMessage().contains("Stream closed")) {
                        getLog().error("There was an error reading the Brandon OS", e);
                        SentryGuard.log(e);
                    }
                } catch (Exception e) {
                    getLog().error("An unexpected error has occurred in Brandon OS", e);
                    SentryGuard.log(e);

                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            // nic
                        }
                    }
                }
            }
        }
    }
}
