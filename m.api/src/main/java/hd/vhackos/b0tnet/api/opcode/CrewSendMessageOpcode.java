package hd.vhackos.b0tnet.api.opcode;

import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;

/**
 * Odešle crew zprávu.
 */
public final class CrewSendMessageOpcode extends Opcode {

    private static final String PARAM_MESSAGE = "message";

    /**
     * Zpráva.
     */
    public void setMessage(String message) {
        addParam(PARAM_MESSAGE, message);
    }

    @Override
    public OpcodeTargetType getTarget() {
        return OpcodeTargetType.CREW;
    }

    @Override
    public String getOpcodeValue() {
        return "1000";
    }
}
