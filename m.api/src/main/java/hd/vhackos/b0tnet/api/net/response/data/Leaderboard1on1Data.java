package hd.vhackos.b0tnet.api.net.response.data;

import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;

/**
 * Informace o 1on1 z leaderboard.
 */
public final class Leaderboard1on1Data {

    public static final String P_CREW_NAME = "crewName";
    public static final String P_CREW_TAG = "crewTag";
    public static final String P_CREW_REPUTATION = "crewReputation";
    public static final String P_CREW_ID = "crewId";
    public static final String P_MEMBERS = "members";

    @AsciiRow("Name")
    @ResponseKey("crew_name")
    private String crewName;

    @AsciiRow("Tag")
    @ResponseKey("crew_tag")
    private String crewTag;

    @AsciiRow("Reputation")
    @ResponseKey("crew_rep")
    private String crewReputation;

    @AsciiRow("ID")
    @ResponseKey("crew_id")
    private Integer crewId;

    @AsciiRow("Members")
    @ResponseKey("members")
    private String members;

    public String getCrewName() {
        return crewName;
    }

    public void setCrewName(String crewName) {
        this.crewName = crewName;
    }

    public String getCrewTag() {
        return crewTag;
    }

    public void setCrewTag(String crewTag) {
        this.crewTag = crewTag;
    }

    public String getCrewReputation() {
        return crewReputation;
    }

    public void setCrewReputation(String crewReputation) {
        this.crewReputation = crewReputation;
    }

    public Integer getCrewId() {
        return crewId;
    }

    public void setCrewId(Integer crewId) {
        this.crewId = crewId;
    }

    public String getMembers() {
        return members;
    }

    public void setMembers(String members) {
        this.members = members;
    }
}
