package hd.vhackos.b0tnet.api.tool;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.dto.ProxyData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Socket;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Základní připojení na soket (čtení a zápis).
 */
public abstract class SocketReceiver<C extends SocketReceiver.IClient, L extends SocketReceiver.IListener> {

    private final Logger log;
    protected final IB0tnet b0tnet;
    protected final List<L> listeners = new ArrayList<>();
    protected C client;
    private Thread clientThread;
    private volatile boolean ready;

    /**
     * Vytvoří novou instanci.
     */
    public SocketReceiver(IB0tnet b0tnet) {
        this.b0tnet = b0tnet;
        log = LoggerFactory.getLogger(getClass());
    }

    /**
     * Zahájí naslouchání.
     */
    protected final void listen(String logMessage, String threadName, C client) {
        getLog().debug(logMessage);
        this.client = client;
        clientThread = new Thread(client);
        clientThread.setDaemon(true);
        clientThread.setPriority(Thread.MIN_PRIORITY);
        clientThread.setName(threadName);
        clientThread.start();
    }

    /**
     * Počká na dokončení vlákna klienta.
     */
    public final void waitForFinish() {
        if (clientThread != null && !clientThread.isInterrupted()) {
            try {
                clientThread.join();
            } catch (InterruptedException e) {
                // nic
            }
        }
    }

    /**
     * Resetne vše.
     */
    public final void reset() {
        closeClientThread();
        listeners.clear();
        ready = false;
    }

    /**
     * Vyhodnotí, zda je vše připravné (stav, kdy je klient plně připojen).
     */
    public final boolean isReady() {
        return ready;
    }

    /**
     * Nastaví stav.
     */
    public final void setReady(boolean ready) {
        this.ready = ready;
    }

    /**
     * Přidá listener pro ochycení událostí.
     */
    public final void addListener(L listener) {
        listeners.add(listener);
    }

    /**
     * Získá instanci loggeru.
     */
    protected final Logger getLog() {
        return log;
    }

    /**
     * Vytvoří nový soket.
     */
    protected final Socket createConnection(ProxyData proxyData, String host, int port) throws IOException {
        Socket socket;

        if (proxyData != null) {
            var addr = new InetSocketAddress(proxyData.getIp(), proxyData.getPort());
            var proxy = new Proxy(Proxy.Type.HTTP, addr);
            socket = new Socket(proxy);
            getLog().debug("Proxy will be used: {}", proxyData);
        } else {
            socket = new Socket();
        }
        socket.setSoLinger(true, 10);
        socket.connect(new InetSocketAddress(host, port));
        return socket;
    }

    /**
     * Pošle zprávu na server.
     */
    protected final void send(BufferedWriter writer, String messageFormat, Object... args) throws IOException {
        var argsList = new ArrayList<String>();
        if (args != null) {
            for (var arg : args) {
                argsList.add(arg.toString());
            }
        }
        String message = MessageFormat.format(messageFormat, argsList.toArray());
        writer.write(message + "\r\n");
        writer.flush();
        getLog().debug("Send a message: {}", message);
    }

    /**
     * Pokusí se uspat aktuální vlákno na výchozí náhodný čas.
     */
    protected final void trySleep() throws InterruptedException {
        var time = b0tnet.getConfig().getSleepDelay();
        getLog().debug("Forced waiting: {}ms", time);
        Thread.sleep(time);
    }

    /**
     * Uzavře vše.
     */
    protected final void close(BufferedReader reader, BufferedWriter writer, Socket socket) {
        getLog().debug("Closing server connection");
        try {
            if (reader != null) {
                reader.close();
            }
        } catch (IOException e) {
            // nic
        }
        try {
            if (writer != null) {
                writer.close();
            }
        } catch (IOException e) {
            // nic
        }
        try {
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            // nic
        }
    }

    /**
     * Ukončí vlákno klienta.
     */
    public final void closeClientThread() {
        if (clientThread != null) {
            clientThread.interrupt();
            clientThread = null;
        }
    }

    /**
     * Rozhraní klienta.
     */
    public interface IClient extends Runnable {

    }

    /**
     * Události pro socket receiver.
     */
    public interface IListener {

    }

    /**
     * Základní klient.
     */
    public static abstract class BasicClient implements IClient {

        protected Socket socket;
        protected BufferedReader reader;
        protected BufferedWriter writer;
        protected Thread receiver;
    }
}
