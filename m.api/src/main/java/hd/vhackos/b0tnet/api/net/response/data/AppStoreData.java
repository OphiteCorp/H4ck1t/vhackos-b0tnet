package hd.vhackos.b0tnet.api.net.response.data;

import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiAppStoreTypeConverter;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiMoneyConverter;

/**
 * Informace o aplikaci v obchodě.
 */
public final class AppStoreData {

    public static final String P_PRICE = "price";
    public static final String P_BASE_PRICE = "basePrice";
    public static final String P_FACTOR = "factor";
    public static final String P_APP_ID = "appId";
    public static final String P_LEVEL = "level";
    public static final String P_REQUIRE_LEVEL = "requireLevel";
    public static final String P_MAX_LEVEL = "maxLevel";
    public static final String P_RUNNING = "running";

    @AsciiRow(value = "Price", converter = AsciiMoneyConverter.class)
    @ResponseKey("price")
    private Integer price;

    @AsciiRow(value = "Base Price", converter = AsciiMoneyConverter.class)
    @ResponseKey("baseprice")
    private Integer basePrice;

    @AsciiRow("Factor")
    @ResponseKey("factor")
    private Integer factor;

    @AsciiRow(value = "ID", converter = AsciiAppStoreTypeConverter.class)
    @ResponseKey("appid")
    private Integer appId;

    @AsciiRow("Level")
    @ResponseKey("level")
    private Integer level;

    @AsciiRow("Require Level")
    @ResponseKey("require")
    private Integer requireLevel;

    @AsciiRow("Max Level")
    @ResponseKey("maxlvl")
    private Integer maxLevel;

    @AsciiRow("Running")
    @ResponseKey("running")
    private Integer running;

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(Integer basePrice) {
        this.basePrice = basePrice;
    }

    public Integer getFactor() {
        return factor;
    }

    public void setFactor(Integer factor) {
        this.factor = factor;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getRequireLevel() {
        return requireLevel;
    }

    public void setRequireLevel(Integer requireLevel) {
        this.requireLevel = requireLevel;
    }

    public Integer getMaxLevel() {
        return maxLevel;
    }

    public void setMaxLevel(Integer maxLevel) {
        this.maxLevel = maxLevel;
    }

    public Integer getRunning() {
        return running;
    }

    public void setRunning(Integer running) {
        this.running = running;
    }
}
