package hd.vhackos.b0tnet.api.net.response.data;

import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.api.net.response.data.interfaces.ICrewData;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiElapsedSecondsTimeConverter;

/**
 * Informace o přihlášce do crew.
 */
public final class CrewRequestData implements ICrewData {

    public static final String P_LAST_ONLINE = "lastOnline";
    public static final String P_LEVEL = "level";
    public static final String P_POSITION = "position";
    public static final String P_USER_ID = "userId";
    public static final String P_USER_NAME = "userName";

    @AsciiRow(value = "Last Online", converter = AsciiElapsedSecondsTimeConverter.class)
    @ResponseKey("lastOnline")
    private Long lastOnline;

    @AsciiRow("Level")
    @ResponseKey("level")
    private Integer level;

    @AsciiRow("Position")
    @ResponseKey("position")
    private Integer position;

    @AsciiRow("User ID")
    @ResponseKey("user_id")
    private Long userId;

    @AsciiRow("User Name")
    @ResponseKey("username")
    private String userName;

    @Override
    public Long getLastOnline() {
        return lastOnline;
    }

    public void setLastOnline(Long lastOnline) {
        this.lastOnline = lastOnline;
    }

    @Override
    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Override
    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    @Override
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
