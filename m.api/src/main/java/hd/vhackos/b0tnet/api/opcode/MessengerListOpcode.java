package hd.vhackos.b0tnet.api.opcode;

import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;

/**
 * Získá všechny interní zprávy.
 */
public final class MessengerListOpcode extends Opcode {

    @Override
    public OpcodeTargetType getTarget() {
        return OpcodeTargetType.MESSENGER;
    }

    @Override
    public String getOpcodeValue() {
        return "100";
    }
}
