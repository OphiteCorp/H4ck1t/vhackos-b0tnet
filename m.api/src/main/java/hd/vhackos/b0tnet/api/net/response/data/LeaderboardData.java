package hd.vhackos.b0tnet.api.net.response.data;

import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;

/**
 * Informace o jednotlivé položce v leaderboards.
 */
public final class LeaderboardData {

    public static final String P_USER = "user";
    public static final String P_EXP_PERCENT = "expPercent";
    public static final String P_LEVEL = "level";

    @AsciiRow("User")
    @ResponseKey("user")
    private String user;

    @AsciiRow("Experience")
    @ResponseKey("exp")
    private String expPercent;

    @AsciiRow("Level")
    @ResponseKey("level")
    private Integer level;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getExpPercent() {
        return expPercent;
    }

    public void setExpPercent(String expPercent) {
        this.expPercent = expPercent;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}
