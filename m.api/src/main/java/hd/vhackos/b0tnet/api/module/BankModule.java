package hd.vhackos.b0tnet.api.module;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.exception.BruteforceAlreadyRunningException;
import hd.vhackos.b0tnet.api.exception.ExploitException;
import hd.vhackos.b0tnet.api.exception.IpNotExistsException;
import hd.vhackos.b0tnet.api.exception.ServerBusyException;
import hd.vhackos.b0tnet.api.module.base.Module;
import hd.vhackos.b0tnet.api.module.base.ModuleHelper;
import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.BankResponse;
import hd.vhackos.b0tnet.api.net.response.RemoteBankResponse;
import hd.vhackos.b0tnet.api.opcode.*;
import hd.vhackos.b0tnet.shared.exception.B0tnetException;
import hd.vhackos.b0tnet.shared.exception.InvalidAccessTokenException;
import hd.vhackos.b0tnet.shared.injection.Inject;

import java.util.Collections;

/**
 * Operace nad bankami.
 */
@Inject
public final class BankModule extends Module {

    private static final String ERR_CODE_BRUTEFORCE_IS_ALREADY_EXISTS = "2";
    private static final String ERR_CODE_BRUTEFORCE_SERVER_IS_BUSY = "3";

    protected BankModule(IB0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Získá banku hráče.
     */
    public synchronized BankResponse getBank() {
        var opcode = new BankOpcode();
        OpcodeRequestResp response = sendRequest(opcode);
        var responseMap = response.getResponse();
        var result = getResultValue(response.getResponse());

        // nestačí pouze kontrola resultu na null, protože pří správné odpovědi též vrací null, ale celkový počet peněz
        // musí být vždy
        if (result == null && responseMap.getOrDefault(BankResponse.KEY_TOTAL, null) == null) {
            throw new InvalidAccessTokenException(null, "Your access token is no longer valid, try signing in again");
        }
        return createBankResponse(response);
    }

    /**
     * Zahájí prolamování banky.
     */
    public synchronized OpcodeRequestResp bruteforce(String ip) {
        var opcode = new BruteforceRemoteBankOpcode();
        opcode.setTargetIp(ip);

        try {
            OpcodeRequestResp response = sendRequest(opcode);
            var innerResponse = response.getResponse();
            var result = getResultValue(innerResponse);

            if (result == null) {
                throw new IpNotExistsException(null,
                        "The IP address '" + ip + "' you are trying to break already does not exist");
            }
            return response;

        } catch (B0tnetException e) {
            if (ERR_CODE_BRUTEFORCE_IS_ALREADY_EXISTS.equals(e.getResultCode())) {
                throw new BruteforceAlreadyRunningException(e.getResultCode(),
                        "Target IP '" + ip + "' already exists in the list");
            }
            if (ERR_CODE_BRUTEFORCE_SERVER_IS_BUSY.equals(e.getResultCode())) {
                throw new ServerBusyException(e.getResultCode(),
                        "The server is busy and refused to process an IP: " + ip);
            }
            throw e;
        }
    }

    /**
     * Získá informace o cílové bance.
     */
    public synchronized RemoteBankResponse getRemoteBank(String ip) {
        var opcode = new RemoteBankOpcode();
        opcode.setTargetIp(ip);

        OpcodeRequestResp response = sendRequest(opcode);
        var result = getResultValue(response.getResponse());

        if (result == null) {
            throw new ExploitException(result, "IP '" + ip + "' does not exist or you do not have permissions");
        }
        var resp = createRemoteBankResponse(response);

        if (resp.getTargetId() == null) {
            throw new ExploitException(result, "IP '" + ip + "' does not exist or you do not have permissions");
        }
        return resp;
    }

    /**
     * Vybere banku z cílové IP.
     */
    public synchronized RemoteBankResponse withdraw(String ip, long amount) {
        var opcode = new RemoteBankWithdrawOpcode();
        opcode.setTargetIp(ip);
        opcode.setAmount(amount);

        OpcodeRequestResp response = sendRequest(opcode);
        return createRemoteBankResponse(response);
    }

    /**
     * Skryje IP pomocí malware.
     */
    public synchronized RemoteBankResponse useMalware(String ip) {
        var opcode = new UseMalwareOpcode();
        opcode.setTargetIp(ip);

        OpcodeRequestResp response = sendRequest(opcode);
        return createRemoteBankResponse(response);
    }

    private BankResponse createBankResponse(OpcodeRequestResp response) {
        var responseMap = response.getResponse();
        var dto = new BankResponse(response);

        ModuleHelper.checkResponseIntegrity(responseMap, BankResponse.class);
        ModuleHelper.setField(responseMap, dto, BankResponse.P_FILLED);
        ModuleHelper.setField(responseMap, dto, BankResponse.P_MONEY);
        ModuleHelper.setField(responseMap, dto, BankResponse.P_SAVINGS);
        ModuleHelper.setField(responseMap, dto, BankResponse.P_USER_NAME);
        ModuleHelper.setField(responseMap, dto, BankResponse.P_TOTAL);
        ModuleHelper.setField(responseMap, dto, BankResponse.P_MAX_SAVINGS);
        ModuleHelper.setField(responseMap, dto, BankResponse.P_TRANSACTIONS_COUNT);

        if (!ModuleHelper.setField(responseMap, dto, BankResponse.P_TRANSACTIONS,
                (f, data) -> ModuleHelper.convertToTransactionData(data))) {
            dto.setTransactions(Collections.emptyList());
        }
        return dto;
    }

    private RemoteBankResponse createRemoteBankResponse(OpcodeRequestResp response) {
        var responseMap = response.getResponse();
        var dto = new RemoteBankResponse(response);

        ModuleHelper.checkResponseIntegrity(responseMap, RemoteBankResponse.class);
        ModuleHelper.setField(responseMap, dto, RemoteBankResponse.P_WITHDRAW);
        ModuleHelper.setField(responseMap, dto, RemoteBankResponse.P_OPEN);
        ModuleHelper.setField(responseMap, dto, RemoteBankResponse.P_IP_REMOVED);
        ModuleHelper.setField(responseMap, dto, RemoteBankResponse.P_TARGET_ID);
        ModuleHelper.setField(responseMap, dto, RemoteBankResponse.P_REMOTE_USER_NAME);
        ModuleHelper.setField(responseMap, dto, RemoteBankResponse.P_USER_NAME);
        ModuleHelper.setField(responseMap, dto, RemoteBankResponse.P_REMOTE_PASSWORD);
        ModuleHelper.setField(responseMap, dto, RemoteBankResponse.P_REMOTE_MONEY);
        ModuleHelper.setField(responseMap, dto, RemoteBankResponse.P_MONEY);
        ModuleHelper.setField(responseMap, dto, RemoteBankResponse.P_SAVINGS);
        ModuleHelper.setField(responseMap, dto, RemoteBankResponse.P_USER_MALWARE_KITS);
        ModuleHelper.setField(responseMap, dto, RemoteBankResponse.P_GOT_BLT);
        ModuleHelper.setField(responseMap, dto, RemoteBankResponse.P_AATT);
        ModuleHelper.setField(responseMap, dto, RemoteBankResponse.P_NEXT_P);
        ModuleHelper.setField(responseMap, dto, RemoteBankResponse.P_TOTAL);
        ModuleHelper.setField(responseMap, dto, RemoteBankResponse.P_MAX_SAVINGS);
        ModuleHelper.setField(responseMap, dto, RemoteBankResponse.P_TRANSACTIONS_COUNT);

        if (!ModuleHelper.setField(responseMap, dto, RemoteBankResponse.P_TRANSACTIONS,
                (f, data) -> ModuleHelper.convertToTransactionData(data))) {
            dto.setTransactions(Collections.emptyList());
        }
        return dto;
    }
}
