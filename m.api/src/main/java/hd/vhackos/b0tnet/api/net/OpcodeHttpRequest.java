package hd.vhackos.b0tnet.api.net;

import hd.vhackos.b0tnet.api.exception.ConnectionBindException;
import hd.vhackos.b0tnet.api.exception.ConnectionException;
import hd.vhackos.b0tnet.api.exception.InvalidResponseCodeException;
import hd.vhackos.b0tnet.api.module.base.IModule;
import hd.vhackos.b0tnet.api.opcode.base.IOpcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;
import hd.vhackos.b0tnet.shared.utils.SentryGuard;
import org.jsoup.Jsoup;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * Vytvoří request pro HTTP opcode.
 */
public final class OpcodeHttpRequest extends AbstractOpcodeRequest<OpcodeHttpRequestResp> {

    /**
     * Vytvoří novou instanci.
     */
    OpcodeHttpRequest(IOpcode opcode) {
        super(opcode);
    }

    /**
     * Získá absolutní URI včetně parametrů.
     *
     * @param target Cíl adresy na kterou se má komunikovat.
     * @param api    Verze herního API.
     * @param params Dodatečné parametry do URL.
     */
    private static String getAbsoluteUri(OpcodeTargetType target, int api, Map<String, String> params) {
        final var restUri = VHACK_URL + api;
        var url = new StringBuilder(String.format("%s/%s.php", restUri, target.getCode()));
        var hasParams = false;

        for (var entry : params.entrySet()) {
            url.append(String.format("%s%s=%s", hasParams ? "&" : "?", entry.getKey(), entry.getValue()));

            if (!hasParams) {
                hasParams = true;
            }
        }
        return url.toString();
    }

    @Override
    public synchronized OpcodeHttpRequestResp sendOpcode(IModule module, Map<String, String> params) {
        // vygeneruje absolutní URI
        var api = module.getB0tnet().getConfig().getGameApi();
        var uri = getAbsoluteUri(opcode.getTarget(), api, params);

        // připravý výstup
        var opcodeResult = new OpcodeHttpRequestResp(opcode.getTarget());
        opcodeResult.setApi(api);
        opcodeResult.setParameters(params);
        opcodeResult.setRawUri(uri);

        HttpsURLConnection conn = null;
        getLog().trace("Target URI: {}", uri);

        try {
            var config = module.getB0tnet().getConfig();
            conn = createConnection(uri, getUserAgent(module), config.getProxyData(), config.getConnectionTimeout());
            var responseCode = conn.getResponseCode();

            if (responseCode == 200) {
                InputStream input = conn.getInputStream();
                var doc = Jsoup.parse(input, StandardCharsets.UTF_8.name(), uri);
                opcodeResult.setDocument(doc);

            } else if (responseCode == 503) {
                throw new ConnectionException("Remote service is unavailable: URI: " + uri);

            } else {
                throw new InvalidResponseCodeException(responseCode,
                        "Failed to get response from vHackOS " + responseCode + " {" + uri + "}");
            }
        } catch (NoRouteToHostException e) {
            throw new ConnectionException(null, "Could not connect to a remote game server. URI: " + uri, e);

        } catch (UnknownHostException e) {
            throw new ConnectionException(null, "Target server is not available. URI: " + uri, e);

        } catch (SocketTimeoutException e) {
            throw new ConnectionException(null, "Response timeout expired. URI: " + uri, e);

        } catch (BindException e) {
            throw new ConnectionBindException("Target server is not available. URI: " + uri, e);

        } catch (IOException e) {
            if (!(e instanceof ConnectException)) {
                SentryGuard.log(e);
                getLog().debug("Sending a request to the server failed", e);
                getLog().error("Something is wrong with processing the request. URI: " + uri);
            }
            throw new ConnectionException(null, "An error occurred while processing request. URI: " + uri, e);
        } finally {
            if (conn != null) {
                getLog().debug("Closing connection for opcode: {}->{}", opcode.getTarget(), opcode.getOpcodeValue());
                conn.disconnect();
            }
        }
        return opcodeResult;
    }
}
