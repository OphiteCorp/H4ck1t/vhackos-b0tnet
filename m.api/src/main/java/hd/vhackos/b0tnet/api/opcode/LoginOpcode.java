package hd.vhackos.b0tnet.api.opcode;

import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;

/**
 * Vytvoří opcode pro přihlášení uživatele.
 */
public final class LoginOpcode extends Opcode {

    private static final String PARAM_USERNAME = "username";
    private static final String PARAM_PASSWORD = "password";

    /**
     * Uživatelské přihlašovací jméno.
     */
    public void setUserName(String userName) {
        addParam(1, PARAM_USERNAME, userName);
    }

    /**
     * Heslo v MD5 pro přihlášení.
     */
    public void setPasswordHash(String md5PasswordHash) {
        addParam(2, PARAM_PASSWORD, md5PasswordHash);
    }

    @Override
    public OpcodeTargetType getTarget() {
        return OpcodeTargetType.LOGIN;
    }
}
