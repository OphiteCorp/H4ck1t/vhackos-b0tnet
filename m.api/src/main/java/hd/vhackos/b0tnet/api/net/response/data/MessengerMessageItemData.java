package hd.vhackos.b0tnet.api.net.response.data;

import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiElapsedTimeConverter;

/**
 * Informace o jedné zprávě.
 */
public final class MessengerMessageItemData {

    public static final String P_MESSAGE = "message";
    public static final String P_TIME = "time";
    public static final String P_USER_ID = "userId";

    @AsciiRow("Message")
    @ResponseKey("message")
    private String message;

    @AsciiRow(value = "Time", converter = AsciiElapsedTimeConverter.class)
    @ResponseKey("time")
    private Long time;

    @AsciiRow("User ID")
    @ResponseKey("user_id")
    private Integer userId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
