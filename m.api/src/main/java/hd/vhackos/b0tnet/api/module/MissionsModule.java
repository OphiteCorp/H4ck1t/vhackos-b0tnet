package hd.vhackos.b0tnet.api.module;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.module.base.Module;
import hd.vhackos.b0tnet.api.module.base.ModuleHelper;
import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.MissionResponse;
import hd.vhackos.b0tnet.api.opcode.MissionsClaimDayOpcode;
import hd.vhackos.b0tnet.api.opcode.MissionsClaimOpcode;
import hd.vhackos.b0tnet.api.opcode.MissionsOpcode;
import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.shared.injection.Inject;

import java.util.Collections;

/**
 * Správa misí.
 */
@Inject
public final class MissionsModule extends Module {

    protected MissionsModule(IB0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Získá informace o misích.
     */
    public synchronized MissionResponse getMissions() {
        var opcode = new MissionsOpcode();
        return createMissionResponse(opcode);
    }

    /**
     * Sebere denní odměnu.
     */
    public synchronized MissionResponse claimDaily() {
        var opcode = new MissionsClaimDayOpcode();
        return createMissionResponse(opcode);
    }

    /**
     * Sebere odměnu.
     */
    public synchronized MissionResponse claimMissionReward(int dailyId) {
        var opcode = new MissionsClaimOpcode();
        opcode.setDailyId(dailyId);
        return createMissionResponse(opcode);
    }

    private MissionResponse createMissionResponse(Opcode opcode) {
        OpcodeRequestResp response = sendRequest(opcode);
        var responseMap = response.getResponse();
        var dto = new MissionResponse(response);

        ModuleHelper.checkResponseIntegrity(responseMap, MissionResponse.class);
        ModuleHelper.setField(responseMap, dto, MissionResponse.P_STAGE);
        ModuleHelper.setField(responseMap, dto, MissionResponse.P_CLAIM);
        ModuleHelper.setField(responseMap, dto, MissionResponse.P_CLAIMED);
        ModuleHelper.setField(responseMap, dto, MissionResponse.P_CLAIM_NEXT_DAY);
        ModuleHelper.setField(responseMap, dto, MissionResponse.P_NEXT_DAILY_RESET);
        ModuleHelper.setField(responseMap, dto, MissionResponse.P_DAILY_COUNT);
        ModuleHelper.setField(responseMap, dto, MissionResponse.P_REWARD_BOOSTERS);
        ModuleHelper.setField(responseMap, dto, MissionResponse.P_REWARD_NETCOINS);
        ModuleHelper.setField(responseMap, dto, MissionResponse.P_REWARD_EXPERIENCE);

        if (!ModuleHelper.setField(responseMap, dto, MissionResponse.P_DAILY,
                (f, data) -> ModuleHelper.convertToMissionItemData(data))) {
            dto.setDaily(Collections.emptyList());
        }
        return dto;
    }
}
