package hd.vhackos.b0tnet.api.module.base;

import com.google.gson.internal.LinkedTreeMap;
import hd.vhackos.b0tnet.api.net.response.base.Response;
import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.api.net.response.data.*;
import hd.vhackos.b0tnet.api.net.response.data.interfaces.ICrewData;
import hd.vhackos.b0tnet.shared.json.Json;
import hd.vhackos.b0tnet.shared.utils.SentryGuard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Pomocná třída pro moduly.
 */
public final class ModuleHelper {

    private static final Logger LOG = LoggerFactory.getLogger(ModuleHelper.class);

    private static final Map<Class, List<String>> RESPONSE_CLASS_CACHE = new HashMap<>();

    /**
     * Překonvertuje jednotlivé mise.
     */
    public static List<MissionItemData> convertToMissionItemData(Object data) {
        var out = new ArrayList<MissionItemData>();
        var list = ((ArrayList) data);
        var integrityData = new ArrayList<ResponseIntegrityData>();

        for (var item : list) {
            var entries = ((LinkedTreeMap<String, Object>) item).entrySet();
            var m = new MissionItemData();

            for (var entry : entries) {
                integrityData.add(checkResponseIntegrity(data, entry.getKey(), MissionItemData.class));
                setField(entry, m, MissionItemData.P_TITLE);
                setField(entry, m, MissionItemData.P_DESCRIPTION);
                setField(entry, m, MissionItemData.P_FINISHED);
                setField(entry, m, MissionItemData.P_EXPERIENCE);
                setField(entry, m, MissionItemData.P_REWARD_TYPE);
                setField(entry, m, MissionItemData.P_REWARD_AMOUNT);
            }
            out.add(m);
        }
        sendSentryIntegrityData(integrityData);
        return out;
    }

    /**
     * Překonvertuje položky pro leaderboard.
     */
    public static List<LeaderboardData> convertToLeaderboardData(Object data) {
        var out = new ArrayList<LeaderboardData>();
        var list = ((ArrayList) data);
        var integrityData = new ArrayList<ResponseIntegrityData>();

        for (var item : list) {
            var entries = ((LinkedTreeMap<String, Object>) item).entrySet();
            var lb = new LeaderboardData();

            for (var entry : entries) {
                integrityData.add(checkResponseIntegrity(data, entry.getKey(), LeaderboardData.class));
                setField(entry, lb, LeaderboardData.P_USER);
                setField(entry, lb, LeaderboardData.P_EXP_PERCENT);
                setField(entry, lb, LeaderboardData.P_LEVEL);
            }
            out.add(lb);
        }
        sendSentryIntegrityData(integrityData);
        return out;
    }

    /**
     * Překonvertuje položky pro 24H turnaj v leaderboardu.
     */
    public static List<Tournament24HData> convertToTournament24HData(Object data) {
        var out = new ArrayList<Tournament24HData>();
        var list = ((ArrayList) data);
        var integrityData = new ArrayList<ResponseIntegrityData>();

        for (var item : list) {
            var entries = ((LinkedTreeMap<String, Object>) item).entrySet();
            var t = new Tournament24HData();

            for (var entry : entries) {
                integrityData.add(checkResponseIntegrity(data, entry.getKey(), Tournament24HData.class));
                setField(entry, t, Tournament24HData.P_USER);
                setField(entry, t, Tournament24HData.P_EXP_GAIN);
                setField(entry, t, Tournament24HData.P_LEVEL);
            }
            out.add(t);
        }
        sendSentryIntegrityData(integrityData);
        return out;
    }

    /**
     * Překonvertuje položky pro crew v leaderboardu.
     */
    public static List<LeaderboardCrewData> convertToLeaderboardCrewData(Object data) {
        var out = new ArrayList<LeaderboardCrewData>();
        var list = ((ArrayList) data);
        var integrityData = new ArrayList<ResponseIntegrityData>();

        for (var item : list) {
            var entries = ((LinkedTreeMap<String, Object>) item).entrySet();
            var crew = new LeaderboardCrewData();

            for (var entry : entries) {
                integrityData.add(checkResponseIntegrity(data, entry.getKey(), LeaderboardCrewData.class));
                setField(entry, crew, LeaderboardCrewData.P_CREW_NAME);
                setField(entry, crew, LeaderboardCrewData.P_CREW_TAG);
                setField(entry, crew, LeaderboardCrewData.P_CREW_REPUTATION);
                setField(entry, crew, LeaderboardCrewData.P_CREW_ID);
                setField(entry, crew, LeaderboardCrewData.P_MEMBERS);
            }
            out.add(crew);
        }
        sendSentryIntegrityData(integrityData);
        return out;
    }

    /**
     * Překonvertuje položky pro 1on1 v leaderboardu.
     */
    public static List<Leaderboard1on1Data> convertToLeaderboard1on1Data(Object data) {
        var out = new ArrayList<Leaderboard1on1Data>();
        var list = ((ArrayList) data);
        var integrityData = new ArrayList<ResponseIntegrityData>();

        for (var item : list) {
            var entries = ((LinkedTreeMap<String, Object>) item).entrySet();
            var crew = new Leaderboard1on1Data();

            for (var entry : entries) {
                integrityData.add(checkResponseIntegrity(data, entry.getKey(), Leaderboard1on1Data.class));
                setField(entry, crew, Leaderboard1on1Data.P_CREW_NAME);
                setField(entry, crew, Leaderboard1on1Data.P_CREW_TAG);
                setField(entry, crew, Leaderboard1on1Data.P_CREW_REPUTATION);
                setField(entry, crew, Leaderboard1on1Data.P_CREW_ID);
                setField(entry, crew, Leaderboard1on1Data.P_MEMBERS);
            }
            out.add(crew);
        }
        sendSentryIntegrityData(integrityData);
        return out;
    }

    /**
     * Překonvertuje data do sken IP dat.
     */
    public static List<IpScanData> convertToIpScanData(Object data) {
        var out = new ArrayList<IpScanData>();
        var list = ((ArrayList) data);
        var integrityData = new ArrayList<ResponseIntegrityData>();

        for (var item : list) {
            var entries = ((LinkedTreeMap<String, Object>) item).entrySet();
            var ip = new IpScanData();

            for (var entry : entries) {
                integrityData.add(checkResponseIntegrity(data, entry.getKey(), IpScanData.class));
                setField(entry, ip, IpScanData.P_IP);
                setField(entry, ip, IpScanData.P_LEVEL);
                setField(entry, ip, IpScanData.P_FIREWALL);
                setField(entry, ip, IpScanData.P_OPEN);
            }
            out.add(ip);
        }
        sendSentryIntegrityData(integrityData);
        return out;
    }

    /**
     * Překonvertuje data do prolomený IP dat.
     */
    public static List<IpBruteData> convertToIpBruteData(Object data) {
        var out = new ArrayList<IpBruteData>();
        var list = ((ArrayList) data);
        var integrityData = new ArrayList<ResponseIntegrityData>();

        for (var item : list) {
            var entries = ((LinkedTreeMap<String, Object>) item).entrySet();
            var ip = new IpBruteData();

            for (var entry : entries) {
                integrityData.add(checkResponseIntegrity(data, entry.getKey(), IpBruteData.class));
                setField(entry, ip, IpBruteData.P_IP);
                setField(entry, ip, IpBruteData.P_USER_NAME);
                setField(entry, ip, IpBruteData.P_BRUTE);
            }
            out.add(ip);
        }
        sendSentryIntegrityData(integrityData);
        return out;
    }

    /**
     * Překonvertuje data do detailní prolomených IP.
     */
    public static List<IpBruteDetailData> convertToIpBruteDetailData(Object data) {
        var out = new ArrayList<IpBruteDetailData>();
        var list = ((ArrayList) data);
        var integrityData = new ArrayList<ResponseIntegrityData>();

        for (var item : list) {
            var entries = ((LinkedTreeMap<String, Object>) item).entrySet();
            var ip = new IpBruteDetailData();

            for (var entry : entries) {
                integrityData.add(checkResponseIntegrity(data, entry.getKey(), IpBruteDetailData.class));
                setField(entry, ip, IpBruteDetailData.P_BRUTE_ID);
                setField(entry, ip, IpBruteDetailData.P_IP);
                setField(entry, ip, IpBruteDetailData.P_START_TIME);
                setField(entry, ip, IpBruteDetailData.P_END_TIME);
                setField(entry, ip, IpBruteDetailData.P_CURRENT_TIME);
                setField(entry, ip, IpBruteDetailData.P_USER_NAME);
                setField(entry, ip, IpBruteDetailData.P_RESULT);
            }
            out.add(ip);
        }
        sendSentryIntegrityData(integrityData);
        return out;
    }

    /**
     * Překonvertuje data na aktualizovaný tásk.
     */
    public static List<TaskUpdateData> convertToTaskUpdateData(Object data) {
        var out = new ArrayList<TaskUpdateData>();
        var list = ((ArrayList) data);
        var integrityData = new ArrayList<ResponseIntegrityData>();

        for (var item : list) {
            var entries = ((LinkedTreeMap<String, Object>) item).entrySet();
            var t = new TaskUpdateData();

            for (var entry : entries) {
                integrityData.add(checkResponseIntegrity(data, entry.getKey(), TaskUpdateData.class));
                setField(entry, t, TaskUpdateData.P_TASK_ID);
                setField(entry, t, TaskUpdateData.P_APP_ID);
                setField(entry, t, TaskUpdateData.P_START);
                setField(entry, t, TaskUpdateData.P_END);
                setField(entry, t, TaskUpdateData.P_NOW);
                setField(entry, t, TaskUpdateData.P_LEVEL);
            }
            out.add(t);
        }
        sendSentryIntegrityData(integrityData);
        return out;
    }

    /**
     * Překonvertuje data do bankovních transakcí.
     */
    public static List<BankTransactionData> convertToTransactionData(Object data) {
        var out = new ArrayList<BankTransactionData>();
        var list = ((ArrayList) data);
        var integrityData = new ArrayList<ResponseIntegrityData>();

        for (var item : list) {
            var entries = ((LinkedTreeMap<String, Object>) item).entrySet();
            var t = new BankTransactionData();

            for (var entry : entries) {
                integrityData.add(checkResponseIntegrity(data, entry.getKey(), BankTransactionData.class));
                setField(entry, t, BankTransactionData.P_TIME);
                setField(entry, t, BankTransactionData.P_FROM_ID);
                setField(entry, t, BankTransactionData.P_FROM_IP);
                setField(entry, t, BankTransactionData.P_TO_ID);
                setField(entry, t, BankTransactionData.P_TO_IP);
                setField(entry, t, BankTransactionData.P_AMOUNT);
            }
            out.add(t);
        }
        sendSentryIntegrityData(integrityData);
        return out;
    }

    /**
     * Překonvertuje data do infromací o aplikaci v obchodu.
     */
    public static List<AppStoreData> convertToAppStoreData(Object data) {
        var out = new ArrayList<AppStoreData>();
        var list = ((ArrayList) data);
        var integrityData = new ArrayList<ResponseIntegrityData>();

        for (var item : list) {
            var entries = ((LinkedTreeMap<String, Object>) item).entrySet();
            var app = new AppStoreData();

            for (var entry : entries) {
                integrityData.add(checkResponseIntegrity(data, entry.getKey(), AppStoreData.class));
                setField(entry, app, AppStoreData.P_PRICE);
                setField(entry, app, AppStoreData.P_BASE_PRICE);
                setField(entry, app, AppStoreData.P_FACTOR);
                setField(entry, app, AppStoreData.P_APP_ID);
                setField(entry, app, AppStoreData.P_LEVEL);
                setField(entry, app, AppStoreData.P_REQUIRE_LEVEL);
                setField(entry, app, AppStoreData.P_MAX_LEVEL);
                setField(entry, app, AppStoreData.P_RUNNING);
            }
            out.add(app);
        }
        sendSentryIntegrityData(integrityData);
        return out;
    }

    /**
     * Překonvertuje data do infromací o členu crew.
     */
    public static List<ICrewData> convertToCrewMemberData(Object data) {
        var out = new ArrayList<ICrewData>();
        var list = ((ArrayList) data);
        var integrityData = new ArrayList<ResponseIntegrityData>();

        for (var item : list) {
            var entries = ((LinkedTreeMap<String, Object>) item).entrySet();
            var member = new CrewMemberData();

            for (var entry : entries) {
                integrityData.add(checkResponseIntegrity(data, entry.getKey(), CrewMemberData.class));
                setField(entry, member, CrewMemberData.P_USER_ID);
                setField(entry, member, CrewMemberData.P_POSITION);
                setField(entry, member, CrewMemberData.P_USER_NAME);
                setField(entry, member, CrewMemberData.P_LEVEL);
                setField(entry, member, CrewMemberData.P_LAST_ONLINE);
                setField(entry, member, CrewMemberData.P_REPEARNED);
                setField(entry, member, CrewMemberData.P_RUSHED);
            }
            out.add(member);
        }
        sendSentryIntegrityData(integrityData);
        return out;
    }

    /**
     * Překonvertuje data do infromací o zprávě v crew.
     */
    public static List<CrewMessageData> convertToCrewMessageData(Object data) {
        var out = new ArrayList<CrewMessageData>();
        var list = ((ArrayList) data);
        var integrityData = new ArrayList<ResponseIntegrityData>();

        for (var item : list) {
            var entries = ((LinkedTreeMap<String, Object>) item).entrySet();
            var message = new CrewMessageData();

            for (var entry : entries) {
                integrityData.add(checkResponseIntegrity(data, entry.getKey(), CrewMessageData.class));
                setField(entry, message, CrewMessageData.P_USER_ID);
                setField(entry, message, CrewMessageData.P_MESSAGE);
                setField(entry, message, CrewMessageData.P_USER_NAME);
                setField(entry, message, CrewMessageData.P_TIME);
            }
            out.add(message);
        }
        sendSentryIntegrityData(integrityData);
        return out;
    }

    /**
     * Překonvertuje data do infromací o žádosti do crew.
     */
    public static List<ICrewData> convertToCrewRequestsData(Object data) {
        var out = new ArrayList<ICrewData>();
        var list = ((ArrayList) data);
        var integrityData = new ArrayList<ResponseIntegrityData>();

        for (var item : list) {
            var entries = ((LinkedTreeMap<String, Object>) item).entrySet();
            var req = new CrewRequestData();

            for (var entry : entries) {
                integrityData.add(checkResponseIntegrity(data, entry.getKey(), CrewRequestData.class));
                setField(entry, req, CrewRequestData.P_LAST_ONLINE);
                setField(entry, req, CrewRequestData.P_LEVEL);
                setField(entry, req, CrewRequestData.P_POSITION);
                setField(entry, req, CrewRequestData.P_USER_ID);
                setField(entry, req, CrewRequestData.P_USER_NAME);
            }
            out.add(req);
        }
        sendSentryIntegrityData(integrityData);
        return out;
    }

    /**
     * Překonvertuje data do infromací o útoku na crew.
     */
    public static List<DefaceData> convertToDefaceData(Object data) {
        var out = new ArrayList<DefaceData>();
        var list = ((ArrayList) data);
        var integrityData = new ArrayList<ResponseIntegrityData>();

        for (var item : list) {
            var entries = ((LinkedTreeMap<String, Object>) item).entrySet();
            var deface = new DefaceData();

            for (var entry : entries) {
                integrityData.add(checkResponseIntegrity(data, entry.getKey(), DefaceData.class));
                setField(entry, deface, DefaceData.P_DEFACE_ALREADY_HACKED);
                setField(entry, deface, DefaceData.P_DEFACE_BY);
                setField(entry, deface, DefaceData.P_DEFACE_FIREWALL);
                setField(entry, deface, DefaceData.P_DEFACE_IP);
                setField(entry, deface, DefaceData.P_DEFACE_TIME_LEFT);
                setField(entry, deface, DefaceData.P_DEFACE_VISITS);
            }
            out.add(deface);
        }
        sendSentryIntegrityData(integrityData);
        return out;
    }

    /**
     * Překonvertuje data do informací o rush pro členy crew.
     */
    public static List<CrewRushData> convertToCrewRushData(Object data) {
        var out = new ArrayList<CrewRushData>();
        var list = ((ArrayList) data);
        var integrityData = new ArrayList<ResponseIntegrityData>();

        for (var item : list) {
            var entries = ((LinkedTreeMap<String, Object>) item).entrySet();
            var rush = new CrewRushData();

            for (var entry : entries) {
                integrityData.add(checkResponseIntegrity(data, entry.getKey(), CrewRushData.class));
                setField(entry, rush, CrewRushData.P_NAME);
                setField(entry, rush, CrewRushData.P_TAG);
                setField(entry, rush, CrewRushData.P_RANK);
                setField(entry, rush, CrewRushData.P_SCORE);
            }
            out.add(rush);
        }
        sendSentryIntegrityData(integrityData);
        return out;
    }

    /**
     * Překonvertuje data do seznamu SKU.
     */
    public static List<String> convertToSKUsData(Object data) {
        var out = new ArrayList<String>();
        var list = ((ArrayList) data);

        for (var item : list) {
            var entries = ((LinkedTreeMap<String, Object>) item).entrySet();

            for (var entry : entries) {
                out.add(entry.getValue().toString());
            }
        }
        return out;
    }

    /**
     * Překonvertuje data do seznamu položek nákupu.
     */
    public static List<BuyItemData> convertToBuyItemData(Object data) {
        var out = new ArrayList<BuyItemData>();
        var list = ((ArrayList) data);
        var integrityData = new ArrayList<ResponseIntegrityData>();

        for (var item : list) {
            var entries = ((LinkedTreeMap<String, Object>) item).entrySet();
            var bi = new BuyItemData();

            for (var entry : entries) {
                integrityData.add(checkResponseIntegrity(data, entry.getKey(), BuyItemData.class));
                setField(entry, bi, BuyItemData.P_SKU);
                setField(entry, bi, BuyItemData.P_PRICE);
                setField(entry, bi, BuyItemData.P_AMOUNT);
                setField(entry, bi, BuyItemData.P_TITLE);
            }
            out.add(bi);
        }
        sendSentryIntegrityData(integrityData);
        return out;
    }

    /**
     * Překonvertuje data do informaci o PvP boostu.
     */
    public static List<ServerPvPBoostData> convertToServerPvPBoostData(Object data) {
        var out = new ArrayList<ServerPvPBoostData>();
        var list = ((ArrayList) data);
        var integrityData = new ArrayList<ResponseIntegrityData>();

        for (var item : list) {
            var entries = ((LinkedTreeMap<String, Object>) item).entrySet();
            var boost = new ServerPvPBoostData();

            for (var entry : entries) {
                integrityData.add(checkResponseIntegrity(data, entry.getKey(), ServerPvPBoostData.class));
                setField(entry, boost, ServerPvPBoostData.P_ACTIVE);
                setField(entry, boost, ServerPvPBoostData.P_ID);
                setField(entry, boost, ServerPvPBoostData.P_RARITY);
            }
            out.add(boost);
        }
        sendSentryIntegrityData(integrityData);
        return out;
    }

    /**
     * Překonvertuje data do seznamu zpráv.
     */
    public static List<MessengerData> convertToMessengerData(Object data) {
        var out = new ArrayList<MessengerData>();
        var list = ((ArrayList) data);
        var integrityData = new ArrayList<ResponseIntegrityData>();

        for (var item : list) {
            var entries = ((LinkedTreeMap<String, Object>) item).entrySet();
            var msg = new MessengerData();

            for (var entry : entries) {
                integrityData.add(checkResponseIntegrity(data, entry.getKey(), MessengerData.class));
                setField(entry, msg, MessengerData.P_MESSAGE);
                setField(entry, msg, MessengerData.P_REQUEST);
                setField(entry, msg, MessengerData.P_TIME);
                setField(entry, msg, MessengerData.P_UNREAD_COUNT);
                setField(entry, msg, MessengerData.P_WITH);
                setField(entry, msg, MessengerData.P_WITH_ID);
            }
            out.add(msg);
        }
        sendSentryIntegrityData(integrityData);
        return out;
    }

    /**
     * Překonvertuje data do seznamu zpráv.
     */
    public static List<MessengerMessageItemData> convertToMessengerMessageItemData(Object data) {
        var out = new ArrayList<MessengerMessageItemData>();
        var list = ((ArrayList) data);
        var integrityData = new ArrayList<ResponseIntegrityData>();

        for (var item : list) {
            var entries = ((LinkedTreeMap<String, Object>) item).entrySet();
            var msg = new MessengerMessageItemData();

            for (var entry : entries) {
                integrityData.add(checkResponseIntegrity(data, entry.getKey(), MessengerMessageItemData.class));
                setField(entry, msg, MessengerMessageItemData.P_MESSAGE);
                setField(entry, msg, MessengerMessageItemData.P_TIME);
                setField(entry, msg, MessengerMessageItemData.P_USER_ID);
            }
            out.add(msg);
        }
        sendSentryIntegrityData(integrityData);
        return out;
    }

    /**
     * Přemapuje odpověd do fieldu v DTO.
     */
    public static boolean setField(Map<String, Object> response, Object dto, String fieldName) {
        return setField(response, dto, fieldName, null);
    }

    /**
     * Přemapuje odpověd do fieldu v DTO.
     */
    public static boolean setField(Map<String, Object> response, Object dto, String fieldName, IFieldMapper mapper) {
        boolean set = false;

        for (var entry : response.entrySet()) {
            if (setField(entry, dto, fieldName, mapper)) {
                set = true;
            }
        }
        return set;
    }

    /**
     * Přemapuje entry do fieldu v DTO.
     */
    public static boolean setField(Map.Entry<String, Object> entry, Object dto, String fieldName) {
        return setField(entry, dto, fieldName, null);
    }

    /**
     * Přemapuje entry do fieldu v DTO.
     */
    public static boolean setField(Map.Entry<String, Object> entry, Object dto, String fieldName, IFieldMapper mapper) {
        Object value = null;
        try {
            var field = dto.getClass().getDeclaredField(fieldName);

            if (field.isAnnotationPresent(ResponseKey.class)) {
                var a = field.getDeclaredAnnotation(ResponseKey.class);
                var type = field.getGenericType();
                var key = entry.getKey();

                if (key != null && key.equals(a.value())) {
                    value = entry.getValue();
                    field.setAccessible(true);

                    if (type.equals(String.class)) {
                        value = String.valueOf(value);
                    } else if (type.equals(Integer.class) || type.equals(int.class)) {
                        value = Integer.valueOf(value.toString());
                    } else if (type.equals(Long.class) || type.equals(long.class)) {
                        value = Long.valueOf(value.toString());
                    } else if (type.equals(Byte.class) || type.equals(byte.class)) {
                        value = Byte.valueOf(value.toString());
                    } else if (type.equals(Boolean.class) || type.equals(boolean.class)) {
                        value = Boolean.valueOf(value.toString());
                    } else if (type.equals(Double.class) || type.equals(double.class)) {
                        value = Double.valueOf(value.toString());
                    } else if (type.equals(Float.class) || type.equals(float.class)) {
                        value = Float.valueOf(value.toString());
                    } else if (type.equals(Object.class)) {
                        value = value.toString();
                    } else {
                        if (mapper != null) {
                            value = mapper.convert(field, value);
                        } else {
                            var ex = new IllegalStateException(
                                    "This is not a primitive data type '" + type.getTypeName() +
                                    "'. Custom mapper is not set");
                            SentryGuard.log(ex);
                            throw ex;
                        }
                    }
                    field.set(dto, value);
                    return true;
                }
            } else {
                throw new IllegalStateException("The Field '" + field.getName() + "' is not marked with an '" +
                                                ResponseKey.class.getSimpleName() + "' annotation");
            }
        } catch (NoSuchFieldException e) {
            throw new IllegalStateException(
                    "Field '" + fieldName + "' in the '" + dto.getClass().getSimpleName() + "' instance does " +
                    "not exist", e);

        } catch (IllegalAccessException e) {
            throw new RuntimeException("An unexpected error has occurred", e);

        } catch (NumberFormatException e) {
            LOG.error("There was an error converting the value: {} under field: {}", value, fieldName);
            throw new NumberFormatException(
                    "There was an error converting the value: " + value + " under field: " + fieldName);
        }
        return false;
    }

    /**
     * Zkontroluje integritu parametru v odpovedi s mapovanim tridy.
     */
    public static ResponseIntegrityData checkResponseIntegrity(Object data, String responseKey, Class responseClass) {
        synchronized (RESPONSE_CLASS_CACHE) {
            List<String> respKeyList = getAvailableResponseClassKeys(responseClass);

            var output = new ResponseIntegrityData();
            output.valid = true;
            output.value = data;
            output.key = responseKey;

            if (!respKeyList.contains(responseKey)) {
                var msg = String.format("Missing fields mapping in '%s'. Undefined response parameter is: %s",
                        responseClass.getSimpleName(), responseKey);
                LOG.warn(msg);

                output.message = msg;
                output.valid = false;
            }
            return output;
        }
    }

    /**
     * Zkontroluje integritu parametru v odpovedi s mapovanim tridy.
     */
    public static int checkResponseIntegrity(Map<String, Object> data, Class responseClass) {
        synchronized (RESPONSE_CLASS_CACHE) {
            var respKeyList = getAvailableResponseClassKeys(responseClass);
            var undefinedData = new LinkedHashMap<String, Object>();

            for (var key : data.keySet()) {
                if (!respKeyList.contains(key)) {
                    undefinedData.put(key, data.get(key));
                }
            }
            if (!undefinedData.isEmpty()) {
                var msg = String.format("Missing fields mapping in '%s'. Undefined response parameter is: %s",
                        responseClass.getSimpleName(), undefinedData.keySet());
                LOG.warn(msg);
                SentryGuard.logWarning(msg, Json.toJson(undefinedData));
            }
            return undefinedData.size();
        }
    }

    private static void sendSentryIntegrityData(List<ResponseIntegrityData> data) {
        for (var item : data) {
            if (!item.valid) {
                SentryGuard.logWarning(item.message, Json.toJson(item));
            }
        }
    }

    private static List<String> getAvailableResponseClassKeys(Class responseClass) {
        List<String> respKeyList;

        if (!RESPONSE_CLASS_CACHE.containsKey(responseClass)) {
            var fields = responseClass.getDeclaredFields();
            respKeyList = new ArrayList<>();

            for (var field : fields) {
                if (field.isAnnotationPresent(ResponseKey.class)) {
                    var a = field.getDeclaredAnnotation(ResponseKey.class);
                    respKeyList.add(a.value());
                }
            }
            if (Response.class.isAssignableFrom(responseClass.getSuperclass())) {
                fields = responseClass.getSuperclass().getDeclaredFields();

                for (var field : fields) {
                    if (field.isAnnotationPresent(ResponseKey.class)) {
                        var a = field.getDeclaredAnnotation(ResponseKey.class);
                        respKeyList.add(a.value());
                    }
                }
            }
            RESPONSE_CLASS_CACHE.put(responseClass, respKeyList);
        } else {
            respKeyList = RESPONSE_CLASS_CACHE.get(responseClass);
        }
        return respKeyList;
    }

    private static final class ResponseIntegrityData {

        private transient boolean valid;
        private transient String message;
        private String key;
        private Object value;
    }
}
