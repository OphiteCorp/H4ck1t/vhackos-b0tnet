package hd.vhackos.b0tnet.api.net.response;

import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.base.Response;
import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiBooleanConverter;

/**
 * Informace o vzdáleném systému.
 */
public final class RemoteSystemResponse extends Response {

    public static final String P_CUSTOM_BACKGROUND = "customBackground";
    public static final String P_IP = "ip";
    public static final String P_REMOTE_IP = "remoteIp";
    public static final String P_LEVEL = "level";
    public static final String P_USER_NAME = "userName";
    public static final String P_NOTEPAD = "notepad";
    public static final String P_LEADERBOARD = "leaderboard";
    public static final String P_MISSIONS = "missions";
    public static final String P_JOBS = "jobs";
    public static final String P_COMMUNITY = "community";
    public static final String P_INTERNET_CONNECTION = "internetConnection";
    public static final String P_REMOTE_CREW = "remoteCrew";
    public static final String P_TAGGED = "tagged";
    public static final String P_CAN_DEFACE = "canDeface";
    public static final String P_DEFACE = "defaced";
    public static final String P_DEFACE_BY = "defacedBy";

    @AsciiRow("Custom Background")
    @ResponseKey("customBG")
    private String customBackground;

    @AsciiRow("IP")
    @ResponseKey("remoteIP")
    private String ip;

    @AsciiRow("Remote IP")
    @ResponseKey("remote_ip")
    private String remoteIp;

    @AsciiRow("Level")
    @ResponseKey("remoteLevel")
    private Integer level;

    @AsciiRow("User")
    @ResponseKey("remoteUsername")
    private String userName;

    @AsciiRow(value = "Notepad", converter = AsciiBooleanConverter.class)
    @ResponseKey("notepad")
    private Integer notepad;

    @AsciiRow(value = "Leaderboard", converter = AsciiBooleanConverter.class)
    @ResponseKey("lb")
    private Integer leaderboard;

    @AsciiRow(value = "Missions", converter = AsciiBooleanConverter.class)
    @ResponseKey("missions")
    private Integer missions;

    @AsciiRow(value = "Jobs", converter = AsciiBooleanConverter.class)
    @ResponseKey("jobs")
    private Integer jobs;

    @AsciiRow(value = "Community", converter = AsciiBooleanConverter.class)
    @ResponseKey("community")
    private Integer community;

    @AsciiRow("Internet Connection")
    @ResponseKey("remoteConnection")
    private String internetConnection;

    @AsciiRow("Remote Crew")
    @ResponseKey("remoteCrew")
    private String remoteCrew;

    @AsciiRow(value = "Tagged", converter = AsciiBooleanConverter.class)
    @ResponseKey("tagged")
    private Integer tagged;

    @AsciiRow(value = "Can Deface", converter = AsciiBooleanConverter.class)
    @ResponseKey("can_deface")
    private Integer canDeface;

    @AsciiRow("Defaced")
    @ResponseKey("defaced")
    private Integer defaced;

    @AsciiRow("Defaced By")
    @ResponseKey("defaced_by")
    private String defacedBy;

    public RemoteSystemResponse(OpcodeRequestResp response) {
        super(response);
    }

    public String getDefacedBy() {
        return defacedBy;
    }

    public void setDefacedBy(String defacedBy) {
        this.defacedBy = defacedBy;
    }

    public Integer getTagged() {
        return tagged;
    }

    public void setTagged(Integer tagged) {
        this.tagged = tagged;
    }

    public Integer getCanDeface() {
        return canDeface;
    }

    public void setCanDeface(Integer canDeface) {
        this.canDeface = canDeface;
    }

    public Integer getDefaced() {
        return defaced;
    }

    public void setDefaced(Integer defaced) {
        this.defaced = defaced;
    }

    public String getRemoteCrew() {
        return remoteCrew;
    }

    public void setRemoteCrew(String remoteCrew) {
        this.remoteCrew = remoteCrew;
    }

    public String getRemoteIp() {
        return remoteIp;
    }

    public void setRemoteIp(String remoteIp) {
        this.remoteIp = remoteIp;
    }

    public String getCustomBackground() {
        return customBackground;
    }

    public void setCustomBackground(String customBackground) {
        this.customBackground = customBackground;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getNotepad() {
        return notepad;
    }

    public void setNotepad(Integer notepad) {
        this.notepad = notepad;
    }

    public Integer getLeaderboard() {
        return leaderboard;
    }

    public void setLeaderboard(Integer leaderboard) {
        this.leaderboard = leaderboard;
    }

    public Integer getMissions() {
        return missions;
    }

    public void setMissions(Integer missions) {
        this.missions = missions;
    }

    public Integer getJobs() {
        return jobs;
    }

    public void setJobs(Integer jobs) {
        this.jobs = jobs;
    }

    public Integer getCommunity() {
        return community;
    }

    public void setCommunity(Integer community) {
        this.community = community;
    }

    public String getInternetConnection() {
        return internetConnection;
    }

    public void setInternetConnection(String internetConnection) {
        this.internetConnection = internetConnection;
    }
}
