package hd.vhackos.b0tnet.api.net.response;

import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.base.Response;
import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.api.net.response.data.CrewMessageData;
import hd.vhackos.b0tnet.api.net.response.data.CrewRushData;
import hd.vhackos.b0tnet.api.net.response.data.DefaceData;
import hd.vhackos.b0tnet.api.net.response.data.interfaces.ICrewData;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiBooleanConverter;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiElapsedTimeConverter;

import java.util.List;

/**
 * Informace o crew.
 */
public final class CrewResponse extends Response {

    public static final String P_CREATED = "created";
    public static final String P_REQUESTED = "requested";
    public static final String P_ACCEPTED = "accepted";
    public static final String P_SENT = "sent";
    public static final String P_CHANGE_LOGO = "changeLogo";
    public static final String P_PROMOTE = "promote";
    public static final String P_DEMOTE = "demote";
    public static final String P_LEAVE = "leave";
    public static final String P_KICKED = "kicked";
    public static final String P_CREW_MESSAGES = "crewMessages";
    public static final String P_MY_POSITION = "myPosition";
    public static final String P_CREW_NAME = "crewName";
    public static final String P_CREW_TAG = "crewTag";
    public static final String P_IS_MEMBER = "isMember";
    public static final String P_CREW_ID = "crewId";
    public static final String P_CREW_LOGO = "crewLogo";
    public static final String P_CREW_OWNER = "crewOwner";
    public static final String P_CREW_MEMBERS_COUNT = "crewMembersCount";
    public static final String P_CREW_MEMBERS = "crewMembers";
    public static final String P_CREW_REPUTATION = "crewReputation";
    public static final String P_CREW_RANK = "crewRank";
    public static final String P_REQ_COUNT = "reqCount";
    public static final String P_CREW_NEW_MESSAGES = "crewNewMessages";
    public static final String P_MESSAGE = "message";
    public static final String P_TIME = "time";
    public static final String P_USER_ID = "userId";
    public static final String P_USER_NAME = "userName";
    public static final String P_MEMBERS = "members";
    public static final String P_MESSAGES = "messages";
    public static final String P_REQUESTS = "requests";
    public static final String P_DEFACES = "defaces";
    public static final String P_DEFACE_COUNT = "defaceCount";
    public static final String P_RUSH_ACTIVE = "rushActive";
    public static final String P_RUSHES = "rushes";
    public static final String P_RUSH_LEFT = "rushLeft";
    public static final String P_RUSH_TARGET = "rushTarget";
    public static final String P_RUSH_CREW_SCORE = "rushCrewScore";
    public static final String P_RUSH_WON = "rushWon";
    public static final String P_RUSH_RANK = "rushRank";

    @AsciiRow(value = "Created", converter = AsciiBooleanConverter.class)
    @ResponseKey("created")
    private Integer created;

    @AsciiRow(value = "Requested", converter = AsciiBooleanConverter.class)
    @ResponseKey("requested")
    private Integer requested;

    @AsciiRow(value = "Accepted", converter = AsciiBooleanConverter.class)
    @ResponseKey("accepted")
    private Integer accepted;

    @AsciiRow(value = "Sent", converter = AsciiBooleanConverter.class)
    @ResponseKey("sent")
    private Integer sent;

    @AsciiRow(value = "Change Logo", converter = AsciiBooleanConverter.class)
    @ResponseKey("chgLogo")
    private Integer changeLogo;

    @AsciiRow(value = "Promote", converter = AsciiBooleanConverter.class)
    @ResponseKey("promote")
    private Integer promote;

    @AsciiRow(value = "Demote", converter = AsciiBooleanConverter.class)
    @ResponseKey("demote")
    private Integer demote;

    @AsciiRow(value = "Leave", converter = AsciiBooleanConverter.class)
    @ResponseKey("leave")
    private Integer leave;

    @AsciiRow(value = "Kicked", converter = AsciiBooleanConverter.class)
    @ResponseKey("kicked")
    private Integer kicked;

    @AsciiRow("Crew Messages")
    @ResponseKey("crew_messages")
    private Integer crewMessages;

    @AsciiRow("My Position")
    @ResponseKey("myposition")
    private Integer myPosition;

    @AsciiRow("Crew Name")
    @ResponseKey("crew_name")
    private String crewName;

    @AsciiRow("Crew Tag")
    @ResponseKey("crew_tag")
    private String crewTag;

    @AsciiRow(value = "Is Member", converter = AsciiBooleanConverter.class)
    @ResponseKey("ismember")
    private Integer isMember;

    @AsciiRow("Crew ID")
    @ResponseKey("crew_id")
    private Long crewId;

    @AsciiRow("Crew Logo")
    @ResponseKey("crew_logo")
    private String crewLogo;

    @AsciiRow("Crew Owner")
    @ResponseKey("crew_owner")
    private String crewOwner;

    @AsciiRow("Crew Members Count")
    @ResponseKey("crew_member_count")
    private Integer crewMembersCount;

    @AsciiRow("Crew Members")
    @ResponseKey("crew_members")
    private String crewMembers;

    @AsciiRow("Crew Reputation")
    @ResponseKey("crew_rep")
    private Integer crewReputation;

    @AsciiRow("Crew Rank")
    @ResponseKey("crew_rank")
    private Integer crewRank;

    @AsciiRow("Request Count")
    @ResponseKey("req_count")
    private Integer reqCount;

    @AsciiRow("Crew New Messages")
    @ResponseKey("crew_new_messages")
    private Integer crewNewMessages;

    @AsciiRow("Message")
    @ResponseKey("message")
    private String message;

    @AsciiRow(value = "Time", converter = AsciiElapsedTimeConverter.class)
    @ResponseKey("time")
    private Long time;

    @AsciiRow("User ID")
    @ResponseKey("user_id")
    private Long userId;

    @AsciiRow("User")
    @ResponseKey("username")
    private String userName;

    @AsciiRow("Deface Count")
    @ResponseKey("deface_count")
    private Integer defaceCount;

    @AsciiRow("Rush Active")
    @ResponseKey("rush_active")
    private Integer rushActive;

    @AsciiRow("Rush Left")
    @ResponseKey("rush_left")
    private Integer rushLeft;

    @AsciiRow("Rush Target")
    @ResponseKey("rush_target")
    private String rushTarget;

    @AsciiRow("Rush Crew Score")
    @ResponseKey("rush_crew_score")
    private Integer rushCrewScore;

    @AsciiRow("Rush Won")
    @ResponseKey("rush_won")
    private Integer rushWon;

    @AsciiRow("Rush Rank")
    @ResponseKey("rush_rank")
    private Integer rushRank;

    @AsciiRow("Members")
    @ResponseKey("members")
    private List<ICrewData> members;

    @AsciiRow("Messages")
    @ResponseKey("crewmessages")
    private List<CrewMessageData> messages;

    @AsciiRow("Defaces")
    @ResponseKey("defaces")
    private List<DefaceData> defaces;

    @AsciiRow("Rushes")
    @ResponseKey("rush_lb")
    private List<CrewRushData> rushes;

    @AsciiRow("Requests")
    @ResponseKey("requests")
    private List<ICrewData> requests;

    public List<CrewRushData> getRushes() {
        return rushes;
    }

    public void setRushes(List<CrewRushData> rushes) {
        this.rushes = rushes;
    }

    public Integer getRushActive() {
        return rushActive;
    }

    public void setRushActive(Integer rushActive) {
        this.rushActive = rushActive;
    }

    public CrewResponse(OpcodeRequestResp response) {
        super(response);
    }

    public List<ICrewData> getRequests() {
        return requests;
    }

    public void setRequests(List<ICrewData> requests) {
        this.requests = requests;
    }

    public List<DefaceData> getDefaces() {
        return defaces;
    }

    public void setDefaces(List<DefaceData> defaces) {
        this.defaces = defaces;
    }

    public Integer getDefaceCount() {
        return defaceCount;
    }

    public void setDefaceCount(Integer defaceCount) {
        this.defaceCount = defaceCount;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getCreated() {
        return created;
    }

    public void setCreated(Integer created) {
        this.created = created;
    }

    public Integer getRequested() {
        return requested;
    }

    public void setRequested(Integer requested) {
        this.requested = requested;
    }

    public Integer getAccepted() {
        return accepted;
    }

    public void setAccepted(Integer accepted) {
        this.accepted = accepted;
    }

    public Integer getSent() {
        return sent;
    }

    public void setSent(Integer sent) {
        this.sent = sent;
    }

    public Integer getChangeLogo() {
        return changeLogo;
    }

    public void setChangeLogo(Integer changeLogo) {
        this.changeLogo = changeLogo;
    }

    public Integer getPromote() {
        return promote;
    }

    public void setPromote(Integer promote) {
        this.promote = promote;
    }

    public Integer getDemote() {
        return demote;
    }

    public void setDemote(Integer demote) {
        this.demote = demote;
    }

    public Integer getLeave() {
        return leave;
    }

    public void setLeave(Integer leave) {
        this.leave = leave;
    }

    public Integer getKicked() {
        return kicked;
    }

    public void setKicked(Integer kicked) {
        this.kicked = kicked;
    }

    public Integer getCrewMessages() {
        return crewMessages;
    }

    public void setCrewMessages(Integer crewMessages) {
        this.crewMessages = crewMessages;
    }

    public Integer getMyPosition() {
        return myPosition;
    }

    public void setMyPosition(Integer myPosition) {
        this.myPosition = myPosition;
    }

    public String getCrewName() {
        return crewName;
    }

    public void setCrewName(String crewName) {
        this.crewName = crewName;
    }

    public String getCrewTag() {
        return crewTag;
    }

    public void setCrewTag(String crewTag) {
        this.crewTag = crewTag;
    }

    public Integer getIsMember() {
        return isMember;
    }

    public void setIsMember(Integer isMember) {
        this.isMember = isMember;
    }

    public Long getCrewId() {
        return crewId;
    }

    public void setCrewId(Long crewId) {
        this.crewId = crewId;
    }

    public String getCrewLogo() {
        return crewLogo;
    }

    public void setCrewLogo(String crewLogo) {
        this.crewLogo = crewLogo;
    }

    public String getCrewOwner() {
        return crewOwner;
    }

    public void setCrewOwner(String crewOwner) {
        this.crewOwner = crewOwner;
    }

    public Integer getCrewMembersCount() {
        return crewMembersCount;
    }

    public void setCrewMembersCount(Integer crewMembersCount) {
        this.crewMembersCount = crewMembersCount;
    }

    public String getCrewMembers() {
        return crewMembers;
    }

    public void setCrewMembers(String crewMembers) {
        this.crewMembers = crewMembers;
    }

    public Integer getCrewReputation() {
        return crewReputation;
    }

    public void setCrewReputation(Integer crewReputation) {
        this.crewReputation = crewReputation;
    }

    public Integer getCrewRank() {
        return crewRank;
    }

    public void setCrewRank(Integer crewRank) {
        this.crewRank = crewRank;
    }

    public Integer getReqCount() {
        return reqCount;
    }

    public void setReqCount(Integer reqCount) {
        this.reqCount = reqCount;
    }

    public Integer getCrewNewMessages() {
        return crewNewMessages;
    }

    public void setCrewNewMessages(Integer crewNewMessages) {
        this.crewNewMessages = crewNewMessages;
    }

    public List<ICrewData> getMembers() {
        return members;
    }

    public void setMembers(List<ICrewData> members) {
        this.members = members;
    }

    public List<CrewMessageData> getMessages() {
        return messages;
    }

    public void setMessages(List<CrewMessageData> messages) {
        this.messages = messages;
    }

    public Integer getRushLeft() {
        return rushLeft;
    }

    public void setRushLeft(Integer rushLeft) {
        this.rushLeft = rushLeft;
    }

    public String getRushTarget() {
        return rushTarget;
    }

    public void setRushTarget(String rushTarget) {
        this.rushTarget = rushTarget;
    }

    public Integer getRushCrewScore() {
        return rushCrewScore;
    }

    public void setRushCrewScore(Integer rushCrewScore) {
        this.rushCrewScore = rushCrewScore;
    }

    public Integer getRushWon() {
        return rushWon;
    }

    public void setRushWon(Integer rushWon) {
        this.rushWon = rushWon;
    }

    public Integer getRushRank() {
        return rushRank;
    }

    public void setRushRank(Integer rushRank) {
        this.rushRank = rushRank;
    }
}
