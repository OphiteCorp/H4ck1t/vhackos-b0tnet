package hd.vhackos.b0tnet.api.exception;

import hd.vhackos.b0tnet.shared.exception.B0tnetException;

/**
 * Bruteforce banky již běží.
 */
public final class BruteforceAlreadyRunningException extends B0tnetException {

    public BruteforceAlreadyRunningException(String resultCode, String message) {
        super(resultCode, message);
    }
}
