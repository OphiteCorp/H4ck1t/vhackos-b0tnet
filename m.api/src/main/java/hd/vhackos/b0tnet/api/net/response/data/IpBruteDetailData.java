package hd.vhackos.b0tnet.api.net.response.data;

import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiBruteStateConverter;

/**
 * Informace o IP, která je na seznamu "brute" IP mezi tásky.
 */
public final class IpBruteDetailData {

    public static final String P_BRUTE_ID = "bruteId";
    public static final String P_IP = "ip";
    public static final String P_START_TIME = "startTime";
    public static final String P_END_TIME = "endTime";
    public static final String P_CURRENT_TIME = "currentTime";
    public static final String P_USER_NAME = "userName";
    public static final String P_RESULT = "result";

    @AsciiRow("Brute ID")
    @ResponseKey("id")
    private Long bruteId;

    @AsciiRow("IP")
    @ResponseKey("user_ip")
    private String ip;

    @AsciiRow("Start")
    @ResponseKey("start")
    private Long startTime;

    @AsciiRow("End")
    @ResponseKey("end")
    private Long endTime;

    @AsciiRow("Current Time")
    @ResponseKey("now")
    private Long currentTime;

    @AsciiRow("User")
    @ResponseKey("username")
    private String userName;

    @AsciiRow(value = "State", converter = AsciiBruteStateConverter.class)
    @ResponseKey("result")
    private Integer result;

    public Long getBruteId() {
        return bruteId;
    }

    public void setBruteId(Long bruteId) {
        this.bruteId = bruteId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(Long currentTime) {
        this.currentTime = currentTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }
}
