package hd.vhackos.b0tnet.api.module;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.exception.RemoteException;
import hd.vhackos.b0tnet.api.module.base.Module;
import hd.vhackos.b0tnet.api.module.base.ModuleHelper;
import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.RemoteSystemResponse;
import hd.vhackos.b0tnet.api.opcode.DefaceOpcode;
import hd.vhackos.b0tnet.api.opcode.RemoteTargetOpcode;
import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.shared.injection.Inject;

/**
 * Modul obsahující metody pro vzdálenou kontrolu uživatele. Aby metody bylo možné zavolat, tak cíl musí být exploitnut.
 */
@Inject
public final class RemoteModule extends Module {

    private static final String ERR_CODE_IP_NOT_AVAILABLE_FOR_REMOTE = "1";

    protected RemoteModule(IB0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Získá informace o vzdáleném cíli.
     */
    public synchronized RemoteSystemResponse getSystemInfo(String ip) {
        var opcode = new RemoteTargetOpcode();
        opcode.setTargetIp(ip);

        return createRemoteSystemResponse(ip, opcode);
    }

    /**
     * Převezme kontrolu nad cílovým systémem.
     */
    public synchronized RemoteSystemResponse deface(String ip) {
        var opcode = new DefaceOpcode();
        opcode.setTargetIp(ip);

        return createRemoteSystemResponse(ip, opcode);
    }

    private RemoteSystemResponse createRemoteSystemResponse(String ip, Opcode opcode) {
        OpcodeRequestResp response = sendRequest(opcode);
        var responseMap = response.getResponse();
        var result = getResultValue(responseMap);

        if (result == null || ERR_CODE_IP_NOT_AVAILABLE_FOR_REMOTE.equals(result)) {
            throw new RemoteException(result, "IP '" + ip + "' does not exist or you do not have permissions");
        }
        var dto = new RemoteSystemResponse(response);

        ModuleHelper.checkResponseIntegrity(responseMap, RemoteSystemResponse.class);
        ModuleHelper.setField(responseMap, dto, RemoteSystemResponse.P_CUSTOM_BACKGROUND);
        ModuleHelper.setField(responseMap, dto, RemoteSystemResponse.P_IP);
        ModuleHelper.setField(responseMap, dto, RemoteSystemResponse.P_REMOTE_IP);
        ModuleHelper.setField(responseMap, dto, RemoteSystemResponse.P_LEVEL);
        ModuleHelper.setField(responseMap, dto, RemoteSystemResponse.P_USER_NAME);
        ModuleHelper.setField(responseMap, dto, RemoteSystemResponse.P_NOTEPAD);
        ModuleHelper.setField(responseMap, dto, RemoteSystemResponse.P_LEADERBOARD);
        ModuleHelper.setField(responseMap, dto, RemoteSystemResponse.P_MISSIONS);
        ModuleHelper.setField(responseMap, dto, RemoteSystemResponse.P_JOBS);
        ModuleHelper.setField(responseMap, dto, RemoteSystemResponse.P_COMMUNITY);
        ModuleHelper.setField(responseMap, dto, RemoteSystemResponse.P_INTERNET_CONNECTION);
        ModuleHelper.setField(responseMap, dto, RemoteSystemResponse.P_REMOTE_CREW);
        ModuleHelper.setField(responseMap, dto, RemoteSystemResponse.P_TAGGED);
        ModuleHelper.setField(responseMap, dto, RemoteSystemResponse.P_CAN_DEFACE);
        ModuleHelper.setField(responseMap, dto, RemoteSystemResponse.P_DEFACE);
        ModuleHelper.setField(responseMap, dto, RemoteSystemResponse.P_DEFACE_BY);

        return dto;
    }
}
