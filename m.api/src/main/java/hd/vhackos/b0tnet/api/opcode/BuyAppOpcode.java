package hd.vhackos.b0tnet.api.opcode;

import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;
import hd.vhackos.b0tnet.shared.dto.AppStoreType;

/**
 * Koupí aplikaci v obchodu.
 */
public class BuyAppOpcode extends Opcode {

    private static final String PARAM_APP_CODE = "appcode";

    /**
     * Typ aplikace.
     */
    public void setApp(AppStoreType app) {
        addParam(PARAM_APP_CODE, String.valueOf(app.getId()));
    }

    @Override
    public OpcodeTargetType getTarget() {
        return OpcodeTargetType.STORE;
    }

    @Override
    public String getOpcodeValue() {
        return "100";
    }
}
