package hd.vhackos.b0tnet.api.exception;

import hd.vhackos.b0tnet.shared.exception.B0tnetException;

/**
 * Nastala chyba při registraci uživatele.
 */
public final class RegisterException extends B0tnetException {

    public RegisterException(String resultCode, String message) {
        super(resultCode, message);
    }
}
