package hd.vhackos.b0tnet.api.opcode;

import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;

/**
 * Zruší aktivní bruteforce.
 */
public final class AbortBruteOpcode extends Opcode {

    private static final String PARAM_UPDATE_ID = "updateid";

    /**
     * Bruteforce ID.
     */
    public void setBruteforceId(long bruteId) {
        addParam(PARAM_UPDATE_ID, String.valueOf(bruteId));
    }

    @Override
    public OpcodeTargetType getTarget() {
        return OpcodeTargetType.TASKS;
    }

    @Override
    public String getOpcodeValue() {
        return "10001";
    }
}
