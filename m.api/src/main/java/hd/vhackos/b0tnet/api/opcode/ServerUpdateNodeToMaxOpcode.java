package hd.vhackos.b0tnet.api.opcode;

/**
 * Vylepší node na serveru na max.
 */
public final class ServerUpdateNodeToMaxOpcode extends ServerUpdateNode5xOpcode {

    @Override
    public String getOpcodeValue() {
        return "1818";
    }
}
