package hd.vhackos.b0tnet.api.module;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.module.base.Module;
import hd.vhackos.b0tnet.api.module.base.ModuleHelper;
import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.BuyResponse;
import hd.vhackos.b0tnet.api.opcode.BuyListOpcode;
import hd.vhackos.b0tnet.api.opcode.BuyNewIpOpcode;
import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.shared.injection.Inject;

import java.util.Collections;

/**
 * Stará se o nákupy za peníze nebo netcoins.
 */
@Inject
public final class BuyModule extends Module {

    protected BuyModule(IB0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Získá seznam položek ke koupi.
     */
    public synchronized BuyResponse getBuyList() {
        var opcode = new BuyListOpcode();
        return createBuyResponse(opcode);
    }

    /**
     * Zakoupí novou IP adresu.
     */
    public synchronized BuyResponse buyNewIpAddress() {
        var opcode = new BuyNewIpOpcode();
        return createBuyResponse(opcode);
    }

    private BuyResponse createBuyResponse(Opcode opcode) {
        OpcodeRequestResp response = sendRequest(opcode);
        var responseMap = response.getResponse();
        var dto = new BuyResponse(response);

        ModuleHelper.checkResponseIntegrity(responseMap, BuyResponse.class);
        ModuleHelper.setField(responseMap, dto, BuyResponse.P_INET_UPGRADED);
        ModuleHelper.setField(responseMap, dto, BuyResponse.P_IP_CHANGE);
        ModuleHelper.setField(responseMap, dto, BuyResponse.P_BOUGHT);
        ModuleHelper.setField(responseMap, dto, BuyResponse.P_PURCHASE_LIMIT);
        ModuleHelper.setField(responseMap, dto, BuyResponse.P_INET);
        ModuleHelper.setField(responseMap, dto, BuyResponse.P_INET_COSTS);
        ModuleHelper.setField(responseMap, dto, BuyResponse.P_REQ_LEVEL_INET);
        ModuleHelper.setField(responseMap, dto, BuyResponse.P_NEXT_INET);
        ModuleHelper.setField(responseMap, dto, BuyResponse.P_CAN);
        ModuleHelper.setField(responseMap, dto, BuyResponse.P_CAN_MAX);
        ModuleHelper.setField(responseMap, dto, BuyResponse.P_SKU_COUNT);
        ModuleHelper.setField(responseMap, dto, BuyResponse.P_CAN_STARTER);
        ModuleHelper.setField(responseMap, dto, BuyResponse.P_PACK_BOUGHT);
        ModuleHelper.setField(responseMap, dto, BuyResponse.P_MY_VTC);
        ModuleHelper.setField(responseMap, dto, BuyResponse.P_PS);

        if (!ModuleHelper
                .setField(responseMap, dto, BuyResponse.P_SKUS, (f, data) -> ModuleHelper.convertToSKUsData(data))) {
            dto.setSKUs(Collections.emptyList());
        }

        if (!ModuleHelper.setField(responseMap, dto, BuyResponse.P_ITEMS,
                (f, data) -> ModuleHelper.convertToBuyItemData(data))) {
            dto.setItems(Collections.emptyList());
        }
        return dto;
    }
}
