package hd.vhackos.b0tnet.api.net;

import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeType;

import java.util.Map;

/**
 * Odpověď z opcode requestu.
 */
public final class OpcodeRequestResp implements IOpcodeRequestResp {

    private String rawUri;
    private String user;
    private String pass;
    private int api;
    private String target;
    private Map<String, String> parameters;
    private Map<String, Object> response;

    private transient final OpcodeTargetType opcodeTargetType;

    /**
     * Vytvoří novou instanci.
     */
    public OpcodeRequestResp(OpcodeTargetType opcodeTargetType) {
        this.opcodeTargetType = opcodeTargetType;
    }

    @Override
    public final OpcodeType getOpcodeType() {
        return OpcodeType.STANDARD;
    }

    @Override
    public final OpcodeTargetType getOpcodeTargetType() {
        return opcodeTargetType;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public int getApi() {
        return api;
    }

    public void setApi(int api) {
        this.api = api;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    void setPass(String pass) {
        this.pass = pass;
    }

    @Override
    public String getRawUri() {
        return rawUri;
    }

    void setRawUri(String rawUri) {
        this.rawUri = rawUri;
    }

    public Map<String, Object> getResponse() {
        return response;
    }

    public void setResponse(Map<String, Object> response) {
        this.response = response;
    }

    @Override
    public Map<String, String> getParameters() {
        return parameters;
    }

    void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }
}
