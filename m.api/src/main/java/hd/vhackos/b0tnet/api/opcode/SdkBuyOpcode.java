package hd.vhackos.b0tnet.api.opcode;

/**
 * Koupí za netcoins další SDK pro exploit.
 */
public final class SdkBuyOpcode extends SdkOpcode {

    @Override
    public String getOpcodeValue() {
        return "100";
    }
}
