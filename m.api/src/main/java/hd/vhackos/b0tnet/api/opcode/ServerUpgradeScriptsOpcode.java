package hd.vhackos.b0tnet.api.opcode;

import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;
import hd.vhackos.b0tnet.shared.dto.ServerScriptType;

/**
 * Vylepší skripty na serveru.
 */
public final class ServerUpgradeScriptsOpcode extends Opcode {

    private static final String PARAM_TYPE = "type";

    /**
     * Nastaví typ skriptu.
     */
    public void setType(ServerScriptType scriptType) {
        addParam(PARAM_TYPE, String.valueOf(scriptType.getCode()));
    }

    @Override
    public OpcodeTargetType getTarget() {
        return OpcodeTargetType.SERVER;
    }

    @Override
    public String getOpcodeValue() {
        return "6868";
    }
}
