package hd.vhackos.b0tnet.api.net.response.data;

import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiBooleanConverter;

/**
 * Informace o skenované IP.
 */
public final class IpScanData {

    public static final String P_IP = "ip";
    public static final String P_LEVEL = "level";
    public static final String P_FIREWALL = "firewall";
    public static final String P_OPEN = "open";

    @AsciiRow("IP")
    @ResponseKey("ip")
    private String ip;

    @AsciiRow("Level")
    @ResponseKey("level")
    private Integer level;

    @AsciiRow("Firewall")
    @ResponseKey("fw")
    private Integer firewall;

    @AsciiRow(value = "Open", converter = AsciiBooleanConverter.class)
    @ResponseKey("open")
    private Integer open;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getFirewall() {
        return firewall;
    }

    public void setFirewall(Integer firewall) {
        this.firewall = firewall;
    }

    public Integer getOpen() {
        return open;
    }

    public void setOpen(Integer open) {
        this.open = open;
    }
}
