package hd.vhackos.b0tnet.api.opcode;

import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;

/**
 * Zahájí prolamování cílové banky.
 */
public final class BruteforceRemoteBankOpcode extends Opcode {

    private static final String PARAM_TARGET = "target";

    /**
     * Cílová IP.
     */
    public void setTargetIp(String ip) {
        addParam(PARAM_TARGET, ip);
    }

    @Override
    public OpcodeTargetType getTarget() {
        return OpcodeTargetType.START_BRUTEFORCE;
    }
}
