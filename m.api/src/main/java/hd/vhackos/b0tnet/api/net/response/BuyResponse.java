package hd.vhackos.b0tnet.api.net.response;

import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.base.Response;
import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.api.net.response.data.BuyItemData;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiBooleanConverter;

import java.util.List;

/**
 * Informace o obchodu za peníze nebo netcoins.
 */
public final class BuyResponse extends Response {

    public static final String P_INET_UPGRADED = "inetUpgraded";
    public static final String P_IP_CHANGE = "ipChange";
    public static final String P_BOUGHT = "bought";
    public static final String P_PURCHASE_LIMIT = "purchaseLimit";
    public static final String P_INET = "inet";
    public static final String P_INET_COSTS = "inetCosts";
    public static final String P_REQ_LEVEL_INET = "reqLevelInet";
    public static final String P_NEXT_INET = "nextInet";
    public static final String P_CAN = "can";
    public static final String P_CAN_MAX = "canMax";
    public static final String P_SKU_COUNT = "SKUCount";
    public static final String P_SKUS = "SKUs";
    public static final String P_ITEMS = "items";
    public static final String P_CAN_STARTER = "canStarter";
    public static final String P_PACK_BOUGHT = "packBought";
    public static final String P_MY_VTC = "myVtc";
    public static final String P_PS = "ps";

    @AsciiRow(value = "Inet Upgraded", converter = AsciiBooleanConverter.class)
    @ResponseKey("inetupgraded")
    private Integer inetUpgraded;

    @AsciiRow(value = "IP Change", converter = AsciiBooleanConverter.class)
    @ResponseKey("ipchange")
    private Integer ipChange;

    @AsciiRow(value = "Bought", converter = AsciiBooleanConverter.class)
    @ResponseKey("bought")
    private Integer bought;

    @AsciiRow("Purchase Limit")
    @ResponseKey("purchaselimit")
    private Integer purchaseLimit;

    @AsciiRow("Inet")
    @ResponseKey("inet")
    private Integer inet;

    @AsciiRow("Inet Costs")
    @ResponseKey("inetcosts")
    private Integer inetCosts;

    @AsciiRow("Required Level Inet")
    @ResponseKey("reqlevelinet")
    private Integer reqLevelInet;

    @AsciiRow("Next Inet")
    @ResponseKey("nextinet")
    private String nextInet;

    @AsciiRow("Can")
    @ResponseKey("can")
    private Integer can;

    @AsciiRow("Can Max")
    @ResponseKey("canmax")
    private Integer canMax;

    @AsciiRow("SKU Count")
    @ResponseKey("SKUCount")
    private Integer SKUCount;

    @AsciiRow("Pack Bought")
    @ResponseKey("packBought")
    private Integer packBought;

    @AsciiRow("My VTC")
    @ResponseKey("myVTC")
    private Integer myVtc;

    @AsciiRow(value = "Can Starter", converter = AsciiBooleanConverter.class)
    @ResponseKey("can_starter")
    private Integer canStarter;

    @AsciiRow("PS")
    @ResponseKey("ps")
    private Integer ps;

    @AsciiRow("SKUs")
    @ResponseKey("SKU")
    private List<String> SKUs;

    @AsciiRow("Items")
    @ResponseKey("item")
    private List<BuyItemData> items;

    public Integer getPs() {
        return ps;
    }

    public void setPs(Integer ps) {
        this.ps = ps;
    }

    public BuyResponse(OpcodeRequestResp response) {
        super(response);
    }

    public Integer getPackBought() {
        return packBought;
    }

    public void setPackBought(Integer packBought) {
        this.packBought = packBought;
    }

    public Integer getMyVtc() {
        return myVtc;
    }

    public void setMyVtc(Integer myVtc) {
        this.myVtc = myVtc;
    }

    public Integer getCanStarter() {
        return canStarter;
    }

    public void setCanStarter(Integer canStarter) {
        this.canStarter = canStarter;
    }

    public Integer getInetUpgraded() {
        return inetUpgraded;
    }

    public void setInetUpgraded(Integer inetUpgraded) {
        this.inetUpgraded = inetUpgraded;
    }

    public Integer getIpChange() {
        return ipChange;
    }

    public void setIpChange(Integer ipChange) {
        this.ipChange = ipChange;
    }

    public Integer getBought() {
        return bought;
    }

    public void setBought(Integer bought) {
        this.bought = bought;
    }

    public Integer getPurchaseLimit() {
        return purchaseLimit;
    }

    public void setPurchaseLimit(Integer purchaseLimit) {
        this.purchaseLimit = purchaseLimit;
    }

    public Integer getInet() {
        return inet;
    }

    public void setInet(Integer inet) {
        this.inet = inet;
    }

    public Integer getInetCosts() {
        return inetCosts;
    }

    public void setInetCosts(Integer inetCosts) {
        this.inetCosts = inetCosts;
    }

    public Integer getReqLevelInet() {
        return reqLevelInet;
    }

    public void setReqLevelInet(Integer reqLevelInet) {
        this.reqLevelInet = reqLevelInet;
    }

    public String getNextInet() {
        return nextInet;
    }

    public void setNextInet(String nextInet) {
        this.nextInet = nextInet;
    }

    public Integer getCan() {
        return can;
    }

    public void setCan(Integer can) {
        this.can = can;
    }

    public Integer getCanMax() {
        return canMax;
    }

    public void setCanMax(Integer canMax) {
        this.canMax = canMax;
    }

    public Integer getSKUCount() {
        return SKUCount;
    }

    public void setSKUCount(Integer SKUCount) {
        this.SKUCount = SKUCount;
    }

    public List<String> getSKUs() {
        return SKUs;
    }

    public void setSKUs(List<String> SKUs) {
        this.SKUs = SKUs;
    }

    public List<BuyItemData> getItems() {
        return items;
    }

    public void setItems(List<BuyItemData> items) {
        this.items = items;
    }
}
