package hd.vhackos.b0tnet.api.exception;

import hd.vhackos.b0tnet.shared.exception.B0tnetException;

/**
 * Server vrátil neplatný kód odpovědi.
 */
public final class InvalidResponseCodeException extends B0tnetException {

    private int responseCode;

    public InvalidResponseCodeException(int responseCode, String message) {
        super(null, message);
        this.responseCode = responseCode;
    }

    public int getResponseCode() {
        return responseCode;
    }
}
