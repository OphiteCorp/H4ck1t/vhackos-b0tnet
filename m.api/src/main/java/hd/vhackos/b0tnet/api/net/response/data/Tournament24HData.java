package hd.vhackos.b0tnet.api.net.response.data;

import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;

/**
 * Informace o položce v 24H turnaji v leaderboards.
 */
public final class Tournament24HData {

    public static final String P_USER = "user";
    public static final String P_EXP_GAIN = "expGain";
    public static final String P_LEVEL = "level";

    @AsciiRow("User")
    @ResponseKey("user")
    private String user;

    @AsciiRow("Experience Gain")
    @ResponseKey("expgain")
    private Long expGain;

    @AsciiRow("Level")
    @ResponseKey("level")
    private Integer level;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Long getExpGain() {
        return expGain;
    }

    public void setExpGain(Long expGain) {
        this.expGain = expGain;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}
