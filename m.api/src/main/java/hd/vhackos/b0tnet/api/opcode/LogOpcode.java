package hd.vhackos.b0tnet.api.opcode;

import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;

/**
 * Získá log uživatele.
 */
public final class LogOpcode extends Opcode {

    @Override
    public OpcodeTargetType getTarget() {
        return OpcodeTargetType.LOG;
    }
}
