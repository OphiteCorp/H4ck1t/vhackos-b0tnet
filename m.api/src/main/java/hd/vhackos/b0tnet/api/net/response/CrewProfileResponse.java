package hd.vhackos.b0tnet.api.net.response;

import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.base.Response;
import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiBooleanConverter;

/**
 * Informace o crew.
 */
public final class CrewProfileResponse extends Response {

    public static final String P_REQUESTED = "requested";
    public static final String P_CREW_NAME = "crewName";
    public static final String P_CREW_TAG = "crewTag";
    public static final String P_CREW_LOGO = "crewLogo";
    public static final String P_CREW_REPUTATION = "crewReputation";
    public static final String P_CREW_MEMBERS = "crewMembers";
    public static final String P_CAN_REQUEST = "canRequest";
    public static final String P_WON_CREW_TOURNAMENTS = "wonCrewTournaments";
    public static final String P_WON_RUSH_TOURNAMENTS = "wonRushTournaments";

    @AsciiRow(value = "Requested", converter = AsciiBooleanConverter.class)
    @ResponseKey("requested")
    private Integer requested;

    @AsciiRow("Name")
    @ResponseKey("crew_name")
    private String crewName;

    @AsciiRow("Tag")
    @ResponseKey("crew_tag")
    private String crewTag;

    @AsciiRow("Logo")
    @ResponseKey("crew_logo")
    private String crewLogo;

    @AsciiRow("Reputation")
    @ResponseKey("crew_rep")
    private Integer crewReputation;

    @AsciiRow("Members")
    @ResponseKey("crew_members")
    private Integer crewMembers;

    @AsciiRow("Won Crew Tournaments")
    @ResponseKey("won_crew_tournaments")
    private Integer wonCrewTournaments;

    @AsciiRow("Won Rush Tournaments")
    @ResponseKey("won_rush_tournaments")
    private Integer wonRushTournaments;

    @AsciiRow(value = "Can Request", converter = AsciiBooleanConverter.class)
    @ResponseKey("can_request")
    private Integer canRequest;

    public CrewProfileResponse(OpcodeRequestResp response) {
        super(response);
    }

    public Integer getWonCrewTournaments() {
        return wonCrewTournaments;
    }

    public void setWonCrewTournaments(Integer wonCrewTournaments) {
        this.wonCrewTournaments = wonCrewTournaments;
    }

    public Integer getWonRushTournaments() {
        return wonRushTournaments;
    }

    public void setWonRushTournaments(Integer wonRushTournaments) {
        this.wonRushTournaments = wonRushTournaments;
    }

    public Integer getRequested() {
        return requested;
    }

    public void setRequested(Integer requested) {
        this.requested = requested;
    }

    public String getCrewName() {
        return crewName;
    }

    public void setCrewName(String crewName) {
        this.crewName = crewName;
    }

    public String getCrewTag() {
        return crewTag;
    }

    public void setCrewTag(String crewTag) {
        this.crewTag = crewTag;
    }

    public String getCrewLogo() {
        return crewLogo;
    }

    public void setCrewLogo(String crewLogo) {
        this.crewLogo = crewLogo;
    }

    public Integer getCrewReputation() {
        return crewReputation;
    }

    public void setCrewReputation(Integer crewReputation) {
        this.crewReputation = crewReputation;
    }

    public Integer getCrewMembers() {
        return crewMembers;
    }

    public void setCrewMembers(Integer crewMembers) {
        this.crewMembers = crewMembers;
    }

    public Integer getCanRequest() {
        return canRequest;
    }

    public void setCanRequest(Integer canRequest) {
        this.canRequest = canRequest;
    }
}
