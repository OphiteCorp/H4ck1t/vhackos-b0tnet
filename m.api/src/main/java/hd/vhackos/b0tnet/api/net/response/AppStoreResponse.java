package hd.vhackos.b0tnet.api.net.response;

import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.base.Response;
import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.api.net.response.data.AppStoreData;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiBooleanConverter;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiMoneyConverter;

import java.util.List;

/**
 * Informace o aplikacích z obchodu.
 */
public final class AppStoreResponse extends Response {

    public static final String P_LEVEL_UP = "levelUp";
    public static final String P_UPDATED = "updated";
    public static final String P_INSTALLED = "installed";
    public static final String P_FILLED = "filled";
    public static final String P_LEVEL = "level";
    public static final String P_MONEY = "money";
    public static final String P_SPAM = "spam";
    public static final String P_APP_COUNT = "appCount";
    public static final String P_APPS = "apps";
    public static final String P_LEVEL_NEW = "levelNew";
    public static final String P_LEVEL_UP_REWARD = "levelUpReward";
    public static final String P_VIP = "vip";

    @AsciiRow(value = "Level Up", converter = AsciiBooleanConverter.class)
    @ResponseKey("lvlup")
    private Integer levelUp;

    @AsciiRow("Updated")
    @ResponseKey("updated")
    private Integer updated;

    @AsciiRow("Installed")
    @ResponseKey("installed")
    private Integer installed;

    @AsciiRow("Filled")
    @ResponseKey("filled")
    private Integer filled;

    @AsciiRow("Level")
    @ResponseKey("level")
    private Integer level;

    @AsciiRow(value = "Money", converter = AsciiMoneyConverter.class)
    @ResponseKey("money")
    private Long money;

    @AsciiRow("Spam")
    @ResponseKey("spam")
    private Integer spam;

    @AsciiRow("App Count")
    @ResponseKey("appCount")
    private Integer appCount;

    @AsciiRow("Level Up Reward")
    @ResponseKey("lvlupreward")
    private Integer levelUpReward;

    @AsciiRow("Level New")
    @ResponseKey("lvlnew")
    private Integer levelNew;

    @AsciiRow("VIP")
    @ResponseKey("vip")
    private Integer vip;

    @AsciiRow("Apps")
    @ResponseKey("apps")
    private List<AppStoreData> apps;

    public AppStoreResponse(OpcodeRequestResp response) {
        super(response);
    }

    public Integer getVip() {
        return vip;
    }

    public void setVip(Integer vip) {
        this.vip = vip;
    }

    public Integer getLevelUpReward() {
        return levelUpReward;
    }

    public void setLevelUpReward(Integer levelUpReward) {
        this.levelUpReward = levelUpReward;
    }

    public Integer getLevelNew() {
        return levelNew;
    }

    public void setLevelNew(Integer levelNew) {
        this.levelNew = levelNew;
    }

    public Integer getLevelUp() {
        return levelUp;
    }

    public void setLevelUp(Integer levelUp) {
        this.levelUp = levelUp;
    }

    public Integer getUpdated() {
        return updated;
    }

    public void setUpdated(Integer updated) {
        this.updated = updated;
    }

    public Integer getInstalled() {
        return installed;
    }

    public void setInstalled(Integer installed) {
        this.installed = installed;
    }

    public Integer getFilled() {
        return filled;
    }

    public void setFilled(Integer filled) {
        this.filled = filled;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Long getMoney() {
        return (money != null) ? money : 0;
    }

    public void setMoney(Long money) {
        this.money = money;
    }

    public Integer getSpam() {
        return spam;
    }

    public void setSpam(Integer spam) {
        this.spam = spam;
    }

    public Integer getAppCount() {
        return appCount;
    }

    public void setAppCount(Integer appCount) {
        this.appCount = appCount;
    }

    public List<AppStoreData> getApps() {
        return apps;
    }

    public void setApps(List<AppStoreData> apps) {
        this.apps = apps;
    }
}
