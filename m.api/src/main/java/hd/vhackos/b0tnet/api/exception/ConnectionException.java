package hd.vhackos.b0tnet.api.exception;

import hd.vhackos.b0tnet.shared.exception.B0tnetException;

/**
 * Nebylo možné se připojit k serveru.
 */
public final class ConnectionException extends B0tnetException {

    public ConnectionException(String message) {
        super(null, message);
    }

    public ConnectionException(String resultCode, String message, Throwable cause) {
        super(resultCode, message, cause);
    }
}
