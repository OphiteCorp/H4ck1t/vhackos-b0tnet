package hd.vhackos.b0tnet.api.opcode.base;

/**
 * Typ opcode.
 */
public enum OpcodeType {

    /**
     * Běžný požadavek na server.
     */
    STANDARD,
    /**
     * HTTP požadavek (je očekáván HTML výstup).
     */
    HTTP
}
