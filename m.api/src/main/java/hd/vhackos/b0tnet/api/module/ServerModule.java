package hd.vhackos.b0tnet.api.module;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.module.base.Module;
import hd.vhackos.b0tnet.api.module.base.ModuleHelper;
import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.ServerResponse;
import hd.vhackos.b0tnet.api.opcode.*;
import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.shared.dto.ServerNodeType;
import hd.vhackos.b0tnet.shared.dto.ServerScriptType;
import hd.vhackos.b0tnet.shared.injection.Inject;

import java.util.Collections;

/**
 * Správa serveru.
 */
@Inject
public final class ServerModule extends Module {

    protected ServerModule(IB0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Získá informace o serveru.
     */
    public synchronized ServerResponse getServer() {
        var opcode = new ServerOpcode();
        return createServerResponse(opcode);
    }

    /**
     * Přidá node pro antivirus.
     */
    public synchronized ServerResponse addAntivirusNode() {
        var opcode = new ServerAddAvNodeOpcode();
        return createServerResponse(opcode);
    }

    /**
     * Přidá node pro firewall.
     */
    public synchronized ServerResponse addFirewallNode() {
        var opcode = new ServerAddFwNodeOpcode();
        return createServerResponse(opcode);
    }

    /**
     * Aktualizuje node 1x.
     */
    public synchronized ServerResponse updateNode1x(ServerNodeType nodeType, int number) {
        var opcode = new ServerUpdateNode1xOpcode();
        opcode.setNodeType(nodeType);
        opcode.setNodeNumber(number);

        return createServerResponse(opcode);
    }

    /**
     * Aktualizuje node 5x.
     */
    public synchronized ServerResponse updateNode5x(ServerNodeType nodeType, int number) {
        var opcode = new ServerUpdateNode5xOpcode();
        opcode.setNodeType(nodeType);
        opcode.setNodeNumber(number);

        return createServerResponse(opcode);
    }

    /**
     * Aktualizuje node na max.
     */
    public synchronized ServerResponse updateNodeToMax(ServerNodeType nodeType, int number) {
        var opcode = new ServerUpdateNodeToMaxOpcode();
        opcode.setNodeType(nodeType);
        opcode.setNodeNumber(number);

        return createServerResponse(opcode);
    }

    /**
     * Zakoupí 1 skript.
     */
    public synchronized ServerResponse buy1Script(ServerScriptType scriptType) {
        var opcode = new ServerBuyScriptsOpcode();
        opcode.setType(scriptType);
        opcode.setCount(1);

        return createServerResponse(opcode);
    }

    /**
     * Zakoupí 10 skriptů.
     */
    public synchronized ServerResponse buy10Scripts(ServerScriptType scriptType) {
        var opcode = new ServerBuyScriptsOpcode();
        opcode.setType(scriptType);
        opcode.setCount(10);

        return createServerResponse(opcode);
    }

    /**
     * Vylepší skripty na serveru.
     */
    public synchronized ServerResponse upgradeScripts(ServerScriptType scriptType) {
        var opcode = new ServerUpgradeScriptsOpcode();
        opcode.setType(scriptType);

        return createServerResponse(opcode);
    }

    /**
     * Otevře 1 balíček.
     */
    public synchronized ServerResponse openPackage() {
        var opcode = new ServerOpenPackageOpcode();
        return createServerResponse(opcode);
    }

    /**
     * Otevře všechny balíčky.
     */
    public synchronized ServerResponse openAllPackages() {
        var opcode = new ServerOpenAllPackagesOpcode();
        return createServerResponse(opcode);
    }

    /**
     * Otevře 1 bronzový balíček.
     */
    public synchronized ServerResponse openBronzePack() {
        var opcode = new ServerOpenBronzePackOpcode();
        return createServerResponse(opcode);
    }

    /**
     * Koupí N(1-10) balíčků.
     */
    public synchronized ServerResponse buyPackages(int count) {
        var opcode = new ServerBuyPackageOpcode();
        opcode.setCount(count);

        return createServerResponse(opcode);
    }

    private ServerResponse createServerResponse(Opcode opcode) {
        OpcodeRequestResp response = sendRequest(opcode);
        var responseMap = response.getResponse();
        var dto = new ServerResponse(response);

        ModuleHelper.checkResponseIntegrity(responseMap, ServerResponse.class);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_AV_ADDED);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_FW_ADDED);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_NODE_UPDATED);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SPACK_OPEN);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SPACK_OPEN_ALL);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_BOUGHT_ONE);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_BOUGHT_TEN);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_PACKS_BOUGHT);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_BOUGHT_LIMIT);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SERVER_PIECES);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_AV1_PIECES);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_AV2_PIECES);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_AV3_PIECES);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_FW1_PIECES);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_FW2_PIECES);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_FW3_PIECES);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SERVER_FREE_PIECES);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_AV_FREE_PIECES);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_FW_FREE_PIECES);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_PACKS);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SERVER_MAX_PIECES);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SERVER_STARS);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_AV1_MAX_PIECES);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_AV1_STARS);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_AV2_MAX_PIECES);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_AV2_STARS);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_AV3_MAX_PIECES);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_AV3_STARS);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_FW1_MAX_PIECES);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_FW1_STARS);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_FW2_MAX_PIECES);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_FW2_STARS);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_FW3_MAX_PIECES);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_FW3_STARS);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_AV_NODES);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_FW_NODES);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_NEXT_PACK_IN);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_NODE_TYPE);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_NODE_NUMBER);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SPACK_TYPE);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SHOW_MUCH);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SERVER);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_AV);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_FW);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_BOOST);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_BYPASS_COSTS);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_BYPASS_CREATED);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_BYPASS_SCRIPTS);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_BYPASS_STRENGTH);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_BYPASS_UPGRADE_COSTS);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_BYPASS_UPGRADED);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_BYPASS_VERSION);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_BYPASS_ANZ);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SHUTDOWN_COSTS);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SHUTDOWN_CREATED);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SHUTDOWN_SCRIPTS);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SHUTDOWN_STRENGTH);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SHUTDOWN_UPGRADE_COSTS);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SHUTDOWN_UPGRADED);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SHUTDOWN_VERSION);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SHUTDOWN_ANZ);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SMASH_COSTS);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SMASH_CREATED);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SMASH_SCRIPTS);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SMASH_STRENGTH);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SMASH_UPGRADE_COSTS);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SMASH_UPGRADED);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SMASH_VERSION);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_SMASH_ANZ);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_FRAGS);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_S_FRAGS);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_HEIST);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_CAN_1ON1);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_MY_REPUTATION);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_WIN_RATE);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_S_BRONZE_OPEN);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_BRONZE_PACKS);
        ModuleHelper.setField(responseMap, dto, ServerResponse.P_MY_LEAGUE);

        if (!ModuleHelper.setField(responseMap, dto, ServerResponse.P_PVP_BOOSTS,
                (f, data) -> ModuleHelper.convertToServerPvPBoostData(data))) {
            dto.setPvpBoosts(Collections.emptyList());
        }
        return dto;
    }
}
