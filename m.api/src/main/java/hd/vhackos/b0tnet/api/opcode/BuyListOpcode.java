package hd.vhackos.b0tnet.api.opcode;

import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;

/**
 * Získá seznam položek ke koupi.
 */
public final class BuyListOpcode extends Opcode {

    private static final String PARAM_INFO = "info";

    public BuyListOpcode() {
        addParam(PARAM_INFO, "1");
    }

    @Override
    public OpcodeTargetType getTarget() {
        return OpcodeTargetType.BUY;
    }
}
