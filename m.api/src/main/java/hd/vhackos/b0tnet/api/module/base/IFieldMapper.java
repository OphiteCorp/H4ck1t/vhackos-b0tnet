package hd.vhackos.b0tnet.api.module.base;

import java.lang.reflect.Field;

/**
 * Rozhraní definující vlastní implementaci pro přemapování požadavku odpovědi na jiný typ než primitivní.
 */
public interface IFieldMapper {

    /**
     * Konvertuje data odpovědi na daný typ fieldu.
     */
    Object convert(Field field, Object responseData);
}
