package hd.vhackos.b0tnet.api.opcode;

/**
 * Koupí menu aplikaci v obchodu.
 */
public final class BuyMenuAppOpcode extends BuyAppOpcode {

    @Override
    public String getOpcodeValue() {
        return "200";
    }
}
