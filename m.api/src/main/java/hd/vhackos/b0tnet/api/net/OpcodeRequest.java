package hd.vhackos.b0tnet.api.net;

import com.google.gson.Gson;
import hd.vhackos.b0tnet.api.exception.*;
import hd.vhackos.b0tnet.api.module.base.IModule;
import hd.vhackos.b0tnet.api.opcode.base.IOpcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;
import hd.vhackos.b0tnet.shared.exception.InvalidAccessTokenException;
import hd.vhackos.b0tnet.shared.json.Json;
import hd.vhackos.b0tnet.shared.utils.HashUtils;
import hd.vhackos.b0tnet.shared.utils.SentryGuard;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * Vytvoří request pro opcode.
 */
public final class OpcodeRequest extends AbstractOpcodeRequest<OpcodeRequestResp> {

    private static final Gson GSON = new Gson();

    /**
     * Vytvoří novou instanci.
     */
    OpcodeRequest(IOpcode opcode) {
        super(opcode);
    }

    /**
     * Získá absolutní URI včetně parametrů.
     *
     * @param target   Cíl adresy na kterou se má komunikovat.
     * @param userData Data uživatele (v base64).
     * @param pass     Ověřovací hash v md5.
     * @param api      Verze herního API.
     */
    private static String getAbsoluteUri(OpcodeTargetType target, String userData, String pass, int api) {
        final var restUri = VHACK_URL + api;
        return String.format("%s/%s.php?user=%s&pass=%s", restUri, target.getCode(), userData, pass);
    }

    @Override
    public synchronized OpcodeRequestResp sendOpcode(IModule module, Map<String, String> params) {
        // obsahuje JSON všech parametru
        var json = GSON.toJson(params);
        var jsonBytes = json.getBytes();
        // slouží jako uživatelské jméno
        var jsonBase64 = HashUtils.toBase64(jsonBytes, 0, jsonBytes.length, HashUtils.WEBSAFE_ALPHABET, false);
        // vygeneruje ověřovací parametr "pass"
        var pass = HashUtils.toHexMd5(json + json + HashUtils.toHexMd5(json));
        // vygeneruje absolutní URI
        var api = module.getB0tnet().getConfig().getGameApi();
        var uri = getAbsoluteUri(opcode.getTarget(), jsonBase64, pass, api);

        // připravý výstup
        var opcodeResult = new OpcodeRequestResp(opcode.getTarget());
        opcodeResult.setUser(jsonBase64);
        opcodeResult.setPass(pass);
        opcodeResult.setApi(api);
        opcodeResult.setTarget(opcode.getTarget().getCode());
        opcodeResult.setParameters(params);
        opcodeResult.setRawUri(uri);

        HttpsURLConnection conn = null;
        Map responseMap;

        getLog().trace("Target URI: {}", uri);

        try {
            var config = module.getB0tnet().getConfig();
            conn = createConnection(uri, getUserAgent(module), config.getProxyData(), config.getConnectionTimeout());
            var responseCode = conn.getResponseCode();

            if (responseCode == 200) {
                String response;

                try (var baos = new ByteArrayOutputStream()) {
                    var is = new BufferedInputStream(conn.getInputStream());

                    for (var reader = is.read(); reader != -1; reader = is.read()) {
                        baos.write(reader);
                    }
                    response = baos.toString(StandardCharsets.UTF_8);
                    getLog().debug("Response for opcode '{}->{}' from URI '{}' is: {}", opcode.getTarget(),
                            opcode.getOpcodeValue(), uri, response);
                    responseMap = Json.toMap(response);

                    // v některých případech může být null (např. smazání neexistujícího účtu)
                    if (responseMap == null) {
                        responseMap = new HashMap<>();
                        responseMap.put(null, null);

                        // z nějakého důvodu vrací response místo mapy číselnou hodnotu
                    } else if (responseMap.size() == 1 && responseMap.containsKey(null)) {
                        var value = responseMap.get(null);
                        getLog().debug("Strange server response. Value: {}", value);
                    }
                    if (responseMap.containsKey("result")) {
                        var result = responseMap.get("result");

                        // null může nastat a v takovém případě je to v pořádku
                        if (result != null) {
                            // může se stát, že se vrátí LinkedTreeMap místo stringu (osobně s emi to nestalo)
                            if (!(result instanceof String)) {
                                throw new InvalidResponseException(result,
                                        "The server returned a response that the bot can not process");
                            }
                            switch ((String) result) {
                                case "10":
                                    throw new InvalidRequestException("10",
                                            "The server returned error 10, bot is outdated. Try to increase the game API in the configuration or wait for the next bot update.");

                                case "36":
                                    throw new InvalidAccessTokenException("36",
                                            "Token is invalid, you must sign in again to get a new one");

                                case "0":
                                    getLog().debug("Returns 0 - OK");
                                    break;

                                default:
                                    getLog().debug("Returns {}", result);
                                    break;
                            }
                        } else {
                            getLog().debug("Returns NULL - OK");
                        }
                    }
                }
            } else if (responseCode == 503) {
                throw new ConnectionException("Remote service is unavailable: URI: " + uri);

            } else {
                throw new InvalidResponseCodeException(responseCode,
                        "Failed to get response from vHackOS " + responseCode + " {" + uri + "}");
            }
        } catch (NoRouteToHostException e) {
            throw new ConnectionException(null, "Could not connect to a remote game server. URI: " + uri, e);

        } catch (UnknownHostException e) {
            throw new ConnectionException(null, "Target server is not available. URI: " + uri, e);

        } catch (SocketTimeoutException e) {
            throw new ConnectionException(null, "Response timeout expired. URI: " + uri, e);

        } catch (BindException e) {
            throw new ConnectionBindException("Target server is not available. URI: " + uri, e);

        } catch (IOException e) {
            if (!(e instanceof ConnectException)) {
                SentryGuard.log(e);
                getLog().debug("Sending a request to the server failed", e);
                getLog().error("Something is wrong with processing the request. URI: " + uri);
            }
            throw new ConnectionException(null, "An error occurred while processing request. URI: " + uri, e);
        } finally {
            if (conn != null) {
                getLog().debug("Closing connection for opcode: {}->{}", opcode.getTarget(), opcode.getOpcodeValue());
                conn.disconnect();
            }
        }
        opcodeResult.setResponse(responseMap);
        return opcodeResult;
    }
}
