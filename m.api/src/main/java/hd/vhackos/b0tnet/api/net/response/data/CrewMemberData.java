package hd.vhackos.b0tnet.api.net.response.data;

import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.api.net.response.data.interfaces.ICrewData;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiCrewPositionTypeConverter;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiElapsedSecondsTimeConverter;

/**
 * Informace o členu v crew.
 */
public final class CrewMemberData implements ICrewData {

    public static final String P_USER_ID = "userId";
    public static final String P_POSITION = "position";
    public static final String P_USER_NAME = "userName";
    public static final String P_LEVEL = "level";
    public static final String P_LAST_ONLINE = "lastOnline";
    public static final String P_REPEARNED = "repearned";
    public static final String P_RUSHED = "rushed";

    @AsciiRow("User ID")
    @ResponseKey("user_id")
    private Long userId;

    @AsciiRow(value = "Position", converter = AsciiCrewPositionTypeConverter.class)
    @ResponseKey("position")
    private Integer position;

    @AsciiRow("User")
    @ResponseKey("username")
    private String userName;

    @AsciiRow("Level")
    @ResponseKey("level")
    private Integer level;

    @AsciiRow("Repearned")
    @ResponseKey("repearned")
    private Integer repearned;

    @AsciiRow(value = "Last Online", converter = AsciiElapsedSecondsTimeConverter.class)
    @ResponseKey("lastOnline")
    private Long lastOnline;

    @AsciiRow("Rushed")
    @ResponseKey("rushed")
    private Integer rushed;

    public Integer getRushed() {
        return rushed;
    }

    public void setRushed(Integer rushed) {
        this.rushed = rushed;
    }

    public Integer getRepearned() {
        return repearned;
    }

    public void setRepearned(Integer repearned) {
        this.repearned = repearned;
    }

    @Override
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    @Override
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Override
    public Long getLastOnline() {
        return lastOnline;
    }

    public void setLastOnline(Long lastOnline) {
        this.lastOnline = lastOnline;
    }
}
