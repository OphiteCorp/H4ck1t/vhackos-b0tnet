package hd.vhackos.b0tnet.api.net;

import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeType;
import org.jsoup.nodes.Document;

import java.util.Map;

/**
 * Odpověď z opcode HTTP requestu.
 */
public final class OpcodeHttpRequestResp implements IOpcodeRequestResp {

    private String rawUri;
    private int api;
    private Map<String, String> parameters;
    private Document document;

    private transient final OpcodeTargetType opcodeTargetType;

    /**
     * Vytvoří novou instanci.
     */
    OpcodeHttpRequestResp(OpcodeTargetType opcodeTargetType) {
        this.opcodeTargetType = opcodeTargetType;
    }

    @Override
    public final OpcodeType getOpcodeType() {
        return OpcodeType.HTTP;
    }

    @Override
    public final OpcodeTargetType getOpcodeTargetType() {
        return opcodeTargetType;
    }

    @Override
    public String getRawUri() {
        return rawUri;
    }

    void setRawUri(String rawUri) {
        this.rawUri = rawUri;
    }

    @Override
    public int getApi() {
        return api;
    }

    public void setApi(int api) {
        this.api = api;
    }

    public Document getDocument() {
        return document;
    }

    void setDocument(Document document) {
        this.document = document;
    }

    @Override
    public Map<String, String> getParameters() {
        return parameters;
    }

    void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }
}
