package hd.vhackos.b0tnet.api.module;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.exception.CrewNotExistsException;
import hd.vhackos.b0tnet.api.exception.WithoutCrewException;
import hd.vhackos.b0tnet.api.module.base.Module;
import hd.vhackos.b0tnet.api.module.base.ModuleHelper;
import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.CrewProfileResponse;
import hd.vhackos.b0tnet.api.net.response.CrewResponse;
import hd.vhackos.b0tnet.api.opcode.*;
import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.shared.utils.SharedUtils;

import java.util.Collections;

/**
 * Správa crew.
 */
@Inject
public final class CrewModule extends Module {

    private static final String ERR_CODE_CREW_NOT_EXISTS = "1";

    protected CrewModule(IB0tnet b0tnet) {
        super(b0tnet);
    }

    private static void checkUserInAnyCrew(CrewResponse resp) {
        if (!SharedUtils.toBoolean(resp.getIsMember())) {
            throw new WithoutCrewException(null, "You are not in any crew");
        }
    }

    /**
     * Získá profil crew.
     */
    public synchronized CrewProfileResponse getProfile(long crewId) {
        var opcode = new CrewProfileOpcode();
        opcode.setCrewId(crewId);

        OpcodeRequestResp response = sendRequest(opcode);
        var responseMap = response.getResponse();
        var result = getResultValue(responseMap);

        if (ERR_CODE_CREW_NOT_EXISTS.equals(result)) {
            throw new CrewNotExistsException(result, "Crew with ID '" + crewId + "' does not exist");
        }
        var dto = new CrewProfileResponse(response);

        ModuleHelper.checkResponseIntegrity(responseMap, CrewProfileResponse.class);
        ModuleHelper.setField(responseMap, dto, CrewProfileResponse.P_REQUESTED);
        ModuleHelper.setField(responseMap, dto, CrewProfileResponse.P_CREW_NAME);
        ModuleHelper.setField(responseMap, dto, CrewProfileResponse.P_CREW_TAG);
        ModuleHelper.setField(responseMap, dto, CrewProfileResponse.P_CREW_LOGO);
        ModuleHelper.setField(responseMap, dto, CrewProfileResponse.P_CREW_REPUTATION);
        ModuleHelper.setField(responseMap, dto, CrewProfileResponse.P_CREW_MEMBERS);
        ModuleHelper.setField(responseMap, dto, CrewProfileResponse.P_CAN_REQUEST);
        ModuleHelper.setField(responseMap, dto, CrewProfileResponse.P_WON_CREW_TOURNAMENTS);
        ModuleHelper.setField(responseMap, dto, CrewProfileResponse.P_WON_RUSH_TOURNAMENTS);
        return dto;
    }

    /**
     * Získá informace o aktuální crew.
     */
    public synchronized CrewResponse getCrew() {
        var opcode = new CrewOpcode();
        var resp = createCrewResponse(opcode);

        checkUserInAnyCrew(resp);
        return resp;
    }

    /**
     * Odešle crew zprávu.
     */
    public synchronized CrewResponse sendMessage(String message) {
        var opcode = new CrewSendMessageOpcode();
        var resp = createCrewResponse(opcode);

        checkUserInAnyCrew(resp);
        return resp;
    }

    /**
     * Opustí crew.
     */
    public synchronized CrewResponse leaveCrew() {
        var opcode = new CrewLeaveOpcode();
        var resp = createCrewResponse(opcode);

        checkUserInAnyCrew(resp);
        return resp;
    }

    /**
     * Odešle žádost do crew.
     */
    public synchronized boolean sendRequest(String crewTag) {
        var opcode = new CrewSendRequestOpcode();
        opcode.setCrewTag(crewTag);

        OpcodeRequestResp response = sendRequest(opcode);
        var responseMap = response.getResponse();

        if (responseMap.size() == 1) {
            return false;
        } else {
            var resp = createCrewResponse(opcode);
            return SharedUtils.toBoolean(resp.getRequested());
        }
    }

    private CrewResponse createCrewResponse(Opcode opcode) {
        OpcodeRequestResp response = sendRequest(opcode);
        var responseMap = response.getResponse();
        var dto = new CrewResponse(response);

        ModuleHelper.checkResponseIntegrity(responseMap, CrewResponse.class);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_CREATED);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_REQUESTED);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_ACCEPTED);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_SENT);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_CHANGE_LOGO);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_PROMOTE);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_DEMOTE);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_LEAVE);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_KICKED);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_CREW_MESSAGES);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_MY_POSITION);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_CREW_NAME);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_CREW_TAG);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_IS_MEMBER);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_CREW_ID);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_CREW_LOGO);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_CREW_OWNER);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_CREW_MEMBERS_COUNT);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_CREW_MEMBERS);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_CREW_REPUTATION);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_CREW_RANK);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_REQ_COUNT);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_CREW_NEW_MESSAGES);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_MESSAGE);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_TIME);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_USER_ID);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_USER_NAME);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_DEFACE_COUNT);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_RUSH_ACTIVE);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_RUSH_LEFT);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_RUSH_TARGET);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_RUSH_CREW_SCORE);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_RUSH_WON);
        ModuleHelper.setField(responseMap, dto, CrewResponse.P_RUSH_RANK);

        if (!ModuleHelper.setField(responseMap, dto, CrewResponse.P_MEMBERS,
                (f, data) -> ModuleHelper.convertToCrewMemberData(data))) {
            dto.setMembers(Collections.emptyList());
        }
        if (!ModuleHelper.setField(responseMap, dto, CrewResponse.P_MESSAGES,
                (f, data) -> ModuleHelper.convertToCrewMessageData(data))) {
            dto.setMessages(Collections.emptyList());
        }
        if (!ModuleHelper.setField(responseMap, dto, CrewResponse.P_REQUESTS,
                (f, data) -> ModuleHelper.convertToCrewRequestsData(data))) {
            dto.setRequests(Collections.emptyList());
        }
        if (!ModuleHelper.setField(responseMap, dto, CrewResponse.P_DEFACES,
                (f, data) -> ModuleHelper.convertToDefaceData(data))) {
            dto.setDefaces(Collections.emptyList());
        }
        if (!ModuleHelper.setField(responseMap, dto, CrewResponse.P_RUSHES,
                (f, data) -> ModuleHelper.convertToCrewRushData(data))) {
            dto.setDefaces(Collections.emptyList());
        }
        return dto;
    }
}
