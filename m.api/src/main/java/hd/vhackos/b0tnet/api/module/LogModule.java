package hd.vhackos.b0tnet.api.module;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.exception.RemoteException;
import hd.vhackos.b0tnet.api.module.base.Module;
import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.opcode.LogOpcode;
import hd.vhackos.b0tnet.api.opcode.RemoteLogOpcode;
import hd.vhackos.b0tnet.api.opcode.UpdateLogOpcode;
import hd.vhackos.b0tnet.api.opcode.UpdateRemoteLogOpcode;
import hd.vhackos.b0tnet.shared.exception.B0tnetException;
import hd.vhackos.b0tnet.shared.injection.Inject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Modul pro logování.
 */
@Inject
public class LogModule extends Module {

    private static final String OK_CODE_LOG_SET = "2";
    private static final String ERR_CODE_IP_NOT_AVAILABLE = "22";

    protected LogModule(IB0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Získá vlastní log.
     */
    public synchronized List<String> getLog() {
        var opcode = new LogOpcode();
        OpcodeRequestResp response = sendRequest(opcode);
        var innerResponse = response.getResponse();
        var data = getLogInternal(innerResponse);
        return toLogList(data);
    }

    /**
     * Nastaví vlastní log.
     */
    public synchronized void setLog(String log) {
        var opcode = new UpdateLogOpcode();
        opcode.setLog(fixLog(log));

        try {
            sendRequest(opcode);

        } catch (B0tnetException e) {
            if (!OK_CODE_LOG_SET.equals(e.getResultCode())) {
                throw e;
            }
        }
    }

    /**
     * Získá obsah vzdáleného logu IP. Vrací řádky v logu.
     */
    public synchronized List<String> getRemoteLog(String ip) {
        var opcode = new RemoteLogOpcode();
        opcode.setTargetIp(ip);

        OpcodeRequestResp response = sendRequest(opcode);
        var innerResponse = response.getResponse();
        var result = getResultValue(innerResponse);

        if (result == null || result.equals(ERR_CODE_IP_NOT_AVAILABLE)) {
            throw new RemoteException(result, "Target IP '" + ip + "' is not available or broken");
        }
        var data = getLogInternal(innerResponse);
        return toLogList(data);
    }

    /**
     * Nastaví obsah vzdáleného logu IP.
     */
    public synchronized void setRemoteLog(String ip, String log) {
        var opcode = new UpdateRemoteLogOpcode();
        opcode.setTargetIp(ip);
        opcode.setLog(fixLog(log));

        try {
            OpcodeRequestResp response = sendRequest(opcode);
            var innerResponse = response.getResponse();
            var result = getResultValue(innerResponse);

            if (result == null || result.equals(ERR_CODE_IP_NOT_AVAILABLE)) {
                throw new RemoteException(result, "Target IP '" + ip + "' is not available or broken");
            }
        } catch (B0tnetException e) {
            if (!OK_CODE_LOG_SET.equals(e.getResultCode())) {
                throw e;
            }
        }
    }

    private List<String> toLogList(String logs) {
        getLogger().debug("Get log message: {}", logs);

        var data = logs.split("\n");
        var list = new ArrayList<String>(data.length);

        for (var line : data) {
            // nastaví mezery na nezalomitelné znaky, protože v logu můžou být i ASCII obrazce
            var fixedLine = line.replaceAll(" ", " ");
            list.add(fixedLine);
            getLogger().debug(fixedLine);
        }
        return list;
    }

    private String fixLog(String log) {
        getLogger().debug("Set log message1: {}", log);
        log = log.replaceAll("<br>", "\n");
        //log = log.replaceAll(" ", " ");
        getLogger().debug("Set log message2: {}", log);
        return log;
    }

    private static String getLogInternal(Map<String, Object> response) {
        if (response != null) {
            var logs = response.get("logs");
            return (logs != null) ? logs.toString() : "";
        }
        return "";
    }
}
