package hd.vhackos.b0tnet.api.module;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.exception.InvalidRequestException;
import hd.vhackos.b0tnet.api.module.base.Module;
import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.opcode.NotepadOpcode;
import hd.vhackos.b0tnet.api.opcode.UpdateNotepadOpcode;
import hd.vhackos.b0tnet.shared.exception.B0tnetException;
import hd.vhackos.b0tnet.shared.exception.InvalidAccessTokenException;
import hd.vhackos.b0tnet.shared.injection.Inject;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Spravuje poznámkový blok.
 */
@Inject
public final class NotepadModule extends Module {

    private static final String OK_CODE_NOTEPAD_SET = "2";
    private static final String ERR_CODE_NOTEPAD_INVALID_TOKEN = "1";
    private static final String ERR_CODE_SET_NOTEPAD_INVALID_COMMAND = "1";

    protected NotepadModule(IB0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Získání obsahu poznámkového bloku.
     */
    public synchronized List<String> getNotepad() {
        var opcode = new NotepadOpcode();
        OpcodeRequestResp response = sendRequest(opcode);
        var innerResponse = response.getResponse();
        var result = getResultValue(innerResponse);

        if (ERR_CODE_NOTEPAD_INVALID_TOKEN.equals(result)) {
            throw new InvalidAccessTokenException(result, "Your access token is no longer valid, try signing in again");
        }
        if (innerResponse.containsKey("notepad")) {
            var notepad = innerResponse.get("notepad").toString().split("\n");
            return new ArrayList<>(Arrays.asList(notepad));
        } else {
            return Collections.emptyList();
        }
    }

    /**
     * Nastaví data do poznámkového bloku.
     */
    public synchronized void setNotepad(List<String> lines) {
        var linesStr = StringUtils.join(lines, "\\n");
        linesStr = linesStr.replaceAll("\\\\n", "\n");

        var opcode = new UpdateNotepadOpcode();
        opcode.setNotepad(linesStr);

        try {
            OpcodeRequestResp response = sendRequest(opcode);
            var innerResponse = response.getResponse();
            var result = getResultValue(innerResponse);

            if (ERR_CODE_SET_NOTEPAD_INVALID_COMMAND.equals(result)) {
                throw new InvalidRequestException(result, "The command contains invalid values in the parameter");
            }
        } catch (B0tnetException e) {
            if (OK_CODE_NOTEPAD_SET.equals(e.getResultCode())) {
                // nic, poznámkový blok byl uložen
                return;
            }
            throw e;
        }
    }
}
