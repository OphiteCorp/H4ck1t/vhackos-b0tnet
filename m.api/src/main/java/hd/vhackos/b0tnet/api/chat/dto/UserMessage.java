package hd.vhackos.b0tnet.api.chat.dto;

/**
 * Zpráva uživatele v chatu.
 */
public final class UserMessage extends AbstractMessage {

    private String channel;
    private String toUser;
    private String message;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
