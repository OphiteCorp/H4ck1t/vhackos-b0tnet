package hd.vhackos.b0tnet.api.chat.dto;

/**
 * Základní informace o každé zprávě.
 */
public abstract class AbstractMessage {

    private String user;
    private int uid;
    private String host;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
