package hd.vhackos.b0tnet.api.module;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.module.base.Module;
import hd.vhackos.b0tnet.api.net.OpcodeHttpRequestResp;
import hd.vhackos.b0tnet.api.opcode.WheelEventOpcode;
import hd.vhackos.b0tnet.shared.injection.Inject;

/**
 * Správe různých událostí ve hře.
 */
@Inject
public final class EventModule extends Module {

    protected EventModule(IB0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Zatočí kolem štěstí.
     */
    public synchronized String spinLuckyWheel() {
        var opcode = new WheelEventOpcode();
        opcode.setUserUid(getB0tnet().getConnectionData().getUid());
        opcode.setAccessToken(getB0tnet().getConnectionData().getAccessToken());

        OpcodeHttpRequestResp response = sendRequest(opcode);

        if (response.getDocument() != null) {
            var elem = response.getDocument().select("#prize");
            return (elem != null) ? elem.text() : "NULL";
        }
        return "NULL";
    }
}
