package hd.vhackos.b0tnet.api.net.response;

import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.base.Response;
import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiBooleanConverter;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiElapsedSecondsTimeConverter;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiMinerStateConverter;

/**
 * Informace o netcoin mineru.
 */
public final class MiningResponse extends Response {

    public static final String P_UPGRADED = "upgraded";
    public static final String P_RUNNING = "running";
    public static final String P_APPLIED = "applied";
    public static final String P_CLAIMED = "claimed";
    public static final String P_STARTED = "started";
    public static final String P_MINED = "mined";
    public static final String P_NETCOINS = "netCoins";
    public static final String P_GPU_COUNT = "gpuCount";
    public static final String P_UPGRADABLE = "upgradable";
    public static final String P_MINING_SPEED = "miningSpeed";
    public static final String P_NEW_GPU_COSTS = "newGpuCosts";
    public static final String P_IS = "is";
    public static final String P_LEFT = "left";
    public static final String P_NEED = "need";
    public static final String P_WILL_EARN = "willEarn";

    @AsciiRow(value = "Upgraded", converter = AsciiBooleanConverter.class)
    @ResponseKey("upgraded")
    private Integer upgraded;

    @AsciiRow(value = "Running", converter = AsciiMinerStateConverter.class)
    @ResponseKey("running")
    private Integer running;

    @AsciiRow(value = "Applied", converter = AsciiBooleanConverter.class)
    @ResponseKey("applied")
    private Integer applied;

    @AsciiRow(value = "Claimed", converter = AsciiBooleanConverter.class)
    @ResponseKey("claimed")
    private Integer claimed;

    @AsciiRow(value = "Started", converter = AsciiBooleanConverter.class)
    @ResponseKey("started")
    private Integer started;

    @AsciiRow("Mined")
    @ResponseKey("mined")
    private Integer mined;

    @AsciiRow("NetCoins")
    @ResponseKey("netcoins")
    private Integer netCoins;

    @AsciiRow("GPU Count")
    @ResponseKey("gpuCount")
    private Integer gpuCount;

    @AsciiRow(value = "Upgradable", converter = AsciiBooleanConverter.class)
    @ResponseKey("upgradable")
    private Integer upgradable;

    @AsciiRow("Mining Speed")
    @ResponseKey("miningSpeed")
    private Double miningSpeed;

    @AsciiRow("New GPU Costs")
    @ResponseKey("newGPUCosts")
    private Integer newGpuCosts;

    @AsciiRow(value = "Elapsed", converter = AsciiElapsedSecondsTimeConverter.class)
    @ResponseKey("is")
    private Long is;

    @AsciiRow(value = "Left", converter = AsciiElapsedSecondsTimeConverter.class)
    @ResponseKey("left")
    private Long left;

    @AsciiRow(value = "Need", converter = AsciiElapsedSecondsTimeConverter.class)
    @ResponseKey("need")
    private Long need;

    @AsciiRow("Will Earn")
    @ResponseKey("willearn")
    private Integer willEarn;

    public MiningResponse(OpcodeRequestResp response) {
        super(response);
    }

    public Long getIs() {
        return is;
    }

    public void setIs(Long is) {
        this.is = is;
    }

    public Long getLeft() {
        return left;
    }

    public void setLeft(Long left) {
        this.left = left;
    }

    public Long getNeed() {
        return need;
    }

    public void setNeed(Long need) {
        this.need = need;
    }

    public Integer getWillEarn() {
        return willEarn;
    }

    public void setWillEarn(Integer willEarn) {
        this.willEarn = willEarn;
    }

    public Integer getNewGpuCosts() {
        return newGpuCosts;
    }

    public void setNewGpuCosts(Integer newGpuCosts) {
        this.newGpuCosts = newGpuCosts;
    }

    public Integer getUpgraded() {
        return upgraded;
    }

    public void setUpgraded(Integer upgraded) {
        this.upgraded = upgraded;
    }

    public Integer getRunning() {
        return running;
    }

    public void setRunning(Integer running) {
        this.running = running;
    }

    public Integer getApplied() {
        return applied;
    }

    public void setApplied(Integer applied) {
        this.applied = applied;
    }

    public Integer getClaimed() {
        return claimed;
    }

    public void setClaimed(Integer claimed) {
        this.claimed = claimed;
    }

    public Integer getStarted() {
        return started;
    }

    public void setStarted(Integer started) {
        this.started = started;
    }

    public Integer getMined() {
        return mined;
    }

    public void setMined(Integer mined) {
        this.mined = mined;
    }

    public Integer getNetCoins() {
        return netCoins;
    }

    public void setNetCoins(Integer netCoins) {
        this.netCoins = netCoins;
    }

    public Integer getGpuCount() {
        return gpuCount;
    }

    public void setGpuCount(Integer gpuCount) {
        this.gpuCount = gpuCount;
    }

    public Integer getUpgradable() {
        return upgradable;
    }

    public void setUpgradable(Integer upgradable) {
        this.upgradable = upgradable;
    }

    public Double getMiningSpeed() {
        return miningSpeed;
    }

    public void setMiningSpeed(Double miningSpeed) {
        this.miningSpeed = miningSpeed;
    }
}
