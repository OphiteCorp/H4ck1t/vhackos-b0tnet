package hd.vhackos.b0tnet.api.chat;

import hd.vhackos.b0tnet.api.tool.SocketReceiver;

/**
 * Rozhraní chatu.
 */
public interface IChatClient extends SocketReceiver.IClient {

    /**
     * Odešle příkaz na server.
     */
    void sendCommand(String command);

    /**
     * Odešle zprávu.
     */
    void sendMessage(String message);

    /**
     * Odešle zprávu do určitého kanálu.
     */
    void sendMessage(String channel, String message);
}
