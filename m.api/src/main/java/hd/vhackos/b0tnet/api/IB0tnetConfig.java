package hd.vhackos.b0tnet.api;

import hd.vhackos.b0tnet.api.dto.ProxyData;

/**
 * Konfigurace pro B0tnet API.
 */
public interface IB0tnetConfig {

    /**
     * Login uživatele.
     */
    String getUserName();

    /**
     * Heslo uživatele.
     */
    String getPassword();

    /**
     * Zpráva, která se má zanechat v logu oběti.
     */
    String getMessageLog();

    /**
     * Maximální počet pokusů o navázání připojení.
     */
    int getMaxRequestAttempts();

    /**
     * Prodleva pro uspání vlákna.
     */
    long getSleepDelay();

    /**
     * Získá informace o proxy serveru.
     */
    ProxyData getProxyData();

    /**
     * Získá informace o proxy serveru pro chat.
     */
    ProxyData getChatProxyData();

    /**
     * Získá informace o proxy serveru pro Brandon OS.
     */
    ProxyData getBrandonOsProxyData();

    /**
     * Je chat povolený?
     */
    boolean isChatEnable();

    /**
     * Je Brandon OS povolený?
     */
    boolean isBrandonOsEnable();

    /**
     * Je povolen agresivní mód?
     */
    boolean isAggressiveMode();

    /**
     * Prodleva pro odeslání a čtení požadavku.
     */
    int getConnectionTimeout();

    /**
     * Verze herního API.
     */
    int getGameApi();

    /**
     * Zvedne verzi API o +1.
     */
    void increaseGameApi();

    /**
     * Kontrolní klíč.
     */
    String getControlKey();
}
