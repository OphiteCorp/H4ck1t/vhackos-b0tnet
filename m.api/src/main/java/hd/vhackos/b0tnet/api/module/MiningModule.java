package hd.vhackos.b0tnet.api.module;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.module.base.Module;
import hd.vhackos.b0tnet.api.module.base.ModuleHelper;
import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.MiningResponse;
import hd.vhackos.b0tnet.api.opcode.MiningBuyGpuOpcode;
import hd.vhackos.b0tnet.api.opcode.MiningCollectOpcode;
import hd.vhackos.b0tnet.api.opcode.MiningOpcode;
import hd.vhackos.b0tnet.api.opcode.MiningStartOpcode;
import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.shared.injection.Inject;

/**
 * Správa netcoin mineru.
 */
@Inject
public final class MiningModule extends Module {

    protected MiningModule(IB0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Získá informace o netcoin mineru.
     */
    public synchronized MiningResponse getMining() {
        var opcode = new MiningOpcode();
        return createMiningResponse(opcode);
    }

    /**
     * Vybere netcoins z mineru.
     */
    public synchronized MiningResponse collect() {
        var opcode = new MiningCollectOpcode();
        return createMiningResponse(opcode);
    }

    /**
     * Zakoupí další GPU do mineru.
     */
    public synchronized MiningResponse buyGpu() {
        var opcode = new MiningBuyGpuOpcode();
        return createMiningResponse(opcode);
    }

    /**
     * Spustí netcoin miner.
     */
    public synchronized MiningResponse start() {
        var opcode = new MiningStartOpcode();
        return createMiningResponse(opcode);
    }

    private MiningResponse createMiningResponse(Opcode opcode) {
        OpcodeRequestResp response = sendRequest(opcode);
        var responseMap = response.getResponse();
        var dto = new MiningResponse(response);

        ModuleHelper.checkResponseIntegrity(responseMap, MiningResponse.class);
        ModuleHelper.setField(responseMap, dto, MiningResponse.P_UPGRADED);
        ModuleHelper.setField(responseMap, dto, MiningResponse.P_RUNNING);
        ModuleHelper.setField(responseMap, dto, MiningResponse.P_APPLIED);
        ModuleHelper.setField(responseMap, dto, MiningResponse.P_CLAIMED);
        ModuleHelper.setField(responseMap, dto, MiningResponse.P_STARTED);
        ModuleHelper.setField(responseMap, dto, MiningResponse.P_MINED);
        ModuleHelper.setField(responseMap, dto, MiningResponse.P_NETCOINS);
        ModuleHelper.setField(responseMap, dto, MiningResponse.P_GPU_COUNT);
        ModuleHelper.setField(responseMap, dto, MiningResponse.P_UPGRADABLE);
        ModuleHelper.setField(responseMap, dto, MiningResponse.P_MINING_SPEED);
        ModuleHelper.setField(responseMap, dto, MiningResponse.P_NEW_GPU_COSTS);
        ModuleHelper.setField(responseMap, dto, MiningResponse.P_IS);
        ModuleHelper.setField(responseMap, dto, MiningResponse.P_LEFT);
        ModuleHelper.setField(responseMap, dto, MiningResponse.P_NEED);
        ModuleHelper.setField(responseMap, dto, MiningResponse.P_WILL_EARN);
        return dto;
    }
}
