package hd.vhackos.b0tnet.api.net.response;

import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.base.Response;
import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiBooleanConverter;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiDateConverter;

/**
 * Odpověď serveru při přihlášení. Používá se i pro registraci uživatele.
 */
public final class LoginResponse extends Response {

    public static final String P_USER_NAME = "userName";
    public static final String P_ACCESS_TOKEN = "accessToken";
    public static final String P_UID = "uid";
    public static final String P_NOTEPAD = "notepad";
    public static final String P_LEADERBOARD = "leaderboard";
    public static final String P_MISSIONS = "missions";
    public static final String P_JOBS = "jobs";
    public static final String P_COMMUNITY = "community";
    public static final String P_EXPERIENCE = "experience";
    public static final String P_REQUIRED_EXPERIENCE = "requiredExperience";
    public static final String P_EXP_PC = "expPc";
    public static final String P_NETCOINS = "netCoins";
    public static final String P_LEVEL = "level";
    public static final String P_MONEY = "money";
    public static final String P_IP = "ip";
    public static final String P_APP_FIREWALL = "appFirewall";
    public static final String P_APP_ANTIVIRUS = "appAntivirus";
    public static final String P_APP_SDK = "appSdk";
    public static final String P_C_COLOR = "cColor";
    public static final String P_APP_BRUTEFORCE = "appBruteforce";
    public static final String P_APP_SPAM = "appSpam";
    public static final String P_MALWARE_KIT = "malwareKit";
    public static final String P_MODERATOR = "moderator";
    public static final String P_EMAIL = "email";
    public static final String P_CREW = "crew";
    public static final String P_MINER = "miner";
    public static final String P_TIME = "time";
    public static final String P_SERVER = "server";
    public static final String P_INTERNET_CONNECTION = "internetConnection";

    // aktuální login uživatele (měl by být stejný jako v konfiguraci)
    @AsciiRow("User")
    @ResponseKey("username")
    private String userName;

    // přístupový token - většinou vrací stejný (ještě se nestalo, že by vrátil jiný)
    @AsciiRow("Access Token")
    @ResponseKey("accesstoken")
    private String accessToken;

    // unikátní ID uživatele na serveru
    @AsciiRow("User UID")
    @ResponseKey("uid")
    private Integer uid;

    @AsciiRow(value = "Notepad", converter = AsciiBooleanConverter.class)
    @ResponseKey("notepad")
    private Integer notepad;

    @AsciiRow(value = "Leaderboard", converter = AsciiBooleanConverter.class)
    @ResponseKey("lb")
    private Integer leaderboard;

    @AsciiRow(value = "Missions", converter = AsciiBooleanConverter.class)
    @ResponseKey("missions")
    private Integer missions;

    @AsciiRow(value = "Jobs", converter = AsciiBooleanConverter.class)
    @ResponseKey("jobs")
    private Integer jobs;

    @AsciiRow(value = "Community", converter = AsciiBooleanConverter.class)
    @ResponseKey("community")
    private Integer community;

    @AsciiRow("Experience")
    @ResponseKey("exp")
    private Long experience;

    @AsciiRow("Required Experience")
    @ResponseKey("expreq")
    private Long requiredExperience;

    @AsciiRow("ExpPC")
    @ResponseKey("exppc")
    private Long expPc;

    @AsciiRow("NetCoins")
    @ResponseKey("netcoins")
    private Integer netCoins;

    @AsciiRow("Level")
    @ResponseKey("level")
    private Integer level;

    @AsciiRow("Money")
    @ResponseKey("money")
    private Long money;

    // aktuální IP adresa hráče (herní)
    @AsciiRow("IP")
    @ResponseKey("ipaddress")
    private String ip;

    @AsciiRow("Firewall")
    @ResponseKey("fw")
    private Integer appFirewall;

    @AsciiRow("Antivirus")
    @ResponseKey("av")
    private Integer appAntivirus;

    @AsciiRow("SDK")
    @ResponseKey("sdk")
    private Integer appSdk;

    @AsciiRow("CColor")
    @ResponseKey("ccolor")
    private Integer cColor;

    // úroveň BF
    @AsciiRow("BruteForce")
    @ResponseKey("brute")
    private Integer appBruteforce;

    // úroveň SPAMu
    @AsciiRow("Spam")
    @ResponseKey("spam")
    private Integer appSpam;

    // počet malware kitů
    @AsciiRow(value = "Malware Kit", converter = AsciiBooleanConverter.class)
    @ResponseKey("mwk")
    private Integer malwareKit;

    // má uživatel oprávnění moderátora
    @AsciiRow(value = "Moderator", converter = AsciiBooleanConverter.class)
    @ResponseKey("mod")
    private Integer moderator;

    @AsciiRow("Email")
    @ResponseKey("email")
    private String email;

    @AsciiRow(value = "Crew", converter = AsciiBooleanConverter.class)
    @ResponseKey("crew")
    private Integer crew;

    @AsciiRow(value = "Miner", converter = AsciiBooleanConverter.class)
    @ResponseKey("miner")
    private Integer miner;

    // aktuální čas serveru (unixtime)
    @AsciiRow(value = "Time", converter = AsciiDateConverter.class)
    @ResponseKey("time")
    private Long time;

    @AsciiRow(value = "Server", converter = AsciiBooleanConverter.class)
    @ResponseKey("server")
    private Integer server;

    // název aktuálního zakoupeného internetového připojení ve hře
    @AsciiRow("Internet Connection")
    @ResponseKey("inet")
    private String internetConnection;

    public Long getMoney() {
        return money;
    }

    public void setMoney(Long money) {
        this.money = money;
    }

    public LoginResponse(OpcodeRequestResp response) {
        super(response);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getNotepad() {
        return notepad;
    }

    public void setNotepad(Integer notepad) {
        this.notepad = notepad;
    }

    public Integer getLeaderboard() {
        return leaderboard;
    }

    public void setLeaderboard(Integer leaderboard) {
        this.leaderboard = leaderboard;
    }

    public Integer getMissions() {
        return missions;
    }

    public void setMissions(Integer missions) {
        this.missions = missions;
    }

    public Integer getJobs() {
        return jobs;
    }

    public void setJobs(Integer jobs) {
        this.jobs = jobs;
    }

    public Integer getCommunity() {
        return community;
    }

    public void setCommunity(Integer community) {
        this.community = community;
    }

    public Long getExperience() {
        return experience;
    }

    public void setExperience(Long experience) {
        this.experience = experience;
    }

    public Long getRequiredExperience() {
        return requiredExperience;
    }

    public void setRequiredExperience(Long requiredExperience) {
        this.requiredExperience = requiredExperience;
    }

    public Long getExpPc() {
        return expPc;
    }

    public void setExpPc(Long expPc) {
        this.expPc = expPc;
    }

    public Integer getNetCoins() {
        return netCoins;
    }

    public void setNetCoins(Integer netCoins) {
        this.netCoins = netCoins;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getAppFirewall() {
        return appFirewall;
    }

    public void setAppFirewall(Integer appFirewall) {
        this.appFirewall = appFirewall;
    }

    public Integer getAppAntivirus() {
        return appAntivirus;
    }

    public void setAppAntivirus(Integer appAntivirus) {
        this.appAntivirus = appAntivirus;
    }

    public Integer getAppSdk() {
        return appSdk;
    }

    public void setAppSdk(Integer appSdk) {
        this.appSdk = appSdk;
    }

    public Integer getcColor() {
        return cColor;
    }

    public void setcColor(Integer cColor) {
        this.cColor = cColor;
    }

    public Integer getAppBruteforce() {
        return appBruteforce;
    }

    public void setAppBruteforce(Integer appBruteforce) {
        this.appBruteforce = appBruteforce;
    }

    public Integer getAppSpam() {
        return appSpam;
    }

    public void setAppSpam(Integer appSpam) {
        this.appSpam = appSpam;
    }

    public Integer getMalwareKit() {
        return malwareKit;
    }

    public void setMalwareKit(Integer malwareKit) {
        this.malwareKit = malwareKit;
    }

    public Integer getModerator() {
        return moderator;
    }

    public void setModerator(Integer moderator) {
        this.moderator = moderator;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getCrew() {
        return crew;
    }

    public void setCrew(Integer crew) {
        this.crew = crew;
    }

    public Integer getMiner() {
        return miner;
    }

    public void setMiner(Integer miner) {
        this.miner = miner;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Integer getServer() {
        return server;
    }

    public void setServer(Integer server) {
        this.server = server;
    }

    public String getInternetConnection() {
        return internetConnection;
    }

    public void setInternetConnection(String internetConnection) {
        this.internetConnection = internetConnection;
    }
}
