package hd.vhackos.b0tnet.api.module.base;

import hd.vhackos.b0tnet.api.IB0tnet;

/**
 * Rozhraní modulu.
 */
public interface IModule {

    /**
     * Získá instanci B0tnet.
     */
    IB0tnet getB0tnet();
}
