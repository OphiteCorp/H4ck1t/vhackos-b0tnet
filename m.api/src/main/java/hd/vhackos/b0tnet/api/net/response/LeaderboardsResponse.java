package hd.vhackos.b0tnet.api.net.response;

import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.base.Response;
import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.api.net.response.data.Leaderboard1on1Data;
import hd.vhackos.b0tnet.api.net.response.data.LeaderboardCrewData;
import hd.vhackos.b0tnet.api.net.response.data.LeaderboardData;
import hd.vhackos.b0tnet.api.net.response.data.Tournament24HData;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiElapsedSecondsTimeConverter;

import java.util.List;

/**
 * Informace z leaderboards.
 */
public final class LeaderboardsResponse extends Response {

    public static final String P_COUNT = "count";
    public static final String P_MY_LEVEL = "myLevel";
    public static final String P_MY_EXP = "myExp";
    public static final String P_MY_EXP_REQ = "myExpReq";
    public static final String P_EXP_GAIN = "expGain";
    public static final String P_TOURNAMENT_LEFT = "tournamentLeft";
    public static final String P_TOURNAMENT_RANK = "tournamentRank";
    public static final String P_MY_RANK = "myRank";
    public static final String P_CREW_COUNT = "crewCount";
    public static final String P_LEADERBOARD_DATA = "leaderboardData";
    public static final String P_TOURNAMENT_DATA = "tournamentData";
    public static final String P_CREWS_DATA = "crewsData";
    public static final String P_DAILY_CREW_RANK = "dailyCrewRank";
    public static final String P_DAILY_CREW_REPUTATION = "dailyCrewReputation";
    public static final String P_DAILY_CREW_TIME_LEFT = "dailyCrewTimeLeft";
    public static final String P_ONE_ON_ONE = "oneOnOne";
    public static final String P_MY_1ON1_REPUSTATION = "my1on1Repustation";
    public static final String P_MY_1ON1_RANK = "my1on1Rank";

    @AsciiRow("Count")
    @ResponseKey("count")
    private Integer count;

    @AsciiRow("My Level")
    @ResponseKey("mylevel")
    private Integer myLevel;

    @AsciiRow("My Experience")
    @ResponseKey("myexp")
    private Long myExp;

    @AsciiRow("My Required Experience")
    @ResponseKey("myexpreq")
    private Long myExpReq;

    @AsciiRow("Experience Gain")
    @ResponseKey("expgain")
    private Long expGain;

    @AsciiRow(value = "Tournament Left", converter = AsciiElapsedSecondsTimeConverter.class)
    @ResponseKey("tournamentleft")
    private Long tournamentLeft;

    @AsciiRow("Tournament Rank")
    @ResponseKey("tournamentrank")
    private Integer tournamentRank;

    @AsciiRow("My Rank")
    @ResponseKey("myrank")
    private Integer myRank;

    @AsciiRow("Crew Count")
    @ResponseKey("crewcount")
    private Integer crewCount;

    @AsciiRow("Daily Crew Rank")
    @ResponseKey("dailycrewrank")
    private Integer dailyCrewRank;

    @AsciiRow("Daily Crew Reputation")
    @ResponseKey("dailycrewrep")
    private Integer dailyCrewReputation;

    @AsciiRow(value = "Daily Crew Time Left", converter = AsciiElapsedSecondsTimeConverter.class)
    @ResponseKey("dailycrewtimeleft")
    private Integer dailyCrewTimeLeft;

    @AsciiRow("My 1on1 Reputation")
    @ResponseKey("my1on1Rep")
    private Integer my1on1Repustation;

    @AsciiRow("My 1on1 Rank")
    @ResponseKey("my1on1Rank")
    private Integer my1on1Rank;

    @AsciiRow("Data")
    @ResponseKey("data")
    private List<LeaderboardData> leaderboardData;

    @AsciiRow("Tournament Data")
    @ResponseKey("tm")
    private List<Tournament24HData> tournamentData;

    @AsciiRow("Crews Data")
    @ResponseKey("crews")
    private List<LeaderboardCrewData> crewsData;

    @AsciiRow("1on1")
    @ResponseKey("1on1")
    private List<Leaderboard1on1Data> oneOnOne;

    public LeaderboardsResponse(OpcodeRequestResp response) {
        super(response);
    }

    public Integer getMy1on1Repustation() {
        return my1on1Repustation;
    }

    public void setMy1on1Repustation(Integer my1on1Repustation) {
        this.my1on1Repustation = my1on1Repustation;
    }

    public Integer getMy1on1Rank() {
        return my1on1Rank;
    }

    public void setMy1on1Rank(Integer my1on1Rank) {
        this.my1on1Rank = my1on1Rank;
    }

    public List<Leaderboard1on1Data> getOneOnOne() {
        return oneOnOne;
    }

    public void setOneOnOne(List<Leaderboard1on1Data> oneOnOne) {
        this.oneOnOne = oneOnOne;
    }

    public Integer getDailyCrewReputation() {
        return dailyCrewReputation;
    }

    public void setDailyCrewReputation(Integer dailyCrewReputation) {
        this.dailyCrewReputation = dailyCrewReputation;
    }

    public Integer getDailyCrewTimeLeft() {
        return dailyCrewTimeLeft;
    }

    public void setDailyCrewTimeLeft(Integer dailyCrewTimeLeft) {
        this.dailyCrewTimeLeft = dailyCrewTimeLeft;
    }

    public Integer getDailyCrewRank() {
        return dailyCrewRank;
    }

    public void setDailyCrewRank(Integer dailyCrewRank) {
        this.dailyCrewRank = dailyCrewRank;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getMyLevel() {
        return myLevel;
    }

    public void setMyLevel(Integer myLevel) {
        this.myLevel = myLevel;
    }

    public Long getMyExp() {
        return (myExp != null) ? myExp : 0;
    }

    public void setMyExp(Long myExp) {
        this.myExp = myExp;
    }

    public Long getMyExpReq() {
        return (myExpReq != null) ? myExpReq : 0;
    }

    public void setMyExpReq(Long myExpReq) {
        this.myExpReq = myExpReq;
    }

    public Long getExpGain() {
        return (expGain != null) ? expGain : 0;
    }

    public void setExpGain(Long expGain) {
        this.expGain = expGain;
    }

    public Long getTournamentLeft() {
        return tournamentLeft;
    }

    public void setTournamentLeft(Long tournamentLeft) {
        this.tournamentLeft = tournamentLeft;
    }

    public Integer getTournamentRank() {
        return tournamentRank;
    }

    public void setTournamentRank(Integer tournamentRank) {
        this.tournamentRank = tournamentRank;
    }

    public Integer getMyRank() {
        return myRank;
    }

    public void setMyRank(Integer myRank) {
        this.myRank = myRank;
    }

    public Integer getCrewCount() {
        return crewCount;
    }

    public void setCrewCount(Integer crewCount) {
        this.crewCount = crewCount;
    }

    public List<LeaderboardData> getLeaderboardData() {
        return leaderboardData;
    }

    public void setLeaderboardData(List<LeaderboardData> leaderboardData) {
        this.leaderboardData = leaderboardData;
    }

    public List<Tournament24HData> getTournamentData() {
        return tournamentData;
    }

    public void setTournamentData(List<Tournament24HData> tournamentData) {
        this.tournamentData = tournamentData;
    }

    public List<LeaderboardCrewData> getCrewsData() {
        return crewsData;
    }

    public void setCrewsData(List<LeaderboardCrewData> crewsData) {
        this.crewsData = crewsData;
    }
}
