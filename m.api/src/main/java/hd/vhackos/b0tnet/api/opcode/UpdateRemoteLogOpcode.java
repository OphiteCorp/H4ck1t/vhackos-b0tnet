package hd.vhackos.b0tnet.api.opcode;

import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;

/**
 * Upraví vzdálený log pro IP.
 */
public final class UpdateRemoteLogOpcode extends Opcode {

    private static final String PARAM_TARGET = "target";
    private static final String PARAM_LOG = "log";

    /**
     * Cílová IP.
     */
    public void setTargetIp(String ip) {
        addParam(1, PARAM_TARGET, ip);
    }

    /**
     * Záznam v logu.
     */
    public void setLog(String log) {
        addParam(2, PARAM_LOG, log);
    }

    @Override
    public OpcodeTargetType getTarget() {
        return OpcodeTargetType.REMOTE_LOG;
    }

    @Override
    public String getOpcodeValue() {
        return "100";
    }
}
