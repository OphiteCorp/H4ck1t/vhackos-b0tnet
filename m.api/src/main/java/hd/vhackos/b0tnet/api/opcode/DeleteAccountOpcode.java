package hd.vhackos.b0tnet.api.opcode;

import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;

/**
 * Umožní smazat účet uživatele.
 */
public final class DeleteAccountOpcode extends Opcode {

    private static final String PARAM_UID = "uid";
    private static final String PARAM_ACCESS_TOKEN = "accesstoken";
    private static final String PARAM_LANGUAGE = "lang";
    private static final String PARAM_PASSWORD = "password2";

    /**
     * Unikátní UID uživatele.
     */
    public void setUid(String uid) {
        addParam(1, PARAM_UID, uid);
    }

    /**
     * Přístupový token uživatele.
     */
    public void setAccessToken(String accessToken) {
        addParam(2, PARAM_ACCESS_TOKEN, accessToken);
    }

    /**
     * Kód jazyku v ISO2.
     */
    public void setLanguage(String langCodeIso2) {
        addParam(3, PARAM_LANGUAGE, langCodeIso2);
    }

    /**
     * Ověřovací heslo v MD5.
     */
    public void setPasswordHash(String md5PasswordHash) {
        addParam(4, PARAM_PASSWORD, md5PasswordHash);
    }

    @Override
    public OpcodeTargetType getTarget() {
        return OpcodeTargetType.DELETE_ACCOUNT;
    }

    @Override
    public boolean isStandaloneOpcode() {
        return true;
    }
}
