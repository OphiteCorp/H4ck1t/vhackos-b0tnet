package hd.vhackos.b0tnet.api.brandonos;

import hd.vhackos.b0tnet.api.tool.SocketReceiver;

/**
 * Události pro herní Brandon OS.
 */
public interface IBrandonOsListener extends SocketReceiver.IListener {

    /**
     * Volá se pro každý řádek v původním tvaru bez formátování.
     */
    void readRaw(String line);
}
