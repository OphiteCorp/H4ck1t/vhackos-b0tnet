package hd.vhackos.b0tnet.api.opcode;

import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;

/**
 * Přidá uživatele mezi přátelé.
 */
public final class AddFriendOpcode extends Opcode {

    private static final String PARAM_USER_ID = "user_id";

    /**
     * User ID.
     */
    public void setUserId(int userId) {
        addParam(PARAM_USER_ID, String.valueOf(userId));
    }

    @Override
    public OpcodeTargetType getTarget() {
        return OpcodeTargetType.PROFILE;
    }

    @Override
    public String getOpcodeValue() {
        return "100";
    }
}
