package hd.vhackos.b0tnet.api.exception;

import hd.vhackos.b0tnet.shared.exception.B0tnetException;

/**
 * Profil uživatele neexistuje.
 */
public final class UserProfileNotExistsException extends B0tnetException {

    public UserProfileNotExistsException(String resultCode, String message) {
        super(resultCode, message);
    }
}
