package hd.vhackos.b0tnet.api.chat.dto;

import java.util.Comparator;

/**
 * Informace o uživateli v chatu.
 */
public final class ChatUser {

    public static final Comparator COMPARATOR = Comparator.comparing((ChatUser p) -> p.role.getOrder())
            .thenComparing(p -> p.name);

    private String name;
    private ChatRole role;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ChatRole getRole() {
        return role;
    }

    public void setRole(ChatRole role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return String.format("[%s] %s", role.getAlias(), name);
    }
}
