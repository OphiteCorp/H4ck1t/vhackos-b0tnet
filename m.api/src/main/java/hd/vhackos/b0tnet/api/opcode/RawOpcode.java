package hd.vhackos.b0tnet.api.opcode;

import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;

import java.util.Map;

/**
 * Opcode, který je možné nadefinovat podle představ.
 */
public final class RawOpcode extends Opcode {

    private static final String PARAM_UID = "uid";
    private static final String PARAM_ACCESS_TOKEN = "accesstoken";
    private static final String PARAM_LANGUAGE = "lang";

    private final OpcodeTargetType opcodeType;

    /**
     * Vytvoří novou instanci opcode.
     */
    public RawOpcode(OpcodeTargetType opcodeType) {
        super();
        this.opcodeType = opcodeType;
    }

    /**
     * Unikátní UID uživatele.
     */
    public void setUid(int uid) {
        addParam(1, PARAM_UID, String.valueOf(uid));
    }

    /**
     * Přístupový token uživatele.
     */
    public void setAccessToken(String accessToken) {
        addParam(2, PARAM_ACCESS_TOKEN, accessToken);
    }

    /**
     * Kód jazyku v ISO2.
     */
    public void setLanguage(String langCodeIso2) {
        addParam(3, PARAM_LANGUAGE, langCodeIso2);
    }

    /**
     * Přidá další dodatečné parametry.
     */
    public void setOtherParams(Map<String, String> params) {
        for (var entry : params.entrySet()) {
            addParam(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public OpcodeTargetType getTarget() {
        return opcodeType;
    }

    @Override
    public boolean isStandaloneOpcode() {
        return true;
    }
}
