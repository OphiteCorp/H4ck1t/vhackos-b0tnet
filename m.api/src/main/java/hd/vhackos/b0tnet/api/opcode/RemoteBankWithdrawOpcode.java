package hd.vhackos.b0tnet.api.opcode;

import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;

/**
 * Vybere bankovní účet banky.
 */
public final class RemoteBankWithdrawOpcode extends Opcode {

    private static final String PARAM_TARGET = "target";
    private static final String PARAM_AMOUNT = "amount";

    /**
     * Cílová IP.
     */
    public void setTargetIp(String ip) {
        addParam(1, PARAM_TARGET, ip);
    }

    /**
     * Množství peněz.
     */
    public void setAmount(long amount) {
        addParam(2, PARAM_AMOUNT, String.valueOf(amount));
    }

    @Override
    public OpcodeTargetType getTarget() {
        return OpcodeTargetType.REMOTE_BANKING;
    }

    @Override
    public String getOpcodeValue() {
        return "100";
    }
}
