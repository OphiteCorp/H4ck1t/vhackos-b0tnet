package hd.vhackos.b0tnet.api.brandonos;

import hd.vhackos.b0tnet.api.tool.SocketReceiver;

/**
 * Rozhraní Brandon OS.
 */
public interface IBrandonOsClient extends SocketReceiver.IClient {

    /**
     * Odešle příkaz na odpojení se z Brandon OS.
     */
    void sendQuit();
}
