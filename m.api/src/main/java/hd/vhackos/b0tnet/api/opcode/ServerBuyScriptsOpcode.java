package hd.vhackos.b0tnet.api.opcode;

import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;
import hd.vhackos.b0tnet.shared.dto.ServerScriptType;

/**
 * Zakoupí bypass skripty pro server.
 */
public final class ServerBuyScriptsOpcode extends Opcode {

    private static final String PARAM_TYPE = "type";
    private static final String PARAM_COUNT = "count";

    /**
     * Nastaví typ skriptu.
     */
    public void setType(ServerScriptType scriptType) {
        addParam(1, PARAM_TYPE, String.valueOf(scriptType.getCode()));
    }

    /**
     * Nastaví počet skriptů. Zatím je povoleno pouze 1 a 10.
     */
    public void setCount(int count) {
        addParam(2, PARAM_COUNT, String.valueOf(count));
    }

    @Override
    public OpcodeTargetType getTarget() {
        return OpcodeTargetType.SERVER;
    }

    @Override
    public String getOpcodeValue() {
        return "7070";
    }
}
