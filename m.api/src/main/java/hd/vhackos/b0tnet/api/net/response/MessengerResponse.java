package hd.vhackos.b0tnet.api.net.response;

import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.base.Response;
import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.api.net.response.data.MessengerData;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;

import java.util.List;

/**
 * Informace o zprávě.
 */
public final class MessengerResponse extends Response {

    public static final String P_ACCEPTED = "accepted";
    public static final String P_ADDED = "added";
    public static final String P_C_COUNT = "cCount";
    public static final String P_REMOVED = "removed";
    public static final String P_SENT = "sent";
    public static final String P_MESSAGES = "messages";

    @AsciiRow("Accepted")
    @ResponseKey("accepted")
    private Integer accepted;

    @AsciiRow("Added")
    @ResponseKey("added")
    private Integer added;

    @AsciiRow("Count")
    @ResponseKey("cCount")
    private Integer cCount;

    @AsciiRow("Removed")
    @ResponseKey("removed")
    private Integer removed;

    @AsciiRow("Sent")
    @ResponseKey("sent")
    private Integer sent;

    @AsciiRow("Messages")
    @ResponseKey("convos")
    private List<MessengerData> messages;

    public MessengerResponse(OpcodeRequestResp response) {
        super(response);
    }

    public Integer getAccepted() {
        return accepted;
    }

    public void setAccepted(Integer accepted) {
        this.accepted = accepted;
    }

    public Integer getAdded() {
        return added;
    }

    public void setAdded(Integer added) {
        this.added = added;
    }

    public Integer getcCount() {
        return cCount;
    }

    public void setcCount(Integer cCount) {
        this.cCount = cCount;
    }

    public Integer getRemoved() {
        return removed;
    }

    public void setRemoved(Integer removed) {
        this.removed = removed;
    }

    public Integer getSent() {
        return sent;
    }

    public void setSent(Integer sent) {
        this.sent = sent;
    }

    public List<MessengerData> getMessages() {
        return messages;
    }

    public void setMessages(List<MessengerData> messages) {
        this.messages = messages;
    }
}
