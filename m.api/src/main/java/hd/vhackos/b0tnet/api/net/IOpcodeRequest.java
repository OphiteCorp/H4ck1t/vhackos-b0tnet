package hd.vhackos.b0tnet.api.net;

import hd.vhackos.b0tnet.api.module.base.IModule;

/**
 * Request pro opcode.
 */
public interface IOpcodeRequest<T extends IOpcodeRequestResp> {

    /**
     * Odešle požadavek na server.
     */
    T send(IModule module);
}
