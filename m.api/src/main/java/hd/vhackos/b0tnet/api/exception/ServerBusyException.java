package hd.vhackos.b0tnet.api.exception;

import hd.vhackos.b0tnet.shared.exception.B0tnetException;

/**
 * Server je zaneprázdněn.
 */
public final class ServerBusyException extends B0tnetException {

    public ServerBusyException(String resultCode, String message) {
        super(resultCode, message);
    }
}
