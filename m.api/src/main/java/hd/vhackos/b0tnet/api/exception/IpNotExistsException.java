package hd.vhackos.b0tnet.api.exception;

import hd.vhackos.b0tnet.shared.exception.B0tnetException;

/**
 * IP uživatele již neexistuje.
 */
public final class IpNotExistsException extends B0tnetException {

    public IpNotExistsException(String resultCode, String message) {
        super(resultCode, message);
    }
}
