package hd.vhackos.b0tnet.api.net.response;

import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.base.Response;
import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.api.net.response.data.MessengerMessageItemData;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;

import java.util.List;

/**
 * Informace o rozkliknuté zprávě.
 */
public final class MessengerDetailResponse extends Response {

    public static final String P_ACCEPTED = "accepted";
    public static final String P_ADDED = "added";
    public static final String P_MSG_COUNT = "msgCount";
    public static final String P_REMOVED = "removed";
    public static final String P_SENT = "sent";
    public static final String P_MESSAGES = "messages";

    @AsciiRow("Accepted")
    @ResponseKey("accepted")
    private Integer accepted;

    @AsciiRow("Added")
    @ResponseKey("added")
    private Integer added;

    @AsciiRow("Count")
    @ResponseKey("msgCount")
    private Integer msgCount;

    @AsciiRow("Removed")
    @ResponseKey("removed")
    private Integer removed;

    @AsciiRow("Sent")
    @ResponseKey("sent")
    private Integer sent;

    @AsciiRow("Messages")
    @ResponseKey("chatmsgs")
    private List<MessengerMessageItemData> messages;

    public MessengerDetailResponse(OpcodeRequestResp response) {
        super(response);
    }

    public Integer getAccepted() {
        return accepted;
    }

    public void setAccepted(Integer accepted) {
        this.accepted = accepted;
    }

    public Integer getAdded() {
        return added;
    }

    public void setAdded(Integer added) {
        this.added = added;
    }

    public Integer getMsgCount() {
        return msgCount;
    }

    public void setMsgCount(Integer msgCount) {
        this.msgCount = msgCount;
    }

    public Integer getRemoved() {
        return removed;
    }

    public void setRemoved(Integer removed) {
        this.removed = removed;
    }

    public Integer getSent() {
        return sent;
    }

    public void setSent(Integer sent) {
        this.sent = sent;
    }

    public List<MessengerMessageItemData> getMessages() {
        return messages;
    }

    public void setMessages(List<MessengerMessageItemData> messages) {
        this.messages = messages;
    }
}
