package hd.vhackos.b0tnet.api.chat;

import hd.vhackos.b0tnet.api.chat.dto.ChatUser;
import hd.vhackos.b0tnet.api.chat.dto.JoinedUser;
import hd.vhackos.b0tnet.api.chat.dto.ModeUser;
import hd.vhackos.b0tnet.api.chat.dto.UserMessage;
import hd.vhackos.b0tnet.api.tool.SocketReceiver;

import java.util.List;

/**
 * Události pro herní chat.
 */
public interface IChatListener extends SocketReceiver.IListener {

    /**
     * Seznam připojených uživatelů.
     */
    void incomingUserList(List<ChatUser> users);

    /**
     * Příchozí zpráva.
     */
    void incomingMessage(UserMessage message);

    /**
     * Odeslaná zpráva.
     */
    void sentMessage(String message);

    /**
     * Nový uživatel se připojil.
     */
    void userJoin(JoinedUser user);

    /**
     * Uživatel opustil chat.
     */
    void userQuit(ModeUser user);

    /**
     * Uživateli se změnila oprávnění.
     */
    void userMode(ModeUser user);

    /**
     * Volá se pro každý řádek v původním tvaru bez formátování.
     */
    void readRaw(String line);
}
