package hd.vhackos.b0tnet.api.opcode.base;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Zákadní implementace opcode.
 */
public abstract class Opcode implements IOpcode {

    // stačí 5 parametu, víc snad nikde nebude
    private final Map<String, ParamData> params = new HashMap<>(5);
    private final Logger log;

    /**
     * Vytvoří novou instanci opcode.
     */
    protected Opcode() {
        log = LoggerFactory.getLogger(getClass());
    }

    /**
     * Přidá parametr s hodnotou.
     */
    protected final void addParam(String paramName, String value) {
        addParam(0, paramName, value);
    }

    /**
     * Přidá parametr s hodnotou.
     */
    protected final void addParam(int order, String paramName, String value) {
        params.put(paramName, new ParamData(order, value));
    }

    /**
     * Získá instanci logu.
     */
    protected final Logger getLog() {
        return log;
    }

    @Override
    public OpcodeType getType() {
        return OpcodeType.STANDARD;
    }

    @Override
    public final Map<String, String> getParams() {
        var sortedParams = params.entrySet().stream().sorted(Comparator.comparingInt(e -> e.getValue().order))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        var output = new LinkedHashMap<String, String>(params.size());

        for (var entry : sortedParams.entrySet()) {
            output.put(entry.getKey(), entry.getValue().value);
        }
        return output;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("params", params).toString();
    }

    private static final class ParamData {

        private final int order;
        private final String value;

        private ParamData(int order, String value) {
            this.order = order;
            this.value = value;
        }
    }
}
