package hd.vhackos.b0tnet.api.net.response.data;

import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiElapsedTimeConverter;

/**
 * Crew zpráva.
 */
public final class CrewMessageData {

    public static final String P_USER_ID = "userId";
    public static final String P_MESSAGE = "message";
    public static final String P_USER_NAME = "userName";
    public static final String P_TIME = "time";

    @AsciiRow("User ID")
    @ResponseKey("user_id")
    private Long userId;

    @AsciiRow("Message")
    @ResponseKey("message")
    private String message;

    @AsciiRow("User")
    @ResponseKey("username")
    private String userName;

    @AsciiRow(value = "Time", converter = AsciiElapsedTimeConverter.class)
    @ResponseKey("time")
    private Long time;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

}
