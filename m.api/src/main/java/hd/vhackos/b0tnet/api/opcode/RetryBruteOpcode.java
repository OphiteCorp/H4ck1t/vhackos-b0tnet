package hd.vhackos.b0tnet.api.opcode;

import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;

/**
 * Znovu se pokusí prolomit banku uživatele.
 */
public final class RetryBruteOpcode extends Opcode {

    private static final String PARAM_TARGET_IP = "target";

    /**
     * Cílová IP.
     */
    public void setTargetIp(String targetIp) {
        addParam(PARAM_TARGET_IP, targetIp);
    }

    @Override
    public OpcodeTargetType getTarget() {
        return OpcodeTargetType.TASKS;
    }

    @Override
    public String getOpcodeValue() {
        return "10005";
    }
}
