package hd.vhackos.b0tnet.api.net.response.data;

import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiDateConverter;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiMoneyConverter;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Informace o transakci v bance.
 */
public final class BankTransactionData {

    public static final String P_TIME = "time";
    public static final String P_FROM_ID = "fromId";
    public static final String P_FROM_IP = "fromIp";
    public static final String P_TO_ID = "toId";
    public static final String P_TO_IP = "toIp";
    public static final String P_AMOUNT = "amount";

    @AsciiRow(value = "Time", converter = AsciiDateConverter.class)
    @ResponseKey("time")
    private Long time;

    @AsciiRow("From ID")
    @ResponseKey("from_id")
    private Integer fromId;

    @AsciiRow("From IP")
    @ResponseKey("from_ip")
    private String fromIp;

    @AsciiRow("To ID")
    @ResponseKey("to_id")
    private Integer toId;

    @AsciiRow("To IP")
    @ResponseKey("to_ip")
    private String toIp;

    @AsciiRow(value = "Amount", converter = AsciiMoneyConverter.class)
    @ResponseKey("amount")
    private Long amount;

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Integer getFromId() {
        return fromId;
    }

    public void setFromId(Integer fromId) {
        this.fromId = fromId;
    }

    public String getFromIp() {
        return fromIp;
    }

    public void setFromIp(String fromIp) {
        this.fromIp = fromIp;
    }

    public Integer getToId() {
        return toId;
    }

    public void setToId(Integer toId) {
        this.toId = toId;
    }

    public String getToIp() {
        return toIp;
    }

    public void setToIp(String toIp) {
        this.toIp = toIp;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("time", time).append("fromId", fromId).append("fromIp", fromIp)
                .append("toId", toId).append("toIp", toIp).append("amount", amount).toString();
    }
}
