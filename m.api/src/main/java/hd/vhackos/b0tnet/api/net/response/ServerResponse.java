package hd.vhackos.b0tnet.api.net.response;

import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.base.Response;
import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.api.net.response.data.ServerPvPBoostData;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiBooleanConverter;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiElapsedSecondsTimeConverter;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiServerNodeTypeConverter;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiServerUpgradeScriptStateConverter;

import java.util.List;

/**
 * Informace o serveru.
 */
public final class ServerResponse extends Response {

    public static final String P_AV_ADDED = "avAdded";
    public static final String P_FW_ADDED = "fwAdded";
    public static final String P_NODE_UPDATED = "nodeUpdated";
    public static final String P_SPACK_OPEN = "sPackOpen";
    public static final String P_SPACK_OPEN_ALL = "sPackOpenAll";
    public static final String P_BOUGHT_ONE = "boughtOne";
    public static final String P_BOUGHT_TEN = "boughtTen";
    public static final String P_PACKS_BOUGHT = "packsBought";
    public static final String P_BOUGHT_LIMIT = "boughtLimit";
    public static final String P_SERVER_PIECES = "serverPieces";
    public static final String P_AV1_PIECES = "av1Pieces";
    public static final String P_AV2_PIECES = "av2Pieces";
    public static final String P_AV3_PIECES = "av3Pieces";
    public static final String P_FW1_PIECES = "fw1Pieces";
    public static final String P_FW2_PIECES = "fw2Pieces";
    public static final String P_FW3_PIECES = "fw3Pieces";
    public static final String P_SERVER_FREE_PIECES = "serverFreePieces";
    public static final String P_AV_FREE_PIECES = "avFreePieces";
    public static final String P_FW_FREE_PIECES = "fwFreePieces";
    public static final String P_PACKS = "packs";
    public static final String P_SERVER_MAX_PIECES = "serverMaxPieces";
    public static final String P_SERVER_STARS = "serverStars";
    public static final String P_AV1_MAX_PIECES = "av1MaxPieces";
    public static final String P_AV1_STARS = "av1Stars";
    public static final String P_AV2_MAX_PIECES = "av2MaxPieces";
    public static final String P_AV2_STARS = "av2Stars";
    public static final String P_AV3_MAX_PIECES = "av3MaxPieces";
    public static final String P_AV3_STARS = "av3Stars";
    public static final String P_FW1_MAX_PIECES = "fw1MaxPieces";
    public static final String P_FW1_STARS = "fw1Stars";
    public static final String P_FW2_MAX_PIECES = "fw2MaxPieces";
    public static final String P_FW2_STARS = "fw2Stars";
    public static final String P_FW3_MAX_PIECES = "fw3MaxPieces";
    public static final String P_FW3_STARS = "fw3Stars";
    public static final String P_AV_NODES = "avNodes";
    public static final String P_FW_NODES = "fwNodes";
    public static final String P_NEXT_PACK_IN = "nextPackIn";
    public static final String P_NODE_TYPE = "nodeType";
    public static final String P_NODE_NUMBER = "nodeNumber";
    public static final String P_SPACK_TYPE = "sPackType";
    public static final String P_SHOW_MUCH = "sHowMuch";
    public static final String P_SERVER = "sServer";
    public static final String P_AV = "sAV";
    public static final String P_FW = "sFW";
    public static final String P_BOOST = "sBoost";
    public static final String P_BYPASS_COSTS = "bypassCosts";
    public static final String P_BYPASS_CREATED = "bypassCreated";
    public static final String P_BYPASS_SCRIPTS = "bypassScripts";
    public static final String P_BYPASS_STRENGTH = "bypassStrength";
    public static final String P_BYPASS_UPGRADE_COSTS = "bypassUpgradeCosts";
    public static final String P_BYPASS_UPGRADED = "bypassUpgraded";
    public static final String P_BYPASS_VERSION = "bypassVersion";
    public static final String P_BYPASS_ANZ = "bypassAnz";
    public static final String P_FRAGS = "frags";
    public static final String P_S_FRAGS = "sFrags";
    public static final String P_HEIST = "heist";
    public static final String P_SHUTDOWN_COSTS = "shutdownCosts";
    public static final String P_SHUTDOWN_CREATED = "shutdownCreated";
    public static final String P_SHUTDOWN_SCRIPTS = "shutdownScripts";
    public static final String P_SHUTDOWN_STRENGTH = "shutdownStrength";
    public static final String P_SHUTDOWN_UPGRADE_COSTS = "shutdownUpgradeCosts";
    public static final String P_SHUTDOWN_UPGRADED = "shutdownUpgraded";
    public static final String P_SHUTDOWN_VERSION = "shutdownVersion";
    public static final String P_SHUTDOWN_ANZ = "shutdownAnz";
    public static final String P_SMASH_COSTS = "smashCosts";
    public static final String P_SMASH_CREATED = "smashCreated";
    public static final String P_SMASH_SCRIPTS = "smashScripts";
    public static final String P_SMASH_STRENGTH = "smashStrength";
    public static final String P_SMASH_UPGRADE_COSTS = "smashUpgradeCosts";
    public static final String P_SMASH_UPGRADED = "smashUpgraded";
    public static final String P_SMASH_VERSION = "smashVersion";
    public static final String P_SMASH_ANZ = "smashAnz";
    public static final String P_CAN_1ON1 = "can1on1";
    public static final String P_MY_REPUTATION = "myReputation";
    public static final String P_WIN_RATE = "winRate";
    public static final String P_S_BRONZE_OPEN = "sBronzeOpen";
    public static final String P_BRONZE_PACKS = "bronzePacks";
    public static final String P_MY_LEAGUE = "myLeague";
    public static final String P_PVP_BOOSTS = "pvpBoosts";

    @AsciiRow(value = "AV Added", converter = AsciiBooleanConverter.class)
    @ResponseKey("avadded")
    private Integer avAdded;

    @AsciiRow(value = "FW Added", converter = AsciiBooleanConverter.class)
    @ResponseKey("fwadded")
    private Integer fwAdded;

    @AsciiRow(value = "Node Updated", converter = AsciiBooleanConverter.class)
    @ResponseKey("node_updated")
    private Integer nodeUpdated;

    @AsciiRow(value = "sPack Open", converter = AsciiBooleanConverter.class)
    @ResponseKey("sPackOpen")
    private Integer sPackOpen;

    @AsciiRow(value = "sPack Open All", converter = AsciiBooleanConverter.class)
    @ResponseKey("sPackOpenAll")
    private Integer sPackOpenAll;

    @AsciiRow(value = "Bought One", converter = AsciiBooleanConverter.class)
    @ResponseKey("boughtOne")
    private Integer boughtOne;

    @AsciiRow(value = "Bought Ten", converter = AsciiBooleanConverter.class)
    @ResponseKey("boughtTen")
    private Integer boughtTen;

    @AsciiRow("Packs Bought")
    @ResponseKey("packsBought")
    private Integer packsBought;

    @AsciiRow("Bought Limit")
    @ResponseKey("boughtLimit")
    private Integer boughtLimit;

    @AsciiRow("Server Pieces")
    @ResponseKey("server_str")
    private Integer serverPieces;

    @AsciiRow("AV1 Pieces")
    @ResponseKey("av1_str")
    private Integer av1Pieces;

    @AsciiRow("AV2 Pieces")
    @ResponseKey("av2_str")
    private Integer av2Pieces;

    @AsciiRow("AV3 Pieces")
    @ResponseKey("av3_str")
    private Integer av3Pieces;

    @AsciiRow("FW1 Pieces")
    @ResponseKey("fw1_str")
    private Integer fw1Pieces;

    @AsciiRow("FW2 Pieces")
    @ResponseKey("fw2_str")
    private Integer fw2Pieces;

    @AsciiRow("FW3 Pieces")
    @ResponseKey("fw3_str")
    private Integer fw3Pieces;

    @AsciiRow("Server Free Pieces")
    @ResponseKey("server_pieces")
    private Integer serverFreePieces;

    @AsciiRow("AV Free Pieces")
    @ResponseKey("av_pieces")
    private Integer avFreePieces;

    @AsciiRow("FW Free Pieces")
    @ResponseKey("fw_pieces")
    private Integer fwFreePieces;

    @AsciiRow("Packs")
    @ResponseKey("packs")
    private Integer packs;

    @AsciiRow("Server Max Pieces")
    @ResponseKey("server_str_max")
    private Integer serverMaxPieces;

    @AsciiRow("Server Stars")
    @ResponseKey("server_str_stars")
    private Integer serverStars;

    @AsciiRow("AV1 Max Pieces")
    @ResponseKey("av1_str_max")
    private Integer av1MaxPieces;

    @AsciiRow("AV1 Stars")
    @ResponseKey("av1_str_stars")
    private Integer av1Stars;

    @AsciiRow("AV2 Max Pieces")
    @ResponseKey("av2_str_max")
    private Integer av2MaxPieces;

    @AsciiRow("AV2 Stars")
    @ResponseKey("av2_str_stars")
    private Integer av2Stars;

    @AsciiRow("AV3 Max Pieces")
    @ResponseKey("av3_str_max")
    private Integer av3MaxPieces;

    @AsciiRow("AV3 Stars")
    @ResponseKey("av3_str_stars")
    private Integer av3Stars;

    @AsciiRow("FW1 Max Pieces")
    @ResponseKey("fw1_str_max")
    private Integer fw1MaxPieces;

    @AsciiRow("FW1 Stars")
    @ResponseKey("fw1_str_stars")
    private Integer fw1Stars;

    @AsciiRow("FW2 Max Pieces")
    @ResponseKey("fw2_str_max")
    private Integer fw2MaxPieces;

    @AsciiRow("FW2 Stars")
    @ResponseKey("fw2_str_stars")
    private Integer fw2Stars;

    @AsciiRow("FW3 Max Pieces")
    @ResponseKey("fw3_str_max")
    private Integer fw3MaxPieces;

    @AsciiRow("FW3 Stars")
    @ResponseKey("fw3_str_stars")
    private Integer fw3Stars;

    @AsciiRow("AV Nodes")
    @ResponseKey("avnodes")
    private Integer avNodes;

    @AsciiRow("FW Nodes")
    @ResponseKey("fwnodes")
    private Integer fwNodes;

    @AsciiRow(value = "Next Pack In", converter = AsciiElapsedSecondsTimeConverter.class)
    @ResponseKey("nextpackin")
    private Long nextPackIn;

    @AsciiRow(value = "Node Type", converter = AsciiServerNodeTypeConverter.class)
    @ResponseKey("node_type")
    private Integer nodeType;

    @AsciiRow("Node Number")
    @ResponseKey("node_number")
    private Integer nodeNumber;

    @AsciiRow("sPack Type")
    @ResponseKey("sPackType")
    private Integer sPackType;

    @AsciiRow("sHow Much")
    @ResponseKey("sHowMuch")
    private Integer sHowMuch;

    @AsciiRow("sServer")
    @ResponseKey("sServer")
    private String sServer;

    @AsciiRow("sAV")
    @ResponseKey("sAV")
    private String sAV;

    @AsciiRow("sFW")
    @ResponseKey("sFW")
    private String sFW;

    @AsciiRow("sBoost")
    @ResponseKey("sBoost")
    private String sBoost;

    @AsciiRow("Bypass Costs")
    @ResponseKey("bypass_costs")
    private String bypassCosts;

    @AsciiRow(value = "Bypass Created", converter = AsciiBooleanConverter.class)
    @ResponseKey("bypass_created")
    private Integer bypassCreated;

    @AsciiRow("Bypass Scripts")
    @ResponseKey("bypass_scripts")
    private Integer bypassScripts;

    @AsciiRow("Bypass Strength")
    @ResponseKey("bypass_str")
    private Integer bypassStrength;

    @AsciiRow("Bypass Upgrade Costs")
    @ResponseKey("bypass_upgrade_costs")
    private Integer bypassUpgradeCosts;

    @AsciiRow(value = "Bypass Upgraded", converter = AsciiServerUpgradeScriptStateConverter.class)
    @ResponseKey("bypass_upgraded")
    private Integer bypassUpgraded;

    @AsciiRow("Bypass Version")
    @ResponseKey("bypass_version")
    private String bypassVersion;

    @AsciiRow("Bypass ANZ")
    @ResponseKey("bypass_anz")
    private Integer bypassAnz;

    @AsciiRow("Frags")
    @ResponseKey("frags")
    private Integer frags;

    @AsciiRow("sFrags")
    @ResponseKey("sFrags")
    private Integer sFrags;

    @AsciiRow("Heist")
    @ResponseKey("heist")
    private Integer heist;

    @AsciiRow("Shutdown Costs")
    @ResponseKey("shutdown_costs")
    private String shutdownCosts;

    @AsciiRow(value = "Shutdown Created", converter = AsciiBooleanConverter.class)
    @ResponseKey("shutdown_created")
    private Integer shutdownCreated;

    @AsciiRow("Shutdown Scripts")
    @ResponseKey("shutdown_scripts")
    private Integer shutdownScripts;

    @AsciiRow("Shutdown Strength")
    @ResponseKey("shutdown_str")
    private Integer shutdownStrength;

    @AsciiRow("Shutdown Upgrade Costs")
    @ResponseKey("shutdown_upgrade_costs")
    private Integer shutdownUpgradeCosts;

    @AsciiRow(value = "Shutdown Upgraded", converter = AsciiServerUpgradeScriptStateConverter.class)
    @ResponseKey("shutdown_upgraded")
    private Integer shutdownUpgraded;

    @AsciiRow("Shutdown Version")
    @ResponseKey("shutdown_version")
    private String shutdownVersion;

    @AsciiRow("Shutdown ANZ")
    @ResponseKey("shutdown_anz")
    private Integer shutdownAnz;

    @AsciiRow("Smash Costs")
    @ResponseKey("smash_costs")
    private String smashCosts;

    @AsciiRow(value = "Smash Created", converter = AsciiBooleanConverter.class)
    @ResponseKey("smash_created")
    private Integer smashCreated;

    @AsciiRow("Smash Scripts")
    @ResponseKey("smash_scripts")
    private Integer smashScripts;

    @AsciiRow("Smash Strength")
    @ResponseKey("smash_str")
    private Integer smashStrength;

    @AsciiRow("Smash Upgrade Costs")
    @ResponseKey("smash_upgrade_costs")
    private Integer smashUpgradeCosts;

    @AsciiRow(value = "Smash Upgraded", converter = AsciiServerUpgradeScriptStateConverter.class)
    @ResponseKey("smash_upgraded")
    private Integer smashUpgraded;

    @AsciiRow("Smash Version")
    @ResponseKey("smash_version")
    private String smashVersion;

    @AsciiRow("Smash ANZ")
    @ResponseKey("smash_anz")
    private Integer smashAnz;

    @AsciiRow(value = "Can 1on1", converter = AsciiBooleanConverter.class)
    @ResponseKey("can_1on1")
    private Integer can1on1;

    @AsciiRow("My Reputation")
    @ResponseKey("myrep")
    private Integer myReputation;

    @AsciiRow("Win Rate")
    @ResponseKey("winrate")
    private String winRate;

    @AsciiRow(value = "Bronze Open", converter = AsciiBooleanConverter.class)
    @ResponseKey("sBronzeOpen")
    private Integer sBronzeOpen;

    @AsciiRow("Bronze Packs")
    @ResponseKey("bronze_packs")
    private Integer bronzePacks;

    @AsciiRow("My League")
    @ResponseKey("myleague")
    private String myLeague;

    @AsciiRow("PvP Boosts")
    @ResponseKey("pvpBoosts")
    private List<ServerPvPBoostData> pvpBoosts;

    public ServerResponse(OpcodeRequestResp response) {
        super(response);
    }

    public Integer getBypassAnz() {
        return bypassAnz;
    }

    public void setBypassAnz(Integer bypassAnz) {
        this.bypassAnz = bypassAnz;
    }

    public Integer getsFrags() {
        return sFrags;
    }

    public void setsFrags(Integer sFrags) {
        this.sFrags = sFrags;
    }

    public Integer getShutdownAnz() {
        return shutdownAnz;
    }

    public void setShutdownAnz(Integer shutdownAnz) {
        this.shutdownAnz = shutdownAnz;
    }

    public Integer getSmashAnz() {
        return smashAnz;
    }

    public void setSmashAnz(Integer smashAnz) {
        this.smashAnz = smashAnz;
    }

    public Integer getCan1on1() {
        return can1on1;
    }

    public void setCan1on1(Integer can1on1) {
        this.can1on1 = can1on1;
    }

    public Integer getMyReputation() {
        return myReputation;
    }

    public void setMyReputation(Integer myReputation) {
        this.myReputation = myReputation;
    }

    public String getWinRate() {
        return winRate;
    }

    public void setWinRate(String winRate) {
        this.winRate = winRate;
    }

    public Integer getAvAdded() {
        return avAdded;
    }

    public void setAvAdded(Integer avAdded) {
        this.avAdded = avAdded;
    }

    public Integer getFwAdded() {
        return fwAdded;
    }

    public void setFwAdded(Integer fwAdded) {
        this.fwAdded = fwAdded;
    }

    public Integer getNodeUpdated() {
        return nodeUpdated;
    }

    public void setNodeUpdated(Integer nodeUpdated) {
        this.nodeUpdated = nodeUpdated;
    }

    public Integer getsPackOpen() {
        return sPackOpen;
    }

    public void setsPackOpen(Integer sPackOpen) {
        this.sPackOpen = sPackOpen;
    }

    public Integer getsPackOpenAll() {
        return sPackOpenAll;
    }

    public void setsPackOpenAll(Integer sPackOpenAll) {
        this.sPackOpenAll = sPackOpenAll;
    }

    public Integer getBoughtOne() {
        return boughtOne;
    }

    public void setBoughtOne(Integer boughtOne) {
        this.boughtOne = boughtOne;
    }

    public Integer getBoughtTen() {
        return boughtTen;
    }

    public void setBoughtTen(Integer boughtTen) {
        this.boughtTen = boughtTen;
    }

    public Integer getPacksBought() {
        return (packsBought != null) ? packsBought : 0;
    }

    public void setPacksBought(Integer packsBought) {
        this.packsBought = packsBought;
    }

    public Integer getBoughtLimit() {
        return (boughtLimit != null) ? boughtLimit : 0;
    }

    public void setBoughtLimit(Integer boughtLimit) {
        this.boughtLimit = boughtLimit;
    }

    public Integer getServerPieces() {
        return serverPieces;
    }

    public void setServerPieces(Integer serverPieces) {
        this.serverPieces = serverPieces;
    }

    public Integer getAv1Pieces() {
        return av1Pieces;
    }

    public void setAv1Pieces(Integer av1Pieces) {
        this.av1Pieces = av1Pieces;
    }

    public Integer getAv2Pieces() {
        return av2Pieces;
    }

    public void setAv2Pieces(Integer av2Pieces) {
        this.av2Pieces = av2Pieces;
    }

    public Integer getAv3Pieces() {
        return av3Pieces;
    }

    public void setAv3Pieces(Integer av3Pieces) {
        this.av3Pieces = av3Pieces;
    }

    public Integer getFw1Pieces() {
        return fw1Pieces;
    }

    public void setFw1Pieces(Integer fw1Pieces) {
        this.fw1Pieces = fw1Pieces;
    }

    public Integer getFw2Pieces() {
        return fw2Pieces;
    }

    public void setFw2Pieces(Integer fw2Pieces) {
        this.fw2Pieces = fw2Pieces;
    }

    public Integer getFw3Pieces() {
        return fw3Pieces;
    }

    public void setFw3Pieces(Integer fw3Pieces) {
        this.fw3Pieces = fw3Pieces;
    }

    public Integer getServerFreePieces() {
        return serverFreePieces;
    }

    public void setServerFreePieces(Integer serverFreePieces) {
        this.serverFreePieces = serverFreePieces;
    }

    public Integer getAvFreePieces() {
        return avFreePieces;
    }

    public void setAvFreePieces(Integer avFreePieces) {
        this.avFreePieces = avFreePieces;
    }

    public Integer getFwFreePieces() {
        return fwFreePieces;
    }

    public void setFwFreePieces(Integer fwFreePieces) {
        this.fwFreePieces = fwFreePieces;
    }

    public Integer getPacks() {
        return (packs != null) ? packs : 0;
    }

    public void setPacks(Integer packs) {
        this.packs = packs;
    }

    public Integer getServerMaxPieces() {
        return serverMaxPieces;
    }

    public void setServerMaxPieces(Integer serverMaxPieces) {
        this.serverMaxPieces = serverMaxPieces;
    }

    public Integer getServerStars() {
        return serverStars;
    }

    public void setServerStars(Integer serverStars) {
        this.serverStars = serverStars;
    }

    public Integer getAv1MaxPieces() {
        return av1MaxPieces;
    }

    public void setAv1MaxPieces(Integer av1MaxPieces) {
        this.av1MaxPieces = av1MaxPieces;
    }

    public Integer getAv1Stars() {
        return av1Stars;
    }

    public void setAv1Stars(Integer av1Stars) {
        this.av1Stars = av1Stars;
    }

    public Integer getAv2MaxPieces() {
        return av2MaxPieces;
    }

    public void setAv2MaxPieces(Integer av2MaxPieces) {
        this.av2MaxPieces = av2MaxPieces;
    }

    public Integer getAv2Stars() {
        return av2Stars;
    }

    public void setAv2Stars(Integer av2Stars) {
        this.av2Stars = av2Stars;
    }

    public Integer getAv3MaxPieces() {
        return av3MaxPieces;
    }

    public void setAv3MaxPieces(Integer av3MaxPieces) {
        this.av3MaxPieces = av3MaxPieces;
    }

    public Integer getAv3Stars() {
        return av3Stars;
    }

    public void setAv3Stars(Integer av3Stars) {
        this.av3Stars = av3Stars;
    }

    public Integer getFw1MaxPieces() {
        return fw1MaxPieces;
    }

    public void setFw1MaxPieces(Integer fw1MaxPieces) {
        this.fw1MaxPieces = fw1MaxPieces;
    }

    public Integer getFw1Stars() {
        return fw1Stars;
    }

    public void setFw1Stars(Integer fw1Stars) {
        this.fw1Stars = fw1Stars;
    }

    public Integer getFw2MaxPieces() {
        return fw2MaxPieces;
    }

    public void setFw2MaxPieces(Integer fw2MaxPieces) {
        this.fw2MaxPieces = fw2MaxPieces;
    }

    public Integer getFw2Stars() {
        return fw2Stars;
    }

    public void setFw2Stars(Integer fw2Stars) {
        this.fw2Stars = fw2Stars;
    }

    public Integer getFw3MaxPieces() {
        return fw3MaxPieces;
    }

    public void setFw3MaxPieces(Integer fw3MaxPieces) {
        this.fw3MaxPieces = fw3MaxPieces;
    }

    public Integer getFw3Stars() {
        return fw3Stars;
    }

    public void setFw3Stars(Integer fw3Stars) {
        this.fw3Stars = fw3Stars;
    }

    public Integer getAvNodes() {
        return avNodes;
    }

    public void setAvNodes(Integer avNodes) {
        this.avNodes = avNodes;
    }

    public Integer getFwNodes() {
        return fwNodes;
    }

    public void setFwNodes(Integer fwNodes) {
        this.fwNodes = fwNodes;
    }

    public Long getNextPackIn() {
        return nextPackIn;
    }

    public void setNextPackIn(Long nextPackIn) {
        this.nextPackIn = nextPackIn;
    }

    public Integer getNodeType() {
        return nodeType;
    }

    public void setNodeType(Integer nodeType) {
        this.nodeType = nodeType;
    }

    public Integer getNodeNumber() {
        return nodeNumber;
    }

    public void setNodeNumber(Integer nodeNumber) {
        this.nodeNumber = nodeNumber;
    }

    public Integer getsPackType() {
        return sPackType;
    }

    public void setsPackType(Integer sPackType) {
        this.sPackType = sPackType;
    }

    public Integer getsHowMuch() {
        return sHowMuch;
    }

    public void setsHowMuch(Integer sHowMuch) {
        this.sHowMuch = sHowMuch;
    }

    public String getsServer() {
        return sServer;
    }

    public void setsServer(String sServer) {
        this.sServer = sServer;
    }

    public String getsAV() {
        return sAV;
    }

    public void setsAV(String sAV) {
        this.sAV = sAV;
    }

    public String getsFW() {
        return sFW;
    }

    public void setsFW(String sFW) {
        this.sFW = sFW;
    }

    public String getsBoost() {
        return sBoost;
    }

    public void setsBoost(String sBoost) {
        this.sBoost = sBoost;
    }

    public String getBypassCosts() {
        return bypassCosts;
    }

    public void setBypassCosts(String bypassCosts) {
        this.bypassCosts = bypassCosts;
    }

    public Integer getBypassCreated() {
        return bypassCreated;
    }

    public void setBypassCreated(Integer bypassCreated) {
        this.bypassCreated = bypassCreated;
    }

    public Integer getBypassScripts() {
        return (bypassScripts != null) ? bypassScripts : 0;
    }

    public void setBypassScripts(Integer bypassScripts) {
        this.bypassScripts = bypassScripts;
    }

    public Integer getBypassStrength() {
        return bypassStrength;
    }

    public void setBypassStrength(Integer bypassStrength) {
        this.bypassStrength = bypassStrength;
    }

    public Integer getBypassUpgradeCosts() {
        return (bypassUpgradeCosts != null) ? bypassUpgradeCosts : Integer.MAX_VALUE;
    }

    public void setBypassUpgradeCosts(Integer bypassUpgradeCosts) {
        this.bypassUpgradeCosts = bypassUpgradeCosts;
    }

    public Integer getBypassUpgraded() {
        return bypassUpgraded;
    }

    public void setBypassUpgraded(Integer bypassUpgraded) {
        this.bypassUpgraded = bypassUpgraded;
    }

    public String getBypassVersion() {
        return bypassVersion;
    }

    public void setBypassVersion(String bypassVersion) {
        this.bypassVersion = bypassVersion;
    }

    public Integer getFrags() {
        return (frags != null) ? frags : 0;
    }

    public void setFrags(Integer frags) {
        this.frags = frags;
    }

    public Integer getHeist() {
        return heist;
    }

    public void setHeist(Integer heist) {
        this.heist = heist;
    }

    public String getShutdownCosts() {
        return shutdownCosts;
    }

    public void setShutdownCosts(String shutdownCosts) {
        this.shutdownCosts = shutdownCosts;
    }

    public Integer getShutdownCreated() {
        return shutdownCreated;
    }

    public void setShutdownCreated(Integer shutdownCreated) {
        this.shutdownCreated = shutdownCreated;
    }

    public Integer getShutdownScripts() {
        return (shutdownScripts != null) ? shutdownScripts : 0;
    }

    public void setShutdownScripts(Integer shutdownScripts) {
        this.shutdownScripts = shutdownScripts;
    }

    public Integer getShutdownStrength() {
        return (shutdownStrength != null) ? shutdownStrength : 0;
    }

    public void setShutdownStrength(Integer shutdownStrength) {
        this.shutdownStrength = shutdownStrength;
    }

    public Integer getShutdownUpgradeCosts() {
        return (shutdownUpgradeCosts != null) ? shutdownUpgradeCosts : Integer.MAX_VALUE;
    }

    public void setShutdownUpgradeCosts(Integer shutdownUpgradeCosts) {
        this.shutdownUpgradeCosts = shutdownUpgradeCosts;
    }

    public Integer getShutdownUpgraded() {
        return shutdownUpgraded;
    }

    public void setShutdownUpgraded(Integer shutdownUpgraded) {
        this.shutdownUpgraded = shutdownUpgraded;
    }

    public String getShutdownVersion() {
        return shutdownVersion;
    }

    public void setShutdownVersion(String shutdownVersion) {
        this.shutdownVersion = shutdownVersion;
    }

    public String getSmashCosts() {
        return smashCosts;
    }

    public void setSmashCosts(String smashCosts) {
        this.smashCosts = smashCosts;
    }

    public Integer getSmashCreated() {
        return smashCreated;
    }

    public void setSmashCreated(Integer smashCreated) {
        this.smashCreated = smashCreated;
    }

    public Integer getSmashScripts() {
        return (smashScripts != null) ? smashScripts : 0;
    }

    public void setSmashScripts(Integer smashScripts) {
        this.smashScripts = smashScripts;
    }

    public Integer getSmashStrength() {
        return (smashStrength != null) ? smashStrength : 0;
    }

    public void setSmashStrength(Integer smashStrength) {
        this.smashStrength = smashStrength;
    }

    public Integer getSmashUpgradeCosts() {
        return (smashUpgradeCosts != null) ? smashUpgradeCosts : Integer.MAX_VALUE;
    }

    public void setSmashUpgradeCosts(Integer smashUpgradeCosts) {
        this.smashUpgradeCosts = smashUpgradeCosts;
    }

    public Integer getSmashUpgraded() {
        return smashUpgraded;
    }

    public void setSmashUpgraded(Integer smashUpgraded) {
        this.smashUpgraded = smashUpgraded;
    }

    public String getSmashVersion() {
        return smashVersion;
    }

    public void setSmashVersion(String smashVersion) {
        this.smashVersion = smashVersion;
    }

    public Integer getsBronzeOpen() {
        return sBronzeOpen;
    }

    public void setsBronzeOpen(Integer sBronzeOpen) {
        this.sBronzeOpen = sBronzeOpen;
    }

    public Integer getBronzePacks() {
        return (bronzePacks != null) ? bronzePacks : 0;
    }

    public void setBronzePacks(Integer bronzePacks) {
        this.bronzePacks = bronzePacks;
    }

    public String getMyLeague() {
        return myLeague;
    }

    public void setMyLeague(String myLeague) {
        this.myLeague = myLeague;
    }

    public List<ServerPvPBoostData> getPvpBoosts() {
        return pvpBoosts;
    }

    public void setPvpBoosts(List<ServerPvPBoostData> pvpBoosts) {
        this.pvpBoosts = pvpBoosts;
    }
}
