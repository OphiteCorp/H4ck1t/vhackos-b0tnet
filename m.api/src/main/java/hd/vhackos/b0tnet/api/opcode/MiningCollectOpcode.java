package hd.vhackos.b0tnet.api.opcode;

import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;

/**
 * Vybere netcoins z mineru.
 */
public final class MiningCollectOpcode extends Opcode {

    @Override
    public OpcodeTargetType getTarget() {
        return OpcodeTargetType.MINING;
    }

    @Override
    public String getOpcodeValue() {
        return "200";
    }
}
