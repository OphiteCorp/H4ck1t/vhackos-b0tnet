package hd.vhackos.b0tnet.api.net.response.data;

import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiBooleanConverter;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiElapsedSecondsTimeConverter;

/**
 * Informace o pořázce.
 */
public final class DefaceData {

    public static final String P_DEFACE_ALREADY_HACKED = "defaceAlreadyHacked";
    public static final String P_DEFACE_BY = "defaceBy";
    public static final String P_DEFACE_FIREWALL = "defaceFirewall";
    public static final String P_DEFACE_IP = "defaceIp";
    public static final String P_DEFACE_TIME_LEFT = "defaceTimeLeft";
    public static final String P_DEFACE_VISITS = "defaceVisits";

    @AsciiRow(value = "Deface Already Hacked", converter = AsciiBooleanConverter.class)
    @ResponseKey("deface_already_hacked")
    private Integer defaceAlreadyHacked;

    @AsciiRow("Deface By")
    @ResponseKey("deface_by")
    private String defaceBy;

    @AsciiRow("Deface Firewall")
    @ResponseKey("deface_fw")
    private Integer defaceFirewall;

    @AsciiRow("Deface IP")
    @ResponseKey("deface_ip")
    private String defaceIp;

    @AsciiRow(value = "Deface Time Left", converter = AsciiElapsedSecondsTimeConverter.class)
    @ResponseKey("deface_time_left")
    private Long defaceTimeLeft;

    @AsciiRow("Deface Visits")
    @ResponseKey("deface_visits")
    private Integer defaceVisits;

    public Integer getDefaceAlreadyHacked() {
        return defaceAlreadyHacked;
    }

    public void setDefaceAlreadyHacked(Integer defaceAlreadyHacked) {
        this.defaceAlreadyHacked = defaceAlreadyHacked;
    }

    public String getDefaceBy() {
        return defaceBy;
    }

    public void setDefaceBy(String defaceBy) {
        this.defaceBy = defaceBy;
    }

    public Integer getDefaceFirewall() {
        return defaceFirewall;
    }

    public void setDefaceFirewall(Integer defaceFirewall) {
        this.defaceFirewall = defaceFirewall;
    }

    public String getDefaceIp() {
        return defaceIp;
    }

    public void setDefaceIp(String defaceIp) {
        this.defaceIp = defaceIp;
    }

    public Long getDefaceTimeLeft() {
        return defaceTimeLeft;
    }

    public void setDefaceTimeLeft(Long defaceTimeLeft) {
        this.defaceTimeLeft = defaceTimeLeft;
    }

    public Integer getDefaceVisits() {
        return defaceVisits;
    }

    public void setDefaceVisits(Integer defaceVisits) {
        this.defaceVisits = defaceVisits;
    }
}
