package hd.vhackos.b0tnet.api.net.response;

import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.base.Response;
import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.api.net.response.data.IpBruteData;
import hd.vhackos.b0tnet.api.net.response.data.IpScanData;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiBooleanConverter;

import java.util.List;

/**
 * Odpověď ze skenování IP.
 */
public final class NetworkScanResponse extends Response {

    public static final String P_TUTORIAL = "tutorial";
    public static final String P_EXPLOITS = "exploits";
    public static final String P_CONNECTION_COUNT = "connectionCount";
    public static final String P_IPS = "ips";
    public static final String KEY_IPS = "ips";
    public static final String P_BRUTED_IPS = "brutedIps";

    @AsciiRow(value = "Tutorial", converter = AsciiBooleanConverter.class)
    @ResponseKey("tutor")
    private Integer tutorial;

    @AsciiRow("Exploits")
    @ResponseKey("exploits")
    private Integer exploits;

    @AsciiRow("Connections")
    @ResponseKey("connectionCount")
    private Integer connectionCount;

    @AsciiRow("IPs")
    @ResponseKey(value = KEY_IPS)
    private List<IpScanData> ips;

    @AsciiRow("Bruted IPs")
    @ResponseKey(value = "cm")
    private List<IpBruteData> brutedIps;

    public NetworkScanResponse(OpcodeRequestResp response) {
        super(response);
    }

    public Integer getTutorial() {
        return tutorial;
    }

    public void setTutorial(Integer tutorial) {
        this.tutorial = tutorial;
    }

    public Integer getExploits() {
        return exploits;
    }

    public void setExploits(Integer exploits) {
        this.exploits = exploits;
    }

    public Integer getConnectionCount() {
        return connectionCount;
    }

    public void setConnectionCount(Integer connectionCount) {
        this.connectionCount = connectionCount;
    }

    public List<IpScanData> getIps() {
        return ips;
    }

    public void setIps(List<IpScanData> ips) {
        this.ips = ips;
    }

    public List<IpBruteData> getBrutedIps() {
        return brutedIps;
    }

    public void setBrutedIps(List<IpBruteData> brutedIps) {
        this.brutedIps = brutedIps;
    }
}
