package hd.vhackos.b0tnet.api.opcode;

import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeType;

/**
 * Získá HTML response pro kolo štěstí.
 */
public final class WheelEventOpcode extends Opcode {

    private static final String PARAM_UID = "uid";
    private static final String PARAM_ACCESS_TOKEN = "accesstoken";
    private static final String PARAM_SUBMIT = "submit";

    /**
     * Vytvoří novou instanci.
     */
    public WheelEventOpcode() {
        addParam(3, PARAM_SUBMIT, "SPIN");
    }

    /**
     * UID uživatele.
     */
    public void setUserUid(Integer uid) {
        addParam(1, PARAM_UID, uid.toString());
    }

    /**
     * Přístupový token uživatele.
     */
    public void setAccessToken(String accessToken) {
        addParam(2, PARAM_ACCESS_TOKEN, accessToken);
    }

    @Override
    public OpcodeTargetType getTarget() {
        return OpcodeTargetType.WHEEL;
    }

    @Override
    public OpcodeType getType() {
        return OpcodeType.HTTP;
    }

    @Override
    public boolean isStandaloneOpcode() {
        return true;
    }
}
