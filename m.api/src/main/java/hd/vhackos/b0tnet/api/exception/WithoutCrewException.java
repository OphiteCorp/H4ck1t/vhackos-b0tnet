package hd.vhackos.b0tnet.api.exception;

import hd.vhackos.b0tnet.shared.exception.B0tnetException;

/**
 * Uživatel není v žádné crew.
 */
public final class WithoutCrewException extends B0tnetException {

    public WithoutCrewException(String resultCode, String message) {
        super(resultCode, message);
    }
}
