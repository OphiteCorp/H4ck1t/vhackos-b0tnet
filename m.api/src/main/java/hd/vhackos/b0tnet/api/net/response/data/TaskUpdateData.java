package hd.vhackos.b0tnet.api.net.response.data;

import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiAppStoreTypeConverter;

/**
 * Informace o aktualizaci aplikace v táscích.
 */
public final class TaskUpdateData {

    public static final String P_TASK_ID = "taskId";
    public static final String P_APP_ID = "appId";
    public static final String P_START = "start";
    public static final String P_END = "end";
    public static final String P_NOW = "now";
    public static final String P_LEVEL = "level";

    @AsciiRow("Task ID")
    @ResponseKey("id")
    private Long taskId;

    @AsciiRow(value = "App ID", converter = AsciiAppStoreTypeConverter.class)
    @ResponseKey("appid")
    private Integer appId;

    @AsciiRow("Start")
    @ResponseKey("start")
    private Long start;

    @AsciiRow("End")
    @ResponseKey("end")
    private Long end;

    @AsciiRow("Now")
    @ResponseKey("now")
    private Long now;

    @AsciiRow("Level")
    @ResponseKey("level")
    private Integer level;

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getEnd() {
        return (end != null) ? end : 0;
    }

    public void setEnd(Long end) {
        this.end = end;
    }

    public Long getNow() {
        return (now != null) ? now : 0;
    }

    public void setNow(Long now) {
        this.now = now;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}
