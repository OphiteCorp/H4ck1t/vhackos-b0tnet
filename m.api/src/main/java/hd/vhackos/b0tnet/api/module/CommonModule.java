package hd.vhackos.b0tnet.api.module;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.exception.AccountBlockedException;
import hd.vhackos.b0tnet.api.exception.InvalidLoginException;
import hd.vhackos.b0tnet.api.exception.RegisterException;
import hd.vhackos.b0tnet.api.exception.UserAccountNotExistsException;
import hd.vhackos.b0tnet.api.module.base.Module;
import hd.vhackos.b0tnet.api.module.base.ModuleHelper;
import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.LeaderboardsResponse;
import hd.vhackos.b0tnet.api.net.response.LoginResponse;
import hd.vhackos.b0tnet.api.net.response.UpdateResponse;
import hd.vhackos.b0tnet.api.opcode.*;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.shared.utils.EncryptUtils;
import hd.vhackos.b0tnet.shared.utils.HashUtils;

import java.util.Collections;
import java.util.Map;

/**
 * Modul pro přihlášení uživatele.
 */
@Inject
public final class CommonModule extends Module {

    private static final String ERR_CODE_ACCOUNT_BANNED = "1";
    private static final String ERR_CODE_USER_ALREADY_EXISTS = "1";
    private static final String ERR_CODE_INVALID_LOGIN = "2";
    private static final String ERR_CODE_EMAIL_IS_ALREADY_USED = "3";

    protected CommonModule(IB0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Odešle vlastní request na server.
     */
    public synchronized OpcodeRequestResp raw(String opcodeType, int uid, String tokenId, Map<String, String> params) {

        var target = OpcodeTargetType.getByCode(opcodeType);
        if (target == null) {
            throw new IllegalStateException("Invalid destination opcode type: " + opcodeType);
        }
        var connData = getB0tnet().getConnectionData();
        var opcode = new RawOpcode(target);
        opcode.setUid(uid);
        opcode.setAccessToken(tokenId);
        opcode.setLanguage(connData.getLang());

        if (params != null) {
            opcode.setOtherParams(params);
        }
        return sendRequest(opcode);
    }

    /**
     * Přihlásí uživatele.
     */
    public synchronized LoginResponse login() {
        var rawPassword = EncryptUtils.decrypt(getConfig().getControlKey(), getConfig().getPassword());
        return login(getConfig().getUserName(), rawPassword);
    }

    /**
     * Přihlásí uživatele.
     */
    public synchronized LoginResponse login(String user, String password) {
        if (user == null) {
            getLogger().error("User is not filled!");
        }
        if (password == null) {
            getLogger().error("Password is not filled!");
        }
        var opcode = new LoginOpcode();
        opcode.setUserName(user);
        opcode.setPasswordHash(HashUtils.toHexMd5(password));

        OpcodeRequestResp response = sendRequest(opcode);
        var responseMap = response.getResponse();
        var result = getResultValue(responseMap);

        if (ERR_CODE_ACCOUNT_BANNED.equals(result)) {
            var login = getB0tnet().getConnectionData().getUserName();
            throw new AccountBlockedException(login, "Your account with username '" + login + "' " + "was blocked");

        } else if (ERR_CODE_INVALID_LOGIN.equals(result)) {
            throw new InvalidLoginException(result, "Invalid username or password");
        }
        var dto = createLoginResponse(response);
        var connData = getB0tnet().getConnectionData();
        connData.setUserName(dto.getUserName());
        connData.setAccessToken(dto.getAccessToken());
        connData.setUid(dto.getUid());

        return dto;
    }

    /**
     * Zaregistruje nového uživatele.
     */
    public synchronized LoginResponse register(String userName, String password, String email) {
        if (userName == null) {
            getLogger().error("User name is not filled!");
        }
        if (password == null) {
            getLogger().error("Password is not filled!");
        }
        if (email == null) {
            getLogger().error("Email is not filled!");
        }
        var opcode = new RegisterOpcode();
        opcode.setUserName(userName);
        opcode.setPasswordHash(HashUtils.toHexMd5(password));
        opcode.setEmail(email);
        opcode.setLanguage(getB0tnet().getConnectionData().getLang());

        try {
            OpcodeRequestResp response = sendRequest(opcode);
            var responseMap = response.getResponse();
            var result = getResultValue(responseMap);

            if (ERR_CODE_USER_ALREADY_EXISTS.equals(result)) {
                throw new RegisterException(result, "The username '" + userName + "' is already used");

            } else if (ERR_CODE_EMAIL_IS_ALREADY_USED.equals(result)) {
                throw new RegisterException(result, "This email '" + email + "' is already in use");
            }
            return createLoginResponse(response);

        } catch (AccountBlockedException e) {
            throw new RegisterException(ERR_CODE_USER_ALREADY_EXISTS,
                    "The username '" + userName + "' is already used");
        }
    }

    /**
     * Získá aktuální informace o uživateli.
     */
    public synchronized UpdateResponse update() {
        var opcode = new UpdateOpcode();
        OpcodeRequestResp response = sendRequest(opcode);
        var responseMap = response.getResponse();
        var dto = new UpdateResponse(response);

        ModuleHelper.checkResponseIntegrity(responseMap, UpdateResponse.class);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_ACCESS_TOKEN);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_UID);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_USERNAME);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_EMAIL);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_EXPIRED);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_EASTER_EVENT);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_BLUE);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_GREEN);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_GREY);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_YELLOW);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_ORANGE);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_PURPLE);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_RED);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_TURKIS);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_WHITE);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_EGGS);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_NEW_MESSAGE);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_UNREAD_COUNT);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_EXPLOITS);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_EXPERIENCE);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_REQUIRED_EXPERIENCE);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_EXP_PC);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_NETCOINS);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_LEVEL);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_MONEY);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_IP);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_APP_FIREWALL);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_APP_ANTIVIRUS);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_APP_SDK);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_C_COLOR);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_APP_BRUTEFORCE);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_APP_SPAM);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_MALWARE_KIT);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_MODERATOR);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_CREW);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_MINER);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_TIME);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_SERVER);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_MINER_LEFT);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_CHAT_BAN);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_COM_COUNT);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_VIP);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_CREW_MSG_COUNT);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_NOTEPAD);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_LEADERBOARD);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_MISSIONS);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_JOBS);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_COMMUNITY);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_RUNNING_TASKS);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_INTERNET_CONNECTION);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_DEFACED);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_DEFACED_BY);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_VIP_LEFT);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_HEIST);
        ModuleHelper.setField(responseMap, dto, UpdateResponse.P_SPINS);

        return dto;
    }

    /**
     * Smaže účet uživatele.
     */
    public synchronized void deleteAccount(String uid, String accessToken, String password) {
        if (uid == null) {
            getLogger().error("UID is not filled!");
        }
        if (accessToken == null) {
            getLogger().error("Access token is not filled!");
        }
        if (password == null) {
            getLogger().error("Password is not filled!");
        }
        var opcode = new DeleteAccountOpcode();
        opcode.setUid(uid);
        opcode.setAccessToken(accessToken);
        opcode.setLanguage(getB0tnet().getConnectionData().getLang());
        opcode.setPasswordHash(HashUtils.toHexMd5(password));

        OpcodeRequestResp response = sendRequest(opcode);
        var innerResponse = response.getResponse();
        var result = innerResponse.get(null);

        if (result == null) {
            throw new UserAccountNotExistsException(null, "User account has not been found");
        }
    }

    /**
     * Smaže aktuální účet uživatele.
     */
    public synchronized void deleteAccount() {
        var connData = getB0tnet().getConnectionData();
        var rawPassword = EncryptUtils.decrypt(getConfig().getControlKey(), getConfig().getPassword());
        deleteAccount(String.valueOf(connData.getUid()), connData.getAccessToken(), rawPassword);
    }

    /**
     * Získá informace z leaderboards.
     */
    public synchronized LeaderboardsResponse getLeaderboards() {
        var opcode = new LeaderboardsOpcode();
        OpcodeRequestResp response = sendRequest(opcode);
        var responseMap = response.getResponse();
        var dto = new LeaderboardsResponse(response);

        ModuleHelper.checkResponseIntegrity(responseMap, LeaderboardsResponse.class);
        ModuleHelper.setField(responseMap, dto, LeaderboardsResponse.P_COUNT);
        ModuleHelper.setField(responseMap, dto, LeaderboardsResponse.P_MY_LEVEL);
        ModuleHelper.setField(responseMap, dto, LeaderboardsResponse.P_MY_EXP);
        ModuleHelper.setField(responseMap, dto, LeaderboardsResponse.P_MY_EXP_REQ);
        ModuleHelper.setField(responseMap, dto, LeaderboardsResponse.P_EXP_GAIN);
        ModuleHelper.setField(responseMap, dto, LeaderboardsResponse.P_TOURNAMENT_LEFT);
        ModuleHelper.setField(responseMap, dto, LeaderboardsResponse.P_TOURNAMENT_RANK);
        ModuleHelper.setField(responseMap, dto, LeaderboardsResponse.P_MY_RANK);
        ModuleHelper.setField(responseMap, dto, LeaderboardsResponse.P_CREW_COUNT);
        ModuleHelper.setField(responseMap, dto, LeaderboardsResponse.P_DAILY_CREW_RANK);
        ModuleHelper.setField(responseMap, dto, LeaderboardsResponse.P_DAILY_CREW_REPUTATION);
        ModuleHelper.setField(responseMap, dto, LeaderboardsResponse.P_DAILY_CREW_TIME_LEFT);
        ModuleHelper.setField(responseMap, dto, LeaderboardsResponse.P_MY_1ON1_REPUSTATION);
        ModuleHelper.setField(responseMap, dto, LeaderboardsResponse.P_MY_1ON1_RANK);

        if (!ModuleHelper.setField(responseMap, dto, LeaderboardsResponse.P_LEADERBOARD_DATA,
                (f, data) -> ModuleHelper.convertToLeaderboardData(data))) {
            dto.setLeaderboardData(Collections.emptyList());
        }
        if (!ModuleHelper.setField(responseMap, dto, LeaderboardsResponse.P_TOURNAMENT_DATA,
                (f, data) -> ModuleHelper.convertToTournament24HData(data))) {
            dto.setTournamentData(Collections.emptyList());
        }
        if (!ModuleHelper.setField(responseMap, dto, LeaderboardsResponse.P_CREWS_DATA,
                (f, data) -> ModuleHelper.convertToLeaderboardCrewData(data))) {
            dto.setCrewsData(Collections.emptyList());
        }
        if (!ModuleHelper.setField(responseMap, dto, LeaderboardsResponse.P_ONE_ON_ONE,
                (f, data) -> ModuleHelper.convertToLeaderboard1on1Data(data))) {
            dto.setCrewsData(Collections.emptyList());
        }
        return dto;
    }

    private LoginResponse createLoginResponse(OpcodeRequestResp response) {
        var responseMap = response.getResponse();
        var dto = new LoginResponse(response);

        ModuleHelper.checkResponseIntegrity(responseMap, LoginResponse.class);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_USER_NAME);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_ACCESS_TOKEN);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_UID);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_NOTEPAD);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_LEADERBOARD);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_MISSIONS);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_JOBS);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_COMMUNITY);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_EXPERIENCE);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_REQUIRED_EXPERIENCE);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_EXP_PC);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_NETCOINS);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_LEVEL);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_MONEY);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_IP);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_APP_ANTIVIRUS);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_APP_FIREWALL);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_APP_SDK);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_APP_BRUTEFORCE);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_APP_SPAM);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_C_COLOR);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_MALWARE_KIT);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_MODERATOR);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_EMAIL);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_CREW);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_MINER);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_TIME);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_SERVER);
        ModuleHelper.setField(responseMap, dto, LoginResponse.P_INTERNET_CONNECTION);
        return dto;
    }
}
