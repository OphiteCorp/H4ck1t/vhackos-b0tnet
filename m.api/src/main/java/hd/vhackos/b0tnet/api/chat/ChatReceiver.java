package hd.vhackos.b0tnet.api.chat;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.chat.dto.*;
import hd.vhackos.b0tnet.api.tool.SocketReceiver;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.shared.utils.SentryGuard;

import java.io.*;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Přijímá zprávy z chatu.
 */
@Inject
public final class ChatReceiver extends SocketReceiver<IChatClient, IChatListener> {

    // informace o herním IRC serveru
    private static final String IRC_SERVER = "54.37.205.154";
    private static final int IRC_PORT = 7531;
    private static final String DEFAULT_CHANNEL = "english";

    /**
     * Vytvoří novou instanci.
     */
    public ChatReceiver(IB0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Zahájí naslouchání chatu.
     */
    public void listen() {
        listen("Connecting to chat...", "vHack Chat Client", new ChatClient());
    }

    public void sendCommand(String command) {
        if (client != null && isReady()) {
            client.sendCommand(command);
        }
    }

    public void sendMessage(String message) {
        if (client != null && isReady()) {
            client.sendMessage(message);
            fireSentMessage(message);
        }
    }

    public void sendMessageToUser(String toUser, String message) {
        if (client != null && isReady()) {
            var msg = String.format("@%s %s", toUser, message);
            client.sendMessage(msg);
            fireSentMessage(msg);
        }
    }

    public void sendMessage(String channel, String message) {
        if (client != null && isReady()) {
            client.sendMessage(channel, message);
            fireSentMessage(message);
        }
    }

    public void sendMessageToUser(String channel, String toUser, String message) {
        if (client != null && isReady()) {
            var msg = String.format("@%s %s", toUser, message);
            client.sendMessage(channel, msg);
            fireSentMessage(msg);
        }
    }

    private void fireIncomingMessage(UserMessage message) {
        for (IChatListener listener : listeners) {
            listener.incomingMessage(message);
        }
    }

    private void fireIncomingUserList(List<ChatUser> users) {
        for (IChatListener listener : listeners) {
            listener.incomingUserList(users);
        }
    }

    private void fireSentMessage(String message) {
        for (IChatListener listener : listeners) {
            listener.sentMessage(message);
        }
    }

    private void fireUserJoin(JoinedUser user) {
        for (IChatListener listener : listeners) {
            listener.userJoin(user);
        }
    }

    private void fireUserQuit(ModeUser user) {
        for (IChatListener listener : listeners) {
            listener.userQuit(user);
        }
    }

    private void fireUserMode(ModeUser user) {
        for (IChatListener listener : listeners) {
            listener.userMode(user);
        }
    }

    private void fireReadRaw(String line) {
        for (IChatListener listener : listeners) {
            listener.readRaw(line);
        }
    }

    /**
     * Implementace chat klienta.
     */
    private final class ChatClient extends BasicClient implements IChatClient {

        @Override
        public void run() {
            final var userName = b0tnet.getConnectionData().getUserName();
            final var uid = b0tnet.getConnectionData().getUid();

            try {
                getLog().info("Connecting to a chat server with user: {}", userName);
                socket = createConnection(b0tnet.getConfig().getChatProxyData(), IRC_SERVER, IRC_PORT);
                getLog().info("Connection to the chat server was successful");

                getLog().debug("Opening streams for IO operations");
                reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

                receiver = new Receiver();
                receiver.start();
                trySleep();

                send(writer, "NICK {0}", userName);
                trySleep();
                send(writer, "USER {0} 0 * :vHack.mobile User", uid);

                getLog().debug("Chat client has been started");
                receiver.join();
                getLog().info("Stopping chat client");

            } catch (ConnectException e) {
                getLog().error("Unable to join global chat. Please try again later or change the proxy server");

            } catch (IOException e) {
                getLog().error("There was an error connecting to the chat server", e);
                SentryGuard.log(e);

            } catch (InterruptedException e) {
                getLog().info("Chat client will be stopped");
            } finally {
                if (socket != null) {
                    close();
                }
            }
        }

        @Override
        public void sendCommand(String command) {
            try {
                send(writer, command);
            } catch (IOException e) {
                getLog().warn("There was an unexpected error when sending a custom command", e);
            }
        }

        @Override
        public void sendMessage(String channel, String message) {
            try {
                send(writer, "PRIVMSG #{0} :{1}", channel, message);
            } catch (IOException e) {
                getLog().error("There was an error sending the message", e);
                SentryGuard.logError(e.getMessage(), new Object[]{ channel, message });
            }
        }

        @Override
        public void sendMessage(String message) {
            sendMessage(DEFAULT_CHANNEL, message);
        }

        private void close() {
            getLog().debug("Closing the chat server connection");
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                // nic
            }
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                // nic
            }
            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (IOException e) {
                // nic
            }
        }

        /**
         * Přijímá zprávy a případně odesílá odpovědi.
         */
        private final class Receiver extends Thread {

            private final Pattern patternChannel = Pattern.compile(".@.#\\S+.:");
            private final Pattern patternJoin = Pattern.compile("(.+?)!(\\d+)@(.+?) JOIN.*");
            private final Pattern patternQuit = Pattern.compile("(.+?)!(\\d+)@(.+?) QUIT.*");
            private final Pattern patternMode = Pattern.compile("(.+?)!(\\d+)@(.+?) MODE.*");
            private final Pattern patternMessage = Pattern.compile("(.+?)!(\\d+)@(.+?) PRIVMSG.#(\\S+).*");
            private final Pattern patternMsgToUser = Pattern.compile("@(\\S+) (.+)");

            private Receiver() {
                setDaemon(true);
                setPriority(Thread.MIN_PRIORITY);
                setName("vHack Chat Receiver");
            }

            @Override
            public void run() {
                getLog().debug("Starting listening...");
                String line;

                try {
                    var connectedMembers = new ArrayList<ChatUser>();
                    var endOfMembersList = false;
                    StringBuilder nickSufix = new StringBuilder();

                    while ((line = reader.readLine()) != null) {
                        getLog().trace("Incoming message: {}", line);
                        fireReadRaw(line);

                        if (line.contains(":")) {
                            var tokens = readTokens(line);

                            // odpoví serveru na PING, aby nás neodpojil
                            if ("PING".equals(tokens.get(0))) {
                                send(writer, "PONG {0}", tokens.get(1));
                            }
                            // přihlásí uživatele do kanálu
                            if (line.contains("End of /MOTD command")) {
                                var cd = b0tnet.getConnectionData();
                                send(writer, "PRIVMSG vMobileGuard :.letmein {0} {1} {2}", cd.getUid(),
                                        cd.getAccessToken(), cd.getLang().toLowerCase());
                            }
                            // nick uživatele se již používá
                            if (line.contains("Nickname is already in use")) {
                                nickSufix.append("_");
                                send(writer, "NICK {0}", b0tnet.getConfig().getUserName() + nickSufix);
                            }
                            // získá všechny připojený členy
                            var m = patternChannel.matcher(line);
                            if (m.find()) {
                                var names = tokens.get(2);
                                var namesList = names.split(" ");
                                connectedMembers.addAll(resolveMembers(namesList));
                            }
                            // jsme úspěšně připojeni do chatu
                            if (endOfMembersList) {
                                if (line.contains(" JOIN ")) {
                                    var user = createJoinedUser(tokens);
                                    if (user != null) {
                                        fireUserJoin(user);
                                    }
                                }
                                if (line.contains(" QUIT ")) {
                                    var user = createQuitUser(tokens);
                                    if (user != null) {
                                        fireUserQuit(user);
                                    }
                                }
                                if (line.contains(" MODE ")) {
                                    var user = createModeUser(tokens);
                                    if (user != null) {
                                        fireUserMode(user);
                                    }
                                }
                                if (line.contains(" PRIVMSG ")) {
                                    var msg = createUserMessage(tokens);
                                    if (msg != null) {
                                        fireIncomingMessage(msg);
                                    }
                                }
                            }
                            if (line.contains("End of /NAMES list")) {
                                endOfMembersList = true;
                                connectedMembers.sort(ChatUser.COMPARATOR);
                                fireIncomingUserList(connectedMembers);
                                setReady(true);
                            }
                        }
                    }
                } catch (IOException e) {
                    if (e.getMessage() == null || !e.getMessage().contains("Stream closed")) {
                        getLog().error("There was an error reading the chat", e);
                        SentryGuard.log(e);
                    }
                } catch (Exception e) {
                    getLog().error("An unexpected error has occurred in chat", e);
                    SentryGuard.log(e);

                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            // nic
                        }
                    }
                }
            }

            private List<String> readTokens(String line) {
                var list = new ArrayList<String>();
                var tokens = line.split(":");

                for (var t : tokens) {
                    list.add(t.trim());
                }
                return list;
            }

            private List<ChatUser> resolveMembers(String[] members) {
                var list = new ArrayList<ChatUser>();

                for (var member : members) {
                    var user = new ChatUser();

                    for (var role : ChatRole.values()) {
                        if (member.startsWith(role.getMark())) {
                            user.setRole(role);
                            user.setName(member.substring(role.getMark().length()));
                            break;
                        }
                    }
                    list.add(user);
                }
                return list;
            }

            private JoinedUser createJoinedUser(List<String> tokens) {
                var message = tokens.get(1);
                var m = patternJoin.matcher(message);

                if (m.find()) {
                    var user = new JoinedUser();
                    user.setUser(m.group(1));
                    user.setUid(Integer.valueOf(m.group(2)));
                    user.setHost(m.group(3));
                    user.setChannel(tokens.get(2));
                    return user;
                }
                return null;
            }

            private ModeUser createQuitUser(List<String> tokens) {
                var message = tokens.get(1);
                var m = patternQuit.matcher(message);

                if (m.find()) {
                    return buildModeUser(m, tokens.get(2));
                }
                return null;
            }

            private ModeUser createModeUser(List<String> tokens) {
                var message = tokens.get(1);
                var m = patternMode.matcher(message);

                if (m.find()) {
                    return buildModeUser(m, tokens.get(2));
                }
                return null;
            }

            private UserMessage createUserMessage(List<String> tokens) {
                var message = tokens.get(1);
                var m = patternMessage.matcher(message);

                if (m.find()) {
                    var msg = new UserMessage();
                    msg.setUser(m.group(1));
                    msg.setUid(Integer.valueOf(m.group(2)));
                    msg.setHost(m.group(3));
                    msg.setChannel(m.group(4));

                    var msgPart = tokens.get(2);
                    var pm = patternMsgToUser.matcher(msgPart);
                    if (pm.find()) {
                        msg.setToUser(pm.group(1));
                        msg.setMessage(pm.group(2));
                    } else {
                        msg.setToUser(null);
                        msg.setMessage(msgPart);
                    }
                    return msg;
                }
                return null;
            }

            private ModeUser buildModeUser(Matcher m, String reason) {
                var user = new ModeUser();
                user.setUser(m.group(1));
                user.setUid(Integer.valueOf(m.group(2)));
                user.setHost(m.group(3));
                user.setReason(reason);
                return user;
            }
        }
    }
}
