package hd.vhackos.b0tnet.api.module;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.exception.UserProfileNotExistsException;
import hd.vhackos.b0tnet.api.module.base.Module;
import hd.vhackos.b0tnet.api.module.base.ModuleHelper;
import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.UserProfileResponse;
import hd.vhackos.b0tnet.api.opcode.AddFriendOpcode;
import hd.vhackos.b0tnet.api.opcode.UserProfileOpcode;
import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.shared.injection.Inject;

/**
 * Informace a správa profilu uživatele.
 */
@Inject
public final class ProfileModule extends Module {

    private static final String ERR_CODE_PROFILE_NOT_EXISTS = "1";

    protected ProfileModule(IB0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Získá informace o uživateli z profilu.
     */
    public synchronized UserProfileResponse getProfile(int userId) {
        var opcode = new UserProfileOpcode();
        opcode.setUserId(userId);

        return createUserProfileResponse(opcode, userId);
    }

    /**
     * Přidá uživatele mezi přátelé.
     */
    public synchronized UserProfileResponse addFriend(int userId) {
        var opcode = new AddFriendOpcode();
        opcode.setUserId(userId);

        return createUserProfileResponse(opcode, userId);
    }

    private UserProfileResponse createUserProfileResponse(Opcode opcode, int userId) {
        OpcodeRequestResp response = sendRequest(opcode);
        var responseMap = response.getResponse();
        var result = getResultValue(responseMap);

        if (ERR_CODE_PROFILE_NOT_EXISTS.equals(result)) {
            throw new UserProfileNotExistsException(result, "User profile with ID '" + userId + "' does not exist");
        }
        var dto = new UserProfileResponse(response);

        ModuleHelper.checkResponseIntegrity(responseMap, UserProfileResponse.class);
        ModuleHelper.setField(responseMap, dto, UserProfileResponse.P_REQUESTED);
        ModuleHelper.setField(responseMap, dto, UserProfileResponse.P_LEVEL);
        ModuleHelper.setField(responseMap, dto, UserProfileResponse.P_REGISTRATION_TIME);
        ModuleHelper.setField(responseMap, dto, UserProfileResponse.P_CREW_NAME);
        ModuleHelper.setField(responseMap, dto, UserProfileResponse.P_CREW_TAG);
        ModuleHelper.setField(responseMap, dto, UserProfileResponse.P_CREW_LOGO);
        ModuleHelper.setField(responseMap, dto, UserProfileResponse.P_CREW_REPUTATION);
        ModuleHelper.setField(responseMap, dto, UserProfileResponse.P_CREW_MEMBERS);
        ModuleHelper.setField(responseMap, dto, UserProfileResponse.P_GOT_CREW);
        ModuleHelper.setField(responseMap, dto, UserProfileResponse.P_FRIENDS_COUNT);
        ModuleHelper.setField(responseMap, dto, UserProfileResponse.P_VIP);
        ModuleHelper.setField(responseMap, dto, UserProfileResponse.P_FRIENDS);
        ModuleHelper.setField(responseMap, dto, UserProfileResponse.P_CREW_RANK);

        return dto;
    }
}
