package hd.vhackos.b0tnet.api.module;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.module.base.Module;
import hd.vhackos.b0tnet.api.module.base.ModuleHelper;
import hd.vhackos.b0tnet.api.net.IOpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.AppStoreResponse;
import hd.vhackos.b0tnet.api.opcode.AppStoreOpcode;
import hd.vhackos.b0tnet.api.opcode.BuyAllAppOpcode;
import hd.vhackos.b0tnet.api.opcode.BuyAppOpcode;
import hd.vhackos.b0tnet.api.opcode.BuyMenuAppOpcode;
import hd.vhackos.b0tnet.shared.dto.AppStoreType;
import hd.vhackos.b0tnet.shared.injection.Inject;

import java.util.Collections;

/**
 * Modul pro obchod aplikací.
 */
@Inject
public final class StoreModule extends Module {

    protected StoreModule(IB0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Získá aplikace v obchodu.
     */
    public synchronized AppStoreResponse getApps() {
        var opcode = new AppStoreOpcode();
        var response = sendRequest(opcode);
        return createAppStoreResponse(response);
    }

    /**
     * Zakoupí jednu aplikaci v obchodě.
     */
    public synchronized AppStoreResponse buyApp(AppStoreType app) {
        var opcode = new BuyAppOpcode();
        opcode.setApp(app);

        var response = sendRequest(opcode);
        return createAppStoreResponse(response);
    }

    /**
     * Zakoupí menu aplikaci v obchodě.
     */
    public synchronized AppStoreResponse buyMenuApp(AppStoreType app) {
        var opcode = new BuyMenuAppOpcode();
        opcode.setApp(app);

        var response = sendRequest(opcode);
        return createAppStoreResponse(response);
    }

    /**
     * Zakoupí všechny aplikace (dle volných tásků).
     */
    public synchronized AppStoreResponse buyAllApp(AppStoreType app) {
        var opcode = new BuyAllAppOpcode();
        opcode.setApp(app);

        var response = sendRequest(opcode);
        return createAppStoreResponse(response);
    }

    private AppStoreResponse createAppStoreResponse(IOpcodeRequestResp inputResponse) {
        OpcodeRequestResp response = (OpcodeRequestResp) inputResponse;
        var responseMap = response.getResponse();
        var dto = new AppStoreResponse(response);

        ModuleHelper.checkResponseIntegrity(responseMap, AppStoreResponse.class);
        ModuleHelper.setField(responseMap, dto, AppStoreResponse.P_LEVEL_UP);
        ModuleHelper.setField(responseMap, dto, AppStoreResponse.P_UPDATED);
        ModuleHelper.setField(responseMap, dto, AppStoreResponse.P_INSTALLED);
        ModuleHelper.setField(responseMap, dto, AppStoreResponse.P_FILLED);
        ModuleHelper.setField(responseMap, dto, AppStoreResponse.P_LEVEL);
        ModuleHelper.setField(responseMap, dto, AppStoreResponse.P_MONEY);
        ModuleHelper.setField(responseMap, dto, AppStoreResponse.P_SPAM);
        ModuleHelper.setField(responseMap, dto, AppStoreResponse.P_APP_COUNT);
        ModuleHelper.setField(responseMap, dto, AppStoreResponse.P_LEVEL_NEW);
        ModuleHelper.setField(responseMap, dto, AppStoreResponse.P_LEVEL_UP_REWARD);
        ModuleHelper.setField(responseMap, dto, AppStoreResponse.P_VIP);

        if (!ModuleHelper.setField(responseMap, dto, AppStoreResponse.P_APPS,
                (f, data) -> ModuleHelper.convertToAppStoreData(data))) {
            dto.setApps(Collections.emptyList());
        }
        return dto;
    }
}
