package hd.vhackos.b0tnet.api.net.response.data;

import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiElapsedTimeConverter;

/**
 * Informace o jedné zprávě.
 */
public final class MessengerData {

    public static final String P_MESSAGE = "message";
    public static final String P_REQUEST = "request";
    public static final String P_TIME = "time";
    public static final String P_UNREAD_COUNT = "unreadCount";
    public static final String P_WITH = "with";
    public static final String P_WITH_ID = "withId";

    @AsciiRow("Message")
    @ResponseKey("message")
    private String message;

    @AsciiRow("Request")
    @ResponseKey("request")
    private Integer request;

    @AsciiRow(value = "Time", converter = AsciiElapsedTimeConverter.class)
    @ResponseKey("time")
    private Long time;

    @AsciiRow("Unread Count")
    @ResponseKey("unreadCount")
    private Integer unreadCount;

    @AsciiRow("With")
    @ResponseKey("with")
    private String with;

    @AsciiRow("With ID")
    @ResponseKey("with_id")
    private Integer withId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getRequest() {
        return request;
    }

    public void setRequest(Integer request) {
        this.request = request;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Integer getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(Integer unreadCount) {
        this.unreadCount = unreadCount;
    }

    public String getWith() {
        return with;
    }

    public void setWith(String with) {
        this.with = with;
    }

    public Integer getWithId() {
        return withId;
    }

    public void setWithId(Integer withId) {
        this.withId = withId;
    }
}
