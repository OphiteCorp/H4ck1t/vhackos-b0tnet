package hd.vhackos.b0tnet.api.opcode;

import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;

/**
 * Odešle žádost do crew o připojení se.
 */
public final class CrewSendRequestOpcode extends Opcode {

    private static final String PARAM_CREW_TAG = "crew_tag";

    /**
     * Crew tag.
     */
    public void setCrewTag(String crewTag) {
        addParam(PARAM_CREW_TAG, crewTag);
    }

    @Override
    public OpcodeTargetType getTarget() {
        return OpcodeTargetType.CREW;
    }

    @Override
    public String getOpcodeValue() {
        return "100";
    }
}
