package hd.vhackos.b0tnet.api.net;

import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeType;

import java.util.Map;

/**
 * Odpověď na opcode ze serveru.
 */
public interface IOpcodeRequestResp {

    /**
     * Získá typ opcode, pro který byl požadavek zavolán.
     */
    OpcodeType getOpcodeType();

    /**
     * Získá typ cíle opcode.
     */
    OpcodeTargetType getOpcodeTargetType();

    /**
     * Původní URI, která se volala.
     */
    String getRawUri();

    /**
     * Verze API, která byla použita.
     */
    int getApi();

    /**
     * Získá parametry v URL.
     */
    Map<String, String> getParameters();
}
