package hd.vhackos.b0tnet.api.net;

import hd.vhackos.b0tnet.api.module.base.IModule;
import hd.vhackos.b0tnet.api.opcode.base.IOpcode;

/**
 * Factory pro zavolání opcode.
 */
public final class OpcodeRequestFactory {

    /**
     * Odešle opcode na herní server.
     */
    public static IOpcodeRequestResp sendOpcode(IOpcode opcode, IModule module) {
        IOpcodeRequest<?> request = null;

        switch (opcode.getType()) {
            case HTTP:
                request = new OpcodeHttpRequest(opcode);
                break;

            case STANDARD:
                request = new OpcodeRequest(opcode);
                break;
        }
        if (request != null) {
            return request.send(module);
        }
        throw new IllegalStateException(
                "An unexpected error occurred when evaluating the correct implementation of the request for the opcode");
    }
}
