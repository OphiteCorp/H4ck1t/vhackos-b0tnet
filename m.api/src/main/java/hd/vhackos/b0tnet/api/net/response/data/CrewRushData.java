package hd.vhackos.b0tnet.api.net.response.data;

import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;

/**
 * Crew zpráva.
 */
public final class CrewRushData {

    public static final String P_NAME = "name";
    public static final String P_TAG = "tag";
    public static final String P_RANK = "rank";
    public static final String P_SCORE = "score";

    @AsciiRow("Name")
    @ResponseKey("name")
    private String name;

    @AsciiRow("Tag")
    @ResponseKey("tag")
    private String tag;

    @AsciiRow("Rank")
    @ResponseKey("rank")
    private Integer rank;

    @AsciiRow("Score")
    @ResponseKey("score")
    private Long score;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Long getScore() {
        return score;
    }

    public void setScore(Long score) {
        this.score = score;
    }
}
