package hd.vhackos.b0tnet.api.opcode;

/**
 * Otevře všechny balíčky na serveru.
 */
public final class ServerOpenAllPackagesOpcode extends ServerOpenPackageOpcode {

    @Override
    public String getOpcodeValue() {
        return "2000";
    }
}
