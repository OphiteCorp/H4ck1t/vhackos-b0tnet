package hd.vhackos.b0tnet.api.module;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.module.base.Module;
import hd.vhackos.b0tnet.api.module.base.ModuleHelper;
import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.MessengerDetailResponse;
import hd.vhackos.b0tnet.api.net.response.MessengerResponse;
import hd.vhackos.b0tnet.api.opcode.MessengerListOpcode;
import hd.vhackos.b0tnet.api.opcode.MessengerMessageOpcode;
import hd.vhackos.b0tnet.shared.injection.Inject;

import java.util.Collections;

/**
 * Stará se o interní zprávy.
 */
@Inject
public final class MessengerModule extends Module {

    protected MessengerModule(IB0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Získá seznam všech zpráv.
     */
    public synchronized MessengerResponse getMessanger() {
        var opcode = new MessengerListOpcode();
        OpcodeRequestResp response = sendRequest(opcode);
        var responseMap = response.getResponse();
        var dto = new MessengerResponse(response);

        ModuleHelper.checkResponseIntegrity(responseMap, MessengerResponse.class);
        ModuleHelper.setField(responseMap, dto, MessengerResponse.P_ACCEPTED);
        ModuleHelper.setField(responseMap, dto, MessengerResponse.P_ADDED);
        ModuleHelper.setField(responseMap, dto, MessengerResponse.P_C_COUNT);
        ModuleHelper.setField(responseMap, dto, MessengerResponse.P_REMOVED);
        ModuleHelper.setField(responseMap, dto, MessengerResponse.P_SENT);

        if (!ModuleHelper.setField(responseMap, dto, MessengerResponse.P_MESSAGES,
                (f, data) -> ModuleHelper.convertToMessengerData(data))) {
            dto.setMessages(Collections.emptyList());
        }
        return dto;
    }

    /**
     * Získá informace o partnerské zprávě.
     */
    public synchronized MessengerDetailResponse getPartnerMessage(int partnerId) {
        var opcode = new MessengerMessageOpcode();
        opcode.setPartnerId(partnerId);

        OpcodeRequestResp response = sendRequest(opcode);
        var responseMap = response.getResponse();
        var dto = new MessengerDetailResponse(response);

        ModuleHelper.checkResponseIntegrity(responseMap, MessengerDetailResponse.class);
        ModuleHelper.setField(responseMap, dto, MessengerDetailResponse.P_ACCEPTED);
        ModuleHelper.setField(responseMap, dto, MessengerDetailResponse.P_ADDED);
        ModuleHelper.setField(responseMap, dto, MessengerDetailResponse.P_MSG_COUNT);
        ModuleHelper.setField(responseMap, dto, MessengerDetailResponse.P_REMOVED);
        ModuleHelper.setField(responseMap, dto, MessengerDetailResponse.P_SENT);

        if (!ModuleHelper.setField(responseMap, dto, MessengerDetailResponse.P_MESSAGES,
                (f, data) -> ModuleHelper.convertToMessengerMessageItemData(data))) {
            dto.setMessages(Collections.emptyList());
        }
        return dto;
    }
}
