package hd.vhackos.b0tnet.api.net.response.base;

import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;

/**
 * Odpověď ze serveru.
 */
public abstract class Response {

    private final OpcodeRequestResp raw;

    @ResponseKey("result")
    private String result;

    protected Response(OpcodeRequestResp raw) {
        this.raw = raw;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public OpcodeRequestResp getRaw() {
        return raw;
    }
}
