package hd.vhackos.b0tnet.api.module.base;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.IB0tnetConfig;
import hd.vhackos.b0tnet.api.exception.*;
import hd.vhackos.b0tnet.api.module.CommonModule;
import hd.vhackos.b0tnet.api.net.IOpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.OpcodeRequestFactory;
import hd.vhackos.b0tnet.api.opcode.base.IOpcode;
import hd.vhackos.b0tnet.shared.exception.B0tnetException;
import hd.vhackos.b0tnet.shared.exception.InvalidAccessTokenException;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.utils.SentryGuard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Základní implementace modulu.
 */
public abstract class Module implements IModule {

    private static final String OUTDATED_CODE = "10";
    private static final int MAX_OUTDATED_ERRORS = -1;

    private static final Object LOCK = new Object();

    private static volatile int outdatedErrorCounter = 0;

    private final Logger log;
    private final IB0tnet b0tnet;

    @Autowired
    private CommonModule commonModule;

    protected Module(IB0tnet b0tnet) {
        this.b0tnet = b0tnet;
        log = LoggerFactory.getLogger(getClass());
    }

    @Override
    public final IB0tnet getB0tnet() {
        return b0tnet;
    }

    protected final IB0tnetConfig getConfig() {
        return b0tnet.getConfig();
    }

    protected final Logger getLogger() {
        return log;
    }

    protected final CommonModule getCommonModule() {
        return commonModule;
    }

    /**
     * Získá hodnotu RESULT.
     */
    protected final String getResultValue(Map<String, Object> response) {
        if (response.containsKey("result")) {
            var result = response.get("result");
            return (result != null) ? result.toString() : (String) result;
        }
        return "";
    }

    /**
     * Odešle požadavek na server.
     */
    protected final <T extends IOpcodeRequestResp> T sendRequest(IOpcode opcode) {
        synchronized (LOCK) {
            log.debug("Sending request '{}->{}' for userName: {}", opcode.getTarget(), opcode.getOpcodeValue(),
                    getB0tnet().getConnectionData().getUserName());

            int maxAttempts = getB0tnet().getConfig().getMaxRequestAttempts();
            int attempts = getB0tnet().getConfig().getMaxRequestAttempts();
            Exception prevException = null;

            while (attempts > 0) {
                try {
                    Thread.sleep(getConfig().getSleepDelay());
                    var response = OpcodeRequestFactory.sendOpcode(opcode, this);

                    if (attempts != maxAttempts) {
                        log.info("The request to the server was finally sent");
                    }
                    return (T) response;

                } catch (InvalidResponseException e) {
                    SentryGuard.logWarning("Response: " + e.getResponse().getClass().getSimpleName(), e.getResponse());
                    throw e;

                } catch (InvalidRequestException e) {
                    if (e.getResultCode() != null && e.getResultCode().equals(OUTDATED_CODE)) {
                        if (outdatedErrorCounter > MAX_OUTDATED_ERRORS) {
                            getConfig().increaseGameApi();
                            return sendRequest(opcode);
                        } else {
                            outdatedErrorCounter++;
                        }
                    }
                    throw e;

                } catch (InvalidAccessTokenException e) {
                    prevException = e;
                    try {
                        log.info("Invalid access token. Getting a new...");
                        if (!getConfig().isAggressiveMode()) {
                            Thread.sleep(5000);
                        }
                        try {
                            var loginData = commonModule.login();
                            log.info("User '{}' was re-logged in", loginData.getUserName());
                            getB0tnet().reloginCallback(loginData);

                            if (!getConfig().isAggressiveMode()) {
                                Thread.sleep(3000);
                            }
                        } catch (InvalidLoginException le) {
                            throw le;

                        } catch (B0tnetException be) {
                            attempts--;
                            log.error("Login failed. Remaining attempts: {}/{}", attempts, maxAttempts);
                        }
                    } catch (InterruptedException ex) {
                        break;
                    }
                } catch (ConnectionException | ConnectionBindException e) {
                    prevException = e;
                    attempts--;
                    log.error(
                            "Request failed. Probably failed to establish communication with the server. Remaining attempts: {}/{}",
                            attempts, maxAttempts);
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException ex) {
                        break;
                    }
                } catch (InvalidResponseCodeException e) {
                    if (e.getResponseCode() == 404) {
                        prevException = e;
                        attempts--;
                        log.warn("The server is busy. Remaining attempts: {}/{}", attempts, maxAttempts);
                        try {
                            Thread.sleep(getConfig().isAggressiveMode() ? 5000 : 10000);
                        } catch (InterruptedException ex) {
                            break;
                        }
                    } else {
                        SentryGuard.log(e);
                    }
                } catch (InterruptedException e) {
                    break;
                }
            }
            throw new ConnectionException(null, "Could not send request to server", prevException);
        }
    }
}
