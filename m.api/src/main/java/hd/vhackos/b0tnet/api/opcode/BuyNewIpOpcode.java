package hd.vhackos.b0tnet.api.opcode;

import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;

/**
 * Zakoupí novou IP adresu (za 1000 netcoins).
 */
public final class BuyNewIpOpcode extends Opcode {

    @Override
    public OpcodeTargetType getTarget() {
        return OpcodeTargetType.BUY;
    }

    @Override
    public String getOpcodeValue() {
        return "200";
    }
}
