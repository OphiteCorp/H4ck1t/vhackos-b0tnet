package hd.vhackos.b0tnet.api.opcode;

/**
 * Sebere denní odměnu.
 */
public final class MissionsClaimDayOpcode extends MissionsOpcode {

    @Override
    public String getOpcodeValue() {
        return "100";
    }
}
