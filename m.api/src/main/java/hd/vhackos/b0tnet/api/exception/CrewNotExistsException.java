package hd.vhackos.b0tnet.api.exception;

import hd.vhackos.b0tnet.shared.exception.B0tnetException;

/**
 * Crew neexistuje.
 */
public final class CrewNotExistsException extends B0tnetException {

    public CrewNotExistsException(String resultCode, String message) {
        super(resultCode, message);
    }
}
