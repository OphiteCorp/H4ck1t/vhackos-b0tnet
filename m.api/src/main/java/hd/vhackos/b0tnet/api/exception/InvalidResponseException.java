package hd.vhackos.b0tnet.api.exception;

import hd.vhackos.b0tnet.shared.exception.B0tnetException;

/**
 * Server vrátil neplatnou odpověď.
 */
public final class InvalidResponseException extends B0tnetException {

    private Object response;

    public InvalidResponseException(Object response, String message) {
        super(null, message);
        this.response = response;
    }

    public Object getResponse() {
        return response;
    }
}
