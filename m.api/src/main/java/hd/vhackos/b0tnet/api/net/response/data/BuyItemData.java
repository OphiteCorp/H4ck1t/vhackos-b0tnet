package hd.vhackos.b0tnet.api.net.response.data;

import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiMoneyConverter;

/**
 * Informace o položce ke koupi za peníze nebo netcoins.
 */
public final class BuyItemData {

    public static final String P_SKU = "sku";
    public static final String P_PRICE = "price";
    public static final String P_TITLE = "title";
    public static final String P_AMOUNT = "amount";

    @AsciiRow("SKU")
    @ResponseKey("sku")
    private String sku;

    @AsciiRow(value = "Price", converter = AsciiMoneyConverter.class)
    @ResponseKey("price")
    private Integer price;

    @AsciiRow("Amount")
    @ResponseKey("amount")
    private String amount;

    @AsciiRow("Title")
    @ResponseKey("title")
    private String title;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
