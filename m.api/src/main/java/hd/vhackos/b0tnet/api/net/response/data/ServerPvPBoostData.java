package hd.vhackos.b0tnet.api.net.response.data;

import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiServerPvPBoostTypeConverter;

/**
 * Informace o boostu pro PvP.
 */
public final class ServerPvPBoostData {

    public static final String P_ACTIVE = "active";
    public static final String P_ID = "id";
    public static final String P_RARITY = "rarity";

    @AsciiRow("Active")
    @ResponseKey("active")
    private Integer active;

    @AsciiRow(value = "ID", converter = AsciiServerPvPBoostTypeConverter.class)
    @ResponseKey("id")
    private Integer id;

    @AsciiRow("Rarity")
    @ResponseKey("rarity")
    private Integer rarity;

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRarity() {
        return (rarity != null) ? rarity : 0;
    }

    public void setRarity(Integer rarity) {
        this.rarity = rarity;
    }
}
