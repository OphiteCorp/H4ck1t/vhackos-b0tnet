package hd.vhackos.b0tnet.api.opcode;

/**
 * Vylepší node na serveru o 5.
 */
public class ServerUpdateNode5xOpcode extends ServerUpdateNode1xOpcode {

    @Override
    public String getOpcodeValue() {
        return "600";
    }
}
