package hd.vhackos.b0tnet.api.exception;

import hd.vhackos.b0tnet.shared.exception.B0tnetException;

/**
 * Nastala chyba při získání nějakých informací ze vzdálené IP.
 */
public final class RemoteException extends B0tnetException {

    public RemoteException(String resultCode, String message) {
        super(resultCode, message);
    }
}
