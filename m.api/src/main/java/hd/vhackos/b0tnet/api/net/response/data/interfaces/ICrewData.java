package hd.vhackos.b0tnet.api.net.response.data.interfaces;

/**
 * Sdílené informace mezi daty v crew.
 */
public interface ICrewData {

    Long getLastOnline();

    Integer getLevel();

    Integer getPosition();

    Long getUserId();

    String getUserName();

}
