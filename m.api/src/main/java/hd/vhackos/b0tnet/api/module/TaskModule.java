package hd.vhackos.b0tnet.api.module;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.module.base.Module;
import hd.vhackos.b0tnet.api.module.base.ModuleHelper;
import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.TaskResponse;
import hd.vhackos.b0tnet.api.opcode.*;
import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.shared.injection.Inject;

import java.util.Collections;

/**
 * Modul pro práci s tásky.
 */
@Inject
public final class TaskModule extends Module {

    protected TaskModule(IB0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Získá všechny tásky.
     */
    public synchronized TaskResponse getTasks() {
        var opcode = new TaskOpcode();
        return createTaskResponse(opcode);
    }

    /**
     * Dokončí tásk za netcoins pro dané ID.
     */
    public synchronized TaskResponse finishForNetcoins(long taskId) {
        var opcode = new TaskFinishForNetCoinsOpcode();
        opcode.setTaskId(taskId);

        return createTaskResponse(opcode);
    }

    /**
     * Dokončí všechny tásky za netcoins pro dané ID.
     */
    public synchronized TaskResponse finishAllForNetcoins(long taskId) {
        var opcode = new TasksFinishForNetCoinsOpcode();
        opcode.setTaskId(taskId);

        return createTaskResponse(opcode);
    }

    /**
     * Použije boost na tásk.
     */
    public synchronized TaskResponse boostTask(long taskId) {
        var opcode = new TaskBoostOpcode();
        opcode.setTaskId(taskId);

        return createTaskResponse(opcode);
    }

    /**
     * Použije 5x boost na tásk.
     */
    public synchronized TaskResponse boost5xTask(long taskId) {
        var opcode = new TaskBoost5xOpcode();
        opcode.setTaskId(taskId);

        return createTaskResponse(opcode);
    }

    /**
     * Zruší bruteforce.
     */
    public synchronized TaskResponse abortBruteforce(long bruteId) {
        var opcode = new AbortBruteOpcode();
        opcode.setBruteforceId(bruteId);

        return createTaskResponse(opcode);
    }

    /**
     * Zruší aktualizaci tásku.
     */
    public synchronized TaskResponse abortTask(long taskId) {
        var opcode = new AbortTaskOpcode();
        opcode.setTaskId(taskId);

        return createTaskResponse(opcode);
    }

    /**
     * Dokončí bruteforce za netcoins.
     */
    public synchronized TaskResponse finishBruteforceForNetcoins(long bruteId) {
        var opcode = new FinishBruteForNetcoinsOpcode();
        opcode.setBruteforceId(bruteId);

        return createTaskResponse(opcode);
    }

    /**
     * Odstraní bruteforce z tásků.
     */
    public synchronized TaskResponse removeBruteforce(long bruteId) {
        var opcode = new RemoveBruteOpcode();
        opcode.setBruteforceId(bruteId);

        return createTaskResponse(opcode);
    }

    /**
     * Zkusí neůspěšný bruteforce znovu.
     */
    public synchronized TaskResponse retryBruteforce(String targetIp) {
        var opcode = new RetryBruteOpcode();
        opcode.setTargetIp(targetIp);

        return createTaskResponse(opcode);
    }

    private TaskResponse createTaskResponse(Opcode opcode) {
        OpcodeRequestResp response = sendRequest(opcode);
        var responseMap = response.getResponse();
        var dto = new TaskResponse(response);

        ModuleHelper.checkResponseIntegrity(responseMap, TaskResponse.class);
        ModuleHelper.setField(responseMap, dto, TaskResponse.P_FINISH_ALL);
        ModuleHelper.setField(responseMap, dto, TaskResponse.P_ABORTED);
        ModuleHelper.setField(responseMap, dto, TaskResponse.P_BRUTE_REMOVED);
        ModuleHelper.setField(responseMap, dto, TaskResponse.P_BRUTE_ABORTED);
        ModuleHelper.setField(responseMap, dto, TaskResponse.P_BRUTE_FINISHED);
        ModuleHelper.setField(responseMap, dto, TaskResponse.P_FINISHED);
        ModuleHelper.setField(responseMap, dto, TaskResponse.P_BRUTE_RETRY);
        ModuleHelper.setField(responseMap, dto, TaskResponse.P_BOOSTED);
        ModuleHelper.setField(responseMap, dto, TaskResponse.P_LEVEL_UP);
        ModuleHelper.setField(responseMap, dto, TaskResponse.P_UPDATE_COUNT);
        ModuleHelper.setField(responseMap, dto, TaskResponse.P_BRUTE_COUNT);
        ModuleHelper.setField(responseMap, dto, TaskResponse.P_NEXT_DONE);
        ModuleHelper.setField(responseMap, dto, TaskResponse.P_NEXT_DONE_2);
        ModuleHelper.setField(responseMap, dto, TaskResponse.P_LEVEL);
        ModuleHelper.setField(responseMap, dto, TaskResponse.P_NETCOINS);
        ModuleHelper.setField(responseMap, dto, TaskResponse.P_BOOSTERS);
        ModuleHelper.setField(responseMap, dto, TaskResponse.P_LEVEL_UP_REWARD);
        ModuleHelper.setField(responseMap, dto, TaskResponse.P_LEVEL_NEW);
        ModuleHelper.setField(responseMap, dto, TaskResponse.P_FINISH_ALL_COSTS);

        if (!ModuleHelper.setField(responseMap, dto, TaskResponse.P_BRUTED_IPS,
                (f, data) -> ModuleHelper.convertToIpBruteDetailData(data))) {
            dto.setBrutedIps(Collections.emptyList());
        }
        if (!ModuleHelper.setField(responseMap, dto, TaskResponse.P_UPDATES,
                (f, data) -> ModuleHelper.convertToTaskUpdateData(data))) {
            dto.setUpdates(Collections.emptyList());
        }
        return dto;
    }
}
