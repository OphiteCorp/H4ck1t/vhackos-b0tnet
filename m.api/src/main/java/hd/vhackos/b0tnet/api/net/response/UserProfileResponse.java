package hd.vhackos.b0tnet.api.net.response;

import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.base.Response;
import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiBooleanConverter;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiElapsedSecondsTimeConverter;

/**
 * Informace o uživateli.
 */
public final class UserProfileResponse extends Response {

    public static final String P_REQUESTED = "requested";
    public static final String P_LEVEL = "level";
    public static final String P_REGISTRATION_TIME = "registrationTime";
    public static final String P_CREW_NAME = "crewName";
    public static final String P_CREW_TAG = "crewTag";
    public static final String P_CREW_LOGO = "crewLogo";
    public static final String P_CREW_REPUTATION = "crewReputation";
    public static final String P_CREW_MEMBERS = "crewMembers";
    public static final String P_GOT_CREW = "gotCrew";
    public static final String P_FRIENDS_COUNT = "friendsCount";
    public static final String P_VIP = "vip";
    public static final String P_FRIENDS = "friends";
    public static final String P_CREW_RANK = "crewRank";

    @AsciiRow("Requested")
    @ResponseKey("requested")
    private Integer requested;

    @AsciiRow("Level")
    @ResponseKey("level")
    private Integer level;

    @AsciiRow(value = "Registration Time", converter = AsciiElapsedSecondsTimeConverter.class)
    @ResponseKey("regtime")
    private Long registrationTime;

    @AsciiRow("Crew Name")
    @ResponseKey("crew_name")
    private String crewName;

    @AsciiRow("Crew Tag")
    @ResponseKey("crew_tag")
    private String crewTag;

    @AsciiRow("Crew Logo")
    @ResponseKey("crew_logo")
    private String crewLogo;

    @AsciiRow("Crew Reputation")
    @ResponseKey("crew_rep")
    private Integer crewReputation;

    @AsciiRow("Crew Members")
    @ResponseKey("crew_members")
    private Integer crewMembers;

    @AsciiRow(value = "Got Crew", converter = AsciiBooleanConverter.class)
    @ResponseKey("gotcrew")
    private Integer gotCrew;

    @AsciiRow("Friends Count")
    @ResponseKey("friendsCount")
    private Integer friendsCount;

    @AsciiRow("Crew Rank")
    @ResponseKey("crew_rank")
    private Integer crewRank;

    @AsciiRow(value = "VIP", converter = AsciiBooleanConverter.class)
    @ResponseKey("vip")
    private Integer vip;

    @AsciiRow("Friends")
    @ResponseKey("friends")
    private Integer friends;

    public UserProfileResponse(OpcodeRequestResp response) {
        super(response);
    }

    public Integer getCrewRank() {
        return crewRank;
    }

    public void setCrewRank(Integer crewRank) {
        this.crewRank = crewRank;
    }

    public Integer getRequested() {
        return requested;
    }

    public void setRequested(Integer requested) {
        this.requested = requested;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Long getRegistrationTime() {
        return registrationTime;
    }

    public void setRegistrationTime(Long registrationTime) {
        this.registrationTime = registrationTime;
    }

    public String getCrewName() {
        return crewName;
    }

    public void setCrewName(String crewName) {
        this.crewName = crewName;
    }

    public String getCrewTag() {
        return crewTag;
    }

    public void setCrewTag(String crewTag) {
        this.crewTag = crewTag;
    }

    public String getCrewLogo() {
        return crewLogo;
    }

    public void setCrewLogo(String crewLogo) {
        this.crewLogo = crewLogo;
    }

    public Integer getCrewReputation() {
        return crewReputation;
    }

    public void setCrewReputation(Integer crewReputation) {
        this.crewReputation = crewReputation;
    }

    public Integer getCrewMembers() {
        return crewMembers;
    }

    public void setCrewMembers(Integer crewMembers) {
        this.crewMembers = crewMembers;
    }

    public Integer getGotCrew() {
        return gotCrew;
    }

    public void setGotCrew(Integer gotCrew) {
        this.gotCrew = gotCrew;
    }

    public Integer getFriendsCount() {
        return friendsCount;
    }

    public void setFriendsCount(Integer friendsCount) {
        this.friendsCount = friendsCount;
    }

    public Integer getVip() {
        return vip;
    }

    public void setVip(Integer vip) {
        this.vip = vip;
    }

    public Integer getFriends() {
        return friends;
    }

    public void setFriends(Integer friends) {
        this.friends = friends;
    }
}
