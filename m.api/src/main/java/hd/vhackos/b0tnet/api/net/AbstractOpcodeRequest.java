package hd.vhackos.b0tnet.api.net;

import hd.vhackos.b0tnet.api.dto.ProxyData;
import hd.vhackos.b0tnet.api.module.base.IModule;
import hd.vhackos.b0tnet.api.opcode.base.IOpcode;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.Map;

/**
 * Základní požadavek pro každý opcode.
 */
abstract class AbstractOpcodeRequest<T extends IOpcodeRequestResp> implements IOpcodeRequest<T> {

    static final String VHACK_URL = "https://api.vhack.cc/mobile/";

    private final Logger log;
    protected final IOpcode opcode;

    /**
     * Vytvoří novou instanci.
     */
    AbstractOpcodeRequest(IOpcode opcode) {
        this.opcode = opcode;
        log = LoggerFactory.getLogger(getClass());
    }

    /**
     * Získá instanci loggeru.
     */
    protected final Logger getLog() {
        return log;
    }

    @Override
    public final T send(IModule module) {
        getLog().debug("Sending request to opcode '{}' with opcode value: {}", opcode.getTarget(),
                opcode.getOpcodeValue());

        var params = opcode.getParams();
        if (!opcode.isStandaloneOpcode()) {
            injectInitParams(params, module);
        }
        getLog().debug("Params: {}", params);
        return sendOpcode(module, params);
    }

    /**
     * Odešle opcode na server.
     */
    abstract T sendOpcode(IModule module, Map<String, String> params);

    /**
     * Vytvoří HTTPS připojení.
     */
    final HttpsURLConnection createConnection(String uri, String userAgent, ProxyData proxyData, int connTimeout)
            throws IOException {

        var url = new URL(uri);
        HttpsURLConnection conn;

        if (proxyData != null) {
            var proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyData.getIp(), proxyData.getPort()));
            getLog().debug("A proxy with the address will be used: {}", proxy);
            conn = (HttpsURLConnection) url.openConnection(proxy);
        } else {
            conn = (HttpsURLConnection) url.openConnection();
        }
        conn.setRequestProperty("User-Agent", userAgent);
        conn.setRequestProperty("Accept-Encoding", "gzip");
        conn.setConnectTimeout(connTimeout);
        conn.setReadTimeout(connTimeout);
        return conn;
    }

    /**
     * Získá user-agent pro request.
     */
    final String getUserAgent(IModule module) {
        var connData = module.getB0tnet().getConnectionData();
        var userAgent = connData.getUserAgent();

        if (StringUtils.isEmpty(userAgent)) {
            userAgent = UserAgentGenerator.generateNew();
            connData.setUserAgent(userAgent);
        }
        return userAgent;
    }

    /**
     * Vloží do requestu další parametry (např. o přihlášeném uživateli).
     */
    private synchronized final void injectInitParams(Map<String, String> params, IModule module) {
        var connData = module.getB0tnet().getConnectionData();

        if (connData.getUid() != null && connData.getUid() > 0) {
            params.put("uid", connData.getUid().toString());
        }
        if (StringUtils.isNotEmpty(connData.getAccessToken())) {
            params.put("accesstoken", connData.getAccessToken());
        }
        if (StringUtils.isNotEmpty(connData.getLang())) {
            params.put("lang", connData.getLang());
        }
        if (opcode.getOpcodeValue() != null) {
            params.put("action", String.valueOf(opcode.getOpcodeValue()));
        }
    }
}
