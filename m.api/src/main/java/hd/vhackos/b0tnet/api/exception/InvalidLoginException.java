package hd.vhackos.b0tnet.api.exception;

import hd.vhackos.b0tnet.shared.exception.B0tnetException;

/**
 * Přihlášení se nezdařilo. Špatné uživatelské jméno nebo heslo.
 */
public final class InvalidLoginException extends B0tnetException {

    public InvalidLoginException(String resultCode, String message) {
        super(resultCode, message);
    }
}
