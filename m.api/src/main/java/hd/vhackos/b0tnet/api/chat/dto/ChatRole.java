package hd.vhackos.b0tnet.api.chat.dto;

/**
 * Role uživatele v chatu.
 */
public enum ChatRole {

    VOICE("+", "Voice", 3),
    OPERATOR("@", "Operator", 2),
    ADMIN("&", "Admin", 1),
    VIP("%", "VIP", 4),
    USER("", "User", 5); // musí být poslední

    private final String mark;
    private final String alias;
    private final int order;

    ChatRole(String mark, String alias, int order) {
        this.mark = mark;
        this.alias = alias;
        this.order = order;
    }

    public String getAlias() {
        return alias;
    }

    public String getMark() {
        return mark;
    }

    public int getOrder() {
        return order;
    }
}
