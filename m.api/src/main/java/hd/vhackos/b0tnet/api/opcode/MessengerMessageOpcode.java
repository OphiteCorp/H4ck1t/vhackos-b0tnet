package hd.vhackos.b0tnet.api.opcode;

import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.api.opcode.base.OpcodeTargetType;

/**
 * Získá interní zprávu podle ID.
 */
public final class MessengerMessageOpcode extends Opcode {

    private static final String PARAM_PARTNER_ID = "partner_id";

    /**
     * ID partnerské zprávy.
     */
    public void setPartnerId(int partnerId) {
        addParam(PARAM_PARTNER_ID, String.valueOf(partnerId));
    }

    @Override
    public OpcodeTargetType getTarget() {
        return OpcodeTargetType.MESSENGER;
    }

    @Override
    public String getOpcodeValue() {
        return "200";
    }
}
