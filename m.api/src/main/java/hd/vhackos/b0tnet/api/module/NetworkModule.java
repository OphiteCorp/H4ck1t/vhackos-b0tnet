package hd.vhackos.b0tnet.api.module;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.exception.ExploitException;
import hd.vhackos.b0tnet.api.exception.IpNotExistsException;
import hd.vhackos.b0tnet.api.module.base.Module;
import hd.vhackos.b0tnet.api.module.base.ModuleHelper;
import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.ExploitResponse;
import hd.vhackos.b0tnet.api.net.response.NetworkScanResponse;
import hd.vhackos.b0tnet.api.opcode.ExploitSystemOpcode;
import hd.vhackos.b0tnet.api.opcode.NetworkScanOpcode;
import hd.vhackos.b0tnet.shared.exception.InvalidAccessTokenException;
import hd.vhackos.b0tnet.shared.injection.Inject;
import org.apache.commons.lang3.StringUtils;

import java.util.Collections;

/**
 * Modul pro práci se sítí.
 */
@Inject
public final class NetworkModule extends Module {

    private static final String ERR_CODE_IP_NOT_EXISTS = "1";
    private static final String ERR_CODE_TOO_HIGH_FIREWALL = "3";
    private static final String ERR_CODE_IP_ALREADY_EXPLOITED = "5";

    protected NetworkModule(IB0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Vyhledá další sadu IP pro exploit.
     */
    public synchronized NetworkScanResponse scan() {
        var opcode = new NetworkScanOpcode();
        OpcodeRequestResp response = sendRequest(opcode);
        var responseMap = response.getResponse();
        var result = getResultValue(responseMap);

        // nestačí pouze kontrola resultu na null, protože pří správné odpovědi též vrací null, ale IPs musí
        // obsahovat vždy
        if (StringUtils.isEmpty(result) && responseMap.getOrDefault(NetworkScanResponse.KEY_IPS, null) == null) {
            throw new InvalidAccessTokenException(null, "Your access token is no longer valid, try signing in again");
        }
        var dto = new NetworkScanResponse(response);

        ModuleHelper.checkResponseIntegrity(responseMap, NetworkScanResponse.class);
        ModuleHelper.setField(responseMap, dto, NetworkScanResponse.P_TUTORIAL);
        ModuleHelper.setField(responseMap, dto, NetworkScanResponse.P_EXPLOITS);
        ModuleHelper.setField(responseMap, dto, NetworkScanResponse.P_CONNECTION_COUNT);
        ModuleHelper.setField(responseMap, dto, NetworkScanResponse.P_IPS,
                (f, data) -> ModuleHelper.convertToIpScanData(data));

        if (!ModuleHelper.setField(responseMap, dto, NetworkScanResponse.P_BRUTED_IPS,
                (f, data) -> ModuleHelper.convertToIpBruteData(data))) {
            dto.setBrutedIps(Collections.emptyList());
        }
        return dto;
    }

    /**
     * Exploituje cílovou IP.
     */
    public synchronized ExploitResponse exploit(String ip) {
        var opcode = new ExploitSystemOpcode();
        opcode.setTargetIp(ip);

        OpcodeRequestResp response = sendRequest(opcode);
        var responseMap = response.getResponse();
        var result = getResultValue(responseMap);

        if (ERR_CODE_IP_ALREADY_EXPLOITED.equals(result)) {
            throw new ExploitException(result, "The IP address '" + ip + "' is already being exploited");

        } else if (ERR_CODE_TOO_HIGH_FIREWALL.equals(result)) {
            throw new ExploitException(result, "The IP address '" + ip + "' has a higher firewall than your SDK");

        } else if (ERR_CODE_IP_NOT_EXISTS.equals(result)) {
            throw new IpNotExistsException(result, "The IP address '" + ip + "' no longer exists");
        }
        var dto = new ExploitResponse(response);

        ModuleHelper.checkResponseIntegrity(responseMap, ExploitResponse.class);
        ModuleHelper.setField(responseMap, dto, ExploitResponse.P_EXPLOITS);
        ModuleHelper.setField(responseMap, dto, ExploitResponse.P_CONNECTION_COUNT);

        if (!ModuleHelper.setField(responseMap, dto, ExploitResponse.P_BRUTED_IPS,
                (f, data) -> ModuleHelper.convertToIpBruteData(data))) {
            dto.setBrutedIps(Collections.emptyList());
        }
        return dto;
    }
}
