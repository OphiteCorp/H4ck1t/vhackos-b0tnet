package hd.vhackos.b0tnet.api.net.response.data;

import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiBruteStateConverter;
import hd.vhackos.b0tnet.shared.dto.BruteState;

/**
 * Informace o prolomené IP.
 */
public final class IpBruteData {

    public static final String P_IP = "ip";
    public static final String P_USER_NAME = "userName";
    public static final String P_BRUTE = "brute";

    @AsciiRow("IP")
    @ResponseKey("ip")
    private String ip;

    @AsciiRow("User")
    @ResponseKey("username")
    private String userName;

    @AsciiRow(value = "Brute", converter = AsciiBruteStateConverter.class)
    @ResponseKey("brute")
    private Integer brute;

    public BruteState getBruteState() {
        return BruteState.getbyState(brute);
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getBrute() {
        return brute;
    }

    public void setBrute(Integer brute) {
        this.brute = brute;
    }
}
