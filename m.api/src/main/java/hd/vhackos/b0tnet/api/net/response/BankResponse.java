package hd.vhackos.b0tnet.api.net.response;

import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.base.Response;
import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.api.net.response.data.BankTransactionData;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiBooleanConverter;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiMoneyConverter;

import java.util.List;

/**
 * Informace o vlastní bance.
 */
public final class BankResponse extends Response {

    public static final String P_FILLED = "filled";
    public static final String P_MONEY = "money";
    public static final String P_SAVINGS = "savings";
    public static final String P_USER_NAME = "userName";
    public static final String P_TOTAL = "total";
    public static final String KEY_TOTAL = "total";
    public static final String P_MAX_SAVINGS = "maxSavings";
    public static final String P_TRANSACTIONS_COUNT = "transactionsCount";
    public static final String P_TRANSACTIONS = "transactions";

    @AsciiRow(value = "Filled", converter = AsciiBooleanConverter.class)
    @ResponseKey("filled")
    private Integer filled;

    @AsciiRow(value = "Money", converter = AsciiMoneyConverter.class)
    @ResponseKey("money")
    private Long money;

    @AsciiRow(value = "Savings", converter = AsciiMoneyConverter.class)
    @ResponseKey("savings")
    private Long savings;

    @AsciiRow("User")
    @ResponseKey("username")
    private String userName;

    @AsciiRow(value = "Total", converter = AsciiMoneyConverter.class)
    @ResponseKey(KEY_TOTAL)
    private Long total;

    @AsciiRow(value = "Max Savings", converter = AsciiMoneyConverter.class)
    @ResponseKey("maxsavings")
    private Long maxSavings;

    @AsciiRow("Transactions Count")
    @ResponseKey("transcount")
    private Integer transactionsCount;

    @AsciiRow("Transactions")
    @ResponseKey("transactions")
    private List<BankTransactionData> transactions;

    public BankResponse(OpcodeRequestResp response) {
        super(response);
    }

    public Integer getFilled() {
        return filled;
    }

    public void setFilled(Integer filled) {
        this.filled = filled;
    }

    public Long getMoney() {
        return money;
    }

    public void setMoney(Long money) {
        this.money = money;
    }

    public Long getSavings() {
        return savings;
    }

    public void setSavings(Long savings) {
        this.savings = savings;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getMaxSavings() {
        return maxSavings;
    }

    public void setMaxSavings(Long maxSavings) {
        this.maxSavings = maxSavings;
    }

    public Integer getTransactionsCount() {
        return transactionsCount;
    }

    public void setTransactionsCount(Integer transactionsCount) {
        this.transactionsCount = transactionsCount;
    }

    public List<BankTransactionData> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<BankTransactionData> transactions) {
        this.transactions = transactions;
    }
}
