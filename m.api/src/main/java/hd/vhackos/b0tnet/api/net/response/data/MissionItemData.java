package hd.vhackos.b0tnet.api.net.response.data;

import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiMissionFinishedTypeConverter;

/**
 * Informace o misi.
 */
public final class MissionItemData {

    public static final String P_TITLE = "title";
    public static final String P_DESCRIPTION = "description";
    public static final String P_FINISHED = "finished";
    public static final String P_EXPERIENCE = "experience";
    public static final String P_REWARD_TYPE = "rewardType";
    public static final String P_REWARD_AMOUNT = "rewardAmount";

    @AsciiRow("Title")
    @ResponseKey("title")
    private String title;

    @AsciiRow("Description")
    @ResponseKey("descr")
    private String description;

    @AsciiRow(value = "Finished", converter = AsciiMissionFinishedTypeConverter.class)
    @ResponseKey("finished")
    private Integer finished;

    @AsciiRow("Experience")
    @ResponseKey("exp")
    private Integer experience;

    @AsciiRow("Reward Type")
    @ResponseKey("rewType")
    private String rewardType;

    @AsciiRow("Reward")
    @ResponseKey("rewAmount")
    private Integer rewardAmount;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getFinished() {
        return finished;
    }

    public void setFinished(Integer finished) {
        this.finished = finished;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public String getRewardType() {
        return rewardType;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public Integer getRewardAmount() {
        return rewardAmount;
    }

    public void setRewardAmount(Integer rewardAmount) {
        this.rewardAmount = rewardAmount;
    }
}
