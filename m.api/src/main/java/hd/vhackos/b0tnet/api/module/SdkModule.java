package hd.vhackos.b0tnet.api.module;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.module.base.Module;
import hd.vhackos.b0tnet.api.module.base.ModuleHelper;
import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.SdkResponse;
import hd.vhackos.b0tnet.api.opcode.SdkBuyOpcode;
import hd.vhackos.b0tnet.api.opcode.SdkOpcode;
import hd.vhackos.b0tnet.api.opcode.base.Opcode;
import hd.vhackos.b0tnet.shared.injection.Inject;

/**
 * Správa SDK pro exploit.
 */
@Inject
public final class SdkModule extends Module {

    protected SdkModule(IB0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Získá informace o SDK.
     */
    public synchronized SdkResponse getSdk() {
        var opcode = new SdkOpcode();
        return createSdkResponse(opcode);
    }

    /**
     * Koupí další SDK pro exploit.
     */
    public synchronized SdkResponse buySdk() {
        var opcode = new SdkBuyOpcode();
        return createSdkResponse(opcode);
    }

    private SdkResponse createSdkResponse(Opcode opcode) {
        OpcodeRequestResp response = sendRequest(opcode);
        var responseMap = response.getResponse();
        var dto = new SdkResponse(response);

        ModuleHelper.checkResponseIntegrity(responseMap, SdkResponse.class);
        ModuleHelper.setField(responseMap, dto, SdkResponse.P_APPLIED);
        ModuleHelper.setField(responseMap, dto, SdkResponse.P_SDK);
        ModuleHelper.setField(responseMap, dto, SdkResponse.P_EXPLOITS);
        ModuleHelper.setField(responseMap, dto, SdkResponse.P_NEXT_EXPLOIT);

        return dto;
    }
}
