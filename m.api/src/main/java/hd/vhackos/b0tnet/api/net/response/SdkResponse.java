package hd.vhackos.b0tnet.api.net.response;

import hd.vhackos.b0tnet.api.net.OpcodeRequestResp;
import hd.vhackos.b0tnet.api.net.response.base.Response;
import hd.vhackos.b0tnet.api.net.response.base.ResponseKey;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiBooleanConverter;
import hd.vhackos.b0tnet.shared.ascii.converter.AsciiElapsedSecondsTimeConverter;

/**
 * Odpověď serveru na získání SDK.
 */
public final class SdkResponse extends Response {

    public static final String P_APPLIED = "applied";
    public static final String P_SDK = "sdk";
    public static final String P_EXPLOITS = "exploits";
    public static final String P_NEXT_EXPLOIT = "nextExploit";

    @AsciiRow(value = "Applied", converter = AsciiBooleanConverter.class)
    @ResponseKey("applied")
    private Integer applied;

    @AsciiRow("SDK")
    @ResponseKey("sdk")
    private Integer sdk;

    @AsciiRow("Exploits")
    @ResponseKey("exploits")
    private Integer exploits;

    @AsciiRow(value = "Next Exploit", converter = AsciiElapsedSecondsTimeConverter.class)
    @ResponseKey("nextexploit")
    private Long nextExploit;

    public SdkResponse(OpcodeRequestResp response) {
        super(response);
    }

    public Integer getApplied() {
        return applied;
    }

    public void setApplied(Integer applied) {
        this.applied = applied;
    }

    public Integer getSdk() {
        return (sdk != null) ? sdk : 0;
    }

    public void setSdk(Integer sdk) {
        this.sdk = sdk;
    }

    public Integer getExploits() {
        return (exploits != null) ? exploits : 0;
    }

    public void setExploits(Integer exploits) {
        this.exploits = exploits;
    }

    public Long getNextExploit() {
        return nextExploit;
    }

    public void setNextExploit(Long nextExploit) {
        this.nextExploit = nextExploit;
    }
}
