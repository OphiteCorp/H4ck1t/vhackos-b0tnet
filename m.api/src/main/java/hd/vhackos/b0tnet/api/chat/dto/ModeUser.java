package hd.vhackos.b0tnet.api.chat.dto;

/**
 * Informace o uživateli, který opustil hru (chat) nebo se mu změnila oprávnění.
 */
public final class ModeUser extends AbstractMessage {

    private String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
