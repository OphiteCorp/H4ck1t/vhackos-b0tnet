package hd.vhackos.b0tnet.api.exception;

import hd.vhackos.b0tnet.shared.exception.B0tnetException;

/**
 * Server vrátil neplatnou odpověď.
 */
public final class InvalidRequestException extends B0tnetException {

    public InvalidRequestException(String resultCode, String message) {
        super(resultCode, message);
    }
}
