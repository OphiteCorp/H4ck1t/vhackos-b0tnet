package hd.vhackos.b0tnet.api;

import hd.vhackos.b0tnet.api.dto.ConnectionData;
import hd.vhackos.b0tnet.api.net.response.LoginResponse;
import hd.vhackos.b0tnet.shared.utils.Version;

/**
 * Rozhraní pro B0tnet.
 */
public interface IB0tnet {

    /**
     * Verze B0tnet.
     */
    Version VERSION = Version.create("1.1.4");

    /**
     * Konfigurace b0tnetu.
     */
    IB0tnetConfig getConfig();

    /**
     * Data, která drží informace o aktuálním připojení uživatele k serveru.
     */
    ConnectionData getConnectionData();

    /**
     * Akce po znovu přihlášení uživatele.
     */
    void reloginCallback(LoginResponse loginData);
}
