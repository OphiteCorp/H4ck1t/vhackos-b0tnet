package hd.vhackos.b0tnet.api.exception;

import hd.vhackos.b0tnet.shared.exception.B0tnetException;

/**
 * Uživatelský účet nebyl nalezen.
 */
public final class UserAccountNotExistsException extends B0tnetException {

    public UserAccountNotExistsException(String resultCode, String message) {
        super(resultCode, message);
    }
}
