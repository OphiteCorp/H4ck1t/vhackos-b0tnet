package hd.vhackos.b0tnet.api.exception;

/**
 * Účet uživatele byl zablokován.
 */
public final class AccountBlockedException extends RuntimeException {

    private final String userName;

    public AccountBlockedException(String userName, String message) {
        super(message);
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }
}
