package hd.vhackos.b0tnet.api.exception;

import hd.vhackos.b0tnet.shared.exception.B0tnetException;

/**
 * Nebylo možné zavolat dotaz na server, protože byl již zavolán.
 */
public final class ConnectionBindException extends B0tnetException {

    public ConnectionBindException(String message) {
        super(null, message);
    }

    public ConnectionBindException(String message, Throwable cause) {
        super(null, message, cause);
    }
}
