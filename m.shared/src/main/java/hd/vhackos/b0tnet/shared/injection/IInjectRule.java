package hd.vhackos.b0tnet.shared.injection;

/**
 * Pravidlo pro vytvoření instance třídy. Používá se přimárně v případě, že třídy obsahuje konstruktor bez parametrů.
 */
public interface IInjectRule {

    /**
     * Vytvoří instanci třídy.
     */
    Object createInstance(Class clazz) throws Exception;
}
