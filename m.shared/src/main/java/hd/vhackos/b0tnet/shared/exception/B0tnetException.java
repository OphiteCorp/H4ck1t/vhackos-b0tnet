package hd.vhackos.b0tnet.shared.exception;

/**
 * Hlavní vyjímka B0tnet.
 */
public class B0tnetException extends B0tnetCoreException {

    private final String resultCode;

    public B0tnetException(String resultCode) {
        super();
        this.resultCode = resultCode;
    }

    public B0tnetException(String resultCode, String message) {
        super(message);
        this.resultCode = resultCode;
    }

    public B0tnetException(String resultCode, String message, Throwable cause) {
        super(message, cause);
        this.resultCode = resultCode;
    }

    public String getResultCode() {
        return resultCode;
    }
}
