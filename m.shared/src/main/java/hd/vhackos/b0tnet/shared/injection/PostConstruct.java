package hd.vhackos.b0tnet.shared.injection;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Označí metodu, která se automaticky zavolá po vytvoření instance třídy. Tato metoda musí být bez parametru.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
@interface PostConstruct {

}
