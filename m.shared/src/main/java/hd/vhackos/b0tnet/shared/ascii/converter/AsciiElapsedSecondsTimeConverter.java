package hd.vhackos.b0tnet.shared.ascii.converter;

import hd.vhackos.b0tnet.shared.ascii.IAsciiConverter;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.shared.utils.SharedUtils;

/**
 * Převede čas v sekundach na čitelný formát.
 */
@Inject
public final class AsciiElapsedSecondsTimeConverter implements IAsciiConverter {

    @Override
    public Object convert(Object value) {
        if (value == null) {
            value = 0L;
        }
        var valueLong = Long.valueOf(String.valueOf(value));
        if (valueLong < 0) {
            valueLong = 0L;
        }
        return SharedUtils.toTimeFormat(valueLong * 1000);
    }
}
