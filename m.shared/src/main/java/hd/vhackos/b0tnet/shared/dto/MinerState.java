package hd.vhackos.b0tnet.shared.dto;

/**
 * Stav těžby netcoins.
 */
public enum MinerState {

    STOPPED(0, "Stopped"),
    RUNNING(1, "Running"),
    FINISHED(2, "Finished");

    private final int code;
    private final String alias;

    MinerState(int code, String alias) {
        this.code = code;
        this.alias = alias;
    }

    public static MinerState getByCode(int code) {
        for (var state : values()) {
            if (state.code == code) {
                return state;
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public String getAlias() {
        return alias;
    }
}
