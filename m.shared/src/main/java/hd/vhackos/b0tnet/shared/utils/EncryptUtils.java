package hd.vhackos.b0tnet.shared.utils;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

/**
 * Umožní encrypt/decrypt textového řetězce.
 */
public final class EncryptUtils {

    private static final Logger LOG = LoggerFactory.getLogger(EncryptUtils.class);

    private static final String ENCODING = "UTF8";
    private static final String ENCRYPTION_SCHEME = "DESede";
    private static final int DEFAULT_SECRET_LENGTH = 64;
    private static final String SECRET = "[vxbG*jeVpRk~Fe5hQvasPA#RhW;R!R%-{bfJD9`V6qP&XAoj)Ra!]+7U<X9zzAO"; // 64
    private static final SecretKey SECRET_KEY;

    static {
        SECRET_KEY = createSecretKey(SECRET);
    }

    /**
     * Encryptuje vstupní text pod vlastním heslem.
     */
    public static synchronized String encrypt(String secret, String inputString) {
        String encryptedString = null;
        try {
            var secretKey = StringUtils.isNotBlank(secret) ? createSecretKey(secret) : SECRET_KEY;
            var cipher = Cipher.getInstance(ENCRYPTION_SCHEME);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            byte[] text = inputString.getBytes(ENCODING);
            byte[] encryptedText = cipher.doFinal(text);
            encryptedString = Base64.getEncoder().encodeToString(encryptedText);

        } catch (Exception e) {
            LOG.error("Failed to encrypt text value", e);
        }
        return encryptedString;
    }

    /**
     * Encryptuje vstupní text pod výchozím heslem.
     */
    public static synchronized String encrypt(String inputString) {
        return encrypt(null, inputString);
    }

    /**
     * Decryptuje vstupní hodnotu pod vlastním heslem.
     */
    public static synchronized String decrypt(String secret, String inputString) {
        String decryptedText = null;
        try {
            var secretKey = StringUtils.isNotBlank(secret) ? createSecretKey(secret) : SECRET_KEY;
            var cipher = Cipher.getInstance(ENCRYPTION_SCHEME);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);

            byte[] encryptedText = Base64.getDecoder().decode(inputString);
            byte[] text = cipher.doFinal(encryptedText);
            decryptedText = new String(text);

        } catch (Exception e) {
            LOG.debug("Failed to decrypt text value", e);
        }
        return decryptedText;
    }

    /**
     * Decryptuje vstupní hodnotu pod výchozím heslem.
     */
    public static synchronized String decrypt(String inputString) {
        return decrypt(null, inputString);
    }

    /**
     * Vygeneruje náhodný tajný klíč s vlastní velikostí.
     */
    public static byte[] generateSecretBytes(int length) {
        try {
            byte[] bytes = new byte[length];
            SecureRandom.getInstanceStrong().nextBytes(bytes);
            return bytes;

        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("There was an error generating a random key", e);
        }
    }

    /**
     * Vygeneruje náhodný tajný klíč.
     */
    public static byte[] generateSecretBytes() {
        return generateSecretBytes(DEFAULT_SECRET_LENGTH);
    }

    /**
     * Vytvoří nový tajný klíč pro encrypt/decrypt.
     *
     * @param secret Min. délka je 24 znaků.
     */
    private static SecretKey createSecretKey(String secret) {
        try {
            var bytes = secret.getBytes(ENCODING);
            var spec = new DESedeKeySpec(bytes);
            var scheme = SecretKeyFactory.getInstance(ENCRYPTION_SCHEME);
            return scheme.generateSecret(spec);

        } catch (Exception e) {
            throw new IllegalStateException("Unable to create a secret key", e);
        }
    }
}
