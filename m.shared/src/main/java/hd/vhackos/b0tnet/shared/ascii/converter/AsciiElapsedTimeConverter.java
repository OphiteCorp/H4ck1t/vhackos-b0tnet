package hd.vhackos.b0tnet.shared.ascii.converter;

import hd.vhackos.b0tnet.shared.ascii.IAsciiConverter;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.shared.utils.SharedUtils;

/**
 * Převede čas v milisekundách na čitelný formát.
 */
@Inject
public final class AsciiElapsedTimeConverter implements IAsciiConverter {

    @Override
    public Object convert(Object value) {
        if (value == null) {
            return null;
        }
        long time = (long) value;
        return SharedUtils.toTimeFormat(time);
    }
}
