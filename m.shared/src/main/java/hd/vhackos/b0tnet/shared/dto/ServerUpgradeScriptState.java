package hd.vhackos.b0tnet.shared.dto;

/**
 * Stav vylepšení skriptu na serveru.
 */
public enum ServerUpgradeScriptState {

    NOTHING(0, "Nothing"),
    UPGRADED(1, "Yes"),
    IGNORED(2, "No");

    private final int code;
    private final String alias;

    ServerUpgradeScriptState(int code, String alias) {
        this.code = code;
        this.alias = alias;
    }

    public int getCode() {
        return code;
    }

    public String getAlias() {
        return alias;
    }

    public static ServerUpgradeScriptState getByCode(int code) {
        for (var state : values()) {
            if (state.code == code) {
                return state;
            }
        }
        return null;
    }
}
