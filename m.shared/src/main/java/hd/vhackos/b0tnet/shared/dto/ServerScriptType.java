package hd.vhackos.b0tnet.shared.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Typ skriptu na serveru.
 */
public enum ServerScriptType {

    SMASH(1, "smash", "Smash", true),
    BYPASS(2, "bypass", "Bypass", true),
    SHUTDOWN(3, "shutdown", "Shutdown", true);

    public static final List<String> PURCHASING_SCRIPT_CODES = ServerScriptType.getPurchasingScriptCodes();

    private final String alias;
    private final int code;
    private final String command;
    private final boolean purchasing;

    ServerScriptType(int code, String command, String alias, boolean purchasing) {
        this.code = code;
        this.command = command;
        this.alias = alias;
        this.purchasing = purchasing;
    }

    public static ServerScriptType getByCommand(String command) {
        for (var type : values()) {
            if (type.command.equalsIgnoreCase(command)) {
                return type;
            }
        }
        return null;
    }

    public static ServerScriptType getByCode(int code) {
        for (var type : values()) {
            if (type.getCode() == code) {
                return type;
            }
        }
        return null;
    }

    public static List<String> getCommands() {
        var list = new ArrayList<String>();
        for (var type : values()) {
            list.add(type.command);
        }
        return list;
    }

    private static List<String> getPurchasingScriptCodes() {
        var list = new ArrayList<String>();
        for (var type : values()) {
            if (type.purchasing) {
                list.add(type.getCommand());
            }
        }
        return list;
    }

    public String getAlias() {
        return alias;
    }

    public int getCode() {
        return code;
    }

    public String getCommand() {
        return command;
    }
}
