package hd.vhackos.b0tnet.shared.ascii.converter;

import hd.vhackos.b0tnet.shared.ascii.IAsciiConverter;
import hd.vhackos.b0tnet.shared.dto.CrewPositionType;
import hd.vhackos.b0tnet.shared.injection.Inject;

/**
 * Převede hodnotu pozice v crew na typ.
 */
@Inject
public final class AsciiCrewPositionTypeConverter implements IAsciiConverter {

    @Override
    public Object convert(Object value) {
        if (value == null) {
            return null;
        }
        return CrewPositionType.getbyPosition((int) value).getAlias();
    }
}
