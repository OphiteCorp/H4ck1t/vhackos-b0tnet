package hd.vhackos.b0tnet.shared.dto;

/**
 * Typ PvP boostu.
 */
public enum PvPBoostType {

    FW_EX(1, "FWEx"),
    AV_EX(2, "AVEx"),
    BW_PLUS(3, "BWPlus"),
    CORE_EX(4, "CoreEx");

    private final int id;
    private final String alias;

    PvPBoostType(int id, String alias) {
        this.id = id;
        this.alias = alias;
    }

    public static PvPBoostType getById(int id) {
        for (var type : values()) {
            if (type.getId() == id) {
                return type;
            }
        }
        return null;
    }

    public int getId() {
        return id;
    }

    public String getAlias() {
        return alias;
    }
}
