package hd.vhackos.b0tnet.shared.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Pomocné metody pro hashování.
 */
public final class HashUtils {

    private static final Logger LOG = LoggerFactory.getLogger(HashUtils.class);

    private static final byte EQUALS_SIGN = (byte) 61;
    public static final byte[] WEBSAFE_ALPHABET = new byte[]{
            (byte) 65, (byte) 66, (byte) 67, (byte) 68, (byte) 69, (byte) 70, (byte) 71, (byte) 72, (byte) 73,
            (byte) 74, (byte) 75, (byte) 76, (byte) 77, (byte) 78, (byte) 79, (byte) 80, (byte) 81, (byte) 82,
            (byte) 83, (byte) 84, (byte) 85, (byte) 86, (byte) 87, (byte) 88, (byte) 89, (byte) 90, (byte) 97,
            (byte) 98, (byte) 99, (byte) 100, (byte) 101, (byte) 102, (byte) 103, (byte) 104, (byte) 105, (byte) 106,
            (byte) 107, (byte) 108, (byte) 109, (byte) 110, (byte) 111, (byte) 112, (byte) 113, (byte) 114, (byte) 115,
            (byte) 116, (byte) 117, (byte) 118, (byte) 119, (byte) 120, (byte) 121, (byte) 122, (byte) 48, (byte) 49,
            (byte) 50, (byte) 51, (byte) 52, (byte) 53, (byte) 54, (byte) 55, (byte) 56, (byte) 57, (byte) 45,
            (byte) 95 };

    /**
     * Metoda přebrána z vHackOS.<br>
     * Vytvoří MD5 hash přes hex převod.
     */
    public static String toHexMd5(String string) {
        if (string == null) {
            return null;
        }
        try {
            var digest = MessageDigest.getInstance("MD5");
            digest.update(string.getBytes());

            var messageDigest = digest.digest();
            var hexString = new StringBuilder();

            for (var b : messageDigest) {
                var sb = new StringBuilder(Integer.toHexString(b & 255));

                while (sb.length() < 2) {
                    sb.insert(0, "0");
                }
                hexString.append(sb);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            LOG.error("Invalid digest method", e);
            return "";
        }
    }

    /**
     * Vytvoří SHA256.
     */
    public static String toSha256(byte[] bytes) {
        try {
            var digest = MessageDigest.getInstance("SHA-256");
            var hash = digest.digest(bytes);
            var sb = new StringBuilder();

            for (var b : hash) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1) {
                    sb.append('0');
                }
                sb.append(hex);
            }
            return sb.toString();

        } catch (Exception e) {
            throw new RuntimeException("There was an error creating SHA256", e);
        }
    }

    /**
     * Vytvoří base64.
     */
    public static String toBase64(byte[] source, int offset, int len, byte[] alphabet, boolean doPadding) {
        var outBuff = toBase64(source, offset, len, alphabet, Integer.MAX_VALUE);
        var outLen = outBuff.length;

        while (!doPadding && outLen > 0 && outBuff[outLen - 1] == EQUALS_SIGN) {
            outLen--;
        }
        return new String(outBuff, 0, outLen);
    }

    /**
     * Vytvoří base64.
     */
    private static byte[] toBase64(byte[] source, int offset, int len, byte[] alphabet, int maxLineLength) {
        var len43 = ((len + 2) / 3) * 4;
        var outBuff = new byte[((len43 / maxLineLength) + len43)];
        var d = 0;
        var e = 0;
        var len2 = len - 2;
        var lineLength = 0;

        while (d < len2) {
            var inBuff = (((source[d + offset] << 24) >>> 8) | ((source[(d + 1) + offset] << 24) >>> 16)) |
                         ((source[(d + 2) + offset] << 24) >>> 24);

            outBuff[e] = alphabet[inBuff >>> 18];
            outBuff[e + 1] = alphabet[(inBuff >>> 12) & 63];
            outBuff[e + 2] = alphabet[(inBuff >>> 6) & 63];
            outBuff[e + 3] = alphabet[inBuff & 63];
            lineLength += 4;

            if (lineLength == maxLineLength) {
                outBuff[e + 4] = (byte) 10;
                e++;
                lineLength = 0;
            }
            d += 3;
            e += 4;
        }
        if (d < len) {
            toBase64_3to4(source, d + offset, len - d, outBuff, e, alphabet);

            if (lineLength + 4 == maxLineLength) {
                outBuff[e + 4] = (byte) 10;
                e++;
            }
            e += 4;
        }
        if (e == outBuff.length) {
            return outBuff;
        }
        throw new AssertionError();
    }

    /**
     * Vytvoří base64.
     */
    private static byte[] toBase64_3to4(byte[] src, int srcOffset, int numSigBytes, byte[] dest, int destOffset,
            byte[] alphabet) {

        int i;
        var i2 = 0;

        if (numSigBytes > 0) {
            i = (src[srcOffset] << 24) >>> 8;
        } else {
            i = 0;
        }
        var i3 = (numSigBytes > 1 ? (src[srcOffset + 1] << 24) >>> 16 : 0) | i;

        if (numSigBytes > 2) {
            i2 = (src[srcOffset + 2] << 24) >>> 24;
        }
        var inBuff = i3 | i2;

        switch (numSigBytes) {
            case 1:
                dest[destOffset] = alphabet[inBuff >>> 18];
                dest[destOffset + 1] = alphabet[(inBuff >>> 12) & 63];
                dest[destOffset + 2] = EQUALS_SIGN;
                dest[destOffset + 3] = EQUALS_SIGN;
                break;
            case 2:
                dest[destOffset] = alphabet[inBuff >>> 18];
                dest[destOffset + 1] = alphabet[(inBuff >>> 12) & 63];
                dest[destOffset + 2] = alphabet[(inBuff >>> 6) & 63];
                dest[destOffset + 3] = EQUALS_SIGN;
                break;
            case 3:
                dest[destOffset] = alphabet[inBuff >>> 18];
                dest[destOffset + 1] = alphabet[(inBuff >>> 12) & 63];
                dest[destOffset + 2] = alphabet[(inBuff >>> 6) & 63];
                dest[destOffset + 3] = alphabet[inBuff & 63];
                break;
        }
        return dest;
    }
}
