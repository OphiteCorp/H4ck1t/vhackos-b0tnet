package hd.vhackos.b0tnet.shared.ascii.converter;

import hd.vhackos.b0tnet.shared.ascii.IAsciiConverter;
import hd.vhackos.b0tnet.shared.dto.MissionFinishedType;
import hd.vhackos.b0tnet.shared.injection.Inject;

/**
 * Získá název stavu mise podle kódu.
 */
@Inject
public final class AsciiMissionFinishedTypeConverter implements IAsciiConverter {

    @Override
    public Object convert(Object value) {
        return MissionFinishedType.getByCode((int) value).getAlias();
    }
}
