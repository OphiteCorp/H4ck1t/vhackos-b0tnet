package hd.vhackos.b0tnet.shared.ascii.converter;

import hd.vhackos.b0tnet.shared.ascii.IAsciiConverter;
import hd.vhackos.b0tnet.shared.injection.Inject;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Převede hodnotu času na formátovaný čas.
 */
@Inject
public final class AsciiDateConverter implements IAsciiConverter {

    private static final SimpleDateFormat SDF = new SimpleDateFormat("dd.MM.yyyy | HH:mm:ss");

    @Override
    public Object convert(Object value) {
        if (value == null) {
            return null;
        }
        var time = (long) value;
        var date = new Date(time * 1000);
        return SDF.format(date);
    }
}
