package hd.vhackos.b0tnet.shared.utils;

import hd.vhackos.b0tnet.shared.SharedConst;
import hd.vhackos.b0tnet.shared.exception.InvalidAccessTokenException;
import io.sentry.DefaultSentryClientFactory;
import io.sentry.SentryClient;
import io.sentry.SentryClientFactory;
import io.sentry.event.Event;
import io.sentry.event.EventBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.SocketException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Správa sentry pro online logování.
 */
public final class SentryGuard {

    private static final Logger LOG = LoggerFactory.getLogger(SentryGuard.class);

    private static final String DSN = "https://a9dda1d0f75e423286d19aa02acf0e8f@sentry.io/1362142";
    private static final String TAG_BOTNET_USER = "b0tnet_user";
    private static final boolean ENABLE = !SharedConst.DEBUG;
    private static final List<Class<? extends Exception>> IGNORED_EXCEPTIONS;

    private static SentryClient sentry;
    private static boolean available = true;

    static {
        // vlastní sada ignorovaných vyjímek pro Sentry
        IGNORED_EXCEPTIONS = new ArrayList<>();
        // nemá smysl odesílat, uživatel je debil a změnil si access token
        IGNORED_EXCEPTIONS.add(InvalidAccessTokenException.class);
        // nastavá např. když se resetuje připojení (nevím proč nastává a co uživatel přesně udělal)
        IGNORED_EXCEPTIONS.add(SocketException.class);
    }

    /**
     * Nastaví dostupnost sentry. Výchozí je True.
     */
    public static void setAvailable(boolean available) {
        SentryGuard.available = available;
    }

    /**
     * Vytvoření sentry.
     */
    public static void init(Version b0tnetVersion, String mainPackage) {
        if (ENABLE && available) {
            final var dsn = DSN + "?stacktrace.app.packages=" + mainPackage;

            var factory = new DefaultSentryClientFactory();
            sentry = SentryClientFactory.sentryClient(dsn, factory);

            var userName = getLoggedUser();
            sentry.addTag("server_user", userName);
            sentry.addTag("b0tnet_version", b0tnetVersion.toString());
            sentry.addTag("server_os", System.getProperty("os.name"));

            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    LOG.info("Closing sentry...");
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    // nic
                } finally {
                    try {
                        sentry.clearContext();
                        sentry.closeConnection();
                    } catch (Exception e) {
                        // nic
                    }
                }
            }));
        }
    }

    /**
     * Přidá herní API do každého logu.
     */
    public static void setGameApiToTag(int gameApi) {
        if (sentry != null) {
            sentry.addTag("game_api", String.valueOf(gameApi));
        }
    }

    private static Event createEvent(Event.Level level, String message, Object extra) {
        var evt = new EventBuilder();
        evt.withLevel(level);
        evt.withMessage(message);
        evt.withTimestamp(new Date());

        if (extra != null) {
            evt.withExtra("userData", extra);
        }
        for (var entry : sentry.getTags().entrySet()) {
            evt.withTag(entry.getKey(), entry.getValue());
        }
        return evt.build();
    }

    /**
     * Přidá tag uživatele B0tnet.
     */
    public static void setB0tnetUser(String user) {
        if (ENABLE && available) {
            if (user != null && !sentry.getTags().containsKey(TAG_BOTNET_USER)) {
                sentry.addTag(TAG_BOTNET_USER, user);
            }
        }
    }

    /**
     * Odešle zprávu.
     */
    public static void log(String message) {
        if (ENABLE && available) {
            LOG.debug("Sending a message to the sentry: {}", message);
            try {
                sentry.sendMessage(message);
            } catch (Exception e) {
                // nic - neporařilo se odeslat zprávu
            }
        }
    }

    /**
     * Odešle warning zprávu.
     */
    public static void logWarning(String message) {
        if (ENABLE && available) {
            LOG.debug("Sending a warning message to the sentry: {}", message);
            try {
                sentry.sendEvent(createEvent(Event.Level.WARNING, message, null));
            } catch (Exception e) {
                // nic - neporařilo se odeslat zprávu
            }
        }
    }

    /**
     * Odešle warning zprávu.
     */
    public static void logWarning(String message, Object extra) {
        if (ENABLE && available) {
            LOG.debug("Sending a warning message to the sentry: {}", message);
            try {
                sentry.sendEvent(createEvent(Event.Level.WARNING, message, extra));
            } catch (Exception e) {
                // nic - neporařilo se odeslat zprávu
            }
        }
    }

    /**
     * Odešle debug zprávu.
     */
    public static void logDebug(String message) {
        if (ENABLE && available) {
            LOG.debug("Sending a warning message to the sentry: {}", message);
            try {
                sentry.sendEvent(createEvent(Event.Level.DEBUG, message, null));
            } catch (Exception e) {
                // nic - neporařilo se odeslat zprávu
            }
        }
    }

    /**
     * Odešle error zprávu.
     */
    public static void logError(String message, Object extra) {
        if (ENABLE && available) {
            LOG.debug("Sending an exception to the sentry: {}", message);
            try {
                sentry.sendEvent(createEvent(Event.Level.ERROR, message, extra));
            } catch (Exception e) {
                // nic - neporařilo se odeslat zprávu
            }
        }
    }

    /**
     * Odešle error zprávu.
     */
    public static void logError(String message) {
        if (ENABLE && available) {
            LOG.debug("Sending an exception to the sentry: {}", message);
            try {
                sentry.sendEvent(createEvent(Event.Level.ERROR, message, null));
            } catch (Exception e) {
                // nic - neporařilo se odeslat zprávu
            }
        }
    }

    /**
     * Odešle vyjímku.
     */
    public static void log(Exception ex) {
        if (ENABLE && available) {
            if (!IGNORED_EXCEPTIONS.contains(ex.getClass())) {
                LOG.debug("Sending an exception to the sentry: {}", ex.getMessage());
                try {
                    sentry.sendException(ex);
                } catch (Exception e) {
                    // nic - neporařilo se odeslat zprávu
                }
            }
        }
    }

    /**
     * Získá aktulní logiky uživatele v systému.
     */
    private static String getLoggedUser() {
        var osName = System.getProperty("os.name").toLowerCase();
        var methodName = "getUsername";
        var userName = "?";
        String className = null;

        if (osName.contains("windows")) {
            className = "com.sun.security.auth.module.NTSystem";
            methodName = "getName";

        } else if (osName.contains("linux")) {
            className = "com.sun.security.auth.module.UnixSystem";

        } else if (osName.contains("solaris") || osName.contains("sunos")) {
            className = "com.sun.security.auth.module.SolarisSystem";
        }
        if (className != null) {
            try {
                var c = Class.forName(className);
                var method = c.getDeclaredMethod(methodName);
                var obj = c.getConstructor().newInstance();
                userName = method.invoke(obj).toString();

            } catch (Exception e) {
                // nic
            }
        }
        return userName;
    }
}
