package hd.vhackos.b0tnet.shared.ascii.converter;

import hd.vhackos.b0tnet.shared.ascii.IAsciiConverter;
import hd.vhackos.b0tnet.shared.dto.ServerNodeType;
import hd.vhackos.b0tnet.shared.injection.Inject;

/**
 * Převede hodnotu typu nodu na serveru na typ.
 */
@Inject
public final class AsciiServerNodeTypeConverter implements IAsciiConverter {

    @Override
    public Object convert(Object value) {
        if (value == null) {
            return null;
        }
        return ServerNodeType.getByCode((int) value).getAlias();
    }
}
