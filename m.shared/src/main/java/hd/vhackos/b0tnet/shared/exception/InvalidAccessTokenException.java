package hd.vhackos.b0tnet.shared.exception;

/**
 * Byl zadán neplatný přístupový token do požadavku.
 */
public final class InvalidAccessTokenException extends B0tnetException {

    public InvalidAccessTokenException(String resultCode, String message) {
        super(resultCode, message);
    }
}
