package hd.vhackos.b0tnet.shared.exception;

/**
 * Hlavní vyjímka pro b0tnet.
 */
public class B0tnetCoreException extends RuntimeException {

    public B0tnetCoreException() {
        super();
    }

    B0tnetCoreException(String message) {
        super(message);
    }

    public B0tnetCoreException(String message, Throwable cause) {
        super(message, cause);
    }

    public B0tnetCoreException(Throwable cause) {
        super(cause);
    }

    protected B0tnetCoreException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
