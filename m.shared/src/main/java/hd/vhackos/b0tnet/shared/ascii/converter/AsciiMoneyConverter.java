package hd.vhackos.b0tnet.shared.ascii.converter;

import hd.vhackos.b0tnet.shared.ascii.IAsciiConverter;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.shared.utils.SharedUtils;

/**
 * Prevodník vstupní hodnoty na jinou pro výpis ASCII.
 */
@Inject
public final class AsciiMoneyConverter implements IAsciiConverter {

    @Override
    public Object convert(Object value) {
        return SharedUtils.toMoneyFormat(value);
    }
}
