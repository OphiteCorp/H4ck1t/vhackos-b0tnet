package hd.vhackos.b0tnet.shared.ascii.converter;

import hd.vhackos.b0tnet.shared.ascii.IAsciiConverter;
import hd.vhackos.b0tnet.shared.dto.PvPBoostType;
import hd.vhackos.b0tnet.shared.injection.Inject;

/**
 * Převede hodnotu typu PvP boostu na typ.
 */
@Inject
public final class AsciiServerPvPBoostTypeConverter implements IAsciiConverter {

    @Override
    public Object convert(Object value) {
        if (value == null) {
            return null;
        }
        return PvPBoostType.getById((int) value).getAlias();
    }
}
