package hd.vhackos.b0tnet.shared.ascii;

/**
 * Rozhraní, kterí pouze definuje převodník pro správnou instanci.
 */
public interface IAsciiConverter {

    /**
     * Konverze původní hodnoty na jinou.
     */
    Object convert(Object value);
}
