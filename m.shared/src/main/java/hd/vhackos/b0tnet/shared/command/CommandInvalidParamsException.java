package hd.vhackos.b0tnet.shared.command;

/**
 * Příkaz vyžaduje parametry které mu nebyly poskytnuty.
 */
public final class CommandInvalidParamsException extends RuntimeException {

    CommandInvalidParamsException(String message) {
        super(message);
    }
}
