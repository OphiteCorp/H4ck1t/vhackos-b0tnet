package hd.vhackos.b0tnet.shared.ascii.converter;

import hd.vhackos.b0tnet.shared.ascii.IAsciiConverter;
import hd.vhackos.b0tnet.shared.dto.BruteState;
import hd.vhackos.b0tnet.shared.injection.Inject;

/**
 * Převede hodnotu stavu bruteforce na typ.
 */
@Inject
public final class AsciiBruteStateConverter implements IAsciiConverter {

    @Override
    public Object convert(Object value) {
        return BruteState.getbyState((int) value).getAlias();
    }
}
