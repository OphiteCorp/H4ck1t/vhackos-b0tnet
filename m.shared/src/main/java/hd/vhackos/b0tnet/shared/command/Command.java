package hd.vhackos.b0tnet.shared.command;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Označí metodu jako příkaz a tím půjde zavolat externě.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
public @interface Command {

    /**
     * Název příkazu. Pokud nebude vyplněn, bude použit název metody.
     */
    String value() default "";

    /**
     * Název kategorie, do které příkaz spadá. Není povinné.
     */
    String category() default "";

    /**
     * Komentář příkazu.
     */
    String comment() default "";

    /**
     * Je příkaz skrytý v nabídce?
     */
    boolean hidden() default false;
}
