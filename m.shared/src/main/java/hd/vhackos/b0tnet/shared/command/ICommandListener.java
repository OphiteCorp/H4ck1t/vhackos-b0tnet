package hd.vhackos.b0tnet.shared.command;

/**
 * Listener, který se volá při každém odeslanám příkazu v konzoli.
 */
public interface ICommandListener {

    /**
     * Příchozí příkaz z konzole.
     */
    void incomingCommand(CommandDispatcher dispatcher, String command) throws Exception;
}
