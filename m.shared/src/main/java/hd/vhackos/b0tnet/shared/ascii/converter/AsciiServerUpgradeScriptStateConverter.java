package hd.vhackos.b0tnet.shared.ascii.converter;

import hd.vhackos.b0tnet.shared.ascii.IAsciiConverter;
import hd.vhackos.b0tnet.shared.dto.ServerUpgradeScriptState;
import hd.vhackos.b0tnet.shared.injection.Inject;

/**
 * Převede hodnotu typu skriptu na serveru na typ.
 */
@Inject
public final class AsciiServerUpgradeScriptStateConverter implements IAsciiConverter {

    @Override
    public Object convert(Object value) {
        if (value == null) {
            return null;
        }
        return ServerUpgradeScriptState.getByCode((int) value).getAlias();
    }
}
