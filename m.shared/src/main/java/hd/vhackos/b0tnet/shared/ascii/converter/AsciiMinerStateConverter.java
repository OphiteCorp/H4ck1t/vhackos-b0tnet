package hd.vhackos.b0tnet.shared.ascii.converter;

import hd.vhackos.b0tnet.shared.ascii.IAsciiConverter;
import hd.vhackos.b0tnet.shared.dto.MinerState;
import hd.vhackos.b0tnet.shared.injection.Inject;

/**
 * Převádí stav mineru.
 */
@Inject
public final class AsciiMinerStateConverter implements IAsciiConverter {

    @Override
    public Object convert(Object value) {
        return MinerState.getByCode((int) value).getAlias();
    }
}
