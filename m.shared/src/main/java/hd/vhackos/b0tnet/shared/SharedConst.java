package hd.vhackos.b0tnet.shared;

/**
 * Sdílené konstanty.
 */
public final class SharedConst {

    /**
     * Je povolený debug režim?
     */
    public static final boolean DEBUG = false;
}
