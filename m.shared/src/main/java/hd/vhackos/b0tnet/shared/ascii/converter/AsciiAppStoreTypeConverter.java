package hd.vhackos.b0tnet.shared.ascii.converter;

import hd.vhackos.b0tnet.shared.ascii.IAsciiConverter;
import hd.vhackos.b0tnet.shared.dto.AppStoreType;
import hd.vhackos.b0tnet.shared.injection.Inject;

/**
 * Získá název aplikace podle ID.
 */
@Inject
public final class AsciiAppStoreTypeConverter implements IAsciiConverter {

    @Override
    public Object convert(Object value) {
        var type = AppStoreType.getById((int) value);
        return type.getAlias();
    }
}
