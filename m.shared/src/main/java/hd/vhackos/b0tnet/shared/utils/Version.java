package hd.vhackos.b0tnet.shared.utils;

/**
 * Informace o verzi.
 */
public final class Version {

    private int major;
    private int minor;
    private int update;
    private long numeric;

    private Version() {
    }

    public static Version create(String version) {
        var tokens = version.split("\\.");
        var v = new Version();
        v.major = Integer.valueOf(tokens[0]);
        v.minor = Integer.valueOf(tokens[1]);
        v.update = Integer.valueOf(tokens[2]);
        v.numeric = Long.valueOf(version.replaceAll("\\.", ""));
        return v;
    }

    public boolean isInputHigher(Version version) {
        return (version.numeric > numeric);
    }

    public long getNumbericValue() {
        return numeric;
    }

    @Override
    public String toString() {
        return String.format("%s.%s.%s", major, minor, update);
    }

    private long makeSum() {
        return (long) ((major * 1e6) + (minor * 1e4) + (update * 1e2));
    }
}
