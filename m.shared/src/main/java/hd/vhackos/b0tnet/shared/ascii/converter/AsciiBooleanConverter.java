package hd.vhackos.b0tnet.shared.ascii.converter;

import hd.vhackos.b0tnet.shared.ascii.IAsciiConverter;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.shared.utils.SharedUtils;

/**
 * Převádí boolean na string.
 */
@Inject
public final class AsciiBooleanConverter implements IAsciiConverter {

    @Override
    public Object convert(Object value) {
        return SharedUtils.convertToBoolean(value);
    }
}
