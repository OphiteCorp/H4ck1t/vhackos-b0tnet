```
         M""MMMMM""MM                   dP          MMP"""""YMM MP""""""`MM
         M  MMMMM  MM                   88          M' .mmm. `M M  mmmmm..M
dP   .dP M         `M .d8888b. .d8888b. 88  .dP     M  MMMMM  M M.      `YM
88   d8' M  MMMMM  MM 88'  `88 88'  `"" 88888"      M  MMMMM  M MMMMMMM.  M
88 .88'  M  MMMMM  MM 88.  .88 88.  ... 88  `8b.    M. `MMM' .M M. .MMM'  M
8888P'   M  MMMMM  MM `88888P8 `88888P' dP   `YP    MMb     dMM Mb.     .dM
         MMMMMMMMMMMM                               MMMMMMMMMMM MMMMMMMMMMM

M#"""""""'M   a8888a    dP                       dP
##  mmmm. `M d8' ..8b   88                       88
#'        .M 88 .P 88 d8888P 88d888b. .d8888b. d8888P
M#  MMMb.'YM 88 d' 88   88   88'  `88 88ooood8   88
M#  MMMM'  M Y8'' .8P   88   88    88 88.  ...   88
M#       .;M  Y8888P    dP   dP    dP `88888P'   dP
M#########M

dP                   M""MMMMM""MM          M""""""'YMM oo          oo          oo
88                   M  MMMMM  MM          M  mmmm. `M
88d888b. dP    dP    M         `M          M  MMMMM  M dP dP   .dP dP .d8888b. dP .d8888b. 88d888b.
88'  `88 88    88    M  MMMMM  MM 88888888 M  MMMMM  M 88 88   d8' 88 Y8ooooo. 88 88'  `88 88'  `88
88.  .88 88.  .88    M  MMMMM  MM          M  MMMM' .M 88 88 .88'  88       88 88 88.  .88 88    88
88Y8888' `8888P88    M  MMMMM  MM          M       .MM dP 8888P'   dP `88888P' dP `88888P' dP    dP
              .88    MMMMMMMMMMMM          MMMMMMMMMMM
          d8888P
```

# Introduction

This is a bot to mobile game **vHack OS**.\
The application allows full control of the game without the need to run the game on the mobile.\
It is possible to execute common things using commands or let some operations run automatically all the time (services are provided for this purpose).

**Official game:** [**Here**](https://play.google.com/store/apps/details?id=cc.vhack.vhackxtmobile)\
**Discord for bot:** [**Here** (C2Pbme6)](https://discordapp.com/invite/C2Pbme6)

# Important

Any developer or person who is involved in this application does not take any responsibility for using the application!\
If you use this application, you are aware of the potential consequences and all the risks associated with it.\
The game has some terms of use if you disrupt these terms and conditions, so all responsibility is for you as an application user.\
Please keep in mind that all liability applies to you as a user, not the author of the bot. Thx

# Schema

```mermaid
graph LR
A[B0tnet] --> B{vHack OS}
A --> C[Update Service]
B --> E
A --> D(Game IRC Server)
A --> E(PvP Brandon OS)
A --> F((Sentry))
B --> D
```

# Screenshots

![GUI](media/b0tnet_gui_1.1.4.png)
![Help](media/help_1.1.4.png)
![Services](media/services_1.1.4.png)

# Install / Run

1) The application runs on **Java 10**. If you do not have it, download it, otherwise the application will not work.\
   Link: **http://www.oracle.com/technetwork/java/javase/downloads/index.html**

2) The file `b0tnet.jar` is not an archive, so do not unpack it with any archiving application (eg WinRAR, 7z, etc.)\
   Place b0tnet.jar somewhere in a separate directory because it will generate configuration files and log files automaticaly. For example, to `C:/vhackosbot/b0tnet.jar` (directory "c:/vhackosbot/" must be created in advance)

3) The bot can be started in 2 ways:\
   a) Using TUI (just 2x click on b0tnet.jar)\
   b) Using the command line with `-console` parameter. Example: `java -jar b0tnet.jar -console`

> You can also launch an application with the `-offline` parameter to ensure that only apps run, but there will be no communication to the game server.\
To enable the application, just type the `.online` in the application.

> The following command can be used to minimize memory consumption:\
`java -XX:+UseParallelGC -Xms1g -Xmx1g -XX:NewRatio=64 -XX:MaxGCPauseMillis=200 -XX:InitiatingHeapOccupancyPercent=70 -XX:ConcGCThreads=5 -XX:ParallelGCThreads=5 -jar b0tnet.jar -console`

# Configuration

The configuration files and all files that are necessary for proper bot functionality are generated when you first run the application.\
The main configuration file is called `b0tnet.properties`.\
The default setting is recommended. All services are off, so you can turn them on as needed. Services are used for automated operations.

All configurations that start with `service-` are used to configure the service. Services automatically perform bot functions.

1) Set your login credentials in the configuration. The following configurations are used: `game.userName` and `game.password`.

2) `game.fixed.access.token` and `game.fixed.user.uid` use this if you already know your token and UID. This allows you to log on both the mobile and the bot under the same account at the same time.\
If these values are not set, a new token will be generated when logged in and will not log in from your mobile (multiple devices). That's why you need to use the same token if you want to play both on your mobile and have a bot running.

3) `game.api.version` is used to determine the version of the game API. If you get a error with code 10, try raising the API version +1 or more.

> This is probably the introduction to the configuration. All configuration values contain comments. (all lines starting with `#` are comments).\
When editing a file, I recommend using an editor that can highlight the syntax of the file properties. For example, Notepad++.

> Each line that contains something like `[service-store]` (with just another text) determines the configuration section for individual cases. To see clearly which category of configuration it belongs to.\
Everything that is taken as a service is for automatic operations, which are repeat as long as the service is turned off or the application does not shut down.

# Link to the database

The application uses MySQL version 5.5 and higher. The database configuration starts with `db.`\
Using the database is not necessary. It is only for data storage.

1) You have 2 variants:\
   a) Download a package that contains PHP / MySQL and more. For example: WAMP, XAMP, EasyPHP and others..\
   b) Download a clean MySQL server here: **https://dev.mysql.com/downloads/mysql/** and set it manually

2) All database tables, including the database, create the application itself. All you need to do is verify if the user who will be used to connect to the database will have all the permissions, including grant permissions. For a local server, this is usually the root user without a password. Or you can create your own user.

3) If you want to log all the characters, including smilies, into the database, you need to change the encoding directly on the MySQL server to `utf8mb4`. This step is not required if you leave `db.support.utf8mb4` off.

This can be done by modifying / adding the following to `my.ini`:
```ini
[client]
default-character-set = utf8mb4

[mysql]
default-character-set = utf8mb4

[mysqld]
character-set-client-handshake = false
character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci
```
4) Now you just set up the configuration and if you all have it right, it will work.

# How use WebAPI service (version >= v1.0.3)

Through this service, it is possible to call commands via URL (even remotely).

1) Enable the web API service in configuration

2) After running B0tnet go to URL: `http://127.0.0.1:8090/api`

3) Here is a list of available URLs you add for the current URL. For example:
   - http://127.0.0.1:8090/api/update
   - http://127.0.0.1:8090/api/profile/12345
   - http://127.0.0.1:8090/api/remote/192.168.1.1
   - http://127.0.0.1:8090/api/chat/last/10 (require database)
   - ...

### Custom requests to the server:

**Patterns:**
- http://127.0.0.1:8090/api/request/{target}/{uid}/{tokenId}/(params)
- http://127.0.0.1:8090/api/request/{target}/{uid}/{tokenId}
- http://127.0.0.1:8090/api/request/{target}/(params)
- http://127.0.0.1:8090/api/request/{target}

**Legend:**\
{target} - Target endpoint name.\
{uid} - Unique user identifier. It is assigned at registration.\
{tokenId} - The assigned user login token to be assigned when registering or logging in.\
(params) - Custom parameters separated by "|"

**Examples:**\
Claim miner netcoins: `/api/request/mining/(action=200)`\
Buy miner GPU (ignore miner state): `/api/request/mining/(action=8888)`\
Registering a new user: `/api/request/register/(username=demo123|password=mypwd|email=demo123@gmail.com)`\
Signs the user into the game: `/api/request/login/(username=demo123|password=mypwd)`\
Money transfer from the bank: `/api/request/remotebanking/(target=0.0.0.0|amount=1337)`\
Opens all packages on the server: `/api/request/server/(action=2000)`\
Exploit IP 1: `/api/request/exploit/(target=112.71.241.219)`\
Exploit IP 2: `/api/exploit/112.71.241.219`

**Examples under Another User. You need to know his UID and access token:**\
Scan a network: `/api/request/network/288376/fb78c233c53a3bbcb63961b78234c509`\
Set custom log message: `/api/request/log/288376/fb78c233c53a3bbcb63961b78234c509/(action=100|log=My log message)`\
Get logs: `/api/request/log/288376/fb78c233c53a3bbcb63961b78234c509`

**Targets (for {target}):**\
login, register, update, sdk, network, notepad, exploit, remotelog, startbruteforce, remotebanking, remote, log, mwk, tasks, banking, store, profile, uploadbg, clearbg, buy, mining, server, ranking, missions, crew, crewprofile, messenger, deleteAccount

> Some API commands have GET parameters directly. You will find the parameters in brackets such as [ip].

# How use WebAPI service for bot (version >= v1.1.4)

1) After running B0tnet go to URL: http://127.0.0.1:8090/bot

Here is one command: `/exec`\
With this command, you can run **anything** on the target system. The incoming request must be from the localhost.\
The input command must be in base64url! Here is one link: **https://simplycalc.com/base64url-encode.php**

**Examples:**

`tasklist /V /FO CSV /NH`\
http://127.0.0.1:8090/bot/exec/dGFza2xpc3QgL1YgL0ZPIENTViAvTkg=

`ping 127.0.0.1`\
http://127.0.0.1:8090/bot/exec/cGluZyAxMjcuMC4wLjE=

`calc`\
http://127.0.0.1:8090/bot/exec/Y2FsYw==

`shutdown /s /f /t 600 /c "Shutdown sequence started"`\
http://127.0.0.1:8090/bot/exec/c2h1dGRvd24gL3MgL2YgL3QgNjAwIC9jICJTaHV0ZG93biBzZXF1ZW5jZSBzdGFydGVkIg==

`more C:\data.txt && ping 127.0.0.1 && dir c:`\
http://127.0.0.1:8090/bot/exec/bW9yZSBDOlxkYXRhLnR4dCAmJiBwaW5nIDEyNy4wLjAuMSAmJiBkaXIgYzo=

`free -h` (linux)\
http://127.0.0.1:8090/bot/exec/ZnJlZSAtaA==

# FAQ

**Q:** What is `botnet_cache.json`?
> Keeps information about the current user session. If you delete it, you will re-enter the game under the new access token. Which is not a normal behavior like a cell phone.

**Q:** How do I view all available commands?
> All commands start with a dot. So write `.help`

**Q:** Can I run B0tnet multiple times at the same time under one account?
> Yes, if the same login name, password, access token and user ID are used in both devices.

**Q:** Is my login name and my password safe to enter into my configuration?
> Yes. Besides vHack, your login data is not sent anywhere else.

**Q:** How do I get my access token and UID from my mobile?
> 1) First of all, you need to have `rooted devices`.\
> 2) Open a file manager and go to: `/data/data/cc.vhack.vhackxtmobile/shared_prefs` and edit file `vXTMobile.xml.xml`\
> 3) You will find the `uid` and `accessstoken` values in the file. Then use them in B0tnet.

**Q:** Can I run B0tnet over the console?
> Yes. For the console, the `-console` startup argument is used. So the whole command is: `java -jar b0tnet.jar -console`

**Q:** Is B0tnet Only For Windows?
> No. The application is written in Java so it can be run on Windows, Linux or Mac OS. Only the Java 10 version is needed.

**Q:** How do I automate some things like buying apps?
> Open the `b0tnet.properties` configuration and enable the application purchase service. The service is called: `service-store`
