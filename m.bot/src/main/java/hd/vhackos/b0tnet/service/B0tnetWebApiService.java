package hd.vhackos.b0tnet.service;

import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.config.ApplicationConfig;
import hd.vhackos.b0tnet.service.base.EndpointService;
import hd.vhackos.b0tnet.service.base.IService;
import hd.vhackos.b0tnet.service.base.Service;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.webapi.WebApiRestServer;

/**
 * Služba pro aktualizaci bota.
 */
@Inject
@EndpointService(IService.SERVICE_B0TNET_WEB_API)
public final class B0tnetWebApiService extends Service {

    private static final long DEFAULT_TIMEOUT = Integer.MAX_VALUE; // nebude se spouštět periodicky

    @Autowired
    private ApplicationConfig config;

    @Autowired
    private WebApiRestServer webApiRestServer;

    protected B0tnetWebApiService(B0tnet b0tnet) {
        super(b0tnet);
    }

    @Override
    public String getDescription() {
        return "Opens the API service on the HTTP protocol";
    }

    @Override
    protected void initialize(B0tnet b0tnet) {
        setTimeout(DEFAULT_TIMEOUT);
    }

    @Override
    protected void execute() {
        webApiRestServer.start();
        getLog().info("The Web API service at {}:{}/api was opened", config.getWebApiHost(), config.getWebApiPort());
    }

    @Override
    protected void onStopped() {
        webApiRestServer.stop();
    }
}
