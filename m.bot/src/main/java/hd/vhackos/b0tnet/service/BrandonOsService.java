package hd.vhackos.b0tnet.service;

import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.api.brandonos.BrandonOsReceiver;
import hd.vhackos.b0tnet.api.brandonos.IBrandonOsListener;
import hd.vhackos.b0tnet.service.base.EndpointService;
import hd.vhackos.b0tnet.service.base.IService;
import hd.vhackos.b0tnet.service.base.Service;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.shared.utils.SharedUtils;

/**
 * Služba pro napojení do Brandon OS.
 */
@Inject
@EndpointService(IService.SERVICE_BRANDON_OS)
public final class BrandonOsService extends Service {

    public static final String MSG_PREFIX = "BrandonOS";

    @Autowired
    private BrandonOsReceiver brandonOsReceiver;

    protected BrandonOsService(B0tnet b0tnet) {
        super(b0tnet);
    }

    @Override
    public String getDescription() {
        return "Reads Brandon OS stream and allows you to send commands";
    }

    @Override
    protected void initialize(B0tnet b0tnet) {
        setTimeout(getConfig().getBrandonOsTimeout());

        brandonOsReceiver.reset();
        brandonOsReceiver.addListener(new BrandonOsListener());
    }

    @Override
    protected void execute() {
        brandonOsReceiver.listen();
        brandonOsReceiver.waitForFinish();
        brandonOsReceiver.closeClientThread();

        getLog().info("Brandon OS is down. Next check will be in: {}", SharedUtils.toTimeFormat(getTimeout()));
    }

    @Override
    protected void onStopped() {
        brandonOsReceiver.closeClientThread();
        brandonOsReceiver = null;
    }

    private final class BrandonOsListener implements IBrandonOsListener {

        @Override
        public void readRaw(String line) {
            getLog().info("[{}] {}", MSG_PREFIX, line);
        }
    }
}
