package hd.vhackos.b0tnet.service;

import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.api.module.MissionsModule;
import hd.vhackos.b0tnet.service.base.EndpointService;
import hd.vhackos.b0tnet.service.base.IService;
import hd.vhackos.b0tnet.service.base.Service;
import hd.vhackos.b0tnet.shared.dto.MissionFinishedType;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.shared.utils.SharedUtils;

/**
 * Služba pro dokončení úkolů a sebrání odměny.
 */
@Inject
@EndpointService(IService.SERVICE_MISSION)
public final class MissionService extends Service {

    @Autowired
    private MissionsModule missionsModule;

    protected MissionService(B0tnet b0tnet) {
        super(b0tnet);
    }

    @Override
    public String getDescription() {
        return "Completes the mission and collects rewards";
    }

    @Override
    protected void initialize(B0tnet b0tnet) {
        setTimeout(getConfig().getMissionTimeout());
    }

    @Override
    protected void execute() {
        if (getShared().getUpdateResponse() == null) {
            var resp = getCommonModule().update();
            getShared().setUpdateResponse(resp);
            sleep();
        }
        if (!SharedUtils.toBoolean(getShared().getUpdateResponse().getMissions())) {
            getLog().info("Missions are not available. Next check will be in: {}",
                    SharedUtils.toTimeFormat(getTimeout()));
            return;
        }
        var resp = missionsModule.getMissions();

        if (resp.getClaimNextDay() == null || resp.getClaimNextDay() == 0) {
            sleep();
            resp = missionsModule.claimDaily();
            getLog().info("The daily reward is collected. {}xp, {} Boosters, {} Netcoins", resp.getRewardExperience(),
                    resp.getRewardBoosters(), resp.getRewardNetcoins());
        }
        for (var i = 0; i < resp.getDaily().size(); i++) {
            var mission = resp.getDaily().get(i);
            var type = MissionFinishedType.getByCode(mission.getFinished());

            if (type == MissionFinishedType.READY) {
                sleep();
                resp = missionsModule.claimMissionReward(i);
                getLog().info("Mission #{} has been completed. The reward is: {}xp + {} {}", i, mission.getExperience(),
                        mission.getRewardAmount(), mission.getRewardType());
            }
        }
        if (isRunningAsync()) {
            getLog().info("Done. Next check will be in: {}", SharedUtils.toTimeFormat(getTimeout()));
        }
    }
}
