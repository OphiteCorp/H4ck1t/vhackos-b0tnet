package hd.vhackos.b0tnet.service;

import com.google.gson.Gson;
import hd.vhackos.b0tnet.Application;
import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.command.ApplicationCommand;
import hd.vhackos.b0tnet.dto.B0tnetUpdateData;
import hd.vhackos.b0tnet.exception.ForceUpdateRequiredException;
import hd.vhackos.b0tnet.service.base.EndpointService;
import hd.vhackos.b0tnet.service.base.IService;
import hd.vhackos.b0tnet.service.base.Service;
import hd.vhackos.b0tnet.shared.command.CommandRunner;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.shared.utils.SharedUtils;
import hd.vhackos.b0tnet.shared.utils.Version;
import hd.vhackos.b0tnet.utils.AudioPlayer;
import hd.vhackos.b0tnet.utils.ResourceHelper;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/**
 * Služba pro aktualizaci bota.
 */
@Inject
@EndpointService(IService.SERVICE_B0TNET_UPDATE)
public final class B0tnetUpdateService extends Service {

    private static final String UPDATE_URL = "https://pastebin.com/raw/3DSkv1NU";
    private static final long DEFAULT_TIMEOUT = 3600000; // 1h

    protected B0tnetUpdateService(B0tnet b0tnet) {
        super(b0tnet);
    }

    private static void reformatNews(B0tnetUpdateData data) {
        if (data.getNews() == null || data.getNews().isEmpty()) {
            return;
        }
        var list = new ArrayList<String>(data.getNews().size());

        for (var n : data.getNews()) {
            list.add(SharedUtils.addLinebreaks(n, 75, "\n"));
        }
        data.setNews(list);
    }

    @Override
    public String getDescription() {
        return "Checks for B0tnet updates";
    }

    @Override
    protected void initialize(B0tnet b0tnet) {
        setTimeout(DEFAULT_TIMEOUT);
    }

    @Override
    protected void execute() {
        try (var in = new BufferedReader(
                new InputStreamReader(new URL(UPDATE_URL).openStream(), StandardCharsets.UTF_8))) {

            var data = new Gson().fromJson(in, B0tnetUpdateData.class);
            var version = Version.create(data.getVersion());
            data.setNewVersionAvailable(IB0tnet.VERSION.isInputHigher(version));
            reformatNews(data);
            getShared().setUpdateData(data);

            if (data.isNewVersionAvailable()) {
                getLog().warn("Attention! A new version v{} of B0tnet is available. To download, go to this link: {}",
                        data.getVersion(), data.getDownloadLink());

                var result = CommandRunner.getInstance().run(ApplicationCommand.CMD_LATEST, null);
                if (Application.isConsoleMode()) {
                    System.out.println(result);
                } else {
                    getLog().info("\n" + result + "\n");
                }
                AudioPlayer.play(ResourceHelper.getStream(ResourceHelper.ResourceValue.SOUND_NEW_VERSION));
            } else {
                getLog().info("Your B0tnet version is up to date ;-)");
            }
            getLog().info("Information about the latest version of B0tnet was obtained. Next check will be in: {}",
                    SharedUtils.toTimeFormat(DEFAULT_TIMEOUT));

            if (data.isNewVersionAvailable()) {
                if (data.getForceUpdate() != null && data.getForceUpdate()) {
                    throw new ForceUpdateRequiredException();
                }
            }
        } catch (ForceUpdateRequiredException e) {
            throw e;

        } catch (Exception e) {
            getLog().error(
                    "An error occurred while searching for the latest version of B0tnet. Next check will be in: {}",
                    SharedUtils.toTimeFormat(DEFAULT_TIMEOUT));
        }
    }
}
