package hd.vhackos.b0tnet.service;

import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.api.module.EventModule;
import hd.vhackos.b0tnet.service.base.EndpointService;
import hd.vhackos.b0tnet.service.base.IService;
import hd.vhackos.b0tnet.service.base.Service;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.shared.utils.SharedUtils;

/**
 * Služba pro herní událost kola štěstí.
 */
@Inject
@EndpointService(IService.SERVICE_LUCKY_WHEEL)
public final class LuckyWheelService extends Service {

    @Autowired
    private EventModule eventModule;

    protected LuckyWheelService(B0tnet b0tnet) {
        super(b0tnet);
    }

    @Override
    public String getDescription() {
        return "Will spin periodically with a lucky wheel";
    }

    @Override
    protected void initialize(B0tnet b0tnet) {
        setTimeout(getConfig().getLuckyWheelTimeout());
    }

    @Override
    protected void execute() {
        var prize = eventModule.spinLuckyWheel();

        if (isRunningAsync()) {
            getLog().info("Done.: {}. Next check will be in: {}", prize, SharedUtils.toTimeFormat(getTimeout()));
        } else {
            getLog().info("Done.: {}.", prize);
        }
    }
}
