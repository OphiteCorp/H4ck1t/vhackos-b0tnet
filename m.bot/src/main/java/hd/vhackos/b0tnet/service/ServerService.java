package hd.vhackos.b0tnet.service;

import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.api.module.ServerModule;
import hd.vhackos.b0tnet.api.net.response.ServerResponse;
import hd.vhackos.b0tnet.service.base.EndpointService;
import hd.vhackos.b0tnet.service.base.IService;
import hd.vhackos.b0tnet.service.base.Service;
import hd.vhackos.b0tnet.shared.dto.ServerNodeType;
import hd.vhackos.b0tnet.shared.dto.ServerScriptType;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.shared.utils.SharedUtils;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Služba pro kontrolu serveru a jeho aktualizaci.
 */
@Inject
@EndpointService(IService.SERVICE_SERVER)
public final class ServerService extends Service {

    private static final int NEW_NODE_COST = 100;
    private static final int MAX_NODES = 3;
    private static final int MAX_BOUGHT_PACKAGES = 50;
    private static final int PACKAGE_COST = 40;
    private static final int MIN_FRAGS = 5;

    @Autowired
    private ServerModule serverModule;

    private int updateLimit;
    private int coreUpdateLimit;

    protected ServerService(B0tnet b0tnet) {
        super(b0tnet);
    }

    private static int getFreePieces(ServerResponse resp, ServerNodeType nodeType) {
        switch (nodeType) {
            case ANTIVIRUS:
                return (resp.getAvFreePieces() != null) ? resp.getAvFreePieces() : 0;
            case FIREWALL:
                return (resp.getFwFreePieces() != null) ? resp.getFwFreePieces() : 0;
            case SERVER:
                return (resp.getServerFreePieces() != null) ? resp.getServerFreePieces() : 0;
        }
        return 0;
    }

    private static int getNodes(ServerResponse resp, ServerNodeType nodeType) {
        switch (nodeType) {
            case ANTIVIRUS:
                return (resp.getAvNodes() != null) ? resp.getAvNodes() : 0;
            case FIREWALL:
                return (resp.getFwNodes() != null) ? resp.getFwNodes() : 0;
            case SERVER:
                return 1;
        }
        return 0;
    }

    @Override
    public String getDescription() {
        return "Manages the server";
    }

    @Override
    protected void initialize(B0tnet b0tnet) {
        setTimeout(getConfig().getServerTimeout());
        updateLimit = getConfig().getServerUpdateLimit();
        coreUpdateLimit = getConfig().getServerCoreUpdateLimit();
    }

    @Override
    protected void execute() {
        if (getShared().getUpdateResponse() == null || !SharedUtils
                .toBoolean(getShared().getUpdateResponse().getServer())) {
            return;
        }
        var resp = serverModule.getServer();
        getLog().info("Available parts -> Server: {}, Antivirus: {}, Firewall: {}, Frags: {}",
                resp.getServerFreePieces(), resp.getAvFreePieces(), resp.getFwFreePieces(), resp.getFrags());

        resp = processAntivirus(resp);
        resp = processFirewall(resp);
        resp = processServer(resp);
        resp = processCollectPackages(resp);
        resp = processBuyAndCollectPackages(resp);
        resp = processBuyScripts(resp);
        processUpgradeScripts(resp);

        if (isRunningAsync()) {
            getLog().info("Update complete. Next update will be in: {}", SharedUtils.toTimeFormat(getTimeout()));
        }
    }

    /**
     * @Deprecated Od verze API 32 nebude nutné používat. Server byl přepsán a funguje na principu skriptů.
     */
    @Deprecated
    private ServerResponse processAntivirus(ServerResponse resp) {
        final var nodeType = ServerNodeType.ANTIVIRUS;

        // zkusí koupit další node
        resp = tryBuyNode(resp, nodeType);
        var freePieces = getFreePieces(resp, nodeType);

        // jsou k dispozici nějaké AV nody a jsou prostředky
        if (freePieces > 0) {
            if (getNodes(resp, nodeType) > 0) {

                // první node
                if (resp.getAv1Pieces() < updateLimit) {
                    var left = updateLimit - resp.getAv1Pieces();
                    var x5Left = left / 5;

                    for (var i = 0; i < x5Left && freePieces >= 5; i++) {
                        sleep();
                        resp = serverModule.updateNode5x(nodeType, 1);
                        freePieces = getFreePieces(resp, nodeType);
                        left = (resp.getAv1Pieces() != null) ? updateLimit - resp.getAv1Pieces() : 0;
                        getLog().info("{} 1 was upgraded 5x to {}. Remaining pieces: {}", nodeType.getAlias(),
                                resp.getAv1Pieces(), resp.getAvFreePieces());
                    }
                    for (var i = 0; i < left && freePieces > 0; i++) {
                        sleep();
                        resp = serverModule.updateNode1x(nodeType, 1);
                        freePieces = getFreePieces(resp, nodeType);
                        getLog().info("{} 1 was upgraded 1x to {}. Remaining pieces: {}", nodeType.getAlias(),
                                resp.getAv1Pieces(), resp.getAvFreePieces());
                    }
                }
                resp = tryBuyNode(resp, nodeType);
                freePieces = getFreePieces(resp, nodeType);

                // druhý node
                if (freePieces > 0 && getNodes(resp, nodeType) > 1 && resp.getAv2Pieces() < updateLimit) {
                    var left = updateLimit - resp.getAv2Pieces();
                    var x5Left = left / 5;

                    for (var i = 0; i < x5Left && freePieces >= 5; i++) {
                        sleep();
                        resp = serverModule.updateNode5x(nodeType, 2);
                        freePieces = getFreePieces(resp, nodeType);
                        left = updateLimit - resp.getAv2Pieces();
                        getLog().info("{} 2 was upgraded 5x to {}. Remaining pieces: {}", nodeType.getAlias(),
                                resp.getAv2Pieces(), resp.getAvFreePieces());
                    }
                    for (var i = 0; i < left && freePieces > 0; i++) {
                        sleep();
                        resp = serverModule.updateNode1x(nodeType, 2);
                        freePieces = getFreePieces(resp, nodeType);
                        getLog().info("{} 2 was upgraded 1x to {}. Remaining pieces: {}", nodeType.getAlias(),
                                resp.getAv2Pieces(), resp.getAvFreePieces());
                    }
                }
                resp = tryBuyNode(resp, nodeType);
                freePieces = getFreePieces(resp, nodeType);

                // třetí node
                if (freePieces > 0 && getNodes(resp, nodeType) > 2 && resp.getAv3Pieces() < updateLimit) {
                    var left = updateLimit - resp.getAv3Pieces();
                    var x5Left = left / 5;

                    for (var i = 0; i < x5Left && freePieces >= 5; i++) {
                        sleep();
                        resp = serverModule.updateNode5x(nodeType, 3);
                        freePieces = getFreePieces(resp, nodeType);
                        left = updateLimit - resp.getAv3Pieces();
                        getLog().info("{} 3 was upgraded 5x to {}. Remaining pieces: {}", nodeType.getAlias(),
                                resp.getAv3Pieces(), resp.getAvFreePieces());
                    }
                    for (var i = 0; i < left && freePieces > 0; i++) {
                        sleep();
                        resp = serverModule.updateNode1x(nodeType, 3);
                        freePieces = getFreePieces(resp, nodeType);
                        getLog().info("{} 3 was upgraded 1x to {}. Remaining pieces: {}", nodeType.getAlias(),
                                resp.getAv3Pieces(), resp.getAvFreePieces());
                    }
                }
            }
        }
        return resp;
    }

    /**
     * @Deprecated Od verze API 32 nebude nutné používat. Server byl přepsán a funguje na principu skriptů.
     */
    @Deprecated
    private ServerResponse processFirewall(ServerResponse resp) {
        final var nodeType = ServerNodeType.FIREWALL;

        // zkusí koupit další node
        resp = tryBuyNode(resp, nodeType);
        var freePieces = getFreePieces(resp, nodeType);

        // jsou k dispozici nějaké FW nody a jsou prostředky
        if (freePieces > 0) {
            if (getNodes(resp, nodeType) > 0) {

                // první node
                if (resp.getFw1Pieces() < updateLimit) {
                    var left = updateLimit - resp.getFw1Pieces();
                    var x5Left = left / 5;

                    for (var i = 0; i < x5Left && freePieces >= 5; i++) {
                        sleep();
                        resp = serverModule.updateNode5x(nodeType, 1);
                        freePieces = getFreePieces(resp, nodeType);
                        left = updateLimit - resp.getFw1Pieces();
                        getLog().info("{} 1 was upgraded 5x to {}. Remaining pieces: {}", nodeType.getAlias(),
                                resp.getFw1Pieces(), resp.getFwFreePieces());
                    }
                    for (var i = 0; i < left && freePieces > 0; i++) {
                        sleep();
                        resp = serverModule.updateNode1x(nodeType, 1);
                        freePieces = getFreePieces(resp, nodeType);
                        getLog().info("{} 1 was upgraded 1x to {}. Remaining pieces: {}", nodeType.getAlias(),
                                resp.getFw1Pieces(), resp.getFwFreePieces());
                    }
                }
                resp = tryBuyNode(resp, nodeType);
                freePieces = getFreePieces(resp, nodeType);

                // druhý node
                if (freePieces > 0 && getNodes(resp, nodeType) > 1 && resp.getFw2Pieces() < updateLimit) {
                    var left = updateLimit - resp.getFw2Pieces();
                    var x5Left = left / 5;

                    for (var i = 0; i < x5Left && freePieces >= 5; i++) {
                        sleep();
                        resp = serverModule.updateNode5x(nodeType, 2);
                        freePieces = getFreePieces(resp, nodeType);
                        left = updateLimit - resp.getFw2Pieces();
                        getLog().info("{} 2 was upgraded 5x to {}. Remaining pieces: {}", nodeType.getAlias(),
                                resp.getFw2Pieces(), resp.getFwFreePieces());
                    }
                    for (var i = 0; i < left && freePieces > 0; i++) {
                        sleep();
                        resp = serverModule.updateNode1x(nodeType, 2);
                        freePieces = getFreePieces(resp, nodeType);
                        getLog().info("{} 2 was upgraded 1x to {}. Remaining pieces: {}", nodeType.getAlias(),
                                resp.getFw2Pieces(), resp.getFwFreePieces());
                    }
                }
                resp = tryBuyNode(resp, nodeType);
                freePieces = getFreePieces(resp, nodeType);

                // třetí node
                if (freePieces > 0 && getNodes(resp, nodeType) > 2 && resp.getFw3Pieces() < updateLimit) {
                    var left = updateLimit - resp.getFw3Pieces();
                    var x5Left = left / 5;

                    for (var i = 0; i < x5Left && freePieces >= 5; i++) {
                        sleep();
                        resp = serverModule.updateNode5x(nodeType, 3);
                        freePieces = getFreePieces(resp, nodeType);
                        left = updateLimit - resp.getFw3Pieces();
                        getLog().info("{} 3 was upgraded 5x to {}. Remaining pieces: {}", nodeType.getAlias(),
                                resp.getFw3Pieces(), resp.getFwFreePieces());
                    }
                    for (var i = 0; i < left && freePieces > 0; i++) {
                        sleep();
                        resp = serverModule.updateNode1x(nodeType, 3);
                        freePieces = getFreePieces(resp, nodeType);
                        getLog().info("{} 3 was upgraded 1x to {}. Remaining pieces: {}", nodeType.getAlias(),
                                resp.getFw3Pieces(), resp.getFwFreePieces());
                    }
                }
            }
        }
        return resp;
    }

    /**
     * @Deprecated Od verze API 32 nebude nutné používat. Server byl přepsán a funguje na principu skriptů.
     */
    @Deprecated
    private ServerResponse processServer(ServerResponse resp) {
        final var nodeType = ServerNodeType.SERVER;
        var freePieces = getFreePieces(resp, nodeType);

        if (freePieces > 0 && resp.getServerPieces() < coreUpdateLimit) {
            var left = coreUpdateLimit - resp.getServerPieces();
            var x5Left = left / 5;

            for (var i = 0; i < x5Left && freePieces >= 5; i++) {
                sleep();
                resp = serverModule.updateNode5x(nodeType, 1);
                freePieces = getFreePieces(resp, nodeType);
                left = coreUpdateLimit - resp.getServerPieces();
                getLog().info("{} was upgraded 5x to {}. Remaining pieces: {}", nodeType.getAlias(),
                        resp.getServerPieces(), resp.getServerFreePieces());
            }
            for (var i = 0; i < left && freePieces > 0; i++) {
                sleep();
                resp = serverModule.updateNode1x(nodeType, 1);
                freePieces = getFreePieces(resp, nodeType);
                getLog().info("{} was upgraded 1x to {}. Remaining pieces: {}", nodeType.getAlias(),
                        resp.getServerPieces(), resp.getServerFreePieces());
            }
        }
        return resp;
    }

    private ServerResponse processCollectPackages(ServerResponse resp) {
        var packs = resp.getPacks();
        var bronzePacks = resp.getBronzePacks();

        if (packs > 0) {
            sleep();
            resp = serverModule.openAllPackages();
            getLog().info("All packages ({}) were collected", packs);
        }
        if (bronzePacks > 0) {
            sleep();
            serverModule.openBronzePack();
            getLog().info("Bronze packs ({}) were collected", packs);
        }
        return resp;
    }

    private ServerResponse processBuyScripts(ServerResponse resp) {
        if (resp.getFrags() != null && resp.getFrags() >= MIN_FRAGS && getConfig().isServerUpgradeScriptsEnable()) {
            var scriptList = getConfig().getServerBuyScriptList();

            if (scriptList.isEmpty()) {
                return resp;
            }
            while (Math.floor(resp.getFrags() / MIN_FRAGS) > 0) {
                getLog().debug("Script purchase started with frags: {}", resp.getFrags());

                var map = prepareScriptMap(resp);
                var first = map.entrySet().iterator().next();

                sleep();
                resp = serverModule.buy1Script(first.getKey());
                var purchasedScripts = getPurchasedScripts(first.getKey(), resp) - first.getValue();

                getLog().info("{} scripts for {} were purchased. Frags left: {}", purchasedScripts,
                        first.getKey().getAlias(), resp.getFrags());
            }
            getLog().info("No free frags are already available to buy scripts");
        }
        return resp;
    }

    private ServerResponse processUpgradeScripts(ServerResponse resp) {
        while (resp.getBypassScripts() >= resp.getBypassUpgradeCosts() && resp.getBypassUpgradeCosts() > 0) {
            sleep();
            resp = serverModule.upgradeScripts(ServerScriptType.BYPASS);
            getLog().info("Bypass was upgraded {} to level: {}, Strength: {}, Next upgrade cost: {}",
                    ServerScriptType.BYPASS.getAlias(), resp.getBypassVersion(), resp.getBypassStrength(),
                    resp.getBypassUpgradeCosts());
        }
        while (resp.getSmashScripts() >= resp.getSmashUpgradeCosts() && resp.getSmashUpgradeCosts() > 0) {
            sleep();
            resp = serverModule.upgradeScripts(ServerScriptType.SMASH);
            getLog().info("Smash was upgraded {} to level: {}, Strength: {}, Next upgrade cost: {}",
                    ServerScriptType.SMASH.getAlias(), resp.getSmashVersion(), resp.getSmashStrength(),
                    resp.getSmashUpgradeCosts());
        }
        while (resp.getShutdownScripts() >= resp.getShutdownUpgradeCosts() && resp.getShutdownUpgradeCosts() > 0) {
            sleep();
            resp = serverModule.upgradeScripts(ServerScriptType.SHUTDOWN);
            getLog().info("Shutdown was upgraded {} to level: {}, Strength: {}, Next upgrade cost: {}",
                    ServerScriptType.SHUTDOWN.getAlias(), resp.getShutdownVersion(), resp.getShutdownStrength(),
                    resp.getShutdownUpgradeCosts());
        }
        return resp;
    }

    private ServerResponse processBuyAndCollectPackages(ServerResponse resp) {
        if (getConfig().isServerBuyPackagesForNetcoins()) {
            if (resp == null) {
                sleep();
                resp = serverModule.getServer();
            }
            if (resp != null && resp.getPacksBought() < MAX_BOUGHT_PACKAGES) {
                var netcoins = getShared().getUpdateResponse().getNetCoins();

                if (netcoins <= getConfig().getSafeNetcoins()) {
                    return resp;
                }
                var left = MAX_BOUGHT_PACKAGES - resp.getPacksBought();
                var x10Left = left / 10;

                for (var i = 0; i < x10Left && hasEnoughNetcoins(netcoins); i++) {
                    sleep();
                    resp = serverModule.buyPackages(10);
                    left = resp.getBoughtLimit() - resp.getPacksBought();
                    netcoins -= (PACKAGE_COST * 10);
                    getLog().info("Bought 10 packages for {} netcoins. There are {} netcoins left", PACKAGE_COST * 10,
                            netcoins);
                    sleep();
                    resp = serverModule.openAllPackages();
                    getLog().info("Purchased packages were opened");
                }
                for (var i = 0; i < left && hasEnoughNetcoins(netcoins); i++) {
                    sleep();
                    serverModule.buyPackages(1);
                    netcoins -= PACKAGE_COST;
                    getLog().info("Bought 1 package for {} netcoins. There are {} netcoins left", PACKAGE_COST,
                            netcoins);
                    sleep();
                    resp = serverModule.openAllPackages();
                    getLog().info("Purchased packages were opened");
                }
            }
        }
        return resp;
    }

    private ServerResponse tryBuyNode(ServerResponse resp, ServerNodeType nodeType) {
        var freePieces = getFreePieces(resp, nodeType);
        var nodes = getNodes(resp, nodeType);

        if (nodes < MAX_NODES && freePieces >= NEW_NODE_COST) {
            if (nodeType == ServerNodeType.ANTIVIRUS) {
                sleep();
                resp = serverModule.addAntivirusNode();
                getLog().info("Purchased {} {} node", getNodes(resp, nodeType), nodeType.getAlias());

            } else if (nodeType == ServerNodeType.FIREWALL) {
                sleep();
                resp = serverModule.addFirewallNode();
                getLog().info("Purchased {} {} node", getNodes(resp, nodeType), nodeType.getAlias());
            }
        }
        return resp;
    }

    private Map<ServerScriptType, Integer> prepareScriptMap(ServerResponse resp) {
        var scriptList = getConfig().getServerBuyScriptList();
        var map = new HashMap<ServerScriptType, Integer>();

        for (var script : scriptList) {
            switch (script) {
                case BYPASS:
                    map.put(script, resp.getBypassScripts());
                    break;

                case SMASH:
                    map.put(script, resp.getSmashScripts());
                    break;

                case SHUTDOWN:
                    map.put(script, resp.getShutdownScripts());
                    break;
            }
        }
        // nejnizsi hodnota bude na zacatku mapy
        map = map.entrySet().stream().sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        return map;
    }

    private static int getPurchasedScripts(ServerScriptType scriptType, ServerResponse resp) {
        switch (scriptType) {
            case BYPASS:
                return resp.getBypassScripts();

            case SMASH:
                return resp.getSmashScripts();

            case SHUTDOWN:
                return resp.getShutdownScripts();
        }
        return 0;
    }
}
