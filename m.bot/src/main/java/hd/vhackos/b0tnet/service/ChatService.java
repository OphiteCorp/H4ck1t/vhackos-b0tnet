package hd.vhackos.b0tnet.service;

import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.api.chat.ChatReceiver;
import hd.vhackos.b0tnet.api.chat.IChatListener;
import hd.vhackos.b0tnet.api.chat.dto.ChatUser;
import hd.vhackos.b0tnet.api.chat.dto.JoinedUser;
import hd.vhackos.b0tnet.api.chat.dto.ModeUser;
import hd.vhackos.b0tnet.api.chat.dto.UserMessage;
import hd.vhackos.b0tnet.db.service.DatabaseService;
import hd.vhackos.b0tnet.service.base.EndpointService;
import hd.vhackos.b0tnet.service.base.IService;
import hd.vhackos.b0tnet.service.base.Service;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.utils.AudioPlayer;
import hd.vhackos.b0tnet.utils.ResourceHelper;

import java.util.List;

/**
 * Služba pro napojení do chatu.
 */
@Inject
@EndpointService(IService.SERVICE_CHAT)
public final class ChatService extends Service {

    public static final String MSG_PREFIX = "Message";

    @Autowired
    private ChatReceiver chatReceiver;

    @Autowired
    private DatabaseService databaseService;

    protected ChatService(B0tnet b0tnet) {
        super(b0tnet);
    }

    @Override
    public String getDescription() {
        return "Reads chat messages and allows you to send messages";
    }

    @Override
    protected void initialize(B0tnet b0tnet) {
        setTimeout(getConfig().getChatTimeout());

        if (chatReceiver != null) {
            chatReceiver.reset();
            chatReceiver.addListener(new ChatListener());
        }
    }

    @Override
    protected void execute() {
        chatReceiver.listen();
        chatReceiver.waitForFinish();
        stop();
    }

    @Override
    protected void onStopped() {
        if (chatReceiver != null) {
            chatReceiver.closeClientThread();
        }
        chatReceiver = null;
    }

    private final class ChatListener implements IChatListener {

        @Override
        public void incomingUserList(List<ChatUser> users) {
            for (var user : users) {
                getLog().info("Connected {}", user);
            }
        }

        @Override
        public void incomingMessage(UserMessage message) {
            if (message.getToUser() != null) {
                if (message.getToUser().equals(getConnectionData().getUserName())) {
                    if (getConfig().isChatUseSounds()) {
                        AudioPlayer
                                .play(ResourceHelper.getStream(ResourceHelper.ResourceValue.SOUND_NEW_DIRECT_MESSAGE));
                    }
                }
                getLog().info("[{}] {} ({}) -> {} -> {}", MSG_PREFIX, message.getUser(), message.getUid(),
                        message.getToUser(), message.getMessage());
            } else {
                getLog().info("[{}] {} ({}) -> {}", MSG_PREFIX, message.getUser(), message.getUid(),
                        message.getMessage());
            }
            databaseService.insertChatMessage(message);
        }

        @Override
        public void sentMessage(String message) {
            getLog().info("Sent message: {}", message);
        }

        @Override
        public void userJoin(JoinedUser user) {
            if (getConfig().isChatShowJoined()) {
                getLog().info("Joined: {} ({}) | {}", user.getUser(), user.getUid(), user.getHost());
            }
        }

        @Override
        public void userQuit(ModeUser user) {
            if (getConfig().isChatShowQuit()) {
                getLog().info("Quit: {} ({}) | {} -> {}", user.getUser(), user.getUid(), user.getHost(),
                        user.getReason());
            }
        }

        @Override
        public void userMode(ModeUser user) {
            if (getConfig().isChatShowMode()) {
                getLog().info("Mode: {} ({}) | {} -> {}", user.getUser(), user.getUid(), user.getHost(),
                        user.getReason());
            }
        }

        @Override
        public void readRaw(String line) {
            if (getConfig().isChatShowRawOutput()) {
                getLog().info("Raw: {}", line);
            }
        }
    }
}
