package hd.vhackos.b0tnet.service;

import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.api.exception.ServerBusyException;
import hd.vhackos.b0tnet.api.module.TaskModule;
import hd.vhackos.b0tnet.api.net.response.NetworkScanResponse;
import hd.vhackos.b0tnet.db.service.DatabaseService;
import hd.vhackos.b0tnet.service.base.EndpointService;
import hd.vhackos.b0tnet.service.base.IService;
import hd.vhackos.b0tnet.service.base.Service;
import hd.vhackos.b0tnet.servicemodule.ServiceModule;
import hd.vhackos.b0tnet.shared.exception.B0tnetException;
import hd.vhackos.b0tnet.shared.exception.InvalidAccessTokenException;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.shared.utils.SharedUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Služba, která pouze skenuje síť a ukládá data do DB.
 */
@Inject
@EndpointService(IService.SERVICE_NETWORK_SCAN)
public final class NetworkScanService extends Service {

    @Autowired
    private DatabaseService databaseService;

    @Autowired
    private TaskModule taskModule;

    @Autowired
    private ServiceModule serviceModule;

    private volatile Integer scansCountBeforePause;
    private volatile AtomicInteger counter;
    private List<String> brutedIps;

    protected NetworkScanService(B0tnet b0tnet) {
        super(b0tnet);
    }

    @Override
    public String getDescription() {
        return "Searchs the network and saves data into the database";
    }

    @Override
    protected void initialize(B0tnet b0tnet) {
        setTimeout(getConfig().getNetworkScanTimeout());

        if (scansCountBeforePause == null) {
            scansCountBeforePause = getConfig().getNetworkScanCountBeforePause();
            counter = new AtomicInteger(0);
        }
    }

    @Override
    protected void onStopped() {
        scansCountBeforePause = null;
    }

    @Override
    protected void execute() {
        if (!databaseService.isDatabaseConnected()) {
            getLog().warn("There is no database connection available. This service requires it");
            return;
        }
        var enableBrute = getConfig().isNetworkScanAllowBrute();
        List<String> dbIps = null;

        if (enableBrute && getConfig().isNetworkScanPreferDatabase()) {
            dbIps = databaseService.getScannedIpsWithoutUser(20);
        }
        if (dbIps != null && !dbIps.isEmpty()) {
            getLog().info("{} IPs were obtained from the database and they do not have the assigned user name",
                    dbIps.size());
            brutedIps = new ArrayList(20);
            counter.incrementAndGet();
            processDatabaseIps(dbIps);
        } else {
            if (enableBrute && brutedIps == null) {
                brutedIps = prepareListOfBrutedIps();
                sleep();
            }
            counter.incrementAndGet();
            processNetwork(enableBrute);
        }
    }

    private void processDatabaseIps(List<String> ips) {
        for (var ip : ips) {
            getLog().info("Getting a user from IP: {}", ip);
            bruteIp(ip);
            sleep();
        }
        processBrutedIps();
        brutedIps.clear();
        brutedIps = null;

        var pause = getConfig().getNetworkScanPause();
        getLog().info("Done. Waiting: {}", SharedUtils.toTimeFormat(pause));
        sleep(pause);
    }

    private void processNetwork(boolean enableBrute) {
        var resp = scan();

        for (var ip : resp.getIps()) {
            var scannedIp = databaseService.addScanIp(ip);

            if (enableBrute) {
                if (scannedIp != null && scannedIp.getIp() != null) {
                    bruteIp(scannedIp.getIp());
                }
            }
        }
        if (scansCountBeforePause != null && counter.get() >= scansCountBeforePause) {
            var pause = getConfig().getNetworkScanPause();
            getLog().info("Counter: {}/{}. There will be a break in length: {}", counter, scansCountBeforePause,
                    SharedUtils.toTimeFormat(getTimeout() + pause));
            scansCountBeforePause = null;

            if (enableBrute) {
                processBrutedIps();
                brutedIps.clear();
                brutedIps = null;
            }
            getLog().info("Done. Waiting: {}", SharedUtils.toTimeFormat(pause));
            sleep(pause);
        } else {
            if (isRunningAsync()) {
                getLog().info("Counter: {}/{}. Next scan will be in: {}", counter, scansCountBeforePause,
                        SharedUtils.toTimeFormat(getTimeout()));
            }
        }
    }

    private NetworkScanResponse scan() {
        try {
            return serviceModule.scan();

        } catch (InvalidAccessTokenException e) {
            getLog().warn(
                    "An error occurred while scanning the network. The service will automatically restart in 5 seconds",
                    e);
            stop();
            SharedUtils.runAsyncProcess(() -> {
                sleep(5000);
                start();
            });
            throw e;
        }
    }

    private void bruteIp(String ip) {
        if (brutedIps == null && brutedIps.contains(ip)) {
            return;
        }
        try {
            if (recursiveBruteIp(ip, 1, 3)) {
                brutedIps.add(ip);
            } else {
                getLog().warn(
                        "It was not possible to break IP because the server was too busy. IP '{}' will be skipped", ip);
            }
        } catch (B0tnetException e) {
            getLog().warn("There was an error. It will be skipped. Message: {}", e.getMessage());
        }
    }

    private void processBrutedIps() {
        sleep();
        var resp = taskModule.getTasks();

        for (var brute : resp.getBrutedIps()) {
            databaseService.updateScanIp(brute.getIp(), brute.getUserName(), null);
            taskModule.removeBruteforce(brute.getBruteId());
        }
        getLog().info("Updating all IPs has been completed");
    }

    private List<String> prepareListOfBrutedIps() {
        var resp = taskModule.getTasks();
        getShared().setTaskResponse(resp);

        var list = new ArrayList<String>(resp.getBrutedIps().size());

        for (var brute : resp.getBrutedIps()) {
            list.add(brute.getIp());
        }
        return list;
    }

    private boolean recursiveBruteIp(String ip, int attempts, int max) {
        if (attempts > max) {
            return false;
        }
        try {
            serviceModule.bruteforce(ip);

        } catch (ServerBusyException e) {
            getLog().warn("The server is busy. Current attempt: {}", attempts);
            sleep(5000);
            recursiveBruteIp(ip, ++attempts, max);
        }
        return true;
    }
}
