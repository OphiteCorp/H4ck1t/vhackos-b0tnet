package hd.vhackos.b0tnet.service;

import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.api.module.StoreModule;
import hd.vhackos.b0tnet.api.module.TaskModule;
import hd.vhackos.b0tnet.db.service.DatabaseService;
import hd.vhackos.b0tnet.service.base.EndpointService;
import hd.vhackos.b0tnet.service.base.IService;
import hd.vhackos.b0tnet.service.base.Service;
import hd.vhackos.b0tnet.shared.dto.AppStoreType;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.shared.utils.SharedUtils;

/**
 * Služba, která vykonává aktualizace účtu pro udržení aktuálních dat.
 */
@Inject
@EndpointService(IService.SERVICE_UPDATE)
public final class UpdateService extends Service {

    @Autowired
    private DatabaseService databaseService;

    @Autowired
    private TaskModule taskModule;

    @Autowired
    private StoreModule storeModule;

    protected UpdateService(B0tnet b0tnet) {
        super(b0tnet);
    }

    @Override
    public String getDescription() {
        return "Keeps user information up to date";
    }

    @Override
    protected void initialize(B0tnet b0tnet) {
        setTimeout(getConfig().getUpdateTimeout());
    }

    @Override
    protected void execute() {
        databaseService.correctUserInconsistency();

        var updateResp = getCommonModule().update();
        getShared().setUpdateResponse(updateResp);
        sleep();

        var tasksResp = taskModule.getTasks();
        getShared().setTaskResponse(tasksResp);

        // zakoupí aplikace, které se zobrazují v navigaci (v menu) a které nelze dále vylepšovat
        if (updateResp != null) {
            buyMenuApplications(updateResp.getLevel(), updateResp.getMoney());
        }
        if (isRunningAsync()) {
            getLog().info("Profile updated. Next update will be in: {}", SharedUtils.toTimeFormat(getTimeout()));
        }
    }

    private void buyMenuApplications(Integer playerLevel, Long money) {
        if (playerLevel == null && money == null) {
            return;
        }
        sleep();
        var appsResp = storeModule.getApps();

        for (var app : appsResp.getApps()) {
            var type = AppStoreType.getById(app.getAppId());

            // je možné zakoupit aplikaci?
            if (playerLevel >= app.getRequireLevel() && app.getLevel() == 0) {
                if (money >= app.getPrice()) {
                    sleep();
                    var resp = storeModule.buyMenuApp(type);

                    if (SharedUtils.toBoolean(resp.getInstalled())) {
                        getLog().info("A new application was purchased: {}", type.getAlias());
                    }
                } else {
                    getLog().warn("A new '{}' app is available, but you do not have the money to buy", type.getAlias());
                }
            }
        }
    }
}
