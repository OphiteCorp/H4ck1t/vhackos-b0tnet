package hd.vhackos.b0tnet.service.base;

/**
 * Základní rozhraní služby.
 */
public interface IService {

    String SERVICE_UPDATE = "update";
    String SERVICE_MINER = "miner";
    String SERVICE_MALWARE = "malware";
    String SERVICE_SERVER = "server";
    String SERVICE_STORE = "store";
    String SERVICE_BOOSTER = "booster";
    String SERVICE_MISSION = "mission";
    String SERVICE_NETWORK = "network";
    String SERVICE_NETWORK_SCAN = "netscan";
    String SERVICE_CHAT = "chat";
    String SERVICE_BRANDON_OS = "brandonos";
    String SERVICE_LUCKY_WHEEL = "luckywheel";
    String SERVICE_B0TNET_UPDATE = "b0tnetupdate";
    String SERVICE_B0TNET_WEB_API = "b0tnetwebapi";

    /**
     * Spustí službu asynchronně.
     */
    boolean start();

    /**
     * Spustí službu (je možné spustit službu i synchronně).
     */
    boolean start(ServiceConfig config);

    /**
     * Zastaví službu.
     */
    boolean stop();

    /**
     * Detekuje, zda služba již běží.
     */
    boolean isRunning();

    /**
     * Popis služby.
     */
    default String getDescription() {
        return "";
    }
}
