package hd.vhackos.b0tnet.service.base;

/**
 * Konfigurace spuštění služby.
 */
public final class ServiceConfig {

    private boolean async;
    private boolean firstRunSync;

    boolean isAsync() {
        return async;
    }

    public void setAsync(boolean async) {
        this.async = async;
    }

    boolean isFirstRunSync() {
        return firstRunSync;
    }

    public void setFirstRunSync(boolean firstRunSync) {
        this.firstRunSync = firstRunSync;
    }
}
