package hd.vhackos.b0tnet.service;

import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.api.module.TaskModule;
import hd.vhackos.b0tnet.api.net.response.TaskResponse;
import hd.vhackos.b0tnet.api.net.response.data.TaskUpdateData;
import hd.vhackos.b0tnet.service.base.EndpointService;
import hd.vhackos.b0tnet.service.base.IService;
import hd.vhackos.b0tnet.service.base.Service;
import hd.vhackos.b0tnet.shared.dto.AppStoreType;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.shared.utils.SharedUtils;

/**
 * Aplikuje boosty na tásky.
 */
@Inject
@EndpointService(IService.SERVICE_BOOSTER)
public final class BoosterService extends Service {

    @Autowired
    private TaskModule taskModule;

    protected BoosterService(B0tnet b0tnet) {
        super(b0tnet);
    }

    @Override
    public String getDescription() {
        return "Apply boosters to active tasks";
    }

    @Override
    protected void initialize(B0tnet b0tnet) {
        setTimeout(getConfig().getBoosterTimeout());
    }

    @Override
    protected void execute() {
        var resp = taskModule.getTasks();
        getShared().setTaskResponse(resp);

        if (resp == null || resp.getBoosters() <= getConfig().getSafeBoosters() || resp.getUpdates().isEmpty()) {
            if (resp != null) {
                resp = finishForNetcoins(resp);
            }
            printEndMessage(resp);
            return;
        }
        var data = getLongestLastingTask(resp);
        var seconds = data.getEnd() - data.getNow();

        while (seconds > getConfig().getBoosterReqTime()) {
            sleep();
            resp = taskModule.boostTask(data.getAppId());
            getShared().setTaskResponse(resp);
            data = getLongestLastingTask(resp);

            if (data != null) {
                seconds = data.getEnd() - data.getNow();
                getLog().info("Boost used. The longest time is now: {}", SharedUtils.toTimeFormat(seconds * 1000));
            } else {
                break;
            }
        }
        resp = finishForNetcoins(resp);
        printEndMessage(resp);
    }

    private TaskUpdateData getLongestLastingTask(TaskResponse resp) {
        var maxEnd = Long.MIN_VALUE;
        TaskUpdateData data = null;

        for (var update : resp.getUpdates()) {
            if (update.getEnd() > maxEnd) {
                maxEnd = update.getEnd();
                data = update;
            }
        }
        return data;
    }

    private TaskResponse finishForNetcoins(TaskResponse resp) {
        if (getConfig().isBoosterUseNetcoins()) {
            if (resp.getUpdateCount() > 0) {
                if (resp.getNetCoins() <= getConfig().getSafeNetcoins()) {
                    return resp;
                }
                var updates = resp.getUpdates();

                for (var update : updates) {
                    var remaining = update.getEnd() - update.getNow();

                    if (remaining <= getConfig().getBoosterMaxTimeForNetcoins()) {
                        sleep();
                        resp = taskModule.finishForNetcoins(update.getTaskId());

                        var type = AppStoreType.getById(update.getAppId());
                        getLog().info("The '{}' update was finished with netcoins. Remaining netcoins: {}",
                                type.getAlias(), resp.getNetCoins());
                    }
                }
            }
        }
        return resp;
    }

    private void printEndMessage(TaskResponse resp) {
        if (isRunningAsync()) {
            getLog().info("Done. Remaining {} boosters. Next check will be in: {}", resp.getBoosters(),
                    SharedUtils.toTimeFormat(getTimeout()));
        }
    }
}
