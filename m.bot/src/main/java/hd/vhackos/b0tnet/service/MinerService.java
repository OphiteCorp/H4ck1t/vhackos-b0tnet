package hd.vhackos.b0tnet.service;

import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.api.module.MiningModule;
import hd.vhackos.b0tnet.api.net.response.MiningResponse;
import hd.vhackos.b0tnet.service.base.EndpointService;
import hd.vhackos.b0tnet.service.base.IService;
import hd.vhackos.b0tnet.service.base.Service;
import hd.vhackos.b0tnet.shared.dto.MinerState;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.shared.utils.SharedUtils;

/**
 * Služba, která kontroluje a ovládá miner.
 */
@Inject
@EndpointService(IService.SERVICE_MINER)
public final class MinerService extends Service {

    private static final int MAX_GPU_COUNT = 5;

    @Autowired
    private MiningModule miningModule;

    protected MinerService(B0tnet b0tnet) {
        super(b0tnet);
    }

    @Override
    public String getDescription() {
        return "Controls and manages netcoins mining";
    }

    @Override
    protected void initialize(B0tnet b0tnet) {
        setTimeout(getConfig().getMinerTimeout());
    }

    @Override
    protected void execute() {
        if (getShared().getUpdateResponse() == null) {
            var resp = getCommonModule().update();
            getShared().setUpdateResponse(resp);
            sleep();
        }
        if (!SharedUtils.toBoolean(getShared().getUpdateResponse().getMiner())) {
            if (isRunningAsync()) {
                getLog().info("Miner is not available. Next check will be in: {}",
                        SharedUtils.toTimeFormat(getTimeout()));
            } else {
                getLog().info("Miner is not available");
            }
            return;
        }
        var resp = miningModule.getMining();
        var state = MinerState.getByCode(resp.getRunning());
        var currentNetcoins = (resp.getNetCoins() != null) ? resp.getNetCoins() : 0;

        getLog().info("{} -> Netcoins: {}, GPUs: {}", state.getAlias(), currentNetcoins, resp.getGpuCount());

        switch (state) {
            case STOPPED:
                tryBuyGpu(resp);
                break;

            case FINISHED:
                miningModule.collect();
                sleep();
                resp = miningModule.getMining();
                currentNetcoins = ((resp.getNetCoins() != null) ? resp.getNetCoins() : 0) - currentNetcoins;
                getLog().info("{} netcoins were mined", currentNetcoins);
                sleep();
                tryBuyGpu(resp);
                break;

            case RUNNING:
                getLog().info("Will finish in: {}. Next check will be in: {}",
                        SharedUtils.toTimeFormat(resp.getLeft() * 1000), SharedUtils.toTimeFormat(getTimeout()));
                return;
        }
        miningModule.start();

        if (isRunningAsync()) {
            getLog().info("Miner was started. Next check will be in: {}", SharedUtils.toTimeFormat(getTimeout()));
        } else {
            getLog().info("Miner was started");
        }
    }

    private void tryBuyGpu(MiningResponse resp) {
        // uživatel již má všechny GPU, takže cost přestane nechodit v odpovědi
        if (resp.getNewGpuCosts() != null) {
            if (resp.getGpuCount() < MAX_GPU_COUNT && resp.getNetCoins() >= resp.getNewGpuCosts() && resp
                    .getNetCoins() > getConfig().getSafeNetcoins()) {

                getLog().info("Buying {} GPU for {} netcoins", resp.getGpuCount() + 1, resp.getNewGpuCosts());
                resp = miningModule.buyGpu();
                getLog().info("Next GPU was bought. You have {} netcoins left", resp.getNetCoins());
                sleep();
            }
        }
    }
}
