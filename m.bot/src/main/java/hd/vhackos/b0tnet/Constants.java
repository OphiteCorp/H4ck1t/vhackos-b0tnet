package hd.vhackos.b0tnet;

/**
 * Definuje všechny interní konstanty.
 */
public final class Constants {

    public static final String BASE_PACKAGE = Application.class.getPackage().getName();
    public static final String AUTHOR = "H-Division";
    public static final String YEAR = "2018";
    public static final String DISCORD = "https://discordapp.com/invite/Cdz39vu";
}
