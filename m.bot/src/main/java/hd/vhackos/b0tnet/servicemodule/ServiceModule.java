package hd.vhackos.b0tnet.servicemodule;

import hd.vhackos.b0tnet.api.exception.IpNotExistsException;
import hd.vhackos.b0tnet.api.module.BankModule;
import hd.vhackos.b0tnet.api.module.LogModule;
import hd.vhackos.b0tnet.api.module.NetworkModule;
import hd.vhackos.b0tnet.api.module.RemoteModule;
import hd.vhackos.b0tnet.api.net.response.ExploitResponse;
import hd.vhackos.b0tnet.api.net.response.NetworkScanResponse;
import hd.vhackos.b0tnet.api.net.response.RemoteSystemResponse;
import hd.vhackos.b0tnet.db.service.DatabaseService;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Servisní modul, který obalí volání modulů o volání do databáze.
 */
@Inject
public final class ServiceModule {

    @Autowired
    private DatabaseService databaseService;

    @Autowired
    private BankModule bankModule;

    @Autowired
    private NetworkModule networkModule;

    @Autowired
    private RemoteModule remoteModule;

    @Autowired
    private LogModule logModule;

    /**
     * Proskenuje síť.
     */
    public NetworkScanResponse scan() {
        var resp = networkModule.scan();
        var ips = resp.getIps();
        var brutedIps = resp.getBrutedIps();

        if (ips != null) {
            for (var ip : resp.getIps()) {
                databaseService.addScanIp(ip);
            }
        }
        if (brutedIps != null) {
            for (var ip : resp.getBrutedIps()) {
                databaseService.updateScanIp(ip.getIp(), ip.getUserName(), null);
            }
        }
        return resp;
    }

    /**
     * Exploituje IP.
     */
    public ExploitResponse exploit(String ip) {
        try {
            var resp = networkModule.exploit(ip);

            for (var bruteIp : resp.getBrutedIps()) {
                databaseService.updateScanIp(bruteIp.getIp(), bruteIp.getUserName(), null);
            }
            return resp;

        } catch (IpNotExistsException e) {
            databaseService.invalidIp(ip);
            throw e;
        }
    }

    /**
     * Prolomí IP banky.
     */
    public void bruteforce(String ip) {
        try {
            bankModule.bruteforce(ip);

        } catch (IpNotExistsException e) {
            databaseService.invalidIp(ip);
            throw e;
        }
    }

    /**
     * Získá informace o systému.
     */
    public RemoteSystemResponse getSystemInfo(String ip) {
        var resp = remoteModule.getSystemInfo(ip);
        databaseService.updateScanIp(ip, resp.getUserName(), resp.getLevel());
        return resp;
    }

    /**
     * Získá vzdálený log.
     */
    public List<String> getRemoteLog(String ip) {
        var remoteLog = logModule.getRemoteLog(ip);
        databaseService.addLog(ip, StringUtils.join(remoteLog, '\n'));
        return remoteLog;
    }
}
