package hd.vhackos.b0tnet;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.IB0tnetConfig;
import hd.vhackos.b0tnet.api.dto.ConnectionData;
import hd.vhackos.b0tnet.api.net.response.LoginResponse;
import hd.vhackos.b0tnet.config.ApplicationConfig;
import hd.vhackos.b0tnet.config.ConfigProvider;
import hd.vhackos.b0tnet.dto.B0tnetSharedData;
import hd.vhackos.b0tnet.dto.CacheData;
import hd.vhackos.b0tnet.exception.MissingLoginCredentialException;
import hd.vhackos.b0tnet.gui.LoginDialog;
import hd.vhackos.b0tnet.service.base.IService;
import hd.vhackos.b0tnet.service.base.Service;
import hd.vhackos.b0tnet.service.base.ServiceConfig;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.json.Json;
import hd.vhackos.b0tnet.shared.utils.EncryptUtils;
import hd.vhackos.b0tnet.shared.utils.SentryGuard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Hlavní třída B0tnet.
 */
public final class B0tnet implements IB0tnet {

    private static final Logger LOG = LoggerFactory.getLogger(B0tnet.class);

    @Autowired
    private ApplicationConfig config;

    @Autowired
    private ConnectionData connectionData;

    @Autowired
    private ConfigProvider configProvider;

    private volatile B0tnetSharedData sharedData;
    private boolean started;

    public B0tnet() {
        sharedData = new B0tnetSharedData();
    }

    @Override
    public IB0tnetConfig getConfig() {
        return config;
    }

    @Override
    public ConnectionData getConnectionData() {
        return connectionData;
    }

    @Override
    public void reloginCallback(LoginResponse loginData) {
        createCacheData();
    }

    /**
     * Spustí B0tnet.
     */
    public synchronized void start() {
        if (started) {
            return;
        }
        if (!config.hasValidCredentials()) {
            var loginSet = new AtomicBoolean(false);

            new LoginDialog(config, loginData -> {
                if (loginData.isLoginPressed()) {
                    config.setUserName(loginData.getUserName());
                    config.setPassword(EncryptUtils.encrypt(config.getControlKey(), loginData.getPassword()));
                    configProvider.update(config);
                    loginSet.set(true);
                }
            });
            if (!loginSet.get()) {
                throw new MissingLoginCredentialException();
            }
        }
        // připraví connection data
        connectionData.setLang(Locale.getDefault().getLanguage());
        connectionData.setAccessToken(config.getFixedAccessToken());
        connectionData.setUid(config.getFixedUserUid());
        connectionData.setUserName(config.getUserName());

        // načte cache z předchozí session
        var cache = loadCacheData();
        if (cache != null) {
            var cd = cache.getConnectionData();

            if (cd.getUserName() != null && !cd.getUserName().isEmpty() && cd.getUid() != null && cd
                    .getAccessToken() != null && !cd.getAccessToken().isEmpty()) {
                connectionData.set(cache.getConnectionData());
            }
        }
        // vypíše proxy server
        var proxy = config.getProxyData();
        if (proxy != null) {
            LOG.info("A proxy server is set up. Instead, real IP will be used: {}:{}", proxy.getIp(), proxy.getPort());
        }
        // získá informace o poslední verzi B0tnet
        if (config.isB0tnetUpdateEnable()) {
            LOG.info("Checking the latest version of B0tnet. Please wait...", config.getUserName());
            var serviceConfig = new ServiceConfig();
            serviceConfig.setAsync(true);
            serviceConfig.setFirstRunSync(true);
            Service.getServices().get(IService.SERVICE_B0TNET_UPDATE).start(serviceConfig);
        }
        // zkontroluje, případně přihlásí uživatele s novým tokenem
        LOG.info("Getting user '{}' information. Please wait...", config.getUserName());
        var serviceConfig = new ServiceConfig();
        serviceConfig.setAsync(true);
        serviceConfig.setFirstRunSync(true);
        Service.getServices().get(IService.SERVICE_UPDATE).start(serviceConfig);

        SentryGuard.setB0tnetUser(config.getUserName());
        logUserLogin(cache);

        // spustí služby
        initializeServices();
        started = true;
    }

    public boolean isStarted() {
        return started;
    }

    public B0tnetSharedData getSharedData() {
        return sharedData;
    }

    private void initializeServices() {
        var services = Service.getServices();

        if (config.isChatEnable()) {
            services.get(IService.SERVICE_CHAT).start();
        }
        if (config.isBrandonOsEnable()) {
            services.get(IService.SERVICE_BRANDON_OS).start();
        }
        if (config.isMinerEnable()) {
            services.get(IService.SERVICE_MINER).start();
        }
        if (config.isMalwareEnable()) {
            services.get(IService.SERVICE_MALWARE).start();
        }
        if (config.isServerEnable()) {
            services.get(IService.SERVICE_SERVER).start();
        }
        if (config.isStoreEnable()) {
            services.get(IService.SERVICE_STORE).start();
        }
        if (config.isBoosterEnable()) {
            services.get(IService.SERVICE_BOOSTER).start();
        }
        if (config.isMissionEnable()) {
            services.get(IService.SERVICE_MISSION).start();
        }
        if (config.isNetworkEnable()) {
            services.get(IService.SERVICE_NETWORK).start();
        }
        if (config.isNetworkScanEnable()) {
            services.get(IService.SERVICE_NETWORK_SCAN).start();
        }
        if (config.isLuckyWheelEnable()) {
            services.get(IService.SERVICE_LUCKY_WHEEL).start();
        }
        if (config.isWebApiEnable()) {
            services.get(IService.SERVICE_B0TNET_WEB_API).start();
        }
    }

    /**
     * Ukončí B0tnet. Volá se automaticky při ukončení aplikace.
     */
    void shutdown() {
        LOG.debug("Shutdown B0tnet called");
        var services = Service.getServices();
        for (var service : services.values()) {
            service.stop();
        }
        createCacheData();
    }

    private CacheData loadCacheData() {
        var cacheData = Json.toObject(new File(CacheData.FILE_NAME), CacheData.class);

        if (cacheData != null) {
            LOG.info("Cache was retrieved from file: {}", CacheData.FILE_NAME);
        }
        return cacheData;
    }

    private void createCacheData() {
        var cache = loadCacheData();

        if (cache == null) {
            cache = new CacheData();
        }
        cache.setConnectionData(connectionData);

        if (Json.toFile(CacheData.FILE_NAME, cache) != null) {
            LOG.info("The cache has been saved to a file: {}", CacheData.FILE_NAME);
        }
    }

    /**
     * Zaloguje do sentry přihlášení uživatele pouze v případě, že je to nutné. Sice tím ztratíme přehled o aktivitě
     * uživatelů používající bot, ale snížíme režii pro Sentry, která je ve free verzi omezená.
     */
    private void logUserLogin(CacheData cache) {
        final var lastApi = cache.getLastWorkingGameApi();
        var alreadyLogged = cache.isSuccessfullyLogged();

        if (!alreadyLogged || (lastApi != null && !lastApi.equals(config.getGameApi()))) {
            alreadyLogged = false;
            cache.setLastWorkingGameApi(config.getGameApi());
            cache.setSuccessfullyLogged(true);
            Json.toFile(CacheData.FILE_NAME, cache);
            LOG.debug("The cache has been updated to a file: {}", CacheData.FILE_NAME);
        }
        if (!alreadyLogged) {
            SentryGuard.log("B0tnet was launched under the user: " + config.getUserName());
        }
    }
}
