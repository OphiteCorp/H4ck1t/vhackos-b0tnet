package hd.vhackos.b0tnet.exception;

import hd.vhackos.b0tnet.shared.exception.B0tnetCoreException;

/**
 * V konfiguraci chybí vyplnit přihlašovací jméno nebo heslo.
 */
public final class MissingLoginCredentialException extends B0tnetCoreException {

}
