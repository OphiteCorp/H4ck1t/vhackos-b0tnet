package hd.vhackos.b0tnet.exception;

import hd.vhackos.b0tnet.shared.exception.B0tnetCoreException;

/**
 * Je nařízena aktualizace.
 */
public final class ForceUpdateRequiredException extends B0tnetCoreException {

}
