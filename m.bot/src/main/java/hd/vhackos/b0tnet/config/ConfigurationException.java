package hd.vhackos.b0tnet.config;

/**
 * Nastala chyba při vytvoření nebo získání konfiguračního souboru aplikace.
 */
final class ConfigurationException extends RuntimeException {

    ConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }
}
