package hd.vhackos.b0tnet.config;

import hd.vhackos.b0tnet.shared.utils.SharedUtils;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Pomocné metody pro práci s konfigurací.
 */
public final class ConfigHelper {

    /**
     * Převede textovou hodnotu na boolean.
     */
    static boolean getBoolean(String value) {
        if (value != null && !value.isEmpty()) {
            if ("yes".equalsIgnoreCase(value)) {
                return true;
            }
            return Boolean.valueOf(value);
        }
        return false;
    }

    /**
     * Převede textohou hodnotu na číslo.
     */
    static <T> T getNumbericValue(String value, Class<T> target) {
        value = ConfigValueProcessor.process(value);
        var result = SharedUtils.eval(value);
        var out = result;

        if (result instanceof Double && target != Double.class) {
            var dbl = (Double) result;

            if (target == Long.class) {
                out = dbl.longValue();

            } else if (target == Integer.class) {
                out = dbl.intValue();
            }
        } else if (out.getClass() != target) {
            if (result instanceof Integer && target == Long.class) {
                out = Long.valueOf(result.toString());

            } else if (result instanceof Integer && target == Double.class) {
                out = Double.valueOf(result.toString());
            }
        }
        return (T) out;
    }

    /**
     * Získá konfiguraci jako mapu.
     */
    public static Map<String, Object> asMap(ApplicationConfig config) {
        var map = new LinkedHashMap<String, Object>();
        var fields = config.getClass().getDeclaredFields();

        for (var field : fields) {
            if (field.isAnnotationPresent(ConfigValue.class)) {
                field.setAccessible(true);
                var a = field.getAnnotation(ConfigValue.class);

                try {
                    var value = field.get(config);
                    map.put(a.value(), value);

                } catch (Exception e) {
                    throw new IllegalStateException("An unexpected error has occurred", e);
                }
            }
        }
        return map;
    }

    /**
     * Získá properties z konfigurace.
     */
    static Properties asProperties(ApplicationConfig config) {
        var map = ConfigHelper.asMap(config);
        var prop = new Properties();

        for (var entry : map.entrySet()) {
            prop.put(entry.getKey(), entry.getValue());
        }
        return prop;
    }
}
