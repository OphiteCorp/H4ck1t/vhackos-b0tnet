package hd.vhackos.b0tnet.config;

import java.util.regex.Pattern;

/**
 * Zpracuje výchozí hodnoty a hodnoty z konfigurace.
 */
final class ConfigValueProcessor {

    private static final Pattern PATTERN_F_RAND = Pattern.compile("f_rand\\(\\s*(\\d+)\\s*\\)");
    private static final Pattern PATTERN_F_RAND_RANGE = Pattern.compile("f_rand\\(\\s*(\\d+)\\s*,\\s*(\\d+)\\s*\\)");
    private static final Pattern PATTERN_F_RMSEC = Pattern.compile("f_rmsec\\(\\)");

    /**
     * Zpracuje hodnotu.
     */
    static String process(String value) {
        var m = PATTERN_F_RAND_RANGE.matcher(value);
        if (m.find() && m.groupCount() == 2) {
            var min = Long.valueOf(m.group(1));
            var max = Long.valueOf(m.group(2)) - min + 1;
            value = m.replaceAll(String.format("(Math.floor(Math.random()*%s)+%s)", max, min));
        }
        m = PATTERN_F_RAND.matcher(value);
        if (m.find() && m.groupCount() == 1) {
            var val = Long.valueOf(m.group(1));
            value = m.replaceAll(String.format("(Math.floor(Math.random()*%s))", val));
        }
        m = PATTERN_F_RMSEC.matcher(value);
        if (m.find()) {
            value = m.replaceAll(String.format("(Math.floor(Math.random()*%s)+%s)", 1000, 0));
        }
        return value;
    }
}
