package hd.vhackos.b0tnet.dto;

import hd.vhackos.b0tnet.api.net.response.TaskResponse;
import hd.vhackos.b0tnet.api.net.response.UpdateResponse;
import hd.vhackos.b0tnet.shared.utils.SharedUtils;

/**
 * Data, která jsou sdílená napříč všema službama.
 */
public final class B0tnetSharedData {

    private static final int MAX_TASKS = 10;
    private static final int MAX_VIP_TASKS = 12;

    private volatile UpdateResponse updateResponse;
    private volatile TaskResponse taskResponse;
    private volatile B0tnetUpdateData updateData;

    private int maxTaskUpdates;

    public UpdateResponse getUpdateResponse() {
        return updateResponse;
    }

    public void setUpdateResponse(UpdateResponse updateResponse) {
        if (updateResponse != null) {
            this.updateResponse = updateResponse;
            var vip = SharedUtils.toBoolean(updateResponse.getVip());
            maxTaskUpdates = vip ? MAX_VIP_TASKS : MAX_TASKS;
        }
    }

    public TaskResponse getTaskResponse() {
        return taskResponse;
    }

    public void setTaskResponse(TaskResponse taskResponse) {
        if (taskResponse != null) {
            this.taskResponse = taskResponse;
        }
    }

    public int getMaxTaskUpdates() {
        return maxTaskUpdates;
    }

    public B0tnetUpdateData getUpdateData() {
        return updateData;
    }

    public void setUpdateData(B0tnetUpdateData updateData) {
        this.updateData = updateData;
    }
}
