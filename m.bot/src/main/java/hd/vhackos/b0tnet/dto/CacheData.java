package hd.vhackos.b0tnet.dto;

import com.google.gson.annotations.SerializedName;
import hd.vhackos.b0tnet.api.dto.ConnectionData;

/**
 * Obsahuje cache data, která se
 */
public final class CacheData {

    public static final String FILE_NAME = "b0tnet_cache.json";

    @SerializedName("connection_data")
    private ConnectionData connectionData;

    @SerializedName("successfully_logged")
    private boolean successfullyLogged;

    @SerializedName("last_working_game_api")
    private Integer lastWorkingGameApi;

    public ConnectionData getConnectionData() {
        return connectionData;
    }

    public void setConnectionData(ConnectionData connectionData) {
        this.connectionData = connectionData;
    }

    public boolean isSuccessfullyLogged() {
        return successfullyLogged;
    }

    public void setSuccessfullyLogged(boolean successfullyLogged) {
        this.successfullyLogged = successfullyLogged;
    }

    public Integer getLastWorkingGameApi() {
        return lastWorkingGameApi;
    }

    public void setLastWorkingGameApi(Integer lastWorkingGameApi) {
        this.lastWorkingGameApi = lastWorkingGameApi;
    }
}
