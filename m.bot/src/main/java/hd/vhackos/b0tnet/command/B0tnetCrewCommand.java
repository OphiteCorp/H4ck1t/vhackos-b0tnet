package hd.vhackos.b0tnet.command;

import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.api.module.CommonModule;
import hd.vhackos.b0tnet.api.module.CrewModule;
import hd.vhackos.b0tnet.api.net.response.CrewProfileResponse;
import hd.vhackos.b0tnet.api.net.response.CrewResponse;
import hd.vhackos.b0tnet.api.net.response.data.CrewMessageData;
import hd.vhackos.b0tnet.api.net.response.data.CrewRushData;
import hd.vhackos.b0tnet.api.net.response.data.DefaceData;
import hd.vhackos.b0tnet.api.net.response.data.interfaces.ICrewData;
import hd.vhackos.b0tnet.command.base.BaseCommand;
import hd.vhackos.b0tnet.shared.ascii.AsciiMaker;
import hd.vhackos.b0tnet.shared.command.Command;
import hd.vhackos.b0tnet.shared.command.CommandParam;
import hd.vhackos.b0tnet.shared.dto.CrewPositionType;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.shared.utils.SharedUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Příkazy kolem crew.
 */
@Inject
public final class B0tnetCrewCommand extends BaseCommand {

    @Autowired
    private CrewModule crewModule;

    @Autowired
    private CommonModule commonModule;

    protected B0tnetCrewCommand(B0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Získá profil crew.
     */
    @Command(value = "crew profile", comment = "Gets a crew profile")
    private String getProfile(@CommandParam("crewId") long crewId) {
        return execute("crew profile -> " + crewId, am -> {
            am.setInsideTheme();

            var data = crewModule.getProfile(crewId);
            var fields = getFields(data, true);
            addCrewProfileResponseToAsciiMaker(am, fields);
        });
    }

    /**
     * Získá informace o aktuální crew.
     */
    @Command(value = "crew", comment = "Gets information on the current crew")
    private String getCrew() {
        return execute("crew", am -> {
            am.setInsideTheme();

            var data = crewModule.getCrew();
            var fields = getFields(data, true);
            addCrewResponseToAsciiMaker(am, fields);
        });
    }

    /**
     * Odešle crew zprávu.
     */
    @Command(value = "crew msg", comment = "Sends crew a message")
    private String sendMessage(@CommandParam("message") String message) {
        return execute("send crew message", am -> {
            am.setInsideTheme();

            var data = crewModule.sendMessage(message);
            var fields = getFields(data, true);
            addCrewResponseToAsciiMaker(am, fields);
        });
    }

    /**
     * Opustí crew.
     */
    @Command(value = "crew leave", comment = "Leaves the crew")
    private String leaveCrew() {
        return execute("leave crew", am -> {
            am.setInsideTheme();

            var data = crewModule.leaveCrew();
            var fields = getFields(data, true);
            addCrewResponseToAsciiMaker(am, fields);
        });
    }

    /**
     * Odešle žádost do crew.
     */
    @Command(value = "crew request", comment = "Sends a request to join the crew")
    private String sendCrewRequest(@CommandParam("crewTag") String crewTag) {
        return execute("send crew request", am -> {
            var sent = crewModule.sendRequest(crewTag);

            if (sent) {
                am.add("Success", "Request to join crew has been sent.");
            } else {
                am.add("Error", "Request to join crew was not sent. Probably already in some crew you are.");
            }
        });
    }

    /**
     * Odešle žádost do prvních N crew v TOP 100.
     */
    @Command(value = "needcrew", comment = "Sends a request to join to first N (1-100) crews ^^")
    private String needCrewRequest(@CommandParam("firstTop") String firstTop) {
        var limit = SharedUtils.toInt(firstTop);
        if (limit == null || limit <= 0) {
            getLog().warn("The input value '{}' is not valid. Must be 1-100. The request will be sent to all crews",
                    firstTop);
            limit = Integer.MAX_VALUE;
        }
        final var limitFinal = limit;

        return execute("need crew N", am -> {
            var leaders = commonModule.getLeaderboards();
            var map = new LinkedHashMap<String, String>();
            var count = Math.min(leaders.getCrewsData().size(), limitFinal);

            for (var i = 0; i < count; i++) {
                var crew = leaders.getCrewsData().get(i);
                getLog().info("Sending a request to the crew '{}' with ID: {}", crew.getCrewName(), crew.getCrewId());
                var sent = crewModule.sendRequest(crew.getCrewTag());

                if (sent) {
                    map.put(crew.getCrewTag(), crew.getCrewName());
                }
            }
            if (!map.isEmpty()) {
                am.add("Info", "The request to join the crew was sent to the following crews:");
                am.add("", "");

                for (var crew : map.entrySet()) {
                    put(am, "", String.format("%s | %s", StringUtils.rightPad(crew.getKey(), 6),
                            StringUtils.rightPad(crew.getValue(), 20)));
                }
            } else {
                am.add("Info",
                        "Unfortunately, there was not one request to join the crew. You may already be in some crew.");
            }
        });
    }

    // === Pomocné metody
    // ================================================================================================================

    private void addCrewProfileResponseToAsciiMaker(AsciiMaker am, Map<String, B0tnetCommands.FieldData> fields) {
        var logo = fields.remove(CrewProfileResponse.P_CREW_LOGO);
        put(am, logo);
        put(am, fields.remove(CrewProfileResponse.P_CREW_NAME));
        put(am, fields.remove(CrewProfileResponse.P_CREW_TAG));
        put(am, fields.remove(CrewProfileResponse.P_CREW_MEMBERS));
        put(am, fields.remove(CrewProfileResponse.P_CREW_REPUTATION));
        putRemainings(am, fields, logo != null);
    }

    private void addCrewResponseToAsciiMaker(AsciiMaker am, Map<String, B0tnetCommands.FieldData> fields) {
        var logo = fields.remove(CrewResponse.P_CREW_LOGO);
        put(am, logo);
        put(am, fields.remove(CrewResponse.P_CREW_NAME));
        put(am, fields.remove(CrewResponse.P_CREW_TAG));
        put(am, fields.remove(CrewResponse.P_CREW_MEMBERS));
        put(am, fields.remove(CrewResponse.P_CREW_REPUTATION));
        put(am, fields.remove(CrewResponse.P_CREW_RANK));
        put(am, fields.remove(CrewResponse.P_CREW_OWNER));
        put(am, fields.remove(CrewResponse.P_CREW_MEMBERS));
        put(am, fields.remove(CrewResponse.P_CREW_ID));
        put(am, fields.remove(CrewResponse.P_USER_ID));
        put(am, fields.remove(CrewResponse.P_USER_NAME));
        put(am, fields.remove(CrewResponse.P_MESSAGE));
        put(am, fields.remove(CrewResponse.P_REQ_COUNT));
        put(am, fields.remove(CrewResponse.P_DEFACE_COUNT));

        Object time = fields.remove(CrewResponse.P_TIME);
        if (time != null) {
            time = MESSAGE_TIME_FORMAT.format(calculateTime((Long) ((FieldData) time).rawValue));
        }
        put(am, "Time", time);

        var membersLb = fields.remove(CrewResponse.P_MEMBERS);
        var members = (List<ICrewData>) membersLb.value;
        var messagesLb = fields.remove(CrewResponse.P_MESSAGES);
        var messages = (List<CrewMessageData>) messagesLb.value;
        var requestsLb = fields.remove(CrewResponse.P_REQUESTS);
        var requests = (requestsLb != null && requestsLb.value != null) ? (List<ICrewData>) requestsLb.value : null;
        var defacesLb = fields.remove(CrewResponse.P_DEFACES);
        var defaces = (defacesLb != null) ? (List<DefaceData>) defacesLb.value : null;
        var rushesLb = fields.remove(CrewResponse.P_RUSHES);
        var rushes = (rushesLb != null) ? (List<CrewRushData>) rushesLb.value : null;

        putRemainings(am, fields, logo != null);

        if (!members.isEmpty()) {
            am.addRule();
            convertCrewData(am, membersLb.name, members);
        }
        if (requests != null && !requests.isEmpty()) {
            am.addRule();
            convertCrewData(am, requestsLb.name, requests);
        }
        if (defaces != null && !defaces.isEmpty()) {
            am.addRule();
            convertDefacesData(am, defacesLb.name, defaces);
        }
        if (rushes != null && !rushes.isEmpty()) {
            am.addRule();
            convertRushesData(am, rushesLb.name, rushes);
        }
        if (!messages.isEmpty()) {
            am.addRule();
            convertCrewMessageData(am, messagesLb.name, messages);
        }
    }

    private void convertCrewMessageData(AsciiMaker am, String name, List<CrewMessageData> data) {
        for (var i = data.size() - 1; i >= 0; i--) {
            var lb = data.get(i);
            var time = MESSAGE_TIME_FORMAT.format(calculateTime(lb.getTime()));
            var str = String.format("%s | %s %s", StringUtils.leftPad(lb.getUserId().toString(), 7),
                    StringUtils.rightPad(lb.getUserName(), 20), StringUtils.rightPad(time, 17));

            put(am, (i == data.size() - 1) ? name : "", str);
            put(am, "", lb.getMessage());
            put(am, "", "");
        }
    }

    private <T extends ICrewData> void convertCrewData(AsciiMaker am, String name, List<T> data) {
        for (var i = 0; i < data.size(); i++) {
            var lb = data.get(i);
            var lastOnline = SharedUtils.toTimeFormat(lb.getLastOnline() * 1000);
            var position = CrewPositionType.getbyPosition(lb.getPosition());
            var str = String.format("%s | %s %s %s %s", StringUtils.leftPad(position.getAlias(), 10),
                    StringUtils.rightPad(lb.getUserId().toString(), 7), StringUtils.rightPad(lb.getUserName(), 20),
                    StringUtils.rightPad(lb.getLevel().toString(), 2), StringUtils.leftPad(lastOnline, 15));

            put(am, (i == 0) ? name : "", str);
        }
    }

    private void convertDefacesData(AsciiMaker am, String name, List<DefaceData> data) {
        for (var i = 0; i < data.size(); i++) {
            var deface = data.get(i);
            var timeLeft = SharedUtils.toTimeFormat(deface.getDefaceTimeLeft() * 1000);
            var hacked = SharedUtils.toBoolean(deface.getDefaceAlreadyHacked()) ? "Yes" : "No";

            var str = String.format("%s | %s FW %s Visits %s Left %s | Hacked: %s",
                    StringUtils.rightPad(deface.getDefaceIp(), 15), StringUtils.rightPad(deface.getDefaceBy(), 20),
                    StringUtils.rightPad(deface.getDefaceFirewall().toString(), 6),
                    StringUtils.rightPad(deface.getDefaceVisits().toString(), 3), StringUtils.leftPad(timeLeft, 15),
                    StringUtils.rightPad(hacked, 3));

            put(am, (i == 0) ? name : "", str);
        }
    }

    private void convertRushesData(AsciiMaker am, String name, List<CrewRushData> data) {
        for (var i = 0; i < data.size(); i++) {
            var rush = data.get(i);
            var rank = rush.getRank() + ".";
            var str = String.format("%s | %s | %s | Score: %s", StringUtils.rightPad(rank, 5),
                    StringUtils.rightPad(rush.getName(), 20), StringUtils.rightPad(rush.getTag(), 5),
                    StringUtils.rightPad(rush.getScore().toString(), 6));

            put(am, (i == 0) ? name : "", str);
        }
    }
}
