package hd.vhackos.b0tnet.command;

import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.api.module.ServerModule;
import hd.vhackos.b0tnet.api.net.response.ServerResponse;
import hd.vhackos.b0tnet.api.net.response.data.ServerPvPBoostData;
import hd.vhackos.b0tnet.command.base.BaseCommand;
import hd.vhackos.b0tnet.shared.ascii.AsciiMaker;
import hd.vhackos.b0tnet.shared.command.Command;
import hd.vhackos.b0tnet.shared.command.CommandParam;
import hd.vhackos.b0tnet.shared.dto.PvPBoostType;
import hd.vhackos.b0tnet.shared.dto.ServerNodeType;
import hd.vhackos.b0tnet.shared.dto.ServerScriptType;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.shared.utils.SharedUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * Příkazy pro práci se serverem.
 */
@Inject
public final class B0tnetServerCommand extends BaseCommand {

    @Autowired
    private ServerModule serverModule;

    protected B0tnetServerCommand(B0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Získá informace o serveru.
     */
    @Command(value = "server", comment = "Gets information about the server")
    private String getServer() {
        return execute("server", am -> {
            var data = serverModule.getServer();
            var fields = getFields(data, true, false);
            addServerResponseToAsciiMaker(am, fields);
        });
    }

    /**
     * Přidá nový antivirus node.
     */
    @Command(value = "server add av node", comment = "Adds a new antivirus node")
    private String addAntivirusNode() {
        return execute("server add antivirus node", am -> {
            var data = serverModule.addAntivirusNode();
            var fields = getFields(data, true, false);
            addServerResponseToAsciiMaker(am, fields);
        });
    }

    /**
     * Přidá nový firewall node.
     */
    @Command(value = "server add fw node", comment = "Adds a new firewall node")
    private String addFirewallNode() {
        return execute("server add firewall node", am -> {
            var data = serverModule.addFirewallNode();
            var fields = getFields(data, true, false);
            addServerResponseToAsciiMaker(am, fields);
        });
    }

    /**
     * Aktualizuje 1x node na serveru. Number označuje pořadí nodu (1-3).
     */
    @Command(value = "server update node 1x", comment = "Updates the node on the server 1x")
    private String updateNode1x(@CommandParam("node") String nodeType, @CommandParam("number") int number) {
        var node = ServerNodeType.getByCommand(nodeType);

        return execute("server update node 1x", am -> {
            if (node == null) {
                var commands = ServerNodeType.getCommands();
                put(am, "Error", "The server node code is not valid. Available codes are: " + commands);
            } else {
                var num = (number < 1) ? 1 : (number > 3 ? 3 : number);
                var data = serverModule.updateNode1x(node, num);
                var fields = getFields(data, true, false);
                addServerResponseToAsciiMaker(am, fields);
            }
        });
    }

    /**
     * Aktualizuje 5x node na serveru. Number označuje pořadí nodu (1-3).
     */
    @Command(value = "server update node 5x", comment = "Updates the node on the server 5x")
    private String updateNode5x(@CommandParam("node") String nodeType, @CommandParam("number") int number) {
        var node = ServerNodeType.getByCommand(nodeType);

        return execute("server update node 5x", am -> {
            if (node == null) {
                var commands = ServerNodeType.getCommands();
                put(am, "Error", "The server node code is not valid. Available codes are: " + commands);
            } else {
                var num = (number < 1) ? 1 : (number > 3 ? 3 : number);
                var data = serverModule.updateNode5x(node, num);
                var fields = getFields(data, true, false);
                addServerResponseToAsciiMaker(am, fields);
            }
        });
    }

    /**
     * Aktualizuje node na serveru na max. Number označuje pořadí nodu (1-3).
     */
    @Command(value = "server update node max", comment = "Updates the node on the server to max")
    private String updateNodeToMax(@CommandParam("node") String nodeType, @CommandParam("number") int number) {
        var node = ServerNodeType.getByCommand(nodeType);

        return execute("server update node to max", am -> {
            if (node == null) {
                var commands = ServerNodeType.getCommands();
                put(am, "Error", "The server node code is not valid. Available codes are: " + commands);
            } else {
                var num = (number < 1) ? 1 : (number > 3 ? 3 : number);
                var data = serverModule.updateNodeToMax(node, num);
                var fields = getFields(data, true, false);
                addServerResponseToAsciiMaker(am, fields);
            }
        });
    }

    /**
     * Zakoupí 1 skript na server.
     */
    @Command(value = "server buy 1 script", comment = "Purchases 1 script on the server")
    private String buy1ServerScript(@CommandParam("scriptType") String scriptType) {
        var script = ServerScriptType.getByCommand(scriptType);

        return execute("server buy 1 script", am -> {
            if (script == null) {
                var commands = ServerScriptType.getCommands();
                put(am, "Error", "The server script code is not valid. Available codes are: " + commands);
            } else {
                var data = serverModule.buy1Script(script);
                var fields = getFields(data, true, false);
                addServerResponseToAsciiMaker(am, fields);
            }
        });
    }

    /**
     * Zakoupí 10 skriptů na server.
     */
    @Command(value = "server buy 10 script", comment = "Purchases 10 scripts on the server")
    private String buy10ServerScript(@CommandParam("scriptType") String scriptType) {
        var script = ServerScriptType.getByCommand(scriptType);

        return execute("server buy 10 scripts", am -> {
            if (script == null) {
                var commands = ServerScriptType.getCommands();
                put(am, "Error", "The server script code is not valid. Available codes are: " + commands);
            } else {
                var data = serverModule.buy10Scripts(script);
                var fields = getFields(data, true, false);
                addServerResponseToAsciiMaker(am, fields);
            }
        });
    }

    /**
     * Vylepší skripty na serveru.
     */
    @Command(value = "server upgrade script", comment = "Upgrade scripts on the server")
    private String upgradeServerScripts(@CommandParam("scriptType") String scriptType) {
        var script = ServerScriptType.getByCommand(scriptType);

        return execute("server upgrade script", am -> {
            if (script == null) {
                var commands = ServerScriptType.getCommands();
                put(am, "Error", "The server script code is not valid. Available codes are: " + commands);
            } else {
                var data = serverModule.upgradeScripts(script);
                var fields = getFields(data, true, false);
                addServerResponseToAsciiMaker(am, fields);
            }
        });
    }

    /**
     * Otevře jeden balíček.
     */
    @Command(value = "server open package", comment = "Opens one package")
    private String openPackage() {
        return execute("server open package", am -> {
            var data = serverModule.openPackage();
            var fields = getFields(data, true, false);
            addServerResponseToAsciiMaker(am, fields);
        });
    }

    /**
     * Otevře všechny balíčeky.
     */
    @Command(value = "server open all packages", comment = "Opens all packages")
    private String openAllPackages() {
        return execute("server open all packages", am -> {
            var data = serverModule.openAllPackages();
            var fields = getFields(data, true, false);
            addServerResponseToAsciiMaker(am, fields);
        });
    }

    /**
     * Otevře jeden balíček.
     */
    @Command(value = "server open bronze pack", comment = "Opens one bronze pack")
    private String openBronzePack() {
        return execute("server open bronze pack", am -> {
            var data = serverModule.openBronzePack();
            var fields = getFields(data, true, false);
            addServerResponseToAsciiMaker(am, fields);
        });
    }

    /**
     * Koupí za netcoins N balíčků (max 10).
     */
    @Command(value = "server buy package", comment = "Buys a new server package for netcoins")
    private String buyPackage(@CommandParam("count") int count) {
        return execute("server buy package", am -> {
            var data = serverModule.buyPackages(count);
            var fields = getFields(data, true, false);
            addServerResponseToAsciiMaker(am, fields);
        });
    }

    // === Pomocné metody
    // ================================================================================================================

    private void addServerResponseToAsciiMaker(AsciiMaker am, Map<String, FieldData> fields) {
        put(am, fields.remove(ServerResponse.P_FRAGS));
        put(am, fields.remove(ServerResponse.P_PACKS));
        put(am, fields.remove(ServerResponse.P_PACKS_BOUGHT));
        put(am, fields.remove(ServerResponse.P_NEXT_PACK_IN));
        put(am, fields.remove(ServerResponse.P_BRONZE_PACKS));
        put(am, fields.remove(ServerResponse.P_MY_LEAGUE));
        put(am, fields.remove(ServerResponse.P_MY_REPUTATION));
        put(am, fields.remove(ServerResponse.P_WIN_RATE));

        var bypassScript = String.format("%s | Str: %s | Scripts: %s | Cost: %s | Upgrade cost: %s",
                StringUtils.rightPad(fields.remove(ServerResponse.P_BYPASS_VERSION).value.toString(), 4),
                StringUtils.rightPad(fields.remove(ServerResponse.P_BYPASS_STRENGTH).value.toString(), 4),
                StringUtils.rightPad(fields.remove(ServerResponse.P_BYPASS_SCRIPTS).value.toString(), 4),
                StringUtils.rightPad(fields.remove(ServerResponse.P_BYPASS_COSTS).value.toString(), 5),
                StringUtils.rightPad(fields.remove(ServerResponse.P_BYPASS_UPGRADE_COSTS).value.toString(), 6));
        put(am, "Bypass Scripts", bypassScript);

        var smashScript = String.format("%s | Str: %s | Scripts: %s | Cost: %s | Upgrade cost: %s",
                StringUtils.rightPad(fields.remove(ServerResponse.P_SMASH_VERSION).value.toString(), 4),
                StringUtils.rightPad(fields.remove(ServerResponse.P_SMASH_STRENGTH).value.toString(), 4),
                StringUtils.rightPad(fields.remove(ServerResponse.P_SMASH_SCRIPTS).value.toString(), 4),
                StringUtils.rightPad(fields.remove(ServerResponse.P_SMASH_COSTS).value.toString(), 5),
                StringUtils.rightPad(fields.remove(ServerResponse.P_SMASH_UPGRADE_COSTS).value.toString(), 6));
        put(am, "Smash Scripts", smashScript);

        var shutdownScript = String.format("%s | Str: %s | Scripts: %s | Cost: %s | Upgrade cost: %s",
                StringUtils.rightPad(fields.remove(ServerResponse.P_SHUTDOWN_VERSION).value.toString(), 4),
                StringUtils.rightPad(fields.remove(ServerResponse.P_SHUTDOWN_STRENGTH).value.toString(), 4),
                StringUtils.rightPad(fields.remove(ServerResponse.P_SHUTDOWN_SCRIPTS).value.toString(), 4),
                StringUtils.rightPad(fields.remove(ServerResponse.P_SHUTDOWN_COSTS).value.toString(), 5),
                StringUtils.rightPad(fields.remove(ServerResponse.P_SHUTDOWN_UPGRADE_COSTS).value.toString(), 6));
        put(am, "Shutdown Scripts", shutdownScript);

        var pvpBoosts = fields.remove(ServerResponse.P_PVP_BOOSTS);
        insertPvpBoostsToAsciiMaker(am, (List<ServerPvPBoostData>) pvpBoosts.value, pvpBoosts.name);

        putRemainings(am, fields);
    }

    private void insertPvpBoostsToAsciiMaker(AsciiMaker am, List<ServerPvPBoostData> boosts, String name) {
        for (var i = 0; i < boosts.size(); i++) {
            var boost = boosts.get(i);
            var type = PvPBoostType.getById(boost.getId());
            var active = SharedUtils.convertToBoolean(boost.getActive());
            String str;

            str = String.format("%s | Active: %s | Rarity: %s", StringUtils.rightPad(type.getAlias(), 10),
                    StringUtils.leftPad(active, 5), StringUtils.leftPad(boost.getRarity().toString(), 1));
            put(am, (i == 0) ? name : "", str);
        }
    }
}
