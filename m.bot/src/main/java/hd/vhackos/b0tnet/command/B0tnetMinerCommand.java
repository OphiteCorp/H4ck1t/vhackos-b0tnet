package hd.vhackos.b0tnet.command;

import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.api.module.MiningModule;
import hd.vhackos.b0tnet.api.net.response.MiningResponse;
import hd.vhackos.b0tnet.command.base.BaseCommand;
import hd.vhackos.b0tnet.shared.ascii.AsciiMaker;
import hd.vhackos.b0tnet.shared.command.Command;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;

import java.util.Map;

/**
 * Příkazy pro netcoin miner.
 */
@Inject
public final class B0tnetMinerCommand extends BaseCommand {

    @Autowired
    private MiningModule miningModule;

    protected B0tnetMinerCommand(B0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Zobrazí informace o mineru.
     */
    @Command(value = "miner", comment = "Gets information about ncMiner")
    private String getNetcoinMiner() {
        return execute("ncMiner", am -> {
            var data = miningModule.getMining();
            var fields = getFields(data, true);
            addMiningResponseToAsciiMaker(am, fields);
        });
    }

    /**
     * Sebere vytěžené netcoins z mineru.
     */
    @Command(value = "miner collect", comment = "Collect mined netcoins from the miner")
    private String collectMiner() {
        return execute("collect ncMiner", am -> {
            var data = miningModule.collect();
            var fields = getFields(data, true);
            addMiningResponseToAsciiMaker(am, fields);
        });
    }

    /**
     * Koupí další GPU do mineru.
     */
    @Command(value = "miner buy gpu", comment = "Buy next GPU into a miner")
    private String buyMinerGpu() {
        return execute("buy GPU for ncMiner", am -> {
            var data = miningModule.buyGpu();
            var fields = getFields(data, true);
            addMiningResponseToAsciiMaker(am, fields);
        });
    }

    /**
     * Spustí netcoin miner.
     */
    @Command(value = "miner start", comment = "Starts the netcoin miner")
    private String startMiner() {
        return execute("start ncMiner", am -> {
            var data = miningModule.start();
            var fields = getFields(data, true);
            addMiningResponseToAsciiMaker(am, fields);
        });
    }

    // === Pomocné metody
    // ================================================================================================================

    private void addMiningResponseToAsciiMaker(AsciiMaker am, Map<String, FieldData> fields) {
        put(am, fields.remove(MiningResponse.P_RUNNING));
        put(am, fields.remove(MiningResponse.P_NETCOINS));
        put(am, fields.remove(MiningResponse.P_GPU_COUNT));
        put(am, fields.remove(MiningResponse.P_WILL_EARN));
        put(am, fields.remove(MiningResponse.P_LEFT));
        putRemainings(am, fields);
    }
}
