package hd.vhackos.b0tnet.command;

import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;
import hd.vhackos.b0tnet.Application;
import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.Constants;
import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.command.base.BaseCommand;
import hd.vhackos.b0tnet.config.ApplicationConfig;
import hd.vhackos.b0tnet.config.ConfigHelper;
import hd.vhackos.b0tnet.config.ConfigProvider;
import hd.vhackos.b0tnet.dto.CacheData;
import hd.vhackos.b0tnet.shared.command.Command;
import hd.vhackos.b0tnet.shared.command.CommandParam;
import hd.vhackos.b0tnet.shared.command.CommandRunner;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.shared.injection.InjectionContext;
import hd.vhackos.b0tnet.shared.json.Json;
import hd.vhackos.b0tnet.shared.utils.EncryptUtils;
import hd.vhackos.b0tnet.shared.utils.HashUtils;
import hd.vhackos.b0tnet.shared.utils.SentryGuard;
import hd.vhackos.b0tnet.utils.appender.HackedAppender;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;

import java.io.File;
import java.lang.reflect.Parameter;

/**
 * Příkazy kolem aplikace.
 */
@Inject
public final class ApplicationCommand extends BaseCommand {

    public static final String CMD_LATEST = "latest";
    public static final String CMD_EXIT = "q";
    public static final String CMD_HELP = "help";
    private static final String CMD_HELP_COMMENT = "Displays the help you see now";
    private static final String CMD_ABOUT = "about";
    private static final String CMD_ABOUT_COMMENT = "Gets information about the current version";
    private static final String CMD_LATEST_COMMENT = "Gets a list of changes in the latest version of B0tnet";
    private static final String CMD_CONFIG = "config";
    private static final String CMD_CONFIG_COMMENT = "Get the current configuration";
    private static final String CMD_RELOAD = "reload";
    private static final String CMD_RELOAD_COMMENT = "Forces reloading configuration";
    private static final String CMD_LOGO = "logo";
    private static final String CMD_LOGO_COMMENT = "Prints the logo";
    private static final String CMD_EXIT_COMMENT = "Exit B0tnet";
    private static final String CMD_ONLINE = "online";
    private static final String CMD_ONLINE_COMMENT = "If B0tnet was started using -offline, it will be set to -online";
    private static final String CMD_CACHE = "cache";
    private static final String CMD_CACHE_COMMENT = "Displays content in the cache";
    private static final String CMD_DEBUG = "debug";
    private static final String CMD_DEBUG_COMMENT = "Turns on or off the debug mode";
    private static final String CMD_SHOW_PASSWORD = "showpwd";
    private static final String CMD_SHOW_PASSWORD_COMMENT = "Displays original password (decrypts from configuration)";
    private static final String CMD_GENERATE_CONTROL_KEYS = "gencontrolkeys";
    private static final String CMD_GENERATE_CONTROL_KEYS_COMMENT = "Generates a new control key";

    @Autowired
    private ConfigProvider configProvider;

    @Autowired
    private ApplicationConfig config;

    protected ApplicationCommand(B0tnet b0tnet) {
        super(b0tnet);
    }

    private static String paramsToString(Parameter[] params) {
        var sb = new StringBuilder();
        int i = 0;

        for (var p : params) {
            var name = p.getType().getSimpleName();

            if (p.isAnnotationPresent(CommandParam.class)) {
                var a = p.getDeclaredAnnotation(CommandParam.class);
                name = a.value();
            }
            sb.append(name);
            if (i++ < params.length - 1) {
                sb.append(", ");
            }
        }
        return sb.toString();
    }

    /**
     * Vypíše nápovědu.
     */
    @Command(value = CMD_HELP, hidden = true, comment = CMD_HELP_COMMENT)
    private String getHelp() {
        return execute("help - Available commands", am -> {
            put(am, "." + CMD_HELP, CMD_HELP_COMMENT);
            put(am, "." + CMD_LATEST, CMD_LATEST_COMMENT);
            put(am, "." + CMD_CONFIG, CMD_CONFIG_COMMENT);
            put(am, "." + CMD_RELOAD, CMD_RELOAD_COMMENT);
            put(am, "." + CMD_LOGO, CMD_LOGO_COMMENT);
            put(am, "." + CMD_ONLINE, CMD_ONLINE_COMMENT);
            put(am, "." + CMD_CACHE, CMD_CACHE_COMMENT);
            put(am, "." + CMD_DEBUG + " [boolean]", CMD_DEBUG_COMMENT);
            put(am, "." + CMD_SHOW_PASSWORD, CMD_SHOW_PASSWORD_COMMENT);
            put(am, "." + CMD_GENERATE_CONTROL_KEYS, CMD_GENERATE_CONTROL_KEYS_COMMENT);
            am.addRule();

            var runner = CommandRunner.getInstance();
            var commands = runner.getCommands();

            for (var entry : commands.entrySet()) {
                if (entry.getKey() == null) {
                    for (var cmd : entry.getValue().entrySet()) {
                        var command = cmd.getKey();
                        var method = runner.getCommandMethod(command);
                        var params = (method.getParameterCount() == 0) ? "" : " [" + paramsToString(
                                method.getParameters()) + "]";
                        var a = method.getAnnotation(Command.class);

                        if (!a.hidden()) {
                            var comment = a.comment().isEmpty() ? "" : a.comment();
                            am.add(String.format(".%s%s", command, params), comment);
                        }
                    }
                }
            }
            am.addRule();
            put(am, "." + CMD_ABOUT, CMD_ABOUT_COMMENT);
            put(am, "." + CMD_EXIT, CMD_EXIT_COMMENT);
            am.addRule();

            var copyRow = put(am, null, "by " + Constants.AUTHOR + " | v" + IB0tnet.VERSION);
            copyRow.getCells().get(1).getContext().setTextAlignment(TextAlignment.RIGHT);
        });
    }

    /**
     * Vypíše logo.
     */
    @Command(value = CMD_LOGO, hidden = true, comment = CMD_LOGO_COMMENT)
    private String logo() {
        return execute("logo", am -> {
            am.setTopTheme();
            put(am, " ", Application.LOGO);
        });
    }

    /**
     * Přenačte konfiguraci aplikace.
     */
    @Command(value = CMD_RELOAD, hidden = true, comment = CMD_RELOAD_COMMENT)
    private String reloadConfig() {
        return execute("reload configuration", am -> {
            var config = configProvider.getAppConfig();
            InjectionContext.getInstance().get(ApplicationConfig.class).set(config);
            SentryGuard.setGameApiToTag(config.getGameApi());
            put(am, "Info", "The configuration has been reloaded");
        });
    }

    /**
     * Získá konfiguraci.
     */
    @Command(value = CMD_CONFIG, hidden = true, comment = CMD_CONFIG_COMMENT)
    private String getConfig() {
        return execute("configuration", am -> {
            var map = ConfigHelper.asMap(config);

            for (var entry : map.entrySet()) {
                put(am, entry.getKey(), entry.getValue());
            }
        });
    }

    /**
     * Získá seznam změn v poslední verzi B0tnet.
     */
    @Command(value = CMD_LATEST, hidden = true, comment = CMD_LATEST_COMMENT)
    private String getLatest() {
        return execute("latest", am -> {
            var data = getB0tnet().getSharedData().getUpdateData();

            if (data == null) {
                put(am, "Error",
                        "The service for checking the latest version is either turned off or there was an error getting information");
            } else {
                put(am, "Your Version", "v" + IB0tnet.VERSION);
                am.addRule();
                put(am, "Latest Version", "v" + data.getVersion());
                put(am, "Download", data.getDownloadLink());
                put(am, "Discord", data.getDiscordLink());

                if (data.getNews() != null && !data.getNews().isEmpty()) {
                    for (var i = 0; i < data.getNews().size(); i++) {
                        var item = data.getNews().get(i);
                        put(am, (i == 0) ? "News" : "", "- " + item);
                    }
                } else {
                    put(am, "News", "No news found");
                }
                if (data.getNotice() != null && !data.getNotice().isEmpty()) {
                    am.addRule();
                    put(am, "Notice", data.getNotice());
                }
            }
        });
    }

    /**
     * Převede B0tnet do online.
     */
    @Command(value = CMD_ONLINE, hidden = true, comment = CMD_ONLINE_COMMENT)
    private String online() {
        return execute("online", am -> {
            if (getB0tnet().isStarted()) {
                put(am, "Info", "B0tnet was already running");
            } else {
                getB0tnet().start();
                put(am, "Info", "B0tnet was started");
            }
        });
    }

    /**
     * Zobrazí obsah v cache.
     */
    @Command(value = CMD_CACHE, hidden = true, comment = CMD_CACHE_COMMENT)
    private String getCache() {
        return execute("cache", am -> {
            var cacheData = Json.toObject(new File(CacheData.FILE_NAME), CacheData.class);
            var json = Json.toJson(cacheData);
            put(am, "Data", json);
        });
    }

    /**
     * Změní úroveň logování.
     */
    @Command(value = CMD_DEBUG, hidden = true, comment = CMD_DEBUG_COMMENT)
    private String debug(@CommandParam("boolean") String debug) {
        var isDebug = Boolean.valueOf(debug);

        return execute("debug -> " + isDebug, am -> {
            var level = isDebug ? Level.ALL : Level.INFO;
            Configurator.setRootLevel(level);
            HackedAppender.getInstance(true);
            put(am, "Info", "The debug mode was turned " + (isDebug ? "on" : "off") + ".");
        });
    }

    /**
     * Zobrazí původní heslo (decryptuje).
     */
    @Command(value = CMD_SHOW_PASSWORD, hidden = true, comment = CMD_SHOW_PASSWORD_COMMENT)
    private String showPassword() {
        return execute("show original password", am -> {
            var rawPassword = EncryptUtils.decrypt(config.getControlKey(), config.getPassword());
            put(am, "Original password without first and last quotation marks", "\"" + rawPassword + "\"");
        });
    }

    /**
     * Vygeneruje nový kontrolní klíč.
     */
    @Command(value = CMD_GENERATE_CONTROL_KEYS, hidden = true, comment = CMD_GENERATE_CONTROL_KEYS_COMMENT)
    private String generateControlKeys() {
        return execute("generate control keys", am -> {
            for (var i = 0; i < 5; i++) {
                var keyBytes = EncryptUtils.generateSecretBytes();
                var key = HashUtils.toSha256(keyBytes);
                put(am, (i == 0) ? "Control keys" : "", (i + 1) + ") " + key);
            }
        });
    }

    // === Pomocné metody
    // ================================================================================================================

    /**
     * Získá informace o aktuální verzi.
     */
    @Command(value = CMD_ABOUT, hidden = true, comment = CMD_ABOUT_COMMENT)
    private String getVersion() {
        return execute("about", am -> {
            put(am, "Version", "v" + IB0tnet.VERSION);
            put(am, "Game API", getB0tnet().getConfig().getGameApi());
            put(am, "Author", Constants.AUTHOR);
            put(am, "Year", Constants.YEAR);
            am.addRule();
            put(am, "Discord", Constants.DISCORD);
        });
    }
}
