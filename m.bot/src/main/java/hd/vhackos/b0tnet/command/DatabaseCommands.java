package hd.vhackos.b0tnet.command;

import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.command.base.BaseCommand;
import hd.vhackos.b0tnet.db.dto.ChatRowDto;
import hd.vhackos.b0tnet.db.service.DatabaseService;
import hd.vhackos.b0tnet.shared.ascii.AsciiMaker;
import hd.vhackos.b0tnet.shared.command.Command;
import hd.vhackos.b0tnet.shared.command.CommandParam;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;
import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Databázové příkazy.
 */
@Inject
public final class DatabaseCommands extends BaseCommand {

    private static final SimpleDateFormat MESSAGE_TIME_FORMAT = new SimpleDateFormat("dd.MM.yyyy - HH:mm:ss");

    @Autowired
    private DatabaseService databaseService;

    protected DatabaseCommands(B0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Vyhledá v DB všechny uživatele, kteří mají tuto IP.
     */
    @Command(value = "db find ip", comment = "Search for IP from user list")
    private String searchUserIp(@CommandParam("ip") String ip) {
        return execute("DB | search user -> " + ip, am -> {
            var data = databaseService.searchIp(ip);

            if (data != null && !data.isEmpty()) {
                for (var i = 0; i < data.size(); i++) {
                    var user = data.get(i);
                    var fields = getFields(user, true);

                    for (var entry : fields.entrySet()) {
                        put(am, entry);
                    }
                    if (i < data.size() - 1) {
                        am.add("", StringUtils.repeat('-', 20));
                    }
                }
            } else {
                am.add("Result", "This IP address was not found");
            }
        });
    }

    /**
     * Vyhledá všechny IP pro ID uživatele.
     */
    @Command(value = "db find uid", comment = "Search IP's by user ID")
    private String searchIpByUserId(@CommandParam("uid") int userId) {
        return execute("DB | search user by ID -> " + userId, am -> {
            var data = databaseService.searchIpByUserId(userId);
            printUserIps(am, data);
        });
    }

    /**
     * Vyhledá všechny IP pro jméno uživatele.
     */
    @Command(value = "db find uname", comment = "Search IP's by user name")
    private String searchIpByUserId(@CommandParam("userName") String userName) {
        return execute("DB | search user by name -> " + userName, am -> {
            var data = databaseService.searchIpByUserName(userName);
            printUserIps(am, data);
        });
    }

    /**
     * Vypíše všechny naskenované IP.
     */
    @Command(value = "db scanned list", comment = "Prints a complete list of all scanned IPs")
    private String getScannedIps(@CommandParam("orderColumn") String order) {
        if ("null".equalsIgnoreCase(order)) {
            order = "";
        }
        var orderColumn = order;

        return execute("DB | scanned IP's list", am -> {
            getLog().info("Collecting data to retrieve all IPs by column: {}", orderColumn);
            var data = databaseService.getScannedIPs(orderColumn);

            if (data != null && !data.isEmpty()) {
                put(am, "Count", data.size());

                for (var i = 0; i < data.size(); i++) {
                    var ip = data.get(i);
                    var user = (ip.getUserName() == null) ? "-" : ip.getUserName();
                    var out = String.format("%s | Level: %s | FW: %s | User: %s", StringUtils.rightPad(ip.getIp(), 15),
                            StringUtils.leftPad(String.valueOf(ip.getLevel()), 3),
                            StringUtils.leftPad(String.valueOf(ip.getFirewall()), 5), StringUtils.rightPad(user, 20));

                    put(am, (i == 0) ? "IP" : "", out);
                }
            } else {
                put(am, "Result", "There are no records available yet");
            }
        });
    }

    /**
     * Aktualizuje všechny naskenované uživatele, že se jim pokusí přiřadit jméno z tabulky uživatelů.
     */
    @Command(value = "db update scanned", comment = "Updating user names in the scan table")
    private String updateScannedUsers() {
        return execute("DB | update scanned IP's", am -> {
            databaseService.updateScannedUsers();
            put(am, "State", "Done");
        });
    }

    /**
     * Získá všechny záznamy z chatu.
     */
    @Command(value = "db get chat", comment = "Gets all chat messages")
    private String getAllChatMessages() {
        return execute("DB | get all chat messages", am -> {
            am.setInsideTheme();
            var messages = databaseService.getAllChatMessages();
            printChatMessages(am, messages);
        });
    }

    /**
     * Získá posledních N záznamů z chatu.
     */
    @Command(value = "db get chat last", comment = "Gets last N chat messages")
    private String getAllChatMessages(@CommandParam("lastRowsCount") int lastRows) {
        return execute("DB | get last chat messages -> " + lastRows, am -> {
            am.setInsideTheme();
            var messages = databaseService.getChatMessages(lastRows);
            printChatMessages(am, messages);
        });
    }

    // === Pomocné metody
    // ================================================================================================================

    private static void printChatMessages(AsciiMaker am, List<ChatRowDto> messages) {
        if (messages != null && !messages.isEmpty()) {
            put(am, "Count", messages.size());

            for (var i = 0; i < messages.size(); i++) {
                var msg = messages.get(i);
                var time = MESSAGE_TIME_FORMAT.format(msg.getDate());
                var toUser = (msg.getToUser() != null) ? msg.getToUser() : "";
                var out = String.format("%s | %s -> %s | %s", StringUtils.rightPad(time, 21),
                        StringUtils.leftPad(String.valueOf(msg.getUser()), 20), StringUtils.rightPad(toUser, 20),
                        StringUtils.rightPad(msg.getMessage(), msg.getMessage().length()));
                put(am, (i == 0) ? "Messages" : "", out);
            }
        } else {
            put(am, "Result", "There are no messages available yet");
        }
    }

    private static void printUserIps(AsciiMaker am, List<String> ips) {
        if (ips != null && !ips.isEmpty()) {
            for (var i = 0; i < ips.size(); i++) {
                var ip = ips.get(i);
                put(am, (i == 0) ? "IP" : "", ip);
            }
        } else {
            put(am, "Result", "IP for user ID was not found");
        }
    }
}
