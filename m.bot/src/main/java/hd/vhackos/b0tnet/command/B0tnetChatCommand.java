package hd.vhackos.b0tnet.command;

import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.api.chat.ChatReceiver;
import hd.vhackos.b0tnet.command.base.BaseCommand;
import hd.vhackos.b0tnet.service.base.IService;
import hd.vhackos.b0tnet.service.base.Service;
import hd.vhackos.b0tnet.shared.command.Command;
import hd.vhackos.b0tnet.shared.command.CommandParam;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;

import java.util.Optional;

/**
 * Příkazy pro chat.
 */
@Inject
public final class B0tnetChatCommand extends BaseCommand {

    @Autowired
    private ChatReceiver chatReceiver;

    protected B0tnetChatCommand(B0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Odešlě příkaz do na chat server.
     */
    @Command(value = "sc", comment = "Sends a command to the chat server")
    private Optional sendCommand(@CommandParam("command") String command) {
        if (Service.isRunning(IService.SERVICE_CHAT)) {
            if (chatReceiver.isReady()) {
                chatReceiver.sendCommand(command);
            } else {
                getLog().info("Connections are still in progress. Please wait...");
            }
        } else {
            getLog().info("Chat service is disable");
        }
        return Optional.empty();
    }

    /**
     * Odešlě zprávu do chatu.
     */
    @Command(value = "sm", comment = "Sends a message to the chat")
    private Optional sendMessage(@CommandParam("message") String message) {
        if (Service.isRunning(IService.SERVICE_CHAT)) {
            if (chatReceiver.isReady()) {
                chatReceiver.sendMessage(message);
            } else {
                getLog().info("Connections are still in progress. Please wait...");
            }
        } else {
            getLog().info("Chat service is disable");
        }
        return Optional.empty();
    }

    /**
     * Odešlě zprávu do chatu.
     */
    @Command(value = "sm to", comment = "Sends a message to another player in the chat")
    private Optional sendMessage(@CommandParam("user") String user, @CommandParam("message") String message) {
        if (Service.isRunning(IService.SERVICE_CHAT)) {
            if (chatReceiver.isReady()) {
                chatReceiver.sendMessageToUser(user, message);
            } else {
                getLog().info("Connections are still in progress. Please wait...");
            }
        } else {
            getLog().info("Chat service is disable");
        }
        return Optional.empty();
    }
}
