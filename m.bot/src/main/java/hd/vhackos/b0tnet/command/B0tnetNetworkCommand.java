package hd.vhackos.b0tnet.command;

import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.api.module.RemoteModule;
import hd.vhackos.b0tnet.api.net.response.ExploitResponse;
import hd.vhackos.b0tnet.api.net.response.NetworkScanResponse;
import hd.vhackos.b0tnet.api.net.response.data.IpBruteData;
import hd.vhackos.b0tnet.api.net.response.data.IpScanData;
import hd.vhackos.b0tnet.command.base.BaseCommand;
import hd.vhackos.b0tnet.servicemodule.ServiceModule;
import hd.vhackos.b0tnet.shared.ascii.AsciiMaker;
import hd.vhackos.b0tnet.shared.command.Command;
import hd.vhackos.b0tnet.shared.command.CommandParam;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Příkazy pro práci s herní sítí.
 */
@Inject
public final class B0tnetNetworkCommand extends BaseCommand {

    @Autowired
    private RemoteModule remoteModule;

    @Autowired
    private ServiceModule serviceModule;

    protected B0tnetNetworkCommand(B0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Skenuje síť (získá vždy 10 IP adress, dle nastavení serveru).
     */
    @Command(value = "scan", comment = "It scans the network")
    private String scan() {
        return execute("scan", am -> {
            var data = serviceModule.scan();
            var fields = getFields(data, true);

            convertScannedIps(am, fields.remove(NetworkScanResponse.P_IPS));
            put(am, fields.remove(NetworkScanResponse.P_EXPLOITS));
            put(am, fields.remove(NetworkScanResponse.P_CONNECTION_COUNT));
            convertBrutedIps(am, fields.remove(NetworkScanResponse.P_BRUTED_IPS));
            putRemainings(am, fields);
        });
    }

    /**
     * Exploituje IP.
     */
    @Command(value = "exploit", comment = "Exploits the IP address")
    private String exploit(@CommandParam("ip") String ip) {
        return execute("exploit -> " + ip, am -> {
            var data = serviceModule.exploit(ip);
            var fields = getFields(data, true);

            convertBrutedIps(am, fields.remove(ExploitResponse.P_BRUTED_IPS));
            put(am, fields.remove(ExploitResponse.P_EXPLOITS));
            put(am, fields.remove(ExploitResponse.P_CONNECTION_COUNT));
            putRemainings(am, fields);
        });
    }

    /**
     * Získá informace o hacknutém systému.
     */
    @Command(value = "remote", comment = "Gets information about the remote system")
    private String getRemote(@CommandParam("ip") String ip) {
        return execute("remote -> " + ip, am -> {
            var data = serviceModule.getSystemInfo(ip);
            var fields = getFields(data, true);
            putRemainings(am, fields, false);
        });
    }

    /**
     * Převezme kontrolu nad cílovým systémem.
     */
    @Command(value = "deface", comment = "Takes control of the target system")
    private String deface(@CommandParam("ip") String ip) {
        return execute("deface -> " + ip, am -> {
            var data = remoteModule.deface(ip);
            var fields = getFields(data, true);
            putRemainings(am, fields, false);
        });
    }

    // === Pomocné metody
    // ================================================================================================================

    private void convertScannedIps(AsciiMaker am, FieldData data) {
        var ips = (List<IpScanData>) data.value;
        var name = data.name;

        for (var i = 0; i < ips.size(); i++) {
            var ip = ips.get(i);
            var str = String.format("%s | Level = %s | FW = %s", StringUtils.rightPad(ip.getIp(), 15),
                    StringUtils.leftPad(ip.getLevel().toString(), 3),
                    StringUtils.leftPad(ip.getFirewall().toString(), 5));

            put(am, (i == 0) ? name : "", str);
        }
    }

    private void convertBrutedIps(AsciiMaker am, FieldData data) {
        var ips = (List<IpBruteData>) data.value;
        var name = data.name;

        for (var i = 0; i < ips.size(); i++) {
            var ip = ips.get(i);
            var fields = getFields(ip, true);
            var fIp = fields.get(IpBruteData.P_IP).value;
            var fUser = fields.get(IpBruteData.P_USER_NAME).value;
            var fBrute = fields.get(IpBruteData.P_BRUTE).value;

            var str = String.format("%s [ %s ] Brute = %s", StringUtils.rightPad(fIp.toString(), 15),
                    StringUtils.leftPad(fUser.toString(), 20), StringUtils.leftPad(fBrute.toString(), 7));

            put(am, (i == 0) ? name : "", str);
        }
        if (ips.isEmpty()) {
            put(am, name, "<none>");
        }
    }
}
