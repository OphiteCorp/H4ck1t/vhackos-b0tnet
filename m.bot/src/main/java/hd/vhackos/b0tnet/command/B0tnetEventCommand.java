package hd.vhackos.b0tnet.command;

import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.api.module.EventModule;
import hd.vhackos.b0tnet.command.base.BaseCommand;
import hd.vhackos.b0tnet.shared.command.Command;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;

/**
 * Příkazy kolem událostí ve hře.
 */
@Inject
public final class B0tnetEventCommand extends BaseCommand {

    @Autowired
    private EventModule eventModule;

    protected B0tnetEventCommand(B0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Zatočí kolem štěstí.
     */
    @Command(value = "spinwheel", comment = "Spin lucky wheel (event)")
    private String spinLuckyWheel() {
        return execute("Spin lucky wheel", am -> {
            var prize = eventModule.spinLuckyWheel();
            put(am, "Result", prize);
        });
    }
}
