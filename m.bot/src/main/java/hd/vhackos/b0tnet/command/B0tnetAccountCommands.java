package hd.vhackos.b0tnet.command;

import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.api.module.CommonModule;
import hd.vhackos.b0tnet.api.net.response.LoginResponse;
import hd.vhackos.b0tnet.command.base.BaseCommand;
import hd.vhackos.b0tnet.shared.command.Command;
import hd.vhackos.b0tnet.shared.command.CommandParam;
import hd.vhackos.b0tnet.shared.command.CommandRunner;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;

/**
 * Příkazy vztahující se k samotnému účtu.
 */
@Inject
public final class B0tnetAccountCommands extends BaseCommand {

    @Autowired
    private CommonModule commonModule;

    protected B0tnetAccountCommands(B0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Přihlásí uživatele znovu.
     */
    @Command(value = "login", comment = "Signs in again and gets a new token")
    private String login() {
        return execute("login", am -> {
            var data = commonModule.login();
            var fields = getFields(data, true);
            put(am, fields.remove(LoginResponse.P_USER_NAME));
            put(am, fields.remove(LoginResponse.P_UID));
            put(am, fields.remove(LoginResponse.P_ACCESS_TOKEN));
            put(am, fields.remove(LoginResponse.P_EMAIL));
            put(am, fields.remove(LoginResponse.P_IP));
            put(am, fields.remove(LoginResponse.P_LEVEL));
            put(am, fields.remove(LoginResponse.P_NETCOINS));
            put(am, fields.remove(LoginResponse.P_APP_FIREWALL));
            put(am, fields.remove(LoginResponse.P_APP_ANTIVIRUS));
            put(am, fields.remove(LoginResponse.P_APP_BRUTEFORCE));
            put(am, fields.remove(LoginResponse.P_APP_SPAM));
            put(am, fields.remove(LoginResponse.P_APP_SDK));
            putRemainings(am, fields, false);
        });
    }

    /**
     * Přihlásí jiného uživatele.
     */
    @Command(value = "login user", comment = "Logs on to another user")
    private String loginAnother(@CommandParam("user") String user, @CommandParam("password") String password) {
        return execute("login another", am -> {
            var data = commonModule.login(user, password);
            var fields = getFields(data, true);
            putRemainings(am, fields, false);
        });
    }

    /**
     * Zaregistruje nového uživatele.
     */
    @Command(value = "register", comment = "Registering a new user")
    private String register(@CommandParam("user") String userName, @CommandParam("password") String password,
            @CommandParam("email") String email) {

        return execute("register", am -> {
            var data = commonModule.register(userName, password, email);
            var fields = getFields(data, true);
            putRemainings(am, fields, false);
        });
    }

    /**
     * Smaže účet uživatele.
     */
    @Command(value = "account delete", comment = "Forever deletes user account")
    private String deleteAccount(@CommandParam("uid") String uid, @CommandParam("accessToken") String accessToken,
            @CommandParam("password") String password) {

        return execute("delete account", am -> {
            commonModule.deleteAccount(uid, accessToken, password);
            put(am, "Result", "Account has been successfully deleted");
        });
    }

    /**
     * Smaže účet uživatele.
     */
    @Command(value = "account delete self", comment = "Forever deletes the current user's account")
    private String deleteAccountSelf() {

        return execute("delete account self", am -> {
            commonModule.deleteAccount();
            put(am, "Result", "Account has been successfully deleted");
            CommandRunner.getInstance().run(ApplicationCommand.CMD_EXIT, null);
        });
    }
}
