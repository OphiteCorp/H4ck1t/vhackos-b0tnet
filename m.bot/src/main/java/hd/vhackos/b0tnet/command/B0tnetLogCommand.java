package hd.vhackos.b0tnet.command;

import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.api.module.LogModule;
import hd.vhackos.b0tnet.command.base.BaseCommand;
import hd.vhackos.b0tnet.db.service.DatabaseService;
import hd.vhackos.b0tnet.servicemodule.ServiceModule;
import hd.vhackos.b0tnet.shared.command.Command;
import hd.vhackos.b0tnet.shared.command.CommandParam;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;

/**
 * Příkazy kolem logů.
 */
@Inject
public final class B0tnetLogCommand extends BaseCommand {

    @Autowired
    private DatabaseService databaseService;

    @Autowired
    private LogModule logModule;

    @Autowired
    private ServiceModule serviceModule;

    protected B0tnetLogCommand(B0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Získá vlastní systémový log.
     */
    @Command(value = "log", comment = "Gets own system log")
    private String getSystemLog() {
        return execute("log", am -> {
            am.setInsideTheme();
            var data = logModule.getLog();

            for (var i = 0; i < data.size(); i++) {
                put(am, (i == 0) ? "Log" : "", data.get(i));
            }
        });
    }

    /**
     * Nastaví vlastní systémový log.
     */
    @Command(value = "log set", comment = "Sets own system log")
    private String setSystemLog(@CommandParam("message") String message) {
        return execute("log set", am -> {
            logModule.setLog(message);
            put(am, "Result", "The message to the log has been set");
        });
    }

    /**
     * Nastaví vlastní systémový log.
     */
    @Command(value = "log set default", comment = "Sets own system log with the default value")
    private String setSystemLog() {
        return execute("log set", am -> {
            var message = getB0tnet().getConfig().getMessageLog();
            logModule.setLog(message);
            put(am, "Result", "The message to the log has been set");
        });
    }

    /**
     * Získá informace o vzdáleném logu.
     */
    @Command(value = "log remote", comment = "Get the log from the target IP")
    private String getRemoteSystemLog(@CommandParam("ip") String ip) {
        return execute("remote log -> " + ip, am -> {
            am.setInsideTheme();
            var data = serviceModule.getRemoteLog(ip);

            for (var i = 0; i < data.size(); i++) {
                put(am, (i == 0) ? "Log" : "", data.get(i));
            }
        });
    }

    /**
     * Nastaví vzdálený log.
     */
    @Command(value = "log remote set", comment = "Set the log from the target IP")
    private String setRemoteSystemLog(@CommandParam("ip") String ip, @CommandParam("message") String message) {
        return execute("remote log set -> " + ip, am -> {
            logModule.setRemoteLog(ip, message);
            put(am, "Result", "The message to the log has been set");
        });
    }

    /**
     * Nastaví vzdálený log.
     */
    @Command(value = "log remote set default", comment = "Set the log from the target IP with the default value")
    private String setRemoteSystemLog(@CommandParam("ip") String ip) {
        return execute("remote log set -> " + ip, am -> {
            var message = getB0tnet().getConfig().getMessageLog();
            logModule.setRemoteLog(ip, message);
            put(am, "Result", "The message to the log has been set");
        });
    }
}
