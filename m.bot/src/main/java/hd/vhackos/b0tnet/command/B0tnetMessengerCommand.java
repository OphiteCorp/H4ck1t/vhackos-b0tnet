package hd.vhackos.b0tnet.command;

import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.api.module.MessengerModule;
import hd.vhackos.b0tnet.api.net.response.MessengerDetailResponse;
import hd.vhackos.b0tnet.api.net.response.MessengerResponse;
import hd.vhackos.b0tnet.api.net.response.data.MessengerData;
import hd.vhackos.b0tnet.api.net.response.data.MessengerMessageItemData;
import hd.vhackos.b0tnet.command.base.BaseCommand;
import hd.vhackos.b0tnet.shared.ascii.AsciiMaker;
import hd.vhackos.b0tnet.shared.command.Command;
import hd.vhackos.b0tnet.shared.command.CommandParam;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Příkazy pro messanger.
 */
@Inject
public final class B0tnetMessengerCommand extends BaseCommand {

    @Autowired
    private MessengerModule messengerModule;

    protected B0tnetMessengerCommand(B0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Získá všechny zprávy z messangeru.
     */
    @Command(value = "messenger", comment = "Gets messages from messenger")
    private String getMessanger() {
        return execute("messenger", am -> {
            am.setInsideTheme();

            var data = messengerModule.getMessanger();
            var fields = getFields(data, true);

            var messagesLb = fields.remove(MessengerResponse.P_MESSAGES);
            var messages = (List<MessengerData>) messagesLb.value;
            putRemainings(am, fields, false);

            if (!messages.isEmpty()) {
                am.addRule();
                convertMessengerData(am, messagesLb.name, messages);
            }
        });
    }

    /**
     * Získá všechny zprávy z messangeru.
     */
    @Command(value = "messenger partner", comment = "Gets the details of the partner ID message")
    private String getPartnerMessage(@CommandParam("partnerId") String partnerId) {
        return execute("messenger partner -> " + partnerId, am -> {
            am.setInsideTheme();

            var data = messengerModule.getPartnerMessage(Integer.valueOf(partnerId));
            var fields = getFields(data, true);

            var messagesLb = fields.remove(MessengerDetailResponse.P_MESSAGES);
            var messages = (List<MessengerMessageItemData>) messagesLb.value;
            putRemainings(am, fields, false);

            if (!messages.isEmpty()) {
                am.addRule();
                convertMessengerMessageItemData(am, messagesLb.name, messages);
            }
        });
    }

    // === Pomocné metody
    // ================================================================================================================

    private void convertMessengerData(AsciiMaker am, String name, List<MessengerData> data) {
        for (var i = 0; i < data.size(); i++) {
            var msg = data.get(i);
            var time = MESSAGE_TIME_FORMAT.format(calculateTime(msg.getTime()));
            var str = String.format("Partner ID: %s | %s | %s", StringUtils.rightPad(msg.getWithId().toString(), 7),
                    StringUtils.rightPad(msg.getWith(), 15), StringUtils.rightPad(time, 17));

            put(am, (i == 0) ? name : "", str);
            put(am, "", msg.getMessage());
            put(am, "", "");
        }
    }

    private void convertMessengerMessageItemData(AsciiMaker am, String name, List<MessengerMessageItemData> data) {
        for (var i = 0; i < data.size(); i++) {
            var msg = data.get(i);
            var time = MESSAGE_TIME_FORMAT.format(calculateTime(msg.getTime()));
            var str = String.format("User ID: %s | %s", StringUtils.rightPad(msg.getUserId().toString(), 7),
                    StringUtils.rightPad(time, 17));

            put(am, (i == 0) ? name : "", str);
            put(am, "", msg.getMessage());
            put(am, "", "");
        }
    }
}
