package hd.vhackos.b0tnet.command;

import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.api.module.MissionsModule;
import hd.vhackos.b0tnet.api.net.response.MissionResponse;
import hd.vhackos.b0tnet.api.net.response.data.MissionItemData;
import hd.vhackos.b0tnet.command.base.BaseCommand;
import hd.vhackos.b0tnet.shared.ascii.AsciiMaker;
import hd.vhackos.b0tnet.shared.command.Command;
import hd.vhackos.b0tnet.shared.command.CommandParam;
import hd.vhackos.b0tnet.shared.dto.MissionFinishedType;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * Příkazy pro správu misí.
 */
@Inject
public final class B0tnetMissionsCommand extends BaseCommand {

    @Autowired
    private MissionsModule missionsModule;

    protected B0tnetMissionsCommand(B0tnet b0tnet) {
        super(b0tnet);
    }

    /**
     * Zobrazí informace o mineru.
     */
    @Command(value = "missions", comment = "Gets information about missions")
    private String getMissions() {
        return execute("missions", am -> {
            var data = missionsModule.getMissions();
            var fields = getFields(data, true);
            addMissionsResponseToAsciiMaker(am, fields);
        });
    }

    /**
     * Sebere denní odměnu.
     */
    @Command(value = "mission claim day", comment = "Takes a daily reward")
    private String claimDayReward() {
        return execute("mission claim day reward", am -> {
            var data = missionsModule.claimDaily();
            var fields = getFields(data, true);
            addMissionsResponseToAsciiMaker(am, fields);
        });
    }

    /**
     * Sebere denní odměnu.
     */
    @Command(value = "mission claim", comment = "Complete the mission")
    private String claimMissionReward(@CommandParam("dailyId") int dailyId) {
        return execute("mission claim -> " + dailyId, am -> {
            var data = missionsModule.claimMissionReward(dailyId);
            var fields = getFields(data, true);
            addMissionsResponseToAsciiMaker(am, fields);
        });
    }

    // === Pomocné metody
    // ================================================================================================================

    private void addMissionsResponseToAsciiMaker(AsciiMaker am, Map<String, FieldData> fields) {
        put(am, fields.remove(MissionResponse.P_NEXT_DAILY_RESET));
        put(am, fields.remove(MissionResponse.P_CLAIM_NEXT_DAY));
        put(am, fields.remove(MissionResponse.P_DAILY_COUNT));
        convertMissions(am, fields.remove(MissionResponse.P_DAILY));
        putRemainings(am, fields);
    }

    private void convertMissions(AsciiMaker am, FieldData data) {
        var missions = (List<MissionItemData>) data.value;
        var name = data.name;

        for (var i = 0; i < missions.size(); i++) {
            var mission = missions.get(i);
            var finished = MissionFinishedType.getByCode(mission.getFinished()).getAlias();

            var str = String
                    .format("ID %s | %s -> %s | %s xp | %s", StringUtils.leftPad(String.valueOf(i), 1), StringUtils
                            .rightPad(mission.getRewardType(), 8), StringUtils
                            .leftPad(mission.getRewardAmount().toString(), 4), StringUtils
                            .leftPad(mission.getExperience().toString(), 4), StringUtils.leftPad(finished, 8));

            put(am, (i == 0) ? name : "", str);
        }
        if (missions.isEmpty()) {
            put(am, name, "<none>");
        }
    }
}
