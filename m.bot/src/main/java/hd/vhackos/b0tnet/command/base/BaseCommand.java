package hd.vhackos.b0tnet.command.base;

import de.vandermeer.asciitable.AT_Row;
import hd.vhackos.b0tnet.B0tnet;
import hd.vhackos.b0tnet.Constants;
import hd.vhackos.b0tnet.api.exception.AccountBlockedException;
import hd.vhackos.b0tnet.shared.ascii.AsciiMaker;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import hd.vhackos.b0tnet.shared.exception.B0tnetCoreException;
import hd.vhackos.b0tnet.shared.utils.AsciiUtils;
import hd.vhackos.b0tnet.shared.utils.SentryGuard;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

/**
 * Základní deklarace příkazu.
 */
public abstract class BaseCommand {

    public static final String COMMAND_PACKAGE = Constants.BASE_PACKAGE + ".command";
    protected static final SimpleDateFormat MESSAGE_TIME_FORMAT;

    private final Logger log;
    private final B0tnet b0tnet;

    static {
        MESSAGE_TIME_FORMAT = new SimpleDateFormat("dd.MM.yyyy | HH:mm:ss");
        MESSAGE_TIME_FORMAT.setTimeZone(TimeZone.getTimeZone("GMT+1"));
    }

    protected BaseCommand(B0tnet b0tnet) {
        this.b0tnet = b0tnet;
        log = LoggerFactory.getLogger(getClass());
    }

    protected static AT_Row put(AsciiMaker am, Object key, Object value) {
        if (value != null) {
            return am.add(key, fixValue(value));
        }
        return null;
    }

    protected static AT_Row put(AsciiMaker am, Map.Entry<String, FieldData> data) {
        if (data.getValue() != null) {
            return am.add(data.getValue().name, fixValue(data.getValue().value));
        }
        return null;
    }

    protected static AT_Row put(AsciiMaker am, FieldData data) {
        if (data != null) {
            return am.add(data.name, fixValue(data.value));
        }
        return null;
    }

    protected static void putRemainings(AsciiMaker am, Map<String, FieldData> fields) {
        putRemainings(am, fields, true);
    }

    protected static void putRemainings(AsciiMaker am, Map<String, FieldData> fields, boolean addRule) {
        if (!fields.isEmpty()) {
            if (addRule) {
                am.addRule();
            }
            for (var entry : fields.entrySet()) {
                if (entry.getValue().value != null) {
                    put(am, entry);
                }
            }
        }
    }

    protected static Map<String, FieldData> getFields(Object obj, boolean onlyAsciiRows) {
        return getFields(obj, onlyAsciiRows, true);
    }

    protected static Map<String, FieldData> getFields(Object obj, boolean onlyAsciiRows, boolean skipNullValues) {
        var fields = obj.getClass().getDeclaredFields();
        var resultFields = new TreeMap<String, FieldData>();

        for (var field : fields) {
            if (field.isAnnotationPresent(AsciiRow.class) || !onlyAsciiRows) {
                var name = field.getName();
                var data = new FieldData();

                if (field.isAnnotationPresent(AsciiRow.class)) {
                    var a = field.getDeclaredAnnotation(AsciiRow.class);
                    name = a.value().isEmpty() ? field.getName() : a.value();
                }
                data.name = name;
                data.value = AsciiUtils.getFieldValue(field, obj, true);
                data.rawValue = AsciiUtils.getFieldValue(field, obj, false);

                if (!skipNullValues || data.rawValue != null) {
                    resultFields.put(field.getName(), data);
                }
            }
        }
        return resultFields;
    }

    private static AsciiMaker createAsciiMaker(String title) {
        var am = new AsciiMaker();
        am.addRule();
        am.add(null, "Command » " + title);
        am.addRule();
        return am;
    }

    private static String renderAsciiMaker(AsciiMaker am) {
        am.addRule();
        return am.render();
    }

    private static Object fixValue(Object value) {
        if (value == null) {
            value = "<null>";
        } else if (value instanceof String) {
            if (((String) value).isEmpty()) {
                value = "";
            }
        }
        return value;
    }

    protected final B0tnet getB0tnet() {
        return b0tnet;
    }

    protected final Logger getLog() {
        return log;
    }

    protected final String execute(String title, ISafeCommand safeCommand) {
        var am = createAsciiMaker(title);
        try {
            safeCommand.execute(am);

        } catch (B0tnetCoreException | AccountBlockedException e) {
            log.debug("There was an error processing the command: " + title);
            am = createAsciiMaker(title);
            put(am, "Error", StringUtils.isEmpty(e.getMessage()) ? e.toString() : e.getMessage());

        } catch (NumberFormatException e) {
            log.error("Invalid input value. A number was expected and the string came");
            throw e;

        } catch (Exception e) {
            SentryGuard.log(e);
            log.error("There was an error processing the command: " + title, e);
            am = createAsciiMaker(title);
            put(am, "Error", StringUtils.isEmpty(e.getMessage()) ? e.toString() : e.getMessage());
        }
        return renderAsciiMaker(am);
    }

    protected static Date calculateTime(Long time) {
        return new Date(System.currentTimeMillis() - (((System.currentTimeMillis() / 1000) - time) * 1000));
    }

    protected interface ISafeCommand {

        void execute(AsciiMaker am) throws Exception;
    }

    protected static class FieldData {

        public String name;
        public Object value;
        public Object rawValue;
    }
}
