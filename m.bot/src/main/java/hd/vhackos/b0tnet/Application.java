package hd.vhackos.b0tnet;

import hd.vhackos.b0tnet.api.IB0tnet;
import hd.vhackos.b0tnet.api.exception.AccountBlockedException;
import hd.vhackos.b0tnet.api.exception.ConnectionException;
import hd.vhackos.b0tnet.api.exception.InvalidLoginException;
import hd.vhackos.b0tnet.command.ApplicationCommand;
import hd.vhackos.b0tnet.command.base.BaseCommand;
import hd.vhackos.b0tnet.config.ApplicationConfig;
import hd.vhackos.b0tnet.config.ConfigProvider;
import hd.vhackos.b0tnet.db.HibernateManager;
import hd.vhackos.b0tnet.db.exception.DatabaseConnectionException;
import hd.vhackos.b0tnet.exception.ForceUpdateRequiredException;
import hd.vhackos.b0tnet.exception.MissingLoginCredentialException;
import hd.vhackos.b0tnet.gui.B0tnetGui;
import hd.vhackos.b0tnet.service.base.EndpointService;
import hd.vhackos.b0tnet.service.base.Service;
import hd.vhackos.b0tnet.shared.command.CommandDispatcher;
import hd.vhackos.b0tnet.shared.command.CommandInvalidParamsException;
import hd.vhackos.b0tnet.shared.command.CommandRunner;
import hd.vhackos.b0tnet.shared.command.ICommandListener;
import hd.vhackos.b0tnet.shared.injection.IInjectRule;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.shared.injection.InjectionContext;
import hd.vhackos.b0tnet.shared.utils.EncryptUtils;
import hd.vhackos.b0tnet.shared.utils.SentryGuard;
import hd.vhackos.b0tnet.utils.ApplicationArgsResolver;
import hd.vhackos.b0tnet.utils.Utils;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.reflections.Reflections;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.*;

/**
 * Hlavní třída aplikace.
 */
public final class Application implements ICommandListener {

    public static final int ERROR_CODE_MISSING_LOGIN_CREDENTIAL = 10;
    public static final int ERROR_CODE_ACCOUNT_BANNED = 11;
    public static final int ERROR_CODE_FORCE_UPDATE = 12;

    public static final String LOGO = getB0tnetLogo();
    private static final Logger LOG = LoggerFactory.getLogger(Application.class);

    private static B0tnet b0tnet;
    private static B0tnetGui gui;

    /**
     * EntryPoint aplikace.
     */
    public static void main(String[] args) {
        var time = System.currentTimeMillis();
        var argsResolver = new ApplicationArgsResolver(args);
        var useGui = !argsResolver.isConsoleMode();

        // init sentry pro logování - dostupné pouze pro windows
        if (Utils.isWindows()) {
            SentryGuard.init(IB0tnet.VERSION, Constants.BASE_PACKAGE);
        } else {
            SentryGuard.setAvailable(false);
            LOG.info("Sentry will be ignored. It is only supported for the Windows operating system");
        }
        if (useGui) {
            gui = new B0tnetGui();
            gui.open();
        }
        LOG.info(LOGO);
        LOG.info("The B0tnet starting in version: v" + IB0tnet.VERSION);

        // vytvoří instanci aplikace a B0tnet
        final var app = new Application();
        b0tnet = new B0tnet();

        // vytvoří dependency injection
        var injectRules = prepareInjectRules();
        InjectionContext.initialize(new String[]{ Constants.BASE_PACKAGE }, injectRules);
        InjectionContext.lazyInit(b0tnet);
        var context = InjectionContext.getInstance();

        // načte konfiguraci
        var config = context.get(ConfigProvider.class).getAppConfig();
        context.get(ApplicationConfig.class).set(config);
        SentryGuard.setGameApiToTag(config.getGameApi());

        if (config.isDebug()) {
            Configurator.setRootLevel(Level.ALL);
        }

        // zmigruje konfiguraci a uloží ji
        var configProvider = context.get(ConfigProvider.class);
        var rawPassword = EncryptUtils.decrypt(config.getControlKey(), config.getPassword());
        if (!StringUtils.isBlank(config.getPassword()) && rawPassword == null) {
            config.setPassword(EncryptUtils.encrypt(config.getControlKey(), config.getPassword()));
            configProvider.update(config);
            context.update(ApplicationConfig.class, config);
            InjectionContext.lazyInit(b0tnet);
            LOG.info("Encrypt the user's password to connect to the game server has been completed");
        }

        // do načte všechny třídy, které jsou označené jako lazy (vyžadují např. konfiguraci aplikace)
        InjectionContext.initLazyClasses();

        if (!isConsoleMode()) {
            gui.postProcessing(config);
        }
        // vytvoří databázové připojení
        if (config.isDbEnable()) {
            try {
                HibernateManager.initialize(config);

            } catch (DatabaseConnectionException e) {
                if (e.getCause() != null && e.getCause() instanceof SQLException) {
                    var eSql = (SQLException) e.getCause();
                    LOG.error("A database error has occurred: SQL state {}, message: {}", eSql.getSQLState(),
                            eSql.getMessage());
                } else {
                    LOG.error(
                            "An unexpected error occurred when connecting to the database. The database will not be used. Message: {}",
                            e.getMessage());
                }
            }
        }
        // vyhledá všechny dostupné příkazy
        CommandRunner.initialize(context);
        CommandRunner.initCommands(b0tnet);

        // zahájí čtení konzole pro příkazy
        var dispatcher = CommandDispatcher.getInstance();
        dispatcher.addListener(app);

        try {
            if (!argsResolver.isOffline()) {
                // spustí B0tnet
                b0tnet.start();
            }
            time = System.currentTimeMillis() - time;
            LOG.info("The b0tnet was started in: {}ms", time);
            LOG.info("Type \".{}\" to view available commands", ApplicationCommand.CMD_HELP);

        } catch (MissingLoginCredentialException e) {
            LOG.error("The username or password is not filled in. Open the configuration file and edit it");
            exit(ERROR_CODE_MISSING_LOGIN_CREDENTIAL);

        } catch (AccountBlockedException e) {
            SentryGuard.log("Your account has been blocked: " + e.getUserName());
            LOG.error("Your game account '{}' has been blocked :-(", e.getUserName());
            exit(ERROR_CODE_ACCOUNT_BANNED);

        } catch (ForceUpdateRequiredException e) {
            LOG.warn("An update is forced. Please download the latest version of the application");
            exit(ERROR_CODE_FORCE_UPDATE);

        } catch (InvalidLoginException e) {
            LOG.error("Your username or password is incorrect");
            config.resetPassword();
            configProvider.update(config);
            exit(1);

        } catch (ConnectionException e) {
            LOG.error(
                    "Unable to establish server connection. Check your login and password or proxy server settings or try it later");
            exit(1);

        } catch (Exception e) {
            LOG.error("An unexpected error occurred while booting the b0tnet", e);
            exit(1);
        }
    }

    /**
     * Jedná se o konzolový mód?
     */
    public static boolean isConsoleMode() {
        return (gui == null);
    }

    /**
     * Ukončí aplikaci.
     */
    private static void exit(int exitCode) {
        LOG.info("Shutdown is in progress...");
        b0tnet.shutdown();
        HibernateManager.shutdown();
        LOG.info("B0tnet stopped");

        if (!isConsoleMode()) {
            gui.close(exitCode);
        } else {
            System.exit(exitCode);
        }
    }

    private static Map<Class, IInjectRule> prepareInjectRules() {
        var rules = new HashMap<Class, IInjectRule>();
        rules.put(null, c -> InjectionContext.getConstructor(c, IB0tnet.class).newInstance(b0tnet));

        // služby
        var ref = new Reflections(Service.SERVICES_PACKAGE, new TypeAnnotationsScanner());
        var classes = ref.getTypesAnnotatedWith(EndpointService.class, true);
        for (var clazz : classes) {
            rules.put(clazz, c -> InjectionContext.getConstructor(c, B0tnet.class).newInstance(b0tnet));
        }

        // příkazy
        ref = new Reflections(BaseCommand.COMMAND_PACKAGE, new TypeAnnotationsScanner());
        classes = ref.getTypesAnnotatedWith(Inject.class, true);
        for (var clazz : classes) {
            rules.put(clazz, c -> InjectionContext.getConstructor(c, B0tnet.class).newInstance(b0tnet));
        }
        return rules;
    }

    private static void printCommandResult(Object result) {
        // příkaz je typu void a nevrací žádnou hodnotu
        if (result != null && result instanceof Optional) {
            var optional = (Optional) result;
            if (optional.isPresent()) {
                LOG.info("Executed!");
            }
        } else {
            if (result != null) {
                if (isConsoleMode()) {
                    System.out.println(result);
                } else {
                    LOG.info("\n" + result.toString() + "\n");
                }
            } else {
                LOG.info("Returns: <NULL>");
            }
        }
    }

    private static String getB0tnetLogo() {
        // http://patorjk.com/software/taag-v1/
        char code[] = {
                '\n', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'M', '"', '"', 'M', 'M', 'M', 'M', 'M', '"', '"',
                'M', 'M', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                'd', 'P', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'M', 'M', 'P', '"', '"', '"', '"', '"', 'Y',
                'M', 'M', ' ', 'M', 'P', '"', '"', '"', '"', '"', '"', '`', 'M', 'M', ' ', '\n', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', 'M', ' ', ' ', 'M', 'M', 'M', 'M', 'M', ' ', ' ', 'M', 'M', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '8', '8', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', 'M', '\'', ' ', '.', 'm', 'm', 'm', '.', ' ', '`', 'M', ' ', 'M', ' ',
                ' ', 'm', 'm', 'm', 'm', 'm', '.', '.', 'M', ' ', '\n', 'd', 'P', ' ', ' ', ' ', '.', 'd', 'P', ' ',
                'M', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '`', 'M', ' ', '.', 'd', '8', '8', '8', '8', 'b', '.',
                ' ', '.', 'd', '8', '8', '8', '8', 'b', '.', ' ', '8', '8', ' ', ' ', '.', 'd', 'P', ' ', ' ', ' ', ' ',
                ' ', 'M', ' ', ' ', 'M', 'M', 'M', 'M', 'M', ' ', ' ', 'M', ' ', 'M', '.', ' ', ' ', ' ', ' ', ' ', ' ',
                '`', 'Y', 'M', ' ', '\n', '8', '8', ' ', ' ', ' ', 'd', '8', '\'', ' ', 'M', ' ', ' ', 'M', 'M', 'M',
                'M', 'M', ' ', ' ', 'M', 'M', ' ', '8', '8', '\'', ' ', ' ', '`', '8', '8', ' ', '8', '8', '\'', ' ',
                ' ', '`', '"', '"', ' ', '8', '8', '8', '8', '8', '"', ' ', ' ', ' ', ' ', ' ', ' ', 'M', ' ', ' ', 'M',
                'M', 'M', 'M', 'M', ' ', ' ', 'M', ' ', 'M', 'M', 'M', 'M', 'M', 'M', 'M', '.', ' ', ' ', 'M', ' ',
                '\n', '8', '8', ' ', '.', '8', '8', '\'', ' ', ' ', 'M', ' ', ' ', 'M', 'M', 'M', 'M', 'M', ' ', ' ',
                'M', 'M', ' ', '8', '8', '.', ' ', ' ', '.', '8', '8', ' ', '8', '8', '.', ' ', ' ', '.', '.', '.', ' ',
                '8', '8', ' ', ' ', '`', '8', 'b', '.', ' ', ' ', ' ', ' ', 'M', '.', ' ', '`', 'M', 'M', 'M', '\'',
                ' ', '.', 'M', ' ', 'M', '.', ' ', '.', 'M', 'M', 'M', '\'', ' ', ' ', 'M', ' ', '\n', '8', '8', '8',
                '8', 'P', '\'', ' ', ' ', ' ', 'M', ' ', ' ', 'M', 'M', 'M', 'M', 'M', ' ', ' ', 'M', 'M', ' ', '`',
                '8', '8', '8', '8', '8', 'P', '8', ' ', '`', '8', '8', '8', '8', '8', 'P', '\'', ' ', 'd', 'P', ' ',
                ' ', ' ', '`', 'Y', 'P', ' ', ' ', ' ', ' ', 'M', 'M', 'b', ' ', ' ', ' ', ' ', ' ', 'd', 'M', 'M', ' ',
                'M', 'b', '.', ' ', ' ', ' ', ' ', ' ', '.', 'd', 'M', ' ', '\n', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', ' ', 'M', 'M', 'M', 'M', 'M', 'M',
                'M', 'M', 'M', 'M', 'M', ' ', '\n', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                '\n', 'M', '#', '"', '"', '"', '"', '"', '"', '"', '\'', 'M', ' ', ' ', ' ', 'a', '8', '8', '8', '8',
                'a', ' ', ' ', ' ', ' ', 'd', 'P', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'd', 'P', ' ', ' ', ' ', '\n', '#', '#', ' ', ' ', 'm',
                'm', 'm', 'm', '.', ' ', '`', 'M', ' ', 'd', '8', '\'', ' ', '.', '.', '8', 'b', ' ', ' ', ' ', '8',
                '8', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', '8', '8', ' ', ' ', ' ', '\n', '#', '\'', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '.',
                'M', ' ', '8', '8', ' ', '.', 'P', ' ', '8', '8', ' ', 'd', '8', '8', '8', '8', 'P', ' ', '8', '8', 'd',
                '8', '8', '8', 'b', '.', ' ', '.', 'd', '8', '8', '8', '8', 'b', '.', ' ', 'd', '8', '8', '8', '8', 'P',
                ' ', '\n', 'M', '#', ' ', ' ', 'M', 'M', 'M', 'b', '.', '\'', 'Y', 'M', ' ', '8', '8', ' ', 'd', '\'',
                ' ', '8', '8', ' ', ' ', ' ', '8', '8', ' ', ' ', ' ', '8', '8', '\'', ' ', ' ', '`', '8', '8', ' ',
                '8', '8', 'o', 'o', 'o', 'o', 'd', '8', ' ', ' ', ' ', '8', '8', ' ', ' ', ' ', '\n', 'M', '#', ' ',
                ' ', 'M', 'M', 'M', 'M', '\'', ' ', ' ', 'M', ' ', 'Y', '8', '\'', '\'', ' ', '.', '8', 'P', ' ', ' ',
                ' ', '8', '8', ' ', ' ', ' ', '8', '8', ' ', ' ', ' ', ' ', '8', '8', ' ', '8', '8', '.', ' ', ' ', '.',
                '.', '.', ' ', ' ', ' ', '8', '8', ' ', ' ', ' ', '\n', 'M', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                '.', ';', 'M', ' ', ' ', 'Y', '8', '8', '8', '8', 'P', ' ', ' ', ' ', ' ', 'd', 'P', ' ', ' ', ' ', 'd',
                'P', ' ', ' ', ' ', ' ', 'd', 'P', ' ', '`', '8', '8', '8', '8', '8', 'P', '\'', ' ', ' ', ' ', 'd',
                'P', ' ', ' ', ' ', '\n', 'M', '#', '#', '#', '#', '#', '#', '#', '#', '#', 'M', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '\n', '\n', 'd', 'P', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'M', '"', '"', 'M', 'M', 'M', 'M', 'M', '"', '"',
                'M', 'M', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'M', '"', '"', '"', '"', '"', '"', '\'',
                'Y', 'M', 'M', ' ', 'o', 'o', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'o', 'o', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'o', 'o', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', '\n', '8', '8', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'M', ' ', ' ', 'M', 'M', 'M', 'M', 'M', ' ', ' ', 'M', 'M',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'M', ' ', ' ', 'm', 'm', 'm', 'm', '.', ' ', '`', 'M',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', '\n', '8', '8', 'd', '8', '8', '8', 'b', '.', ' ', 'd', 'P', ' ', ' ', ' ', ' ',
                'd', 'P', ' ', ' ', ' ', ' ', 'M', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '`', 'M', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'M', ' ', ' ', 'M', 'M', 'M', 'M', 'M', ' ', ' ', 'M', ' ', 'd', 'P',
                ' ', 'd', 'P', ' ', ' ', ' ', '.', 'd', 'P', ' ', 'd', 'P', ' ', '.', 'd', '8', '8', '8', '8', 'b', '.',
                ' ', 'd', 'P', ' ', '.', 'd', '8', '8', '8', '8', 'b', '.', ' ', '8', '8', 'd', '8', '8', '8', 'b', '.',
                ' ', '\n', '8', '8', '\'', ' ', ' ', '`', '8', '8', ' ', '8', '8', ' ', ' ', ' ', ' ', '8', '8', ' ',
                ' ', ' ', ' ', 'M', ' ', ' ', 'M', 'M', 'M', 'M', 'M', ' ', ' ', 'M', 'M', ' ', '8', '8', '8', '8', '8',
                '8', '8', '8', ' ', 'M', ' ', ' ', 'M', 'M', 'M', 'M', 'M', ' ', ' ', 'M', ' ', '8', '8', ' ', '8', '8',
                ' ', ' ', ' ', 'd', '8', '\'', ' ', '8', '8', ' ', 'Y', '8', 'o', 'o', 'o', 'o', 'o', '.', ' ', '8',
                '8', ' ', '8', '8', '\'', ' ', ' ', '`', '8', '8', ' ', '8', '8', '\'', ' ', ' ', '`', '8', '8', ' ',
                '\n', '8', '8', '.', ' ', ' ', '.', '8', '8', ' ', '8', '8', '.', ' ', ' ', '.', '8', '8', ' ', ' ',
                ' ', ' ', 'M', ' ', ' ', 'M', 'M', 'M', 'M', 'M', ' ', ' ', 'M', 'M', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', 'M', ' ', ' ', 'M', 'M', 'M', 'M', '\'', ' ', '.', 'M', ' ', '8', '8', ' ', '8', '8',
                ' ', '.', '8', '8', '\'', ' ', ' ', '8', '8', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '8', '8', ' ', '8',
                '8', ' ', '8', '8', '.', ' ', ' ', '.', '8', '8', ' ', '8', '8', ' ', ' ', ' ', ' ', '8', '8', ' ',
                '\n', '8', '8', 'Y', '8', '8', '8', '8', '\'', ' ', '`', '8', '8', '8', '8', 'P', '8', '8', ' ', ' ',
                ' ', ' ', 'M', ' ', ' ', 'M', 'M', 'M', 'M', 'M', ' ', ' ', 'M', 'M', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', 'M', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '.', 'M', 'M', ' ', 'd', 'P', ' ', '8', '8', '8',
                '8', 'P', '\'', ' ', ' ', ' ', 'd', 'P', ' ', '`', '8', '8', '8', '8', '8', 'P', '\'', ' ', 'd', 'P',
                ' ', '`', '8', '8', '8', '8', '8', 'P', '\'', ' ', 'd', 'P', ' ', ' ', ' ', ' ', 'd', 'P', ' ', '\n',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '.', '8', '8', ' ', ' ', ' ', ' ',
                'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '\n', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'd', '8', '8', '8', '8', 'P', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '\n', '\n', 'D', 'i', 's', 'c', 'o',
                'r', 'd', ':', ' ', 'h', 't', 't', 'p', 's', ':', '/', '/', 'd', 'i', 's', 'c', 'o', 'r', 'd', 'a', 'p',
                'p', '.', 'c', 'o', 'm', '/', 'i', 'n', 'v', 'i', 't', 'e', '/', 'C', '2', 'P', 'b', 'm', 'e', '6',
                '\n', '\n' };
        return new String(code);
    }

    @Override
    public void incomingCommand(CommandDispatcher dispatcher, String command) {
        if (command.equalsIgnoreCase(ApplicationCommand.CMD_EXIT)) {
            dispatcher.shutdown();
            exit(0);

        } else {
            var runner = CommandRunner.getInstance();
            // vezme pouze příkazy bez kategorie + je potřeba seřadit příkazy dle jejich délky od (max->min)
            List<String> availableCommands = new ArrayList(runner.getCommands().get(null).keySet());
            availableCommands.sort((o1, o2) -> Integer.compare(o2.length(), o1.length()));
            String exec = null;

            for (var cmd : availableCommands) {
                if (command.startsWith(cmd)) {
                    // je potřeba odstranit příkaz, aby zbyly pouze parametry (pokud nějaké jsou), protože díky tomu
                    // je možné posílat "exec" příkaz složený i z mezer
                    command = command.substring(cmd.length()).trim();
                    exec = cmd;
                    break;
                }
            }
            var method = runner.getCommandMethod(exec);
            if (method != null) {
                // pokud příkaz obsahuje povinné parametry, tak jako exec nastavíme cokoliv jen proto, aby to
                // CommandLine rozparsoval jako exec a ponechal parametry v původním stavu - původní exec byl
                // odstraněn, protože chceme podporovat exec obsahující mezeru, což následný parser neumí - proto fake
                command = (method.getParameterCount() > 0) ? "fake_exec " + command : exec;
                var cmdLine = CommandLine.parse(command);
                var args = cmdLine.getArguments();

                // pokud se jedná o text složený z více slov, tak vždy je umístěn v uvozovkách a parser je ponechává,
                // takže je dodatečně odeberem
                if (args != null && args.length > 0) {
                    for (var i = 0; i < args.length; i++) {
                        var s = args[i];
                        if (s.equals("<null>")) {
                            args[i] = null;
                        } else if (s.startsWith("\"") && s.endsWith("\"")) {
                            args[i] = s.substring(1, s.length() - 1);
                        }
                    }
                }
                try {
                    var result = runner.run(exec, args);
                    printCommandResult(result);
                } catch (CommandInvalidParamsException e) {
                    // nic
                }
            } else {
                LOG.warn("The command '{}' does not exist", command);
            }
        }
    }
}