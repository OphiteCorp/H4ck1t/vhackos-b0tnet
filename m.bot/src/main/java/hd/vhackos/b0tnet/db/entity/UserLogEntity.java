package hd.vhackos.b0tnet.db.entity;

import hd.vhackos.b0tnet.db.entity.base.PersistentEntity;

import javax.persistence.*;

/**
 * Databázová entita mapující uživatele na jeho log.
 */
@Table(name = UserLogEntity.TABLE_SQL)
@Entity(name = UserLogEntity.TABLE)
public final class UserLogEntity extends PersistentEntity {

    static final String TABLE = "UserLog";
    static final String TABLE_SQL = "user_log";

    public static final String USER = "user";
    public static final String LOG = "log";
    static final String USER_SQL = "user_id";
    static final String LOG_SQL = "log";

    @ManyToOne
    @JoinColumn(name = USER_SQL)
    private UserEntity user;

    @Lob
    @Column(name = LOG_SQL, nullable = false)
    private String log;

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }
}
