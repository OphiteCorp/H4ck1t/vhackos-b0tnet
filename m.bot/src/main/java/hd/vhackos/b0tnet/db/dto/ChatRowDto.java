package hd.vhackos.b0tnet.db.dto;

import java.util.Date;

/**
 * Informace o záznamu v chatu.
 */
public final class ChatRowDto {

    private String user;
    private String toUser;
    private String message;
    private Date date;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
