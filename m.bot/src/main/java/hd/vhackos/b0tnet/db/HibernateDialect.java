package hd.vhackos.b0tnet.db;

import org.hibernate.dialect.MySQL5InnoDBDialect;

/**
 * Vlastní collate pro databázové tabulky.
 */
public final class HibernateDialect extends MySQL5InnoDBDialect {

    @Override
    public String getTableTypeString() {
        // kompletní podpora UTF-8
        return " ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ";
    }
}
