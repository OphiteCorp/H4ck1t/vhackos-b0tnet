package hd.vhackos.b0tnet.db.entity.base;

import javax.persistence.*;

/**
 * Základní databázová entita.
 */
@MappedSuperclass
public abstract class PersistentEntity {

    public static final String ID = "id";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
