package hd.vhackos.b0tnet.db.entity;

import hd.vhackos.b0tnet.db.entity.base.PersistentEntity;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

/**

 */
@Table(name = TransactionEntity.TABLE_SQL)
@Entity(name = TransactionEntity.TABLE)
public final class TransactionEntity extends PersistentEntity {

    static final String TABLE = "Transaction";
    static final String TABLE_SQL = "transaction";

    public static final String TIME = "time";
    public static final String FROM_USER = "fromUser";
    public static final String FROM_USER_IP = "fromUserIp";
    public static final String TO_USER = "toUser";
    public static final String TO_USER_IP = "toUserIp";
    public static final String AMOUNT = "amount";
    public static final String CREATED = "created";
    static final String FROM_USER_SQL = "from_user_id";
    static final String TO_USER_SQL = "to_user_id";
    private static final String TIME_SQL = "time";
    private static final String FROM_USER_IP_SQL = "from_user_ip";
    private static final String TO_USER_IP_SQL = "to_user_ip";
    private static final String AMOUNT_SQL = "amount";
    private static final String CREATED_SQL = "created";

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = TIME_SQL, nullable = false)
    private Date time;

    @ManyToOne
    @JoinColumn(name = FROM_USER_SQL)
    private UserEntity fromUser;

    @Column(name = FROM_USER_IP_SQL, length = 15, nullable = false)
    private String fromUserIp;

    @ManyToOne
    @JoinColumn(name = TO_USER_SQL)
    private UserEntity toUser;

    @Column(name = TO_USER_IP_SQL, length = 15, nullable = false)
    private String toUserIp;

    @Column(name = AMOUNT_SQL, nullable = false)
    private long amount;

    @CreationTimestamp
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = CREATED_SQL, nullable = false)
    private Date created;

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public UserEntity getFromUser() {
        return fromUser;
    }

    public void setFromUser(UserEntity fromUser) {
        this.fromUser = fromUser;
    }

    public UserEntity getToUser() {
        return toUser;
    }

    public void setToUser(UserEntity toUser) {
        this.toUser = toUser;
    }

    public String getFromUserIp() {
        return fromUserIp;
    }

    public void setFromUserIp(String fromUserIp) {
        this.fromUserIp = fromUserIp;
    }

    public String getToUserIp() {
        return toUserIp;
    }

    public void setToUserIp(String toUserIp) {
        this.toUserIp = toUserIp;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
