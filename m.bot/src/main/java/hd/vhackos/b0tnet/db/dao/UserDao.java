package hd.vhackos.b0tnet.db.dao;

import hd.vhackos.b0tnet.db.dao.base.Dao;
import hd.vhackos.b0tnet.db.entity.UserEntity;
import hd.vhackos.b0tnet.db.entity.UserIpEntity;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.utils.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * Práce s DB entitou uživatele.
 */
@Inject
public final class UserDao extends Dao<UserEntity> {

    private static final Object LOCK = new Object();

    /**
     * Vytvoří nový záznam uživatele.
     */
    public UserEntity create(int userId, String ip) {
        synchronized (LOCK) {
            var user = getByUserId(userId);

            if (user == null) {
                user = new UserEntity();
                user.setUserId(userId);
                user.setUserName(null);
                user.setIps(new HashSet<>());
                user.setCreated(new Date());

                if (Utils.isValidIp(ip)) {
                    user.getIps().add(ip);
                }
                var finalUser = user;
                user = execute(s -> createOrUpdate(s, finalUser, true));
            } else {
                if (Utils.isValidIp(ip)) {
                    if (user.getIps().add(ip)) {
                        user.setUpdated(new Date());
                        var finalUser = user;

                        user = execute(s -> {
                            var u = createOrUpdate(s, finalUser, false);
                            removeDupeUsers(finalUser.getUserName());
                            return u;
                        });
                    }
                }
            }
            return user;
        }
    }

    /**
     * Vytvoří nový záznam uživatele.
     */
    public UserEntity create(String userName, int userId) {
        synchronized (LOCK) {
            var user = getByUserId(userId);

            if (user == null) {
                user = getByUserName(userName);

                if (user == null) {
                    user = new UserEntity();
                    user.setUserId(userId);
                    user.setUserName(userName);
                    user.setIps(new HashSet<>());
                    user.setCreated(new Date());

                    var finalUser = user;
                    user = execute(s -> createOrUpdate(s, finalUser, true));
                    return user;
                }
            }
            if (user != null) {
                user.setUserName(userName);
                user.setUpdated(new Date());
                var finalUser = user;

                return execute(s -> {
                    var u = createOrUpdate(s, finalUser, false);
                    removeDupeUsers(finalUser.getUserName());
                    return u;
                });
            }
            return user;
        }
    }

    /**
     * Vytvoří nový záznam uživatele.
     */
    public UserEntity createOrUpdate(Integer userId, String userName, String ip) {
        synchronized (LOCK) {
            var userByName = getByUserName(userName);
            var userById = getByUserId(userId);

            if (userByName == null && userById == null) {
                userByName = new UserEntity();
                userByName.setIps(new HashSet<>());
                userByName.setCreated(new Date());
            } else {
                if (userByName == null) {
                    userByName = userById;
                }
            }
            if (userName != null) {
                userByName.setUserName(userName);
            }
            if (Utils.isValidIp(ip)) {
                userByName.getIps().add(ip);
            }
            if (userId != null) {
                userByName.setUserId(userId);
            }
            var finalUser = userByName;
            var createNew = (finalUser.getId() == null);

            if (!createNew) {
                finalUser.setUpdated(new Date());
                return execute(s -> {
                    var u = createOrUpdate(s, finalUser, createNew);
                    removeDupeUsers(finalUser.getUserName());
                    return u;
                });
            }
            return execute(s -> createOrUpdate(s, finalUser, createNew));
        }
    }

    /**
     * Získá všechny uživatele.
     */
    public List<UserEntity> getAll() {
        return execute(s -> {
            var q = s.createQuery(query("select u from {entity} u"), UserEntity.class);
            return q.list();
        });
    }

    /**
     * Získá uživatele podle jeho IP.
     */
    public List<UserEntity> getByIp(String ip) {
        return execute(s -> {
            var hql = query("select u from {entity} u ");
            hql += "join fetch u." + UserEntity.IPS + " ips where ips in (:IP) ";
            hql += "order by u." + UserEntity.ID + " desc";

            var q = s.createQuery(hql, UserEntity.class);
            q.setParameter("IP", ip);
            return q.list();
        });
    }

    /**
     * Získá uživatele podle jeho jména.
     */
    public UserEntity getByUserName(String userName) {
        if (userName == null) {
            return null;
        }
        var hql = query("select u from {entity} u where u." + UserEntity.USER_NAME + " = :USER_NAME order by u." +
                        UserEntity.UPDATED + " desc");

        return execute(s -> {
            var q = s.createQuery(hql, UserEntity.class);
            q.setParameter("USER_NAME", userName);
            var users = q.list();

            if (users.size() <= 1) {
                return users.isEmpty() ? null : users.get(0);
            } else {
                // odstraní duplicity pokud nějaké jsou
                var u = users.get(0);
                for (var i = 1; i < users.size(); i++) {
                    delete(users.get(i));
                }
                return u;
            }
        });
    }

    /**
     * Získá duplicitní uživatele, kteří mají stejné jméno.
     */
    public List<UserEntity> getDupeUsers(String userName) {
        if (userName == null) {
            return null;
        }
        var hql = query("select u from {entity} u where u." + UserEntity.USER_NAME + " = :USER_NAME order by u." +
                        UserEntity.UPDATED + " desc");

        return execute(s -> {
            var q = s.createQuery(hql, UserEntity.class);
            q.setParameter("USER_NAME", userName);
            return q.list();
        });
    }

    /**
     * Získá uživatele podle jeho uživatelského ID.
     */
    public UserEntity getByUserId(Integer userId) {
        if (userId == null) {
            return null;
        }
        var hql = query("select u from {entity} u where u." + UserEntity.USER_ID + " = :USER_ID");

        return execute(s -> {
            var q = s.createQuery(hql, UserEntity.class);
            q.setParameter("USER_ID", userId);
            return q.uniqueResult();
        });
    }

    /**
     * Získá uživatelé podle uživatelského ID.
     */
    public List<UserEntity> getByUsersId(Integer userId) {
        if (userId == null) {
            return null;
        }
        var hql = query("select u from {entity} u where u." + UserEntity.USER_ID + " = :USER_ID");

        return execute(s -> {
            var q = s.createQuery(hql, UserEntity.class);
            q.setParameter("USER_ID", userId);
            return q.list();
        });
    }

    /**
     * Získá všechny IP uživatele podke ID seřazené od nejnovější.
     */
    public List<String> getIps(int userId) {
        return execute(s -> {
            var sql = "select ips." + UserIpEntity.IP_SQL + " from " + UserIpEntity.TABLE_SQL + " ips ";
            sql += "join " + UserEntity.TABLE_SQL + " u on u." + UserEntity.ID + " = ips." + UserIpEntity.USER_SQL +
                   " ";
            sql += "where u." + UserEntity.USER_ID_SQL + " = :ID ";
            sql += "order by ips." + UserIpEntity.ID + " desc";

            var q = s.createNativeQuery(sql);
            q.setParameter("ID", userId);
            return q.list();
        });
    }

    /**
     * Získá duplicitní UID uživatelelů.
     */
    public List<Integer> getDuplicateUserId() {
        return execute(s -> {
            var sql = "select u." + UserEntity.USER_ID_SQL + ", count(u." + UserEntity.USER_ID_SQL + ") from " +
                      UserEntity.TABLE_SQL + " u ";
            sql += "group by u." + UserEntity.USER_ID_SQL + " ";
            sql += "having count(u." + UserEntity.USER_ID_SQL + ") > 1 ";

            var q = s.createNativeQuery(sql);
            var list = q.list();
            var result = new ArrayList<Integer>();

            for (var obj : list) {
                result.add((Integer) ((Object[]) obj)[0]);
            }
            return result;
        });
    }

    private void removeDupeUsers(String userName) {
        var users = getDupeUsers(userName);

        if (users != null && !users.isEmpty()) {
            for (var user : users) {
                delete(user);
            }
        }
    }
}
