package hd.vhackos.b0tnet.db.entity;

import hd.vhackos.b0tnet.db.entity.base.PersistentEntity;

import javax.persistence.*;

/**
 * Databázová entita mapující IP na uživatele.
 */
@Table(name = UserIpEntity.TABLE_SQL, uniqueConstraints = @UniqueConstraint(columnNames = {
        UserIpEntity.USER_SQL, UserIpEntity.IP_SQL }))
@Entity(name = UserIpEntity.TABLE)
public final class UserIpEntity extends PersistentEntity {

    public static final String TABLE_SQL = "user_ip";
    static final String TABLE = "UserIp";

    public static final String USER = "user";
    public static final String USER_SQL = "user_id";
    public static final String IP = "ip";
    public static final String IP_SQL = "ip";

    @ManyToOne
    @JoinColumn(name = USER_SQL)
    private UserEntity user;

    @Column(name = IP_SQL, length = 15, nullable = false)
    private String ip;

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
