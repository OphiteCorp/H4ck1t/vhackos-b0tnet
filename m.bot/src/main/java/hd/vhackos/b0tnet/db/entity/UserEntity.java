package hd.vhackos.b0tnet.db.entity;

import hd.vhackos.b0tnet.db.entity.base.PersistentEntity;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Databázová entita uživatele.
 */
@Table(name = UserEntity.TABLE_SQL, uniqueConstraints = @UniqueConstraint(columnNames = {
        UserEntity.USER_ID_SQL, UserEntity.USER_NAME_SQL }))
@Entity(name = UserEntity.TABLE)
public final class UserEntity extends PersistentEntity {

    public static final String TABLE_SQL = "user";
    static final String TABLE = "User";

    public static final String USER_ID = "userId";
    public static final String USER_ID_SQL = "user_id";
    public static final String USER_NAME = "userName";
    public static final String CREATED = "created";
    public static final String UPDATED = "updated";
    public static final String IPS = "ips";
    public static final String LOGS = "logs";
    public static final String TRANSACTIONS_FROM = "transactionsFrom";
    public static final String TRANSACTIONS_TO = "transactionsTo";
    static final String USER_NAME_SQL = "user_name";
    private static final String CREATED_SQL = "created";
    private static final String UPDATED_SQL = "updated";

    @AsciiRow("User ID")
    @Column(name = USER_ID_SQL, length = 10)
    private Integer userId;

    @AsciiRow("User")
    @Column(name = USER_NAME_SQL, length = 50)
    private String userName;

    @CreationTimestamp
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = CREATED_SQL, nullable = false)
    private Date created;

    @CreationTimestamp
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = UPDATED_SQL)
    private Date updated;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = UserIpEntity.TABLE_SQL, joinColumns = { @JoinColumn(name = UserIpEntity.USER_SQL) })
    @Column(name = UserIpEntity.IP_SQL)
    private Set<String> ips;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = UserLogEntity.TABLE_SQL, joinColumns = { @JoinColumn(name = UserLogEntity.USER_SQL) })
    @Column(name = UserLogEntity.LOG_SQL)
    private Set<String> logs;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = TransactionEntity.FROM_USER_SQL)
    private Set<TransactionEntity> transactionsFrom;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = TransactionEntity.TO_USER_SQL)
    private Set<TransactionEntity> transactionsTo;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = ChatEntity.TABLE_SQL, joinColumns = { @JoinColumn(name = ChatEntity.FROM_USER_SQL) })
    @Column(name = ChatEntity.MESSAGE)
    private Set<String> chatMessages;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Set<String> getIps() {
        return ips;
    }

    public void setIps(Set<String> ips) {
        this.ips = ips;
    }

    public Set<TransactionEntity> getTransactionsFrom() {
        return transactionsFrom;
    }

    public void setTransactionsFrom(Set<TransactionEntity> transactionsFrom) {
        this.transactionsFrom = transactionsFrom;
    }

    public Set<TransactionEntity> getTransactionsTo() {
        return transactionsTo;
    }

    public void setTransactionsTo(Set<TransactionEntity> transactionsTo) {
        this.transactionsTo = transactionsTo;
    }

    public Set<String> getLogs() {
        return logs;
    }

    public void setLogs(Set<String> logs) {
        this.logs = logs;
    }

    public Set<String> getChatMessages() {
        return chatMessages;
    }

    public void setChatMessages(Set<String> chatMessages) {
        this.chatMessages = chatMessages;
    }
}
