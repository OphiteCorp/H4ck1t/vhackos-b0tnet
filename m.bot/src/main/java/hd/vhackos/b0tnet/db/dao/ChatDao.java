package hd.vhackos.b0tnet.db.dao;

import hd.vhackos.b0tnet.db.dao.base.Dao;
import hd.vhackos.b0tnet.db.dto.ChatRowDto;
import hd.vhackos.b0tnet.db.entity.ChatEntity;
import hd.vhackos.b0tnet.db.entity.UserEntity;
import hd.vhackos.b0tnet.shared.injection.Inject;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Práce s DB entitou chatu.
 */
@Inject
public class ChatDao extends Dao<ChatEntity> {

    /**
     * Založí nový záznam do databáze.
     */
    public ChatEntity create(UserEntity user, String toUser, String message, String host, String channel) {
        var entity = new ChatEntity();
        entity.setCreated(new Date());
        entity.setMessage(message);
        entity.setFromUser(user);
        entity.setToUser(toUser);
        entity.setHost(host);
        entity.setChannel(channel);

        return execute(s -> createOrUpdate(s, entity, true));
    }

    /**
     * Získá všechny záznamy z chatu.
     */
    public List<ChatRowDto> getAllMessages() {
        return execute(s -> {
            var hql = "select c from {entity} c ";
            hql += "join c." + ChatEntity.FROM_USER + " fu ";
            hql += "order by c." + ChatEntity.CREATED + " asc";

            var q = s.createQuery(query(hql));
            List<ChatEntity> list = q.list();
            return createChatRows(list);
        });
    }

    /**
     * Získá posledních N záznamů z chatu.
     */
    public List<ChatRowDto> getMessages(int lastCount) {
        return execute(s -> {
            var hql = "select c from {entity} c ";
            hql += "join c." + ChatEntity.FROM_USER + " fu ";
            hql += "order by c." + ChatEntity.CREATED + " desc";

            var q = s.createQuery(query(hql));
            q.setMaxResults(lastCount);
            List<ChatEntity> list = q.list();
            var result = createChatRows(list);
            result.sort(Comparator.comparing(ChatRowDto::getDate));
            return result;
        });
    }

    /**
     * Získá všechny zprávy od uživatele.
     */
    public List<ChatEntity> getChatFrom(int fromUserId) {
        return execute(s -> {
            var hql = "select c from {entity} c ";
            hql += "join c." + ChatEntity.FROM_USER + " fu ";
            hql += "where fu." + UserEntity.USER_ID + " = :USER_ID ";
            hql += "order by c." + ChatEntity.CREATED + " desc";

            var q = s.createQuery(query(hql));
            q.setParameter("USER_ID", fromUserId);
            return q.list();
        });
    }

    private static List<ChatRowDto> createChatRows(List<ChatEntity> messages) {
        var result = new ArrayList<ChatRowDto>(messages.size());

        for (var row : messages) {
            var dto = new ChatRowDto();
            dto.setUser(row.getFromUser().getUserName());
            dto.setToUser(row.getToUser());
            dto.setMessage(row.getMessage());
            dto.setDate(row.getCreated());
            result.add(dto);
        }
        return result;
    }
}
