package hd.vhackos.b0tnet.db.dao.base;

import hd.vhackos.b0tnet.db.HibernateManager;
import hd.vhackos.b0tnet.db.exception.DatabaseConnectionException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Entity;
import java.lang.reflect.ParameterizedType;

/**
 * Základní dao objekt.
 */
public abstract class Dao<E> {

    private final Logger log;
    private final Class entityClass;

    protected Dao() {
        log = LoggerFactory.getLogger(getClass());
        entityClass = (Class) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * Aktualizuje databázovou entitu (entita musí již existovat).
     */
    public final void update(E entity) {
        execute(s -> createOrUpdate(s, entity, false));
    }

    /**
     * Smaže databázovou entitu.
     */
    public final void delete(E entity) {
        execute(s -> delete(s, entity));
    }

    /**
     * Získá instanci loggeru.
     */
    protected final Logger getLog() {
        return log;
    }

    /**
     * Je připojení do databáze aktivní?
     */
    protected final boolean isConnected() {
        return HibernateManager.isConnected();
    }

    /**
     * Vykoná databázový příkaz.
     */
    protected final <T> T execute(ITransactionOnSession tos) {
        if (!HibernateManager.isConnected()) {
            throw new DatabaseConnectionException("There was an error connecting to the database");
        }
        var session = HibernateManager.getSessionFactory().openSession();
        Transaction tx = null;

        try (session) {
            tx = session.beginTransaction();
            var result = tos.execute(session);
            tx.commit();
            return (T) result;

        } catch (Exception e) {
            tos.error(this, e);

            try {
                if (tx != null) {
                    tx.rollback();
                }
            } catch (IllegalStateException ei) {
                // nic
            }
        }
        return null;
    }

    protected final String query(String query) {
        String name = ((Entity) entityClass.getDeclaredAnnotation(Entity.class)).name();
        name = query.replaceAll("\\{entity\\}", name);
        return name;
    }

    /**
     * Vytvoří nebo aktualizuje databázovou entitu.
     */
    protected final E createOrUpdate(Session session, E entity, boolean createNew) {
        if (createNew) {
            session.persist(entity);
        } else {
            session.update(entity);
        }
        return entity;
    }

    /**
     * Smaže databázovou entitu.
     */
    protected final E delete(Session session, E entity) {
        session.delete(entity);
        return entity;
    }

    /**
     * Rozhraní pro tělo databázového příkazu v aktivní transakci.
     */
    protected interface ITransactionOnSession {

        /**
         * Vykoná databázový příkaz.
         */
        Object execute(Session session);

        /**
         * Při zpracování dotazu nastala chyba.
         */
        default void error(Dao dao, Exception e) {
            dao.log.error("There was an error processing the database query", e);
        }
    }
}
