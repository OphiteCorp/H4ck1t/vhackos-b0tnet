package hd.vhackos.b0tnet.db.entity;

import hd.vhackos.b0tnet.db.entity.base.PersistentEntity;
import hd.vhackos.b0tnet.shared.ascii.AsciiRow;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

/**
 * Databázvá entity pro držení zpráv z chatu.
 */
@Table(name = ChatEntity.TABLE_SQL)
@Entity(name = ChatEntity.TABLE)
public final class ChatEntity extends PersistentEntity {

    static final String TABLE = "Chat";
    static final String TABLE_SQL = "chat";

    public static final String MESSAGE = "message";
    private static final String MESSAGE_SQL = "message";
    public static final String CREATED = "created";
    private static final String CREATED_SQL = "created";
    public static final String FROM_USER_SQL = "from_user";
    public static final String FROM_USER = "fromUser";
    private static final String TO_USER_SQL = "to_user";
    public static final String TO_USER = "toUser";
    private static final String HOST_SQL = "host";
    public static final String HOST = "host";
    private static final String CHANNEL_SQL = "channel";
    public static final String CHANNEL = "channel";

    @AsciiRow("User")
    @ManyToOne
    @JoinColumn(name = FROM_USER_SQL)
    private UserEntity fromUser;

    @AsciiRow("To User")
    @Column(name = TO_USER_SQL, length = 50)
    private String toUser;

    @AsciiRow("Message")
    @Column(name = MESSAGE_SQL)
    @Type(type = "text")
    private String message;

    @AsciiRow("Host")
    @Column(name = HOST_SQL, length = 100)
    private String host;

    @AsciiRow("Channel")
    @Column(name = CHANNEL_SQL, length = 100)
    private String channel;

    @AsciiRow("Created")
    @CreationTimestamp
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = CREATED_SQL, nullable = false)
    private Date created;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public UserEntity getFromUser() {
        return fromUser;
    }

    public void setFromUser(UserEntity fromUser) {
        this.fromUser = fromUser;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
