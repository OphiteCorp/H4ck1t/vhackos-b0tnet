package hd.vhackos.b0tnet.db.exception;

/**
 * Nastala chyba při připojení k databázi.
 */
public final class DatabaseConnectionException extends RuntimeException {

    public DatabaseConnectionException(String message) {
        super(message);
    }

    public DatabaseConnectionException(String message, Throwable cause) {
        super(message, cause);
    }
}
