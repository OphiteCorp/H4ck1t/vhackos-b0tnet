package hd.vhackos.b0tnet.db.service;

import hd.vhackos.b0tnet.api.chat.dto.UserMessage;
import hd.vhackos.b0tnet.api.net.response.data.BankTransactionData;
import hd.vhackos.b0tnet.api.net.response.data.IpScanData;
import hd.vhackos.b0tnet.config.ApplicationConfig;
import hd.vhackos.b0tnet.db.HibernateManager;
import hd.vhackos.b0tnet.db.dao.ChatDao;
import hd.vhackos.b0tnet.db.dao.ScannedIpDao;
import hd.vhackos.b0tnet.db.dao.TransactionDao;
import hd.vhackos.b0tnet.db.dao.UserDao;
import hd.vhackos.b0tnet.db.dto.ChatRowDto;
import hd.vhackos.b0tnet.db.entity.ScannedIpEntity;
import hd.vhackos.b0tnet.db.entity.UserEntity;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Služba pro práci s databází.
 */
@Inject
public final class DatabaseService {

    private static final Logger LOG = LoggerFactory.getLogger(DatabaseService.class);

    @Autowired
    private ApplicationConfig config;

    @Autowired
    private UserDao userDao;

    @Autowired
    private TransactionDao transactionDao;

    @Autowired
    private ScannedIpDao scannedIpDao;

    @Autowired
    private ChatDao chatDao;

    /**
     * Ověří, zda je aktivní připojení k databázi.
     */
    public synchronized boolean isDatabaseConnected() {
        return HibernateManager.isConnected();
    }

    /**
     * Vyhldá uživatele podle IP.
     */
    public synchronized List<UserEntity> searchIp(String ip) {
        if (!HibernateManager.isConnected()) {
            return null;
        }
        return userDao.getByIp(ip);
    }

    /**
     * Vyhledá všechny IP podle ID uživatele.
     */
    public synchronized List<String> searchIpByUserId(int userId) {
        if (!HibernateManager.isConnected()) {
            return Collections.emptyList();
        }
        return userDao.getIps(userId);
    }

    /**
     * Vyhledá všechny IP podle jména uživatele.
     */
    public synchronized List<String> searchIpByUserName(String userName) {
        if (!HibernateManager.isConnected()) {
            return Collections.emptyList();
        }
        UserEntity user = userDao.getByUserName(userName);
        return (user != null && user.getIps() != null) ? new ArrayList<>(user.getIps()) : Collections.emptyList();
    }

    /**
     * Získá všechny naskenované IP.
     */
    public synchronized List<ScannedIpEntity> getScannedIPs(String orderColumn) {
        if (!HibernateManager.isConnected()) {
            return Collections.emptyList();
        }
        return scannedIpDao.getScannedIps(orderColumn);
    }

    /**
     * Přidá záznam o skenu IP.
     */
    public synchronized ScannedIpEntity addScanIp(IpScanData scan) {
        if (!HibernateManager.isConnected()) {
            return null;
        }
        LOG.info("Adding a new IP to the database: {}", scan.getIp());
        return scannedIpDao.createOrUpdate(scan.getIp(), scan.getLevel(), scan.getFirewall(), null);
    }

    /**
     * Založí nebo aktualizuje naskenovanou IP.
     */
    public synchronized void updateScanIp(String ip, String userName, Integer level) {
        if (!HibernateManager.isConnected()) {
            return;
        }
        LOG.info("Updating an existing IP: {}", ip);
        var user = userDao.getByUserName(userName);

        if (user != null) {
            // uživatel si změnil IP
            if (!user.getIps().contains(ip)) {
                user.getIps().add(ip);
                userDao.update(user);
            }
        } else {
            userDao.createOrUpdate(null, userName, ip);
        }
        // aktualizuje naskenovanou IP
        var scan = scannedIpDao.getByIp(ip);
        if (scan != null) {
            scan.setUserName(userName);
            scan.setValid(true);

            if (level != null) {
                scan.setLevel(level);
            }
            scannedIpDao.update(scan);
        }
    }

    /**
     * Založí novou bankovní transakci.
     */
    public synchronized Map.Entry<Boolean, Boolean> addTransaction(BankTransactionData trans) {
        if (!HibernateManager.isConnected()) {
            return Map.entry(false, false);
        }
        LOG.info("Processing a database query for transaction: {} -> {}", trans.getFromIp(), trans.getToIp());

        var userFrom = userDao.create(trans.getFromId(), trans.getFromIp());
        var userTo = userDao.create(trans.getToId(), trans.getToIp());
        var existsTrans = transactionDao.isExistsByUserIds(trans.getFromId(), trans.getToId());
        var revealFrom = false;
        var revealTo = false;

        if (!existsTrans) {
            transactionDao
                    .create(new Date(trans.getTime() * 1000), userFrom, trans.getFromIp(), userTo, trans.getToIp(),
                            trans.getAmount());
        }
        if (!Utils.isValidIp(trans.getFromIp())) {
            if (!userFrom.getIps().isEmpty()) {
                trans.setFromIp(userFrom.getIps().iterator().next());
                revealFrom = true;
            }
        }
        if (!Utils.isValidIp(trans.getToIp())) {
            if (!userTo.getIps().isEmpty()) {
                trans.setToIp(userTo.getIps().iterator().next());
                revealTo = true;
            }
        }
        return Map.entry(revealFrom, revealTo);
    }

    /**
     * Přidá log k IP uživatele.
     */
    public synchronized void addLog(String ip, String log) {
        if (!HibernateManager.isConnected()) {
            return;
        }
        LOG.info("Adding the log to the database for IP: {}", ip);
        var users = userDao.getByIp(ip);

        if (!users.isEmpty()) {
            for (var u : users) {
                u.getLogs().add(log);
                userDao.update(u);
            }
        }
    }

    /**
     * Odebere neplatnou IP. U skenu jí označí za neplatnou.
     */
    public synchronized void invalidIp(String ip) {
        if (!HibernateManager.isConnected()) {
            return;
        }
        LOG.info("User IP '{}' is no longer valid. Will be set to invalid", ip);
        // odebere IP na uživateli
        var users = userDao.getByIp(ip);
        if (!users.isEmpty()) {
            for (var u : users) {
                if (u.getIps().contains(ip)) {
                    u.getIps().remove(ip);
                    userDao.update(u);
                }
            }
        }
        // u scanu jí označí za neplatnou
        var scan = scannedIpDao.getByIp(ip);
        if (scan != null) {
            scan.setValid(false);
            scannedIpDao.update(scan);
        }
    }

    /**
     * Aktualizuje jména uživatelů v naskenovaných IP.
     */
    public synchronized void updateScannedUsers() {
        if (!HibernateManager.isConnected()) {
            return;
        }
        var users = userDao.getAll();
        var i = 0;

        for (var user : users) {
            var userIps = user.getIps();

            for (var ip : userIps) {
                updateScanIp(ip, user.getUserName(), null);
                LOG.info("{}/{} | User Update '{}' with IP: {}", i + 1, users.size(), user.getUserName(), ip);
            }
            i++;
        }
        LOG.info("The update has been completed");
    }

    /**
     * Opraví nekonzistence a duplicity na uživateli.
     */
    public synchronized void correctUserInconsistency() {
        if (!HibernateManager.isConnected()) {
            return;
        }
        var userIds = userDao.getDuplicateUserId();
        var map = new HashMap<Integer, List<UserEntity>>();

        for (var userId : userIds) {
            var users = userDao.getByUsersId(userId);

            for (var user : users) {
                var list = map.computeIfAbsent(user.getUserId(), k -> new ArrayList<>());
                list.add(user);
            }
        }
        for (var entry : map.entrySet()) {
            var list = entry.getValue().stream().sorted((p1, p2) -> p2.getCreated().compareTo(p1.getCreated()))
                    .collect(Collectors.toList());
            var found = false;

            for (var user : list) {
                if (!found && user.getUserName() != null) {
                    found = true;
                    continue;
                }
                var chats = chatDao.getChatFrom(user.getUserId());

                for (var c : chats) {
                    chatDao.delete(c);
                }
                userDao.delete(user);
            }
        }
    }

    /**
     * Získá všechny naskenované IP bez uživatele.
     */
    public synchronized List<String> getScannedIpsWithoutUser(int limit) {
        if (!HibernateManager.isConnected()) {
            return Collections.emptyList();
        }
        var ips = scannedIpDao.getIpsWithoutUser(limit);
        var result = new ArrayList<String>(ips.size());

        for (var ip : ips) {
            result.add(ip.getIp());
        }
        return result;
    }

    /**
     * Vloží novou zprávu do databáze.
     */
    public synchronized void insertChatMessage(final UserMessage message) {
        if (!HibernateManager.isConnected()) {
            return;
        }
        // musí být async protože samotné čtení probíhá taky async - jinak nastává deadlock
        var t = new Thread(() -> {
            var user = userDao.getByUserName(message.getUser());
            if (user != null) {
                if (user.getUserId() == null) {
                    user.setUserId(message.getUid());
                    userDao.update(user);
                }
            } else {
                user = userDao.getByUserId(message.getUid());
                if (user != null) {
                    user.setUserName(message.getUser());
                    userDao.update(user);
                }
            }
            if (user == null) {
                user = userDao.create(message.getUser(), message.getUid());
            }
            var msg = message.getMessage();
            if (!config.isDbSupportUtf8mb4()) {
                msg = Utils.removeBadChars(msg);
            }
            chatDao.create(user, message.getToUser(), msg, message.getHost(), message.getChannel());
        });
        t.setDaemon(true);
        t.setName("Insert chat message");
        t.setPriority(Thread.NORM_PRIORITY);
        t.start();
    }

    /**
     * Získá všechny záznamy z chatu.
     */
    public synchronized List<ChatRowDto> getAllChatMessages() {
        if (!HibernateManager.isConnected()) {
            return Collections.emptyList();
        }
        return chatDao.getAllMessages();
    }

    /**
     * Získá posledních N záznamů z chatu.
     */
    public synchronized List<ChatRowDto> getChatMessages(int lastCount) {
        if (!HibernateManager.isConnected()) {
            return Collections.emptyList();
        }
        return chatDao.getMessages(lastCount);
    }
}
