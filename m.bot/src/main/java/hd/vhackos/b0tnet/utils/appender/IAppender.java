package hd.vhackos.b0tnet.utils.appender;

import org.apache.logging.log4j.core.LogEvent;

/**
 * Rozhraní pro logovací appender.
 */
public interface IAppender {

    void append(LogEvent event, String message);
}
