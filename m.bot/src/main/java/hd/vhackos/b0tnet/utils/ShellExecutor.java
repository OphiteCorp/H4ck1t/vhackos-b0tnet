package hd.vhackos.b0tnet.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Umožní přístup k příkazové řádce systému.
 */
public final class ShellExecutor {

    private static final Logger LOG = LoggerFactory.getLogger(ShellExecutor.class);

    private static final ExecutorService EXECUTOR;

    static {
        EXECUTOR = Executors.newFixedThreadPool(5, r -> {
            var t = new Thread(r);
            t.setDaemon(true);
            return t;
        });
    }

    /**
     * Přidá příkaz ke zpracovnání.
     */
    public static Future<List<String>> execute(String command) {
        var shell = new ShellRunner(command);
        return EXECUTOR.submit(shell);
    }

    /**
     * Samostatný proces, který vykoná příkaz.
     */
    private final static class ShellRunner implements Callable<List<String>> {

        private final String command;

        private ShellRunner(String command) {
            this.command = command;
        }

        @Override
        public List<String> call() {
            var time = System.currentTimeMillis();
            var id = Thread.currentThread().getId();
            ProcessBuilder builder;

            if (isWindows()) {
                builder = new ProcessBuilder("cmd", "/C", command);
            } else {
                builder = new ProcessBuilder("/bin/bash", "-c", command);
            }
            builder.redirectOutput(ProcessBuilder.Redirect.PIPE);
            builder.redirectError(ProcessBuilder.Redirect.PIPE);

            var output = new ArrayList<String>();
            try {
                LOG.info("[{}] Starting execution of command: {}", id, command);
                var proc = builder.start();
                handleOutputStream(proc, output);
                handleErrorStream(proc, output);
                var state = proc.waitFor();

                if (state == 0) {
                    LOG.info("[{}] The command has been successfully completed: {}", id, command);
                } else {
                    LOG.error("[{}] Execution of the command ended with an error status code: {}", id, state);
                }
            } catch (IOException | InterruptedException e) {
                LOG.error("[" + id + "] An error occurred while executing the command: " + command, e);
            } catch (Exception e) {
                LOG.error("[" + id + "] An unexpected error occurred while executing the command: " + command, e);
            }
            var sec = (System.currentTimeMillis() - time) / 1000.;
            LOG.info("[{}] The command was executed in {} seconds", id, String.format("%.3f", sec));
            return output;
        }

        private void handleOutputStream(Process proc, List<String> output) throws IOException {
            try (var reader = new BufferedReader(
                    new InputStreamReader(proc.getInputStream(), StandardCharsets.UTF_8))) {

                String line;
                while ((line = reader.readLine()) != null) {
                    LOG.trace("[{}] Shell output: {}", Thread.currentThread().getId(), line);
                    output.add(line);
                }
            }
        }

        private void handleErrorStream(Process proc, List<String> output) throws IOException {
            try (var reader = new BufferedReader(
                    new InputStreamReader(proc.getErrorStream(), StandardCharsets.UTF_8))) {

                String line;
                while ((line = reader.readLine()) != null) {
                    LOG.trace("[{}] Shell error: {}", Thread.currentThread().getId(), line);
                    output.add(line);
                }
            }
        }

        private static boolean isWindows() {
            var osName = System.getProperty("os.name");
            return osName.startsWith("Windows");
        }
    }
}
