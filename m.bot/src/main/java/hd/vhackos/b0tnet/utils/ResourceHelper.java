package hd.vhackos.b0tnet.utils;

import hd.vhackos.b0tnet.Application;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;

/**
 * Konstanty pro statický obsah.
 */
public final class ResourceHelper {

    private static final Logger LOG = LoggerFactory.getLogger(ResourceHelper.class);

    public static InputStream getStream(ResourceValue resource) {
        return Application.class.getResourceAsStream(resource.path);
    }

    public static Font loadFont() {
        try (var is = getStream(ResourceValue.FONT)) {
            return Font.createFont(Font.TRUETYPE_FONT, is);

        } catch (IOException | FontFormatException e) {
            LOG.error("An error occurred while loading the font", e);
        }
        return null;
    }

    public enum ResourceValue {

        FONT("/static/font.ttf"),
        SOUND_NEW_VERSION("/static/new_version.wav"),
        SOUND_NEW_DIRECT_MESSAGE("/static/new_direct_message.wav");

        private final String path;

        ResourceValue(String path) {
            this.path = path;
        }
    }
}
