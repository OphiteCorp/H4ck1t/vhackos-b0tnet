package hd.vhackos.b0tnet.utils;

import hd.vhackos.b0tnet.config.ApplicationConfig;
import org.apache.commons.lang3.StringUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Proxy;
import java.util.Map;

/**
 * Pomocné metody.
 */
public final class Utils {

    /**
     * Vyhodnotí, zda je IP platná.
     */
    public static boolean isValidIp(String ip) {
        return (ip != null && !ip.equals("ANONYMOUS") && !ip.equals("???"));
    }

    /**
     * Vyhodnotí, zda operační systém je windows.
     */
    public static boolean isWindows() {
        var osName = System.getProperty("os.name");
        return (osName != null && osName.startsWith("Windows"));
    }

    /**
     * Odebere špatné znaky (4 byte znaky, které nepodporuje utf8).
     */
    public static String removeBadChars(String s) {
        if (s == null) {
            return null;
        }
        var sb = new StringBuilder();

        for (var i = 0; i < s.length(); i++) {
            if (Character.isHighSurrogate(s.charAt(i))) {
                continue;
            }
            sb.append(s.charAt(i));
        }
        return sb.toString();
    }

    /**
     * Získa index nejnižsí hodnoty ze vstupního pole.
     */
    public static int getLowestValue(int[] values) {
        var min = Long.MAX_VALUE;
        var minIndex = 0;

        for (var i = 0; i < values.length; i++) {
            if (values[i] < min) {
                min = values[i];
                minIndex = i;
            }
        }
        return minIndex;
    }

    /**
     * Vyhodnotí, zda jsou přihlašovací údaje platné.
     */
    public static boolean hasValidCredentials(String userName, String password) {
        if (StringUtils.isEmpty(userName) || StringUtils.isEmpty(password)) {
            return false;
        }
        return (!ApplicationConfig.DEFAULT_USERNAME.equalsIgnoreCase(userName) && !ApplicationConfig.DEFAULT_PASSWORD
                .equalsIgnoreCase(password));
    }

    /**
     * Změní hodnotu v anotaci fieldu.
     */
    public static void changeAnnotationValue(Class<?> clazz, String fieldName,
            Class<? extends Annotation> annotationClazz, String annotationFieldName, Object newValue) {
        try {
            var field = clazz.getDeclaredField(fieldName);
            var annotation = field.getAnnotation(annotationClazz);
            var handler = Proxy.getInvocationHandler(annotation);
            var handlerField = handler.getClass().getDeclaredField("memberValues");
            handlerField.setAccessible(true);
            var memberValues = (Map<String, Object>) handlerField.get(handler);
            memberValues.put(annotationFieldName, newValue);

        } catch (Exception e) {
            throw new RuntimeException("Can not change value in annotation", e);
        }
    }
}
