package hd.vhackos.b0tnet.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Řeší vstupní parametry aplikace.
 */
public final class ApplicationArgsResolver {

    private List<String> args;

    public ApplicationArgsResolver(String[] args) {
        if (args != null) {
            this.args = Arrays.asList(args);
        } else {
            this.args = Collections.emptyList();
        }
    }

    public boolean isConsoleMode() {
        return (containsArg("-console") >= 0);
    }

    public boolean isOffline() {
        return (containsArg("-offline") >= 0);
    }

    private int containsArg(String cmd) {
        if (args != null && !args.isEmpty()) {
            for (var i = 0; i < args.size(); i++) {
                var arg = args.get(i);

                if (arg.equalsIgnoreCase(cmd)) {
                    return i;
                }
            }
        }
        return -1;
    }
}
