package hd.vhackos.b0tnet.webapi;

/**
 * Typ odpovědi API serveru.
 */
public enum ApiResponseType {

    OK(200),
    INTERNAL_ERROR(500),
    INVALID_PARAMETERS(600),
    ILLEGAL_ACCESS(700);

    private final int code;

    ApiResponseType(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
