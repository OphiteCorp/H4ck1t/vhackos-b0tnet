package hd.vhackos.b0tnet.webapi.base;

/**
 * Přístup na URL není povolen.
 */
public final class ApiIllegalAccessException extends RuntimeException {

    public ApiIllegalAccessException(String message) {
        super(message);
    }
}
