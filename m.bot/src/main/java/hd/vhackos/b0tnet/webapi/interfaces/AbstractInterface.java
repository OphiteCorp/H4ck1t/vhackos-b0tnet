package hd.vhackos.b0tnet.webapi.interfaces;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import hd.vhackos.b0tnet.api.module.*;
import hd.vhackos.b0tnet.db.service.DatabaseService;
import hd.vhackos.b0tnet.shared.injection.InjectionContext;
import hd.vhackos.b0tnet.shared.utils.SharedUtils;
import hd.vhackos.b0tnet.webapi.ApiResponse;
import hd.vhackos.b0tnet.webapi.ApiResponseType;
import hd.vhackos.b0tnet.webapi.base.ApiIllegalAccessException;
import hd.vhackos.b0tnet.webapi.base.ApiInvalidArgumentsException;
import hd.vhackos.b0tnet.webapi.base.ApiParams;
import hd.vhackos.b0tnet.webapi.base.CallData;
import org.glassfish.jersey.server.ContainerRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Path;
import java.util.*;

/**
 * Hlavní rozhraní pro REST API B0tnetu.
 */
abstract class AbstractInterface {

    private static final Gson GSON = new GsonBuilder().disableHtmlEscaping().enableComplexMapKeySerialization()
            .serializeSpecialFloatingPointValues().create();
    private static final Map<Class, List<String>> AVAILABLE_PATHS = new HashMap<>();

    private final Logger log;

    AbstractInterface() {
        log = LoggerFactory.getLogger(getClass());
    }

    protected final Logger getLog() {
        return log;
    }

    final CommonModule getCommonModule() {
        return InjectionContext.getInstance().get(CommonModule.class);
    }

    final BankModule getBankModule() {
        return InjectionContext.getInstance().get(BankModule.class);
    }

    final TaskModule getTaskModule() {
        return InjectionContext.getInstance().get(TaskModule.class);
    }

    final LogModule getLogModule() {
        return InjectionContext.getInstance().get(LogModule.class);
    }

    final ServerModule getServerModule() {
        return InjectionContext.getInstance().get(ServerModule.class);
    }

    final CrewModule getCrewModule() {
        return InjectionContext.getInstance().get(CrewModule.class);
    }

    final StoreModule getStoreModule() {
        return InjectionContext.getInstance().get(StoreModule.class);
    }

    final MissionsModule getMissionsModule() {
        return InjectionContext.getInstance().get(MissionsModule.class);
    }

    final SdkModule getSdkModule() {
        return InjectionContext.getInstance().get(SdkModule.class);
    }

    final MiningModule getMiningModule() {
        return InjectionContext.getInstance().get(MiningModule.class);
    }

    final NotepadModule getNotepadModule() {
        return InjectionContext.getInstance().get(NotepadModule.class);
    }

    final MalwareModule getMalwareModule() {
        return InjectionContext.getInstance().get(MalwareModule.class);
    }

    final NetworkModule getNetworkModule() {
        return InjectionContext.getInstance().get(NetworkModule.class);
    }

    final ProfileModule getProfileModule() {
        return InjectionContext.getInstance().get(ProfileModule.class);
    }

    final RemoteModule getRemoteModule() {
        return InjectionContext.getInstance().get(RemoteModule.class);
    }

    final BuyModule getBuyModule() {
        return InjectionContext.getInstance().get(BuyModule.class);
    }

    final MessengerModule getMessengerModule() {
        return InjectionContext.getInstance().get(MessengerModule.class);
    }

    final EventModule getEventModule() {
        return InjectionContext.getInstance().get(EventModule.class);
    }

    final DatabaseService getDatabaseService() {
        return InjectionContext.getInstance().get(DatabaseService.class);
    }

    /**
     * Zavolá logiku pro REST API.
     */
    final String call(ContainerRequest request, ICaller caller) {
        return call(request, caller, new CallData());
    }

    /**
     * Zavolá logiku pro REST API.
     */
    final String call(ContainerRequest request, ICaller caller, CallData data) {
        var startTime = System.currentTimeMillis();
        var reqParamsList =
                !SharedUtils.isNullOrEmpty(data.getRequiredParameters()) ? Arrays.asList(data.getRequiredParameters())
                                                                         : null;
        try {
            getLog().debug("Received a request: {}", request.getRequestUri());
            var params = getParamsKeys(request);

            if (data.isOnlyForLocalhost() && !isFromLocalhost(request)) {
                throw new ApiIllegalAccessException("The requested command is only for localhost calls");
            }
            if (!containsAllRequiredParams(params, reqParamsList)) {
                throw new ApiInvalidArgumentsException("Invalid number of required parameters");
            }
            var result = caller.call();
            var json = GSON.toJson(new ApiResponse(ApiResponseType.OK, result, reqParamsList));
            var endTime = System.currentTimeMillis() - startTime;

            getLog().info("The request '{}' was finished in: {} ms", request.getRequestUri(), endTime);
            return json;

        } catch (Exception e) {
            var type = ApiResponseType.INTERNAL_ERROR;

            if (e instanceof ApiInvalidArgumentsException) {
                type = ApiResponseType.INVALID_PARAMETERS;
            }
            if (e instanceof ApiIllegalAccessException) {
                type = ApiResponseType.ILLEGAL_ACCESS;
            }
            var json = GSON.toJson(new ApiResponse(type, e.getMessage(), reqParamsList));
            var endTime = System.currentTimeMillis() - startTime;
            getLog().info("The request '{}' was finished in: {} ms", request.getRequestUri(), endTime);
            return json;
        }
    }

    /**
     * Získá seznam dostupných URL.
     */
    synchronized List<String> getAvailablePaths() {
        var paths = AVAILABLE_PATHS.get(getClass());

        if (paths == null) {
            var methods = getClass().getDeclaredMethods();
            var list = new ArrayList<String>();

            for (var method : methods) {
                if (method.isAnnotationPresent(Path.class)) {
                    var path = method.getAnnotation(Path.class);
                    var out = path.value();

                    if (method.isAnnotationPresent(ApiParams.class)) {
                        var params = method.getAnnotation(ApiParams.class);
                        var paramsStr = String.join(", ", params.values());
                        out += String.format(" [%s]", paramsStr);
                    }
                    list.add(out);
                }
            }
            Collections.sort(list);
            paths = list;
        }
        return paths;
    }

    /**
     * Získá kolekci všech parametrů (kouze klíče).
     */
    private static List<String> getParamsKeys(ContainerRequest request) {
        var list = new ArrayList<String>(0);

        if (request != null) {
            var params = request.getUriInfo().getQueryParameters();
            var keys = params.keySet();

            if (!keys.isEmpty()) {
                list.addAll(params.keySet());
            }
        }
        return list;
    }

    /**
     * Získá hodnotu parametru z requestu podle klíče.
     */
    static <T> T getParam(ContainerRequest request, String key) {
        if (request != null) {
            var params = request.getUriInfo().getQueryParameters();

            for (var p : params.keySet()) {
                if (p != null && p.equalsIgnoreCase(key)) {
                    var value = params.get(p);

                    if (value != null && value instanceof LinkedList) {
                        var list = (LinkedList) value;

                        if (!list.isEmpty()) {
                            return (T) list.get(0);
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * Pársuje parametry v URL jako např.: /(target=0.0.0.0|amount=1337)
     */
    static Map<String, String> parseParameters(String params) {
        Map<String, String> map;

        if (params != null) {
            var parts = params.split("\\|");
            map = new HashMap<>(parts.length);

            for (var part : parts) {
                var entry = part.split("=");
                map.put(entry[0].trim(), entry[1].trim());
            }
        } else {
            map = Collections.emptyMap();
        }
        return map;
    }

    /**
     * Vyhodnotí, zda požadavek přišel z localhostu.
     */
    private static boolean isFromLocalhost(ContainerRequest request) {
        var host = request.getUriInfo().getRequestUri().getHost();
        return host.equals("127.0.0.1");
    }

    /**
     * Zkontroluje, zda parametry obsahují všechny povinné parametry.
     */
    private boolean containsAllRequiredParams(List<String> params, List<String> requiredParams) {
        var p1 = SharedUtils.isNullOrEmpty(params);
        var p2 = SharedUtils.isNullOrEmpty(requiredParams);

        if (p1 && p2) {
            return true;
        }
        if (p1) {
            return false;
        }
        for (var reqParam : requiredParams) {
            var exists = false;

            for (var p : params) {
                if (p.equalsIgnoreCase(reqParam)) {
                    exists = true;
                    break;
                }
            }
            if (!exists) {
                return false;
            }
        }
        return true;
    }

    /**
     * Rozhraní pro vykonání REST volání.
     */
    protected interface ICaller {

        /**
         * Zavolá REST logiku.
         */
        Object call() throws Exception;
    }
}
