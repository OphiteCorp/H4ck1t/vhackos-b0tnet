package hd.vhackos.b0tnet.webapi.base;

/**
 * Byly zadány neplatné parametry pro příkaz v API.
 */
public final class ApiInvalidArgumentsException extends RuntimeException {

    public ApiInvalidArgumentsException(String message) {
        super(message);
    }
}
