package hd.vhackos.b0tnet.webapi.interfaces;

import hd.vhackos.b0tnet.api.dto.ConnectionData;
import hd.vhackos.b0tnet.service.base.Service;
import hd.vhackos.b0tnet.shared.injection.InjectionContext;
import org.glassfish.jersey.server.ContainerRequest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.Collections;

/**
 * API rozhraní pro b0tnet.
 */
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Path("/api")
public final class InterfaceApi extends AbstractInterface {

    @GET
    public String api(@Context ContainerRequest request) {
        return call(request, this::getAvailablePaths);
    }

    @GET
    @Path("request/{target}/{uid}/{tokenId}/({params})")
    public String request(@Context ContainerRequest request, @PathParam("target") String target,
            @PathParam("uid") String uid, @PathParam("tokenId") String tokenId, @PathParam("params") String params) {

        var uidValue = Integer.parseInt(uid);
        var paramsMap = parseParameters(params);

        return call(request, () -> getCommonModule().raw(target, uidValue, tokenId, paramsMap));
    }

    @GET
    @Path("request/{target}/{uid}/{tokenId}")
    public String request(@Context ContainerRequest request, @PathParam("target") String target,
            @PathParam("uid") String uid, @PathParam("tokenId") String tokenId) {

        var uidValue = Integer.parseInt(uid);

        return call(request, () -> getCommonModule().raw(target, uidValue, tokenId, Collections.emptyMap()));
    }

    @GET
    @Path("request/{target}/({params})")
    public String request(@Context ContainerRequest request, @PathParam("target") String target,
            @PathParam("params") String params) {

        var connection = InjectionContext.getInstance().get(ConnectionData.class);
        var paramsMap = parseParameters(params);

        return call(request,
                () -> getCommonModule().raw(target, connection.getUid(), connection.getAccessToken(), paramsMap));
    }

    @GET
    @Path("request/{target}")
    public String request(@Context ContainerRequest request, @PathParam("target") String target) {

        var connection = InjectionContext.getInstance().get(ConnectionData.class);

        return call(request, () -> getCommonModule()
                .raw(target, connection.getUid(), connection.getAccessToken(), Collections.emptyMap()));
    }

    @GET
    @Path("update")
    public String update(@Context ContainerRequest request) {
        return call(request, () -> getCommonModule().update());
    }

    @GET
    @Path("bank")
    public String bank(@Context ContainerRequest request) {
        return call(request, () -> getBankModule().getBank());
    }

    @GET
    @Path("tasks")
    public String tasks(@Context ContainerRequest request) {
        return call(request, () -> getTaskModule().getTasks());
    }

    @GET
    @Path("buyapps")
    public String buyApps(@Context ContainerRequest request) {
        return call(request, () -> getBuyModule().getBuyList());
    }

    @GET
    @Path("log")
    public String log(@Context ContainerRequest request) {
        return call(request, () -> getLogModule().getLog());
    }

    @GET
    @Path("server")
    public String server(@Context ContainerRequest request) {
        return call(request, () -> getServerModule().getServer());
    }

    @GET
    @Path("leaderboards")
    public String leaderboards(@Context ContainerRequest request) {
        return call(request, () -> getCommonModule().getLeaderboards());
    }

    @GET
    @Path("crew")
    public String crew(@Context ContainerRequest request) {
        return call(request, () -> getCrewModule().getCrew());
    }

    @GET
    @Path("apps")
    public String apps(@Context ContainerRequest request) {
        return call(request, () -> getStoreModule().getApps());
    }

    @GET
    @Path("missions")
    public String missions(@Context ContainerRequest request) {
        return call(request, () -> getMissionsModule().getMissions());
    }

    @GET
    @Path("sdk")
    public String sdk(@Context ContainerRequest request) {
        return call(request, () -> getSdkModule().getSdk());
    }

    @GET
    @Path("miner")
    public String mining(@Context ContainerRequest request) {
        return call(request, () -> getMiningModule().getMining());
    }

    @GET
    @Path("notepad")
    public String notepad(@Context ContainerRequest request) {
        return call(request, () -> getNotepadModule().getNotepad());
    }

    @GET
    @Path("malware")
    public String malware(@Context ContainerRequest request) {
        return call(request, () -> getMalwareModule().getMalwareKit());
    }

    @GET
    @Path("scan")
    public String scan(@Context ContainerRequest request) {
        return call(request, () -> getNetworkModule().scan());
    }

    /* EXAMPLE
    @GET
    @Path("exploit")
    @ApiParams(values = { "ip" })
    public String exploit(@Context ContainerRequest request) {
        var target = (String) getParam(request, "ip");
        return call(request, () -> getNetworkModule().exploit(target), "ip");
    }*/

    @GET
    @Path("luckywheel")
    public String spinLuckyWheel(@Context ContainerRequest request) {
        return call(request, () -> getEventModule().spinLuckyWheel());
    }

    @GET
    @Path("messenger")
    public String messenger(@Context ContainerRequest request) {
        return call(request, () -> getMessengerModule().getMessanger());
    }

    @GET
    @Path("messenger/{partnerId}")
    public String messenger(@Context ContainerRequest request, @PathParam("partnerId") String partnerId) {
        var id = Integer.parseInt(partnerId);
        return call(request, () -> getMessengerModule().getPartnerMessage(id));
    }

    @GET
    @Path("profile/{id}")
    public String profile(@Context ContainerRequest request, @PathParam("id") String profileId) {
        return call(request, () -> {
            var id = Integer.parseInt(profileId);
            return getProfileModule().getProfile(id);
        });
    }

    @GET
    @Path("remote/{ip}")
    public String remote(@Context ContainerRequest request, @PathParam("ip") String remoteIp) {
        return call(request, () -> getRemoteModule().getSystemInfo(remoteIp));
    }

    @GET
    @Path("exploit/{ip}")
    public String exploit(@Context ContainerRequest request, @PathParam("ip") String ip) {
        return call(request, () -> getNetworkModule().exploit(ip));
    }

    @GET
    @Path("brute/{ip}")
    public String bruteforce(@Context ContainerRequest request, @PathParam("ip") String ip) {
        return call(request, () -> getBankModule().bruteforce(ip));
    }

    @GET
    @Path("crew/{id}")
    public String crewProfile(@Context ContainerRequest request, @PathParam("id") String crewId) {
        return call(request, () -> {
            var id = Long.parseLong(crewId);
            return getCrewModule().getProfile(id);
        });
    }

    @GET
    @Path("chat/last/{count}")
    public String lastChats(@Context ContainerRequest request, @PathParam("count") String lastChatRows) {
        return call(request, () -> {
            var count = Integer.parseInt(lastChatRows);
            return getDatabaseService().getChatMessages(count);
        });
    }

    @GET
    @Path("service/{state}/{name}")
    public String serviceStart(@Context ContainerRequest request, @PathParam("state") String state,
            @PathParam("name") String serviceName) {

        return call(request, () -> {
            var s = state.toLowerCase();
            if (!s.equalsIgnoreCase("start") && !s.equalsIgnoreCase("stop")) {
                return "Invalid service status. It can only start / stop";
            }
            var services = Service.getServices();

            for (var service : services.keySet()) {
                if (service.equalsIgnoreCase(serviceName)) {
                    if (s.equals("start")) {
                        return services.get(service).start();
                    } else {
                        return services.get(service).stop();
                    }
                }
            }
            return false;
        });
    }
}
