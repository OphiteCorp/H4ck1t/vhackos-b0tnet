package hd.vhackos.b0tnet.webapi.interfaces;

import org.glassfish.jersey.server.ContainerRequest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.Collections;

/**
 * API rozhraní pro b0tnet.
 */
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Path("/")
public final class InterfaceRoot extends AbstractInterface {

    @GET
    public String api(@Context ContainerRequest request) {
        return call(request, () -> Collections.singletonList("api"));
    }
}
