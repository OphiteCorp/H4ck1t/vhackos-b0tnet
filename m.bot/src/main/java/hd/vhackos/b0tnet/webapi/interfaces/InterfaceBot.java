package hd.vhackos.b0tnet.webapi.interfaces;

import hd.vhackos.b0tnet.utils.ShellExecutor;
import hd.vhackos.b0tnet.webapi.base.CallData;
import org.glassfish.jersey.server.ContainerRequest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * API rozhraní pro b0tnet.
 */
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Path("/bot")
public final class InterfaceBot extends AbstractInterface {

    @GET
    public String bot(@Context ContainerRequest request) {
        return call(request, this::getAvailablePaths);
    }

    @GET
    @Path("exec/{command}")
    public String exec(@Context ContainerRequest request, @PathParam("command") String base64urlCommand) {
        var data = new CallData();
        data.setOnlyForLocalhost(true);

        return call(request, () -> {
            var command = new String(Base64.getUrlDecoder().decode(base64urlCommand));
            var future = ShellExecutor.execute(command);

            try {
                var result = future.get(1, TimeUnit.MINUTES);
                return new ExecOutput(base64urlCommand, command, result);
            } catch (TimeoutException e) {
                getLog().error("The time limit for executing the command has been exceeded", e);
                throw e;
            } catch (Exception e) {
                throw e;
            }
        }, data);
    }

    private static final class ExecOutput {

        private final String encode;
        private final String command;
        private final List<String> result;

        ExecOutput(String encode, String command, List<String> result) {
            this.encode = encode;
            this.command = command;
            this.result = result;
        }
    }
}
