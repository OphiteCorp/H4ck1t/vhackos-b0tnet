package hd.vhackos.b0tnet.webapi.base;

/**
 * Informace pro call ve WebAPI.
 */
public final class CallData {

    private String[] requiredParameters;
    private boolean onlyForLocalhost;

    public String[] getRequiredParameters() {
        return requiredParameters;
    }

    public void setRequiredParameters(String[] requiredParameters) {
        this.requiredParameters = requiredParameters;
    }

    public boolean isOnlyForLocalhost() {
        return onlyForLocalhost;
    }

    public void setOnlyForLocalhost(boolean onlyForLocalhost) {
        this.onlyForLocalhost = onlyForLocalhost;
    }
}
