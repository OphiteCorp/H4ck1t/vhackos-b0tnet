package hd.vhackos.b0tnet.webapi.base;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Parametry pro metody v API.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
public @interface ApiParams {

    /**
     * Názvy parametrů.
     */
    String[] values();
}
