package hd.vhackos.b0tnet.webapi;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Odpověď z API serveru b0tnetu.
 */
public final class ApiResponse {

    @SerializedName("resp_code")
    private final int responseType;

    @SerializedName("data")
    private final Object data;

    @SerializedName("req_params")
    private final List<String> requiredParams;

    public ApiResponse(ApiResponseType responseType, Object data, List<String> requiredParams) {
        this.responseType = responseType.getCode();
        this.data = data;
        this.requiredParams = requiredParams;
    }

    public int getResponseType() {
        return responseType;
    }

    public Object getData() {
        return data;
    }

    public List<String> getRequiredParams() {
        return requiredParams;
    }
}
