package hd.vhackos.b0tnet.webapi;

import com.sun.net.httpserver.HttpServer;
import hd.vhackos.b0tnet.config.ApplicationConfig;
import hd.vhackos.b0tnet.shared.injection.Autowired;
import hd.vhackos.b0tnet.shared.injection.Inject;
import hd.vhackos.b0tnet.webapi.interfaces.InterfaceApi;
import hd.vhackos.b0tnet.webapi.interfaces.InterfaceBot;
import hd.vhackos.b0tnet.webapi.interfaces.InterfaceRoot;
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.UriBuilder;
import java.net.BindException;

/**
 * REST server pro B0tnet.
 */
@Inject
public final class WebApiRestServer {

    private static final Logger LOG = LoggerFactory.getLogger(WebApiRestServer.class);

    @Autowired
    private ApplicationConfig config;

    private HttpServer server;

    /**
     * Spustí HTTP REST server.
     */
    public synchronized void start() {
        if (server != null) {
            return;
        }
        var host = "http://" + config.getWebApiHost() + "/";
        var baseUri = UriBuilder.fromUri(host).port(config.getWebApiPort()).build();
        var resConfig = new ResourceConfig(InterfaceRoot.class, InterfaceApi.class, InterfaceBot.class,
                ApiResponseFilter.class);

        try {
            server = JdkHttpServerFactory.createHttpServer(baseUri, resConfig);
        } catch (Exception e) {
            if (e instanceof BindException) {
                LOG.error("The server port '{}' is already being used by another application. Choose another one",
                        config.getWebApiPort());
            }
            throw e;
        }
    }

    /**
     * Zastaví server.
     */
    public synchronized void stop() {
        if (server != null) {
            server.stop(0);
            server = null;
        }
    }
}
