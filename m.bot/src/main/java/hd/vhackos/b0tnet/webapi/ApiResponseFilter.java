package hd.vhackos.b0tnet.webapi;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

/**
 * Filtr pro všechny odpovědi z WebAPI.
 */
@Provider
public final class ApiResponseFilter implements ContainerResponseFilter {

    @Override
    public void filter(ContainerRequestContext reqCtx, ContainerResponseContext respCtx) {
        respCtx.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
        respCtx.getHeaders().putSingle("Access-Control-Allow-Credentials", "true");
        respCtx.getHeaders().putSingle("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        respCtx.getHeaders().putSingle("Access-Control-Allow-Headers", "Content-Type, Accept");
    }
}
